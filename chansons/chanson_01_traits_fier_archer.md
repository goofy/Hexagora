# Les traits du fier archer

## Chanson populaire

J'ai outil bel et adroit,
Tantôt courbe tantôt droit.
Dieu qu'il est beau quand il s'étend,
Et ne vaut rien si ne se tend,
Aussi roide qu'arc-boutant.

Je pousse aval, je tire amont.
Je touche au loin, et bien profond.
Fier-Archer voilà mon nom !

Je vise parmi pertuis,
Perce à tout coup icelui.
Décoche outre touffue broussaille,
Au travers de belles murailles,
Sans nul effort y trouve faille.

Je pousse aval,
je tire amont.
Je touche au loin, et bien profond.
Fier-Archer voilà mon nom !

Devant piteux, laid connil,
Mon outil est peu habile.
Jeune caille, jolie belette,
Voilà bonne cible à sayette,
Gentillette et fort bien douillette.

Je pousse aval, je tire amont.
Je touche au loin, et bien profond.
Fier-Archer voilà mon nom !

Jaillit parfois de mon trait
Plus qu'espéré n'en aurais.
Giboyer mère et enfançon,
Fait bien effrayante rançon,
Et rend mon outil mollasson.

Je pousse aval, je tire amont.
Je touche au loin, et bien profond.
Fier-Archer voilà mon nom ! ❧

### Notes

Les chansons populaires qui pouvaient se répandre dans la société médiévale n'ont malheureusement laissé presque aucune trace. C'est la raison pour laquelle j'ai décidé de combler ce vide par des inventions de mon cru. Pour celle-ci, je me suis basé sur une vraie devinette, relevé dans le texte de Bruno Roy, *Devinettes françaises du Moyen Âge*, que j'ai développée en conservant du texte humoristique originel l'ambiguïté sur le vrai sujet.

Le texte a été visé, équilibré avec l'aide de Guillaume-Huot Marchand, qui l'a mis en musique et interprète la version téléchargeable sur le site. On peut le joindre par le biais de ce site : <http://www.eutrapelia.fr>

### Références

Bruno Roy, *Devinettes françaises du Moyen Âge* (Cahiers d'Études Médiévales, 3), Montréal, Bellarmin, et Paris, Vrin, 1977.

Une version en ligne existe à <http://www.sites.univ-rennes2.fr/celam/cetm/devinettes/devinettes.html> \[Consulté le 15 décembre 2011\].
