# Oncques ne voulez m'aimer

## Chanson populaire féminine

Oncques ne voulez m'aimer  
Eh ! Bien, point ne m'aimez  
J'en aurai fort grand regret  
Nullement n'en souffrirai  
Mien cœur n'est mie pour vous  
Sachez bien !  
Mien cœur n'est mie pour vous  
Sachez bien !  
Il s'encontre aucun  
Plus aimé de moi que vous  
Sachez bien !  
Plus aimé de moi que vous  
Hou !  
Ah ! Si vous l'encontriez,  
Fort en seriez jaloux  
Jeune et richement orné  
D'orfrois[^orfroi], soie et bijoux.  
Il espère me marier,  
Sachez bien !  
Il espère me marier,  
Sachez bien !  
Serai fort plus heureuse  
Avec lui qu'avec vous,  
Sachez bien !  
Avec lui qu'avec vous !  
Hou ! ❧

### Notes

Chant populaire traditionnel :

>***Vous ne voulez pas m'aimer*** (chant de lavandières)

>Vous ne voulez pas m'aimer,  
Eh ! bien, ne m'aimez pas !  
J'en aurai bien du regret  
Mais je n'en mourrai pas !  
Mon cœur n'est pas pour vous,  
*Voyez-vous !*  
Mon cœur n'est pas pour vous,  
*Voyez-vous !*  
Il y en a un autre  
Que j'aime mieux que vous.  
*Voyez-vous !*  
Que j'aime mieux que vous !  
*Hou !*  
Ah ! Si vous le connaissiez  
Vous en seriez jaloux !  
Il est jeune et habillé  
De soie et de velours.  
Et il veut m'épouser,  
*Voyez-vous !*  
Et il veut m'épouser,  
*Voyez-vous !*  
Je serai plus heureuse  
Avec lui qu'avec vous,  
*Voyez-vous !*  
Avec lui qu'avec vous !  
*Hou !*

Origine : Bas-Maine  
Année de l'arrangement : 1998  
Source : Joseph Canteloube, *Anthologie des Chants Populaires Français*, t. 4 p. 285

Chanté par les lavandières au *douet* et rythmée à coups de battoir.

Chanson que l'on trouve dans : *Les chansons de métiers* de Paul Olivier p. 144.

Musique mid : <http://www.rassat.com/sons9/Vous\_ne\_voulez\_pas\_m-aimer.mid>
