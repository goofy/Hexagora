## Chapitre 5

### Otrante, fin d'après-midi du 26 septembre

La vue était magnifique depuis la galerie de la grande salle. Une arcature délicatement sculptée ouvrait sur le port et, par-delà, sur la mer. Le soleil déclinant ralentissait l'activité fébrile des quais et les navires amarrés reposaient le long des appontements, calmes, leurs mâts ondulant légèrement sous l'effet de la houle. Des nuages d'oiseaux se rassemblaient, pourchassant leur nourriture à haute altitude. Des fumerolles montaient de nombreuses cheminées, indiquant la préparation des repas. Quelques cris d'enfants jouant résonnaient dans des courettes et des ruelles en contrebas.


Herbelot tourna la tête vers l'intérieur de la salle. L'endroit était vraiment confortable. Les larges fenêtres apportaient une lumière agréable dans ce qui constituait sans nul doute la plus belle pièce de la demeure. Sur les murs, un faux parement de pierre avait été peint, recouvert sur la paroi la plus longue d'une tenture représentant les signes du zodiaque autour d'une grande scène de vie urbaine. Un des angles était occupé par une cheminée à hotte ronde, où quelques bûches brûlaient en crépitant, symbole de confort plus que de nécessité par cette magnifique journée.

Une table spacieuse avait été dressée face au panorama, habillée d'une nappe finement brodée et équipée de nombreux plats et accessoires, tous de belle facture. L'endroit appartenait à un riche négociant ami de Mauro Spinola, notable de la ville, charmé d'avoir à sa table les voyageurs de marque du *Falconus*. Herbelot était heureux d'avoir été compté cette fois-ci parmi le nombre et avait donc accompagné Sawirus de Ramathes, Ja'fa al-Akka, Régnier et Ribaldo Malfigliastro. Il était surpris néanmoins de ne pas y retrouver le jeune marchand, Ansaldi Embriaco, dont il croyait qu'il entretenait des relations amicales avec le vieux diplomate.

Le souper ne pouvait être meilleur et le clerc avait tout particulièrement apprécié un plat de filet d'anguille avec une sauce citronnée et le rôti de marsouin au riz sucré et aux amandes frites. Il n'avait pas si bien mangé depuis des semaines qu'il était en voyage. Ils finissaient de déguster des oublies[^oublies], arrosées de vin de grenache d'Espagne, après la compote et les flans. Leur hôte discutait avec Mauro de la situation de la ville et de leur activité économique.

Herbelot ne prit pas la peine de relever. Repu et satisfait, il se contentait de sourire poliment et d'abonder dans le sens de la conversation sans vraiment chercher à intervenir ni même à suivre. Il tendait l'oreille au loin vers un chanteur, peut-être invité pour des festivités dans une maison voisine, accompagné d'un instrument à cordes agréablement employé. La douce mélodie le berçait quelque peu et il était prêt à s'endormir paisiblement lorsqu'il entendit prononcer son nom, ce qui le fit sortir brusquement de sa torpeur. Il tourna un œil hagard vers le reste de la tablée et vit avec angoisse toutes les têtes dirigées vers lui. Régnier perçut son malaise et tenta de lui venir en aide.

« Je ne sais si avoir été ordonné sans avoir charge d'une paroisse entraîne vraiment les mêmes devoirs. Je ne le pense pas. »

Lançant un regard implorant vers Régnier, Herbelot commença à bredouiller quelque chose puis toussa pour s'éclaircir la voix. Ce faisant, il s'efforçait de remettre de l'ordre dans ses idées.

« Heu… C'est assez vrai, mais pas entièrement. Vous pensez à quel type d'obligations ?

— Encadrer des paroissiens, les suivre au jour le jour, se montrer présent sans devenir pesant, se poser en exemple. Tout cela constitue lourd labeur auquel vous échappez pour l'instant, et qui me semble pourtant lié à l'exercice de votre ministère. »

Mauro Spinola marquait son approbation de signes de tête. Il abonda dans le sens de Régnier.

« Célébrer la sainte Eucharistie ne pose bien sûr aucun souci, c'est évident. Mais moult sacrements ne trouvent sens qu'en servant une communauté de fidèles, non ? »

Herbelot se sentit plus assuré, il avait retrouvé le fil de la conversation et se trouvait désormais sur des chemins familiers, balisés par des années de formation à l'école cathédrale de Paris.

« Pas forcément : l'officiant n'est que lien, et non plus personne.

— Il peut être floué si le fidèle ment sur ses intentions ou ses actions.

— Il ne peut tromper Dieu. Il croit abuser le clerc, mais au final, c'est son âme éternelle qu'il met en grand péril. »

Mauro écarquillait de grands yeux, comme s'il entendait une révélation.

« Adoncques si je venais à vous pour confesser mes péchés, vous me recevriez ?

— Bien sûr.

— Même si nous ne nous connaissons pas par ailleurs et ne nous reverrons plus jamais ?

— Cela n'a aucune importance, c'est auprès du Seigneur que vous vous confiez. »

Amusé de ces informations, Mauro lança un regard joyeux à la cantonade. Après avoir repris une gorgée dans son verre, il se tourna de nouveau vers Herbelot, un air espiègle se dessinant sur ses traits.

« Et cela s'encontre souvent qu'on vous demande d'accomplir pareilles choses ?

— Non. Les fidèles se tournent plus volontiers vers leurs familiers. Mais je dois avouer que je ne suis prêtre que depuis peu, mon expérience est donc limitée en ce point.

— Juste avant le voyage si j'ai bien compris.

— Peu avant l'été en fait. J'ai été mis au service de l'archevêque Pierre de Tyr dès ce moment…

— Depuis notre départ, n'avez-vous pas entendu un de ces pèlerins ?

— Ils cheminent en général sous la houlette de leur propre pasteur. »

Il esquissa un sourire malicieux.

« Mais si vous avez désir de venir soulager votre âme, ce sera grand plaisir. »

Mauro Spinola eut un hoquet de surprise, amusé par l'idée. Occupé à s'essuyer les lèvres, il n'eut pas le temps de répondre que leur hôte prit la parole.

« Si vous deviez entendre tel négociant génois, diplomate et soldat de surcroît, il faudrait plus longue traversée ! »

Tout le monde se mit à rire. Les yeux plissés par l'amusement, Mauro se cachait la bouche derrière sa serviette. Il toussa pour se redonner une contenance et répondit.

« Je vous sais gré de votre bienveillance, maître Stiaffini. Mais ne dit-on pas que notre Seigneur est clément avec le fils prodigue ? Laissez-moi donc le temps de rentrer auprès de mon père. J'en suis encore à disperser mon héritage. »

Les valets entrèrent à ce moment, porteurs de cruches ornées de beaux décors verts et bruns, dans lesquelles leur hôte leur fit savoir qu'il se trouvait du vin aux épices et à la sauge. Une fois le service terminé, ils levèrent leur verre à cet excellent repas, pris en si bonne compagnie.

### Otrante, nuit du 26 septembre

Un goût étrange et terreux sur les lèvres réveilla Octobono. Il était affalé sur un tas de vieux cordages en partie moisis, les fesses en l'air et la tête dans une flaque d'eau saumâtre et boueuse. Il essaya d'avaler sa salive, mais eut quelques difficultés, la bouche sèche et la langue râpeuse. Il ouvrit lentement un œil puis l'autre, découvrant un paysage inattendu à ras de sol, d'où il avait une vue imprenable sur des rats occupés à grignoter la base d'un panier à demi décomposé.

Il tenta de décoller la joue des graviers où elle était incrustée et grimaça sous l'effort. Des deux mains il souleva le buste et glissa pour ne plus être en déséquilibre. Il resta quelques minutes ainsi, appuyé sur le ventre, ne sachant trop quoi faire et tournant la tête d'un air indécis de droite et de gauche. Il essayait de se souvenir comment rentrer au bateau. Ce qui impliquait de deviner l'endroit où il pouvait être.

Prenant son courage à deux mains, il se releva en un bond mal assuré, manquant de tomber à la renverse sous la vivacité du mouvement. Il se gratta le crâne et entreprit de longer le quai à main droite, espérant que cela le ramènerait au *Falconus*. Il était aidé dans sa tâche par une belle lune, au début du dernier quartier. Mais il n'était pas toujours convaincu que ses pas le menaient là où il l'avait envisagé lorsqu'il préparait mentalement son trajet.

Bien que relativement direct et simple en principe, le chemin ne fut pas si rapide qu'attendu. Il en profita pour se rafraîchir les idées à une barrique remplie par la pluie et destinée aux animaux de bât du port. L'eau fraîche, qu'il prit soin de ne pas avaler, le réveilla à peu près et il perdit moins de temps par la suite dans des détours aussi aventureux qu'inutiles.

Il était enfin arrivé aux abords du navire, commençant mentalement à se préparer à la douloureuse question du franchissement de la passerelle, modeste planche de bois pas très large et fortement inclinée. Il convint qu'il était nécessaire de ne pas se hâter et de réfléchir au problème avant toute action inconsidérée. Il s'assit donc dans un endroit confortable, une flaque de boue située au niveau de la poupe du bateau, et finit par se dire que le plus simple serait de demander de l'aide au matelot de faction à bord, surveillant de ladite planche. Et effectivement, il se rendit compte que ce dernier le fixait attentivement, sans doute depuis un moment. Il fit un signe de la main et entreprit de se rapprocher.

La position debout n'étant pas la plus facile à maintenir tout en regardant quelqu'un et en lui parlant, Octobono opta pour un déplacement à quatre pattes, peut-être moins digne, mais nettement plus efficace. Alors qu'il arrivait près de la passerelle, il héla son compagnon d'un « holà » générique, ne parvenant pas à se souvenir de son nom. N'entendant pas de réponse, il leva la tête, sans pour autant décoller les mains du sol pour s'assurer une stabilité suffisante lors de cette initiative périlleuse. Surpris par le spectacle, il écarquilla les yeux et sa bouche s'ouvrit malgré lui, de stupéfaction.

Inanimé, le matelot de faction était étendu, le buste en travers du plat-bord, un bras pendant le long de la coque. N'écoutant que son courage, Octobono se dressa aussi vivement qu'il lui était possible et hurla d'un air décidé en tendant un doigt inquisiteur.

« Mordiab' ! Qu'en est-ce qui se frappe là d'abord ? »

Surpris par le son de sa propre voix résonnant dans la nuit, Octobono se figea, encore indécis sur le sens de la phrase qu'il venait de crier. Après quelques secondes de stupeur, il se lança à l'assaut de la passerelle, qu'il franchit avec bravoure et l'aide de ses deux mains, pour se retrouver auprès du marin blessé. Il se mit alors à hurler au secours, ameutant en un instant les hommes assoupis non loin.

Ribaldo Malfigliastro fut prévenu sur-le-champ et il arriva, simplement vêtu de sa chemise et d'un bonnet de lin curieusement posé sur sa tignasse emmêlée. Pendant ce temps, le blessé recevait des soins de Gandulfo, le cuisinier, également chirurgien. Malgré une bosse impressionnante sur le crâne, le marin se remettrait rapidement. Une fois rassuré sur la santé de son matelot, Ribaldo revint vers Octobono, qui s'était efforcé à un réveil plus complet et à un dégrisement plus rapide. Il raconta ce qu'il avait remarqué, c'est-à-dire à peu près rien.

« As-tu vu quelqu'un s'enfuir par la passerelle ?

— Je ne mirais pas en cette direction. Arrivé au pied de l'embarcadère, je me suis précipité pour aider mon camarade, je n'ai pas pensé à guetter à l'entour. »

Le *patronus* tourna la tête vers les quelques matelots qui les avaient rejoints.

« Deux hommes vont descendre fouiller les cales de marchandises et deux autres vérifier que tout est calme vers les passagers. Pas la peine de les éveiller à la mi-nuit, nous aviserons demain matin si certains ont constaté larcin… »

Se retournant soudain vers Octobono, il l'attrapa par l'épaule.

« As-tu remarqué s'il était armé ? »

Le cuisinier de bord, occupé à bander le crâne de son compagnon répondit pour le matelot, poussant du pied un cabillot.

« Je ne pense pas, maître, il a usé ce qui lui est tombé sous la main. Et c'est grande chance car il aurait pu le poignarder ! »

Ribaldo pencha la tête et considéra le matelot blessé. L'homme avait le visage un peu tuméfié, certainement à cause de sa chute, et aurait vraisemblablement une partie de la joue gonflée le lendemain au réveil.

« N'as-tu donc rien remarqué ?

— Rien du tout ! J'avais entr'aperçu Octobono qui s'en venait quand j'ai senti grande douleur, comme si mon crâne éclatait. Puis plus rien. Je me suis éveillé avec les gars à l'entour de moi.

— Il venait du bord ou du quai ?

— Aucune idée ! Il a pu grimper par une des amarres, je ne sais pas… »

Les marins envoyés en inspection revinrent tous bredouilles, n'ayant rien remarqué d'inhabituel. Le *patronus* se gratta la barbe d'un air contrarié, avisant l'attroupement de ses hommes aux abords du lieu de l'incident.

« Allez, retournez vous coucher. Que Rufus et Bonado prennent la garde de concert. À la moindre chose étrange, n'hésitez pas à m'éveiller, compris ? Demain matin, nous partons au large pour plusieurs semaines. S’il est resté à bord, nous le trouverons avec le temps. Et sinon, nous pourrons oublier cette vilaine histoire. »

Il retourna alors vers sa cabine, à côté de celle de Mauro Spinola, quoique de taille plus modeste. Alors qu'il allait retrouver sa chambre, il vit la porte voisine s'entrouvrir et le visage d'un des valets de Spinola s'avancer dans la lumière de la veilleuse.

« Que se passe-t-il ? Nous avons entendu du tapage et maître Spinola s'en est inquiété.

— Rien d'important. Un larron a blessé un des gardes, mais n'a apparemment rien eu le temps de dérober. »

Le valet remercia poliment et referma doucement la porte. De nouveau seul, le *patronus* se dépêcha de retrouver son lit. Renfonçant son bonnet jusqu'aux oreilles, il se laissa littéralement tomber sur sa couche, se pelotonnant sous les couvertures avec délice. Quelques secondes plus tard, ses ronflements faisaient écho au clapotis des vagues qui venaient mourir contre la coque.

### Canal d'Otrante, matin du 27 septembre

Assis sur un petit tabouret aux pieds tournés, Ribaldo prenait son déjeuner, des biscuits trempés dans un gobelet de vin miellé. Il était installé à l'avant de la dunette, surveillant les opérations du bord d'un œil distrait. Lorsqu'il avalait une bouchée, ses lèvres enfouies sous la moustache n'apparaissaient que furtivement et son mâchonnement n'était perceptible que par le mouvement des poils de son menton, comme animés d'une vie propre. Le temps n'était pas au beau et il s'était chaudement habillé, avec une cotte orangée de laine épaisse et de bonne facture, quoique visiblement un peu fatiguée. Il attendait que tous les protagonistes de l'incident soient arrivés pour faire le point avec eux. Les deux hommes restés de faction jusqu'au petit matin étaient allés se coucher après leur nuit de garde et il avait fallu envoyer quelqu'un les réveiller.

Octobono avait retrouvé une fraîcheur insoupçonnée, malgré la quantité de vin avalée la veille. Il avait déjà travaillé au déploiement des voiles lors de la sortie du port et ne semblait garder aucune séquelle de sa beuverie. Bertolotus était plus affecté. Il avait dû s'asseoir, car la station debout lui était pénible en raison d'une forte migraine. Il avait tout le côté du visage gonflé et tuméfié, de l'oreille à l'œil, et n'avait qu'une envie, retourner s'allonger. Le *patronus* avait également convoqué Enrico Maza, le soldat le plus expérimenté du bord, qui pouvait avoir des idées pertinentes sur l'événement.

Les deux hommes de faction à peine arrivés, le *patronus* prit la parole. Ils récapitulèrent ce qu'Octobono et Bertolotus avaient remarqué, à savoir pas grand-chose, et firent le point sur les visites de contrôle des différents ponts et auprès des voyageurs. Ces derniers avaient été interrogés et aucun n'avait mentionné d'incident ou de disparition d'objet. La soute à marchandises était intacte et pas une balle ne semblait avoir été ouverte ou abîmée. Aucun passager clandestin n'était signalé, ni aucune trace qu'il y en ait un à bord. Malgré ces nouvelles que ses hommes trouvaient plutôt bonnes, Ribaldo Malfigliastro était visiblement ennuyé par cette histoire.

« Le plus souciant en cette affaire, c'est que ça se déroule à Otrante ! D'usage, il faut se méfier dans les ports romains[^byzantin] ou égyptiens, pas ici ! Déjà qu'on a eu un déserteur dès Messine ! »

Le *patronus* toussa pour s'éclaircir la voix et un peu les idées, puis il reprit.

« Évitons donc que ça se reproduise… Lors de nos prochaines escales, Maza, veillez à ce qu'en plus du matelot, un sergent d'armes soit toujours à guetter la planche d'accès. Nous lèverons d'ailleurs cette dernière chaque soir. J'ai en outre mandé au cuisinier de prêter attention au niveau des denrées, au cas où les larrons seraient encore à bord… Est-ce que vous avez des questions ? »

Enrico Maza, resté pratiquement muet jusque-là, appuyé contre la rambarde les bras croisés, leva la main.

« De même, il faudrait que le charpentier fasse une petite ronde de temps à autre. Ça pourrait aussi être un naufrageur. »

Malfigliastro souleva un sourcil étonné.

« Un naufrageur ? Tout doux, par la barbe-Dieu ! Et pourquoi s'attaquerait-il à nous ? Nous ne portons ni combattants, ni marchandises particulières. Et ne sommes pas en guerre pour le moment.

— Peut-être que seules certaines personnes du bord sont visées… »

Le guerrier montra du doigt le plancher et, par-delà, la cabine en dessous. Ribaldo leva le menton, soulevant les sourcils d'étonnement tandis qu'il comprenait l'hypothèse du soldat.

« Maître Mauro Spinola ? Je n'y avais pas songé ! Il est entouré de valets et ne peut guère aisément être assassiné !

— De vrai. Couler la nef où il voyage peut s'avérer plus aisé…

— Mordiable, vous avez raison ! Je vais dire à Bota de tout éprouver dans l'heure ! Vous pouvez retourner à vos tâches. »

Finissant d'avaler précipitamment le reste de biscuit qu'il avait dans la main, Ribaldo se leva en même temps, posa son gobelet sur son siège et se dirigea vers l'escalier de descente dans le même mouvement. Les hommes s'écartèrent pour le laisser passer. Il dévala les marches et s'arrêta vers la loge, juste à côté du château arrière. Fulco Bota s'y trouvait justement, en train d'aiguiser des outils. Il fut informé rapidement de sa mission et, abandonnant son travail à son apprenti, se mit à la tâche immédiatement.

Alarmé par l'idée, Malfigliastro entreprit alors de faire une petite ronde par lui-même. Il se rendit tout d'abord dans le gaillard de proue où de nombreux magasins étaient situés et où l'on rangeait des cordages, des voiles, du matériel nécessaire à l'entretien et au bon fonctionnement du *Falconus*. Il était en train de s'assurer que les pièces de voilure n'avaient pas servi de cache lorsqu'il entendit la voix de Régnier l'apostropher depuis l'entrée du local.

« J'ai ouï-dire que vous aviez soucis de sécurité, maître Malfigliastro ? »

Il se retourna et vit, dans l'encadrement de la porte, le chevalier dont la silhouette se dessinait à contre-jour. À ses côtés se tenait le jeune Ernaut, fier comme un paon, et sûrement à l'origine de la diffusion de la nouvelle. Le *patronus* soupira, un peu énervé d'une si rapide propagation.

« Un petit incident à la nuit. Rien de grave, mais j'ai toujours choisi la prudence.

— J'aurais cru au contraire que les navires de négoce faisaient souvent belles proies lors de leurs escales.

— De vrai, mais pas en ports amis comme Otrante. Voilà qui m'intrigue fort. Ni vol ni disparition n'étant signalée, nous en sommes réduits à nous interroger sur les buts de ce voleur. Ou de *ces* voleurs.

— Ils auraient pu être plusieurs ?

— Comment savoir ? La seule chose certaine, c'est que Bertolotus a été aplomé. Le reste… »

Régnier se frotta le visage, comme s'il se plongeait dans une intense réflexion puis, se penchant vers le *patronus*, adopta une attitude un peu solennelle.

« Quoi qu'il en soit, vous pouvez me considérer à votre service, ainsi que mon valet, si vous avez nécessité de guetter, inspecter ou faire examen. J'ai accoutumance de ce genre de choses.

— Je vous en mercie, messire d'Eaucourt. Tant que rien de nouveau n'est révélé, ou avant la prochaine escale, à Rhodes, il ne devrait pas être utile de vous déranger. ».

Repoussant l'épais morceau de toile qu'il avait tiré, le *patronus* jeta un dernier regard circulaire sur la pièce, de façon machinale. Il se rapprocha de la sortie où se trouvait toujours Régnier.

« Mais vu que vous êtes diplomate missionné, je ne saurais que trop vous commander la plus extrême prudence. Il est possible que vous soyez la cible de ces brigands. »

Régnier prit un air franchement étonné et sourit à cette idée.

« Croyez-vous ? Mon estrée[^estree] n'a pourtant rien de secret ! Ni de capital d'ailleurs.

— Ça, c'est ce que vous dites, messire. Moi je veux bien vous accorder crédit, et du reste ça ne me concerne pas. Mais vrai ou pas, cet intrigant larron peut aussi se faire fausses idées… »

Le sourire disparut du visage du chevalier, soudain plus circonspect. Il s'effaça de devant l'ouverture pour laisser passer le commandant du navire, qui tira la porte derrière lui. Puis ce dernier poussa un soupir, affectant un air renfrogné, et salua son interlocuteur d'un signe de tête amical, continuant sa ronde. Le diplomate réfléchit quelques instants à l'hypothèse formulée par le *patronus*. Bien qu'elle lui paraisse un peu difficile à croire, il décida d'aller mettre de l'ordre dans ses documents et de garder les plus importants toujours sur lui. Remerciant d'un geste distrait Ernaut, au garde-à-vous comme à la parade, il s'éloigna vers sa cabine.

### Canal d'Otrante, après-midi du 27 septembre

Après une matinée plutôt grise, le soleil s'était décidé à sortir, suivant de peu un vent régulier qui les propulsait à grande vitesse. Les hommes du bord s'étaient assemblés près du château avant pour prendre leur repas. Quelques-uns s'étaient abrités sous une toile, mais la plupart s'étaient installés de façon à profiter de la chaleur. La journée s'annonçant assez calme au point de vue travail, l'ambiance était bonne, bien que la plupart commentassent les événements de la nuit précédente. L'agression était sur toutes les lèvres et monopolisait la plupart des conversations.

Ugolino arriva à la cuisine peu après Ganelon qui patientait depuis quelque temps que Gandulfo lui donne le dîner de son maître. Tout en inclinant la tête, il sourit à l'intention du jeune valet.

« Vous n'avez pas eu souci ?

— Non pas. Rien n'a disparu et nous n'avons su l'incident qu'au matin. Et vous ?

— Pareil. J'ai ronflé devant la chambre de maître Embriaco et me suis levé tôt, comme d'usage. Les matelots déjà à l'ouvrage m'ont appris l'attaque. »

Il se rapprocha insensiblement et baissa la voix, prenant un air de conspirateur.

« Sûrement un destrousseur malhabile… »

Ganelon fit une moue, pinçant les lèvres et écarquillant les sourcils.

« Possible. À dire le vrai, semblables assauts me sont devenus usuels aux côtés de messire Régnier.

— Il te loue depuis long temps ?

— Comme simple valet parmi autres. Choisi pour l'accompagner au voyage du roi Louis. Alerot, l'autre servant désigné, a été emporté par méchantes fièvres en les monts d'Anatolie. Du coup, me voilà seul à servir, de plus proche façon…

— Il m'agrée fort mieux de servir marchand, c'est de sûr moins dangereux. »

Il s'appuya contre la paroi de bois, cherchant le soleil sur son visage. Ganelon risqua un œil vers l'intérieur et vit que le cuisinier mettait la dernière main à la préparation de son panier. Il se rapprocha à son tour d’Ugolino, parlant moins fort que précédemment.

« Sais-tu que certains sont persuadés que maître Embriaco cache aucune chose en sa cabine ? Et ils pensent que c'était là cible des détrousseurs. »

Sans même ouvrir les paupières, Ugolino haussa les épaules.

« Sottises ! Maître Embriaco recèle juste belle somme en pièces. Rien de bien mystérieux là-dedans… »

Ganelon leva les yeux au ciel et retrouva son appui contre le chambranle.

« Le souci, c'est que les gens entreparlent. Ils cherchent  explication et, à défaut, ont tendance à inventer. »

Gandulfo posa à ce moment-là la main sur l'épaule de Ganelon, l'incitant à se retourner vers l'intérieur de la cuisine. Il lui tendit le panier contenant le repas du chevalier, ainsi que le sien. Ganelon remercia rapidement et s'esquiva, non sans avoir salué les présents et souhaité un bon appétit à tous. Le cuisinier prit alors la corbeille du vieux domestique et en sortit les récipients.

« Manque ton écuelle, Ugo'.

— Elle n'y est pas. Je n'ai guère faim ce jour d'hui : j'ai abusé du vin hier soir…

— Justement, un bon brouet bien épaissi te nourrira les entrailles. J'y ai disposé quelque viande bouillie pour ton maître, en plus du pain. Toujours plus agréable que ces maudits biscuits, non ? »

La remarque arracha un timide sourire au valet, qui se frottait le ventre, l'air un peu nauséeux.

« Oui-da ! Maître Embriaco a fait achat de quelques denrées à Otrante. Et en fait, j'en ai profité pour me faire bonne ventrée de fruits, de gaufres et d'oublies.

— Et tu t'étonnes d'avoir tel flux de boyaux ? Manger si riche après plusieurs jours de mer !

— Je boirai un peu de vin pur, ça aide. Une bonne nuit de repos là-dessus et ce sera souvenir… »

Gandulfo revint avec le panier dans une main et une cruche dans l'autre, et fit passer le tout au valet. Il retourna à sa table et rangea le pot d'herbes séchées dont il avait parsemé la viande du marchand. Puis il nettoya les restes de la préparation du repas et les jeta par une petite fenêtre, à l'intention des oiseaux qui suivaient leur sillage.

### Canal d'Otrante, fin d'après-midi du 27 septembre

Chantonnant pour lui-même, Sawirus de Ramathes cherchait une rime adéquate pour le poème qu'il était en train de composer. Le beau temps l'avait incité à s'installer sur la plate-forme du gaillard d'avant, d'où il admirait la voile gonflée par le vent et écoutait l'étendard claquant en haut du mât. Ayant récemment terminé un récit entamé quelques semaines plus tôt, il avait eu envie de rédiger un petit texte sur le ravissement du voyage le ramenant vers sa demeure.

Cela faisait désormais quelques années qu'il s'était mis à écrire des vers, et il prenait de plus en plus plaisir à le faire. Il notait ses idées sur des chutes de papier ou dans les marges de vieux documents réduits au rang de brouillon. Ce n'était qu'une fois le poème finalisé qu'il s'installait pour recopier les strophes avec soin sur une feuille vierge. Pour l'instant, bien qu'il eût trouvé des termes avec de bonnes rimes, il n'était pas satisfait du rythme de son texte. Le chant l'aidait parfois à raccourcir un mot ici ou rajouter une syllabe là. Ce faisant, il regardait de temps à autre les oiseaux qui volaient aux alentours du navire ou se posaient dans les gréements.

Ernaut et son frère Lambert étaient également montés, occupés à admirer les dauphins sautant dans les vagues que traçait le *Falconus*. Ils s'époustouflaient des sauts et des jeux des animaux marins qu'ils observaient d'aussi près pour la première fois. Les esclaffements du jeune colosse faisaient parfois sursauter Sawirus tandis qu'il écrivait ou réfléchissait ; il lançait alors un regard qu'il s'efforçait de maintenir débonnaire vers les deux frères.

La plate-forme vit bientôt l'arrivée de Régnier et de son valet, s'installant pour quelques parties d'échec comme ils aimaient à le faire les après-midis. Ils saluèrent sans bruit les passagers déjà présents et prirent place dans la zone la plus proche du pont, au centre. Confortablement installés sur le plancher, ils jouaient lentement tout en dégustant du vin, profitant du coup de l'adversaire pour apprécier le soleil et la vue environnante.

Au bout de quelque temps, Sawirus posa son écritoire sur le sol à ses côtés, après y avoir rangé les feuillets à l'abri des courants d'air. Se servant de la balustrade comme d'un dossier, il s'appuya contre elle et ferma les yeux, après avoir allongé les jambes. Il soupira de plaisir en s'étirant avec délectation sous la chaleur des rayons du soleil. Le vent iodé chatouillait agréablement ses narines et le bruit des marsouins qui jaillissaient hors des vagues allait rapidement le bercer pour une sieste bien agréable.

Un pas léger et bondissant se fit entendre près de lui. Il ouvrit les yeux à contrecœur et aperçut auprès de lui son valet qui attendait apparemment qu'on lui donne la parole. Surpris de cette initiative peu habituelle, il le renvoya chercher une couverture et du vin. Voyant que Régnier regardait le jeune esclave partir avec diligence accomplir sa mission, Sawirus lui sourit.

« Peut-être finira-t-il correct valet. Voilà première fois qu'il agit ainsi sans ordre. »

Ganelon tourna la tête. D'un regard, il sollicita auprès de Régnier l'autorisation de parler, puis répondit.

« Je crois qu'il se trouve là quelque influence du valet de maître Embriaco. Il s'est pris d'amitié pour votre jeune serviteur et passe beaucoup de temps avec lui.

— Fort bien ! Il me fait excellente impression, compétent et appliqué à son travail. Si cela pouvait guider Yazid, ce serait bonne chose. S'il m'arrivait malheur, je n'ose imaginer ce qu'il adviendrait. »

Régnier intervint à son tour.

« Il reviendrait à l'un de vos enfants, non ?

— Oh ! ce n'est pas à cela que je pensais, il serait affranchi ainsi que je l'ai prévu… D'où mon inquiétude. Étant ma propriété, il est en quelque sorte protégé. Mais une fois seul et devant répondre de ses actes, je crains que sa situation ne se gâte rapidement. »

Régnier hocha la tête en assentiment.

« Voilà fréquent problème avec ces jeunes domestiques : sans relâche les surveiller, faire guidance tels des parents, pour leur propre sauveté.

— Exactement, constant souci, épuisant en ce qui concerne Yazid. Les rares moments où je n'ai garde qu'il lui arrive malheur, c'est lorsque je redoute qu'il n'ait créé grande catastrophe. »

Tandis qu'ils parlaient, le sujet de leur conversation pointa bientôt son visage par l'escalier, porteur d'une cruche, d'un gobelet et d'une couverture. Il s'empressa de déposer les récipients vers Sawirus et de lui mettre les jambes au chaud.

« Autre chose, maître ?

— Simplement réponse à petite question. Ne prends pas ombrage de celle-ci, car quelle qu'en soit la réponse, elle me siéra. » L'adolescent le regarda, un peu médusé par cette formulation ambiguë. « Es-tu venu tout à l'heure de ton propre chef ou t'aurait-on soufflé l'idée ? »

Yazid sembla hésiter un instant, se mordant la lèvre, l'air indécis. Il finit par répondre dans un murmure.

« C'est Ugolino, maître, qui m'a conseillé de m'enquérir de vous. »

Sawirus eut un sourire, tout en lançant en coin un regard à l'intention de Ganelon.

« Que voilà excellent conseil, mon garçon ! Ce valet est prud'homme et me voilà bien aise de te voir le suivre. Être bien entouré d'amis sérieux est chose importante… »

Puis, assujettissant correctement la couverture sur son ventre, il marmonna d'un air satisfait et renvoya le jeune esclave après l'avoir encouragé une nouvelle fois. Il jeta un dernier coup d'œil aux autres personnes de la plate-forme, chacune étant de nouveau plongée dans ses occupations. Le plus repérable de tous était Ernaut, assis dangereusement à califourchon sur le plat-bord, penché pour voir de plus près les dauphins qu'il interpellait de façon aussi bruyante qu'inutile, les incitant à faire la course avec le navire.

### Canal d'Otrante, soirée du 27 septembre

Lorsque Yazid sortit de la chambre de Sawirus de Ramathes, il vit Ugolino penché, qui farfouillait dans son coffre, disposé en face de la porte de son employeur. Sans prêter attention au jeune homme, celui-ci en retira une couverture et un coussin, qu'il posa à ses pieds, puis referma le couvercle, s'assurant du verrou après un tour de clef dans la serrure. Il reprit ensuite la petite lanterne qu'il gardait vers lui le soir et la déposa sur le meuble de bois.

Ce fut seulement alors qu'il tourna la tête vers l'autre valet, ayant pris réellement conscience de sa présence. Il marqua un temps, apparemment surpris, et lui fit un rictus amical. Il ne leur fallut pas longtemps pour convenir de la meilleure façon de passer la soirée : quelques jeux de hasard tout en s'arrosant le gosier de lampées de vin coupé.

Les deux domestiques s'installèrent dans le couloir, assis sur leur couche, lançant les dés tout en posant des mises fictives, tandis qu'Ugolino expliquait à son compagnon les règles et quelques trucs utiles pour améliorer ses chances. Ils prenaient à peine garde au passage des quelques pèlerins ou marins qui avaient à se rendre dans ce secteur du bateau, comme hypnotisés par les petits cubes d'os blancs qui brillaient sous la lumière crue de la lanterne. À un moment, Yazid sentit un regard insistant derrière lui. Lorsqu'il se retourna, il se trouva face au sourire carnassier d'Enrico Maza, crânement appuyé contre la paroi de bois et qui suivait leurs lancers tout en se curant les dents de façon négligée. Surpris, Yazid lui lança un œil mauvais, comme prêt à mordre. Le soldat montra ses deux mains en protestation d'innocence, l'air sournois.

« Tout doux, l'enfançon, je regarde juste le jeu ! »

Ugolino continuait à jouer sans lever la tête, mais intervint en avançant le menton pour indiquer l'extrémité du couloir.

« La halle aux voyageurs est au bout du passage, ici est réservé aux marchands et à leurs maisons.

— Calme, l'ancien, j'œuvre utile. Le *patronus* a ordonné qu'on fasse patrouille. Alors, j'erre, je surveille…

— Il n'y avait nul souci avant votre venue ! » rétorqua Yazid. « Vous pouvez ensuivre votre ronde.

— On y va, on y va ! Marrant ce jeu, je ne connaissais ces variantes.

— D'aucunes façons, personne n'a envie de jeter les dés avec vous, sale truand… »

Enrico se pencha au-dessus du jeune homme. Se rapprochant de la flamme, sa tête prit une teinte écarlate creusée d'ombres profondes qui le faisait ressembler à un démon.

« Avise bien, hommasse : apprends à demeurer en ta place, minable petit valet… »

Ugolino leva soudain la main.

« Chut ! »

Les deux hommes tournèrent le visage vers lui, le regard étonné, bouche bée. Ils rétorquèrent à l'unisson :

« Quoi ?

— Chut ! Faites silence ! Vous n'avez pas entendu ? »

Enrico se redressa.

« Entendu quoi ?

— Un grincement ou un grognement puis un choc sourd. Ça venait de la chambre du maître ! »

Il se leva d'un bond et se plaça devant la porte de la cabine pour appeler son employeur. Ils tendirent tous les trois l'oreille, mais sans  percevoir autre chose que les craquements habituels du navire. Le valet recommença sa tentative plusieurs fois, criant de plus en plus fort, toujours sans succès. Il éprouva de la main la solidité du battant.

« Elle est close, il ne peut être ailleurs. »

C'est à ce moment que Sawirus de Ramathes entrouvrit sa porte et passa la tête.

« Que se passe-t-il ? Vous m'avez éveillé avec tous vos cris ! »

Yazid, également debout désormais, se retourna vers le marchand.

« Le jeune maître génois ne répond pas. On a ouï un bruit mais il reste muet et n'ouvre pas.

— Eh bien, qu'attendez-vous ? Enfoncez l'huis ! »

Enrico Maza, un peu en retrait dans le couloir, secoua la tête en dénégation.

« Ça m'étonnerait que ça cède. Le panneau est de chêne, comme les loquets. Il faudrait outil d'acier pour faire levier. »

Sawirus sortit le bras et poussa le soldat de la main, le chargeant de la mission de façon assez autoritaire. Pendant ce temps, Ugolino commençait à éprouver la solidité de la porte en tapant de l'épaule. Malgré sa détermination et l'énergie qu'il pouvait déployer, sa frêle carcasse n'arrivait guère qu'à rebondir sur le bâti de chêne. Avec le raffut qu'ils faisaient, quelques passagers de la salle commune s'étaient un peu avancés et s'inquiétaient de ce qui se passait. Parmi eux, Sawirus repéra Ernaut, toujours parmi les premiers curieux et dont la masse imposante empêchait ceux restés derrière lui de distinguer quoi que ce soit. Le vieil homme lui fit signe, de façon insistante.

« Jeune Ernaut, approche-t'en ! Viens nous aider ! »

Cherchant à se faufiler entre les personnes devant lui, mais n'arrivant qu'à les compresser contre les parois, il parvint jusqu'à la porte de bois qu'Ugolino avait vainement tenté de forcer. Attendant une confirmation, un sourire niais sur les lèvres, il éprouva sa résistance d'une main. Un peu désappointé, le vieux marchand l'incita de la main à se hâter. Alors, s'appuyant sur le coffre du serviteur face à l'ouverture, Ernaut donna un puissant coup d'épaule qui fit résonner toute la charpente, écartant l'huisserie d'un bon centimètre.

Vexé de ce premier échec, la mâchoire crispée et le regard décidé, il repoussa d'un geste délicat Ugolino resté à ses côtés, tout en se massant l'épaule. Son regard à lui seul cherchait à briser l'obstacle. Arc-bouté sur les deux jambes, il prit une profonde inspiration et se propulsa avec violence contre les planches de bois, qui cédèrent après une fraction de seconde où le temps parut s'arrêter, s'ouvrant en un grincement indigné et entraînant Ernaut dans une douloureuse chute sur le plancher de la cabine. Un peu assommé par le choc, il secoua la tête pour reprendre ses esprits et releva le buste. La bouche béante de stupéfaction et les yeux emplis d'effroi, Ugolino avait avancé sa lampe dans la pièce noire, éclairant Ansaldi Embriaco, bizarrement assis sur sa chaise, les bras le long du corps, allongé sur son écritoire, le dos de sa cotte nimbé d'une large tache foncée. De surprise, Ernaut porta la main à ses lèvres. Sentant un goût salé, il regarda ses paumes et s'aperçut qu'elles étaient, comme son torse et sa joue, pleines de sang séché.

### Canal d'Otrante, nuit du 27 septembre

De nombreuses lampes avaient été apportées pour donner de la lumière à la petite pièce, ainsi qu'au couloir adjacent. Un drap avait été pudiquement posé sur le cadavre du jeune homme qu’avait été Ansaldi Embriaco. Le *patronus* se tenait dans la chambre, seul, l'air dépité et se frottant la barbe du plat de la main de façon nerveuse. Il laissait errer son regard de-ci, de-là sans vraiment comprendre ni même chercher quelque chose de précis. Sawirus était à l'entrée de la pièce et étudiait attentivement l'endroit, regardait le lit et les différentes affaires rangées sur les étagères comme s'il en connaissait la disposition habituelle.

Ugolino avait été emmené à l'écart et les curieux étaient maintenus à distance par Enrico et surtout Ernaut qui, de toute façon, occupait à lui seul pratiquement la largeur du passage, penché qu'il était pour voir par l'huis tout en s'efforçant de se nettoyer du sang à l'aide d'une serviette. Il ne perdait pas une miette des événements, excluant par avance qu'on le mette de côté en raison de son rôle actif dans la découverte du meurtre. Bien qu'effrayé par la vision qu'il avait eue à la lumière tremblotante de la mèche, il était décidé à apporter sa contribution pour élucider ce mystère. À vrai dire, il était plus excité et désemparé qu'inquiet.

Sawirus prit la parole le premier, toussant d'abord pour s'éclaircir la gorge, visiblement ému par la situation.

« Il serait bon d'appeler un prêtre, ce pauvre garçon a de sûr été murdri en état de péché. »

Ribaldo ouvrit grand les yeux, comme s'il essayait de s'extraire d'un mauvais rêve. Il semblait ne consentir à sortir de ses pensées qu'à contrecœur. Il acquiesça dans un souffle et, d'un mouvement de menton autoritaire, interpella Enrico qu'il envoya chercher Herbelot ou Aubelet, le premier qu'il trouverait. C'était la première fois que le maître du navire paraissait dénué de toute volonté, ne sachant quoi faire ni que dire. Au bout d'un moment, il se racla la gorge et prit un air plus résolu. Se retournant vers les autres personnes présentes, il s'exprima d'une voix qui avait retrouvé toute son assurance.

« Nous ne pouvons aider guère plus avant ce malheureux. Que les clercs s'occupent de lui. Alors nous évacuerons sa dépouille. Il sera temps ensuite de vérifier avec le valet si aucunes choses ont disparu. Nous aviserons à partir de là… »

Les hommes présents hochèrent la tête, n'ayant pas vraiment d'idée sur ce qu'il convenait de faire en pareilles circonstances. Il ajouta :

« Je vais désigner deux gardes, pour être sûr que rien ne sera volé ou bougé. En attendant, que chacun retourne se coucher. »

Il accompagna son discours d'un geste las. Sawirus sortit de la cabine, incitant les passagers agglutinés à repartir pour leur couche, aidé en cela par Ernaut. Les curieux disparus, Sawirus vit que l'adolescent restait là, ne se sentant pas concerné par l'ordre du *patronus*. Alors qu'il enjambait le pas de sa porte, le vieux marchand sourit affectueusement au garçon, lui posant une main sur le bras.

« Les ordres de maître Malfigliastro s'adressent à tous. Toi et moi compris. »

Hochant la tête de façon polie, Ernaut se mordilla la lèvre et s'éloigna d'un pas glissant, à contrecœur. Il jetait régulièrement un coup d'œil vers le couloir, où le *patronus* finissait de donner ses instructions. Ne regardant pas où il allait, il manqua de peu de rentrer dans Herbelot et Enrico Maza.

Ernaut soupira et frappa du pied un caillou imaginaire, s'appuyant contre un poteau non loin de là d'où il conservait une vue minimale sur le couloir. Lorsqu'il vit Ribaldo Malfigliastro s'approcher, il fit mine de l'aborder, désireux d'apporter son concours. Le commandant le héla d'un grondement puissant où se retrouvait toute son autorité.

« As-tu les oreilles cireuses ? Personne sur les ponts, dans les coursives ou ailleurs que dans son lit. C'est clair ?

— J'y allais, maître, j'ai juste désir… »

Le *patronus* ne le laissa pas finir. Il le coupa d'une voix où perçait un agacement manifeste.

« Ce ne sont pas tes habituelles sornettes que je veux ouïr, jeune homme, seulement le bruit de tes pas qui s'éloignent. Tu m'as bien entendu ? Immé-dia-te-ment ! »

Surpris de cette véhémence, Ernaut n'osa pas aller plus loin. Il réalisa que ses incartades précédentes ne plaidaient pas en sa faveur et que le *patronus* devait se sentir écrasé face aux problèmes qu'il devait affronter. Il salua discrètement et s'éloigna sans regimber, déterminé à revenir à la charge dès que possible. Satisfait de le voir obtempérer, le commandant du navire prit lentement la direction de l'escalier du pont supérieur. Il s'assurait d'un coup d'œil rageur que personne ne traînait plus dans les différents endroits qu'il traversait, prêt à réprimander sévèrement tout nouveau contrevenant.

Agacé par le comportement du colosse, il était surtout inquiet de ce qu'il allait devoir faire par la suite. La victime appartenait à une des familles les plus riches, les plus puissantes et les plus influentes de Gênes. Il ne pouvait pas traiter ce problème à la légère. Le simple fait d'avoir été le maître à bord lorsque le meurtre avait été commis risquait déjà de lui coûter très cher. Il n'avait pas attaché d'importance aux rumeurs d'un assassin qui s'en prenait à toutes les grandes maisons. Et voilà que ça continuait sur le *Falconus*. Il lui fallait absolument démasquer le coupable et le livrer aux autorités dès leur arrivée en Outremer s'il voulait éviter une disgrâce trop importante. C'était là son unique échappatoire : quoiqu'il lui en coûtât, il était indispensable qu'il trouve une personne à remettre au seigneur Embriaco de Gibelet.
