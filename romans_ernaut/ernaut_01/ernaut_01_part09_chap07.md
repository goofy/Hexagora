## Chapitre 7

### Îles ioniennes, matin du 3 octobre

Du poing, Régnier frappa plusieurs coups sur la porte. C'était la première fois qu'il venait dans la cabine du *patronus*. Ce dernier l'avait fait demander tôt le matin, alors que les hommes remettaient à peine le navire en route. Ayant obtenu l'autorisation d'entrer, il poussa le battant et enjamba l'huisserie basse pour pénétrer dans la pièce. Bien que pas très large, elle semblait très luxueuse par rapport aux conditions de voyage des autres passagers. Des volets obturaient en partie deux ouvertures, donnant sur le côté et l'arrière du *Falconus*. Cela apportait une lumière agréable et une odeur de fraîcheur qui tranchait singulièrement avec le mélange empuanti du pont passager.

Une table était disposée devant la couche, servant de siège, et il y avait des placards sur tout un pan de mur. Sur une étagère étaient posés divers documents et une petite écritoire fatiguée. Les cloisons étaient décorées de motifs géométriques ocre, jaune et rouge, et plusieurs icônes byzantines de saints guerriers avaient été fixées au-dessus du lit. Ribaldo Malfigliastro finissait d'attacher une ceinture qu'il avait apparemment extraite d'un coffre encore ouvert posé non loin de la porte. Il invita de la main Régnier à s'asseoir à table tandis que lui-même faisait le tour pour prendre place sur sa couchette. Il empoigna ensuite le pichet et leur servit à tous deux un verre. Agissant comme à contrecœur, il semblait vouloir prendre son temps avant de parler. Le chevalier attendit patiemment, souriant de façon amicale tout en buvant son vin. Ribaldo jouait avec son gobelet, le posant et le reprenant tout en le fixant comme s'il cherchait à y trouver une explication de ce qui se passait à bord. S'éclaircissant la voix, il inspira bruyamment et expliqua enfin ce qu'il avait en tête.

« Merci de vous être dérangé si tôt. J'ai  fort pensé, il serait profitable que quelqu'un de moins affairé que moi pourchasse le coupable. Comme vous me l'avez amiablement proposé… »

Régnier sourit en silence, marquant ainsi son acquiescement sans interrompre le *patronus*.

« Mon seul souci, c'est que je suis persuadé que tout cela est lié à la *Compagna* et à notre ville. Et vous y êtes étranger… ».

Régnier ouvrit la bouche pour intervenir, mais il en fut dissuadé par un geste de la main du commandant.

« Mais… cela peut aussi faire avantage. Vous aurez regard neuf et ne serez pas soumis aux mêmes pressions qu'un Génois. Vous avez donc toute mon autorité pour pister, interroger, fouiller… Sur le *Falconus*, vous ne devrez compte qu'à moi. Si vous êtes d'accord, j'en ferai annonce dans la matinée. »

Régnier hochait déjà la tête avant d'accepter de vive voix.

« Je serais très heureux de vous aider. Cette meurtrerie me révolte. J'avais eu quelques bavardages avec maître Embriaco et je ne m'explique pas tel geste de haine à son encontre.

— Je crains que l'histoire ne soit fort embrouillée. Un meurtrier s'en prend depuis plusieurs mois à des notables. D'abord Lafranco Rustico, dont le corps a été planté de moult coups de couteau dans une venelle près la cathédrale. Ça a continué avec Jacomo Platealonga, repêché dans le port, à demi dévoré par poissons et crabes. Son vêtement aurait également été percé d'un poignard. Et à peine trois semaines avant notre départ, Oberto Pedegola a été retrouvé par son valet, gisant dans une mare de sang en son arrière-cour. Je ne saurais l'éclaircir, mais je me tiens sûr qu'Ansaldi Embriaco a été occis aux mêmes motifs que ces trois hommes. Et je ne suis pas seul. »

Le chevalier fixait Malfigliastro sans laisser paraître ses pensées. Faisant claquer sa langue sur ses dents, on aurait cru qu'il cherchait la faille dans ce qu'il entendait. Ce n'est qu'après un long silence, pesant pour le *patronus*, qu'il se décida enfin à ouvrir la bouche de nouveau.

« Qui partage cet avis ?

— Mauro Spinola lui-même ! C'est pour ça qu'il est à bord. Il ne  l'a pas reconnu, bien sûr, mais je suis certain qu'il piste le ou les meurtriers. Il ne l'avouera jamais, en intrigant qu'il est, peu désireux de se faire voler ses succès. Mais je m'en souviens fort bien qu'il a laissé entendre que cette série de meurtres allait s'arrêter. Nous devisions de choses et d'autres lorsqu'il s'est plus ou moins dévoilé, peu avant notre escale à Messine. Mais impossible d'en savoir plus.

— Quel intérêt trouve-t-il à cela ?

— Il a pu se voir confier une mission de la part des consuls. On le sait diplomate et soldat, vétéran, qui voyage depuis longtemps. Son jugement est sûr. »

Régnier réfléchit quelques instants. Il redoutait d'avoir à affronter un homme tel que Spinola. Il ne pouvait pas le questionner avant d'avoir lui-même amassé quelques informations, sinon il n'obtiendrait rien de sa part.

« Connaissez-vous un lien entre les quatre victimes ?

— Non. J'ai interrogé le valet, en vain. »

Régnier se redressa et fit le gros dos pour se décontracter tout en cherchant à y voir clair. Il lui fallait garder à l'esprit que le *patronus* lui-même pouvait être impliqué ou avoir des intérêts à défendre dans l'histoire, à commencer par les siens propres.

« Je ne peux également négliger ce qu'a évoqué le maître charpentier lundi dernier. Il me semble incroyable que maître Embriaco ait pu s'adonner à un quelconque trafic. Mais je ne peux écarter l'idée sans preuve. Vous avez eu furieuse réaction l'autre jour à cette mention, pouvez-vous vous en ouvrir à moi ? »

Le *patronus* continuait à jouer avec son gobelet, désormais vide, et picorait dans un plat de raisin sec tout en parlant.

« Ansaldi appartenait à une famille qui a perdu grand nombre des siens lors des guerres contre les infidèles. Il pouvait bien sûr être une brebis galeuse, mais, comme vous, j'ai peine à le croire. Plus jeune, j'ai ferraillé aux côtés de sa parentèle. Je ne peux accepter l'idée que l'un des leurs ait pu ainsi trahir.

— Je vois. Connaissez-vous la réputation des trois autres ?

— Non, aucunement. »

Régnier se frottait le menton d'une main nerveuse, commençant à échafauder des hypothèses et à établir un plan d'attaque. Il ne saurait résoudre l'affaire sans quelques appuis solides. Mais il savait où les trouver.

« Fort bien. Je vous informerai de mes avancées quand la vue se sera éclaircie.

— Veillez simplement à ne pas fâcher maître Spinola. Bien qu'à bord il soit sous mon autorité, on ne peut la lui imposer. »

Ils se levèrent presque en même temps et se serrèrent la main vigoureusement. Régnier sourit avec chaleur.

« Soyez assuré que je ne vais pas quérir les ennuis. S'il détient effectivement quelques clefs pour notre affaire, je m'efforcerai de collaborer plutôt que batailler. »

### Îles ioniennes, Fin de matinée du 3 octobre

Le clerc que Régnier cherchait déambulait tranquillement, tout en haut du gaillard d'arrière, le regard perdu dans le vague. Les yeux mi clos, il se récitait apparemment des passages d'un texte. Régnier s'annonça en l'interpellant depuis une bonne distance, afin de ne pas se montrer importun. Herbelot se retourna lentement, les pointes de sa moustache se relevant en même temps que les commissures de ses lèvres.

« Le bonjour, messire d'Eaucourt.

— Je n'interromps pas quelque oraison, j'ose croire ? »

Herbelot se prit à rire, ce qui le faisait tressauter de façon assez comique. Son air ravi n'était pas moins gourmand que lorsqu'il dévorait une friandise.

« Le père prieur serait assez amusé d'une telle question.

— Que voulez-vous dire ? »

Le jeune clerc adopta un visage de conspirateur sans se départir de son sourire.

« Voilà, je vous dévoile tout : je suis grand amateur de textes à caractère… pour le moins profane. »

Ce disant, il adopta un masque légèrement contrit, largement contredit par l'étincelle dans son regard. Puis il montra l'immensité maritime derrière eux et les côtes qu'ils apercevaient.

« Je me remémorais quelques vers du roman d'Alexandre, lorsqu'il affronte Porus. Je trouvais le lieu bien choisi… Je dois avouer être friand de la matière de Rome[^romanalexandre]. Vous aimeriez en ouïr quelques strophes ?

— J'en aurais été fort aise, mais je vous cherchais à dessein, mon ami. J'aurais emploi pour vos talents. Je vous sais homme de rigueur et ennemi acharné du Malin. »

Le clerc se frotta la moustache et la barbe, ravi de ces compliments.

« Vous m'intriguez !

— Le *patronus* m'a confié délicate mission: dévoiler la main qui a tenu le poignard meurtrier. Et j'aurai fort à faire, d'où mon besoin d'un compagnon de confiance, aussi révolté que moi à l'idée du crime impuni. »

Ébahi de cette franchise, et flatté de cette confiance, Herbelot prit une profonde inspiration et secoua la tête énergiquement en guise d'accord, de façon enthousiaste.

« Ce serait grande joie d'apporter mon aide. Je suis honoré que vous ayez pensé à moi. Mais je dois avouer qu'il ne m'est pas commun de pister ainsi les criminels.

— Moi non plus, ce n'est pas mon habitude. Mais je crois que nous aurons surtout besoin de bonne volonté. En outre, vous avez habitude de penser, d'échafauder habiles hypothèses et de raisonner avec méthode. »

Le jeune clerc se rengorgea devant tant de qualificatifs élogieux qu'il appréciait de voir appliqués à sa personne par un autre que lui. Il allait remercier de façon tout à fait courtoise lorsqu'une voix familière se manifesta derrière eux.

« Grand pardon de vous interrompre, messires, mais moi aussi je pourrais m'avérer utile. »

Ils se retournèrent à l'unisson et découvrirent Ernaut, les mains sur les hanches, le sourire aux lèvres et le regard plein d'espoir. Et si Régnier fut amusé par cette vision, il n'en fut pas de même pour Herbelot qui se renfrogna immédiatement, mécontent d'avoir été coupé dans son élan. Il lança un œil noir à l'adolescent.

« Pour ta gouverne, garçon, apprends qu'il n'est pas correct de s'immiscer de pareille façon dans une conversation. Je ne doute que chez toi cela puisse se concevoir, mais pas avec un clerc de la chancellerie de l'archevêque de Tyr ou un chevalier du roi Baudoin. »

Ernaut ne se laissa pas démonter par la rebuffade.

« Soyez assuré que les miens parents m'ont enseigné que cela ne se faisait pas. Mais il me semblait important de me signaler à vous, même au prix d'une impolitesse. Il en va tout de même du murdriment d'un homme. »

La réponse arracha un sourire à Régnier, vite effacé, au cas où Herbelot l'aurait regardé. Il s'efforça de prendre un air sérieux et autoritaire lorsqu'il répondit.

« À quel titre pourrais-tu être utile ? J'ai déjà valet…

— Il a un travail, ce me semble : vous servir. Moi c'est en tant qu'homme libre que je m'offre. Vous êtes tous deux notables de premier plan, on a tendance à se comporter différemment en votre présence. Moi, je ne suis pas grand-chose, alors je peux plus facilement guetter les gens… »

Herbelot leva les yeux au ciel, abasourdi de l'innocence du garçon qui pensait passer inaperçu malgré sa taille gigantesque. Le chevalier, lui, prit le temps de réfléchir à ces arguments. Le jeune homme n'avait pas totalement tort. Et il avait su habilement glisser un compliment à leur intention, innocemment ou à dessein. Dans les deux hypothèses, cela faisait preuve d'une plus grande finesse que ce qu'on aurait pu attendre de lui à première vue. Décidément, il l'appréciait de plus en plus.

Il lança un regard interrogateur vers Herbelot. Ce dernier comprit immédiatement les idées du chevalier et lui répondit par un air aussi ébahi que contrarié. Ne faisant pas cas, Régnier se tourna vers Ernaut.

« De fait, tu pourrais nous être utile à l'occasion. Je te le ferai savoir par Ganelon. D'ici là, n'hésite pas à laisser traîner tes yeux et tes oreilles. Et avise-moi si tu notes quoi que ce soit qui te semble étrange ou digne d'intérêt. »

Ernaut commençait déjà à trépigner sur place, prêt à se ruer en contrebas pour s'atteler à sa mission. Régnier le stoppa d'un geste.

« Néanmoins, sois attentionné et discret. Celui qui a tué peut recommencer et je n'ai guère envie de te retrouver une lame dans le dos. »

Ernaut salua très respectueusement et bondit littéralement jusqu'à la trappe de descente dans laquelle il se laissa tomber. Il avait à peine disparu que le clerc adressa un regard dépité à Régnier.

« Je ne vois guère l'intérêt de s'adjoindre tel benêt, vilain sans éducation. »

Régnier prit un air très sérieux, ne se donnant même pas la peine de relever les insultes émises par son compagnon.

« Vous savez, mon ami, il n'y a guère de personnes à bord que je pense exclure dès ce jour de la liste des coupables possibles. Ce garçon en est. Bien que nécessitant un guide, il est plein de bonne volonté. Alors je préfère l'avoir à mes côtés. »

Le clerc ne répondit pas, réalisant que ce chevalier faisait preuve une nouvelle fois d'une finesse d'analyse qu'il n'aurait pas soupçonnée. Et à sa grande honte, il comprit que lui-même n'avait pas poussé sa réflexion aussi loin, malgré ses prétentions intellectuelles. Il battit intérieurement sa coulpe, contrarié qu'une fois encore il ait cédé au premier et au plus insidieux des péchés, l'orgueil.

### Îles ioniennes, après-midi du 3 octobre

Herbelot s'était assis au fond du lit, pour laisser un peu de place à Régnier. Ils avaient sorti toutes les archives d'Ansaldi et les parcouraient avec la plus grande attention. Lettres, relevés de change, contrats, listes de prix, ils vérifiaient chacune des pièces que le marchand avait conservées. Le clerc posa une nouvelle feuille sur la pile des documents qui avaient été épluchés. Il en restait encore un bon nombre, mais jusqu'à présent ils n'avaient rien trouvé de concluant. Les marchandises étaient toujours tout à fait légales et autorisées. Les rares fois où Embriaco avait commercé du fer, du bois ou des armes, c'était à destination des royaumes latins.

Régnier soupira et posa à son tour un papier. Il lança un regard vers Herbelot. Il n'était pas vraiment homme d'archives et cette tâche fastidieuse lui pesait. Même s'il la savait nécessaire. Herbelot sentit son désappointement et se désintéressa un instant de sa pile pour celle de son compagnon.

« Quelque chose de louche chez vous ?

— Non, rien. Achat, vente, accords de négoce, uniquement paperasse sans grand intérêt.

— J'ai peut-être trouvé quelque chose d'intéressant. »

Régnier leva un sourcil circonspect. Le clerc hocha la tête en dénégation.

« Oh, rien de bien notable, mais qui éclaire l'affaire différemment : maître Embriaco était fort pieux, ce qui me rassure pour son âme.

— D'où le tenez-vous ?

— Quelques lettres, où il demandait à des contacts de faire des ventes dont les bénéfices furent versés à des œuvres charitables. Il opérait avec grande discrétion, mais il a donné belle somme, en particulier à l'hôpital de Saint-Jean de Jérusalem. Rien que sur les feuillets que j'ai ici, j'en arrive à un total de presque 50 livres, ce n'est pas rien. »

Le chevalier tendit la main pour examiner les quelques pages que lui remit Herbelot. Comptant rapidement sur ses doigts, il en arriva à la même conclusion que le clerc.

« C'est étonnant, personne n'a jamais évoqué ce point, même pas lui.

— Regardez, il indiquait chaque fois à ses agents de ne pas en parler. Ici il écrit, je cite : "Veille à ce que cette somme soit bien remise, mais sans en rechercher vaine gloire". Par ailleurs, à bord, il a toujours assisté aux offices. À Messine il me cherchait peut-être pour une question de Foi.

— Pouvez-vous m'en dévoiler plus à ce propos ?

— Il n'y a rien à en dire, une fois à sa cabine, il m'a aimablement reçu. Mais il avait pu trouver réponse auprès de personnes en ville. Il ne s'est pas étendu et je n'eus pas le front d'insister. Je dois admettre que je le regrette un peu car cela pourrait nous aider ce jour d'hui. »

Cherchant dans ses souvenirs, le regard du chevalier regardait la flamme de la lampe créer des ombres dansantes sur le mur face à lui.

« Il s'était ouvert à moi de son souhait de s'impliquer dans la défense des royaumes d'Outremer, à la mesure ses capacités. Je m'aperçois aujourd'hui que son désir était fort profond.

— Avec une telle Foi, vendre des armes ou toute marchandie défendue à nos ennemis semble impossible.

— De vrai. La vie est souvent plus variée qu'on ne le pense, mais tout cela pointe dans le même sens. »

Régnier s'appuya à la paroi et réfléchit quelques minutes. Il essayait de comprendre quelle caractéristique d'Ansaldi Embriaco avait été suffisamment insupportable à quelqu'un pour l'inciter à passer au meurtre. Il était brillant, peut-être trop. Et très incliné vers la religion, ce qui peut être un problème pour un homme de négoce. Se serait-il attiré des inimitiés par un comportement intransigeant ?

De son côté, Herbelot avait commencé à parcourir les carnets de cire du marchand, avec ses notes les plus récentes. Il plissait les yeux pour arriver à déchiffrer l'écriture, peu facile à suivre avec la faible lumière des deux lampes à huile. Sentant l'attention du chevalier tournée vers lui, il commenta ce qu'il y voyait, sans quitter du regard les tablettes.

« Il était aussi fort scrupuleux apparemment. Un vrai modèle de vertu.

— Pourquoi ?

— Il prenait note de nombreuses denrées pour d'autres, préparait des listes de fournitures pour ses contacts. D'après ces lignes, il a entreparti un beau profit réalisé sur un lot de papier avec un sien cousin, pour lequel il avait cédé quelques semaines plus tôt une balle semblable à moindre prix. Il a équilibré les revenus des deux ventes et en a réparti le fruit selon leur quantité de marchandise propre. »

Régnier eut une moue approbatrice.

« Le valet l'affirme, homme de grande loyauté. »

Herbelot leva les yeux une seconde et fit un sourire.

« Plus facile à croire avec telle preuve sous mon regard. Par contre, moult contacts ont nom génois, ce me semble. Il n'accordait guère confiance. Intègre, mais méfiant. »

Régnier posa le dernier feuillet qu'il avait à examiner et resta pensif, le regard perdu sur le tas de documents répandus sur le lit. Soudain il réalisa qu'ils n'avaient pas étudié les pièces du coffre où se trouvait l'argent. Elles devaient être d'une certaine valeur puisque le marchand avait estimé utile de les y enfermer. Il sauta à bas de la couchette et prit la lampe à huile sur l'étagère au-dessus de sa tête. Il la posa sur la table et se pencha pour tirer à lui le lourd meuble de chêne de sous le lit.

Tout en s'efforçant de trouver la bonne clé, il vérifia machinalement s'il n'y avait pas de traces indiquant qu'il eut été forcé. Sur la gauche, il aperçut quelque chose qui piqua sa curiosité. Il attrapa la lampe et l'installa au sol de façon à éclairer la face latérale du coffre. Herbelot se pencha vers lui, intrigué.

« Qu'avez-vous ?

— Je ne sais, une étrange chose. »

Il fit glisser son doigt le long de la ferrure au bas du meuble, grattant une croûte marron. Il fronça les sourcils et examina son ongle.

« Je trouve du sang au flanc de la huche. »

Herbelot se rassit au fond du lit.

« Il n'y a rien de surprenant, il a pu s'en répandre là lors de l'assaut.

— Justement non, bien nettes en sont les traces, qui ne vont guère sous la couchette. Le sang a couru jusqu'à la porte, mais ne s'est pas avancé dextre ou senestre[^dextresenestre]. »

Il souleva le meuble et en examina la base.

« D'ailleurs, il ne s'en trouve pas dessous, seulement au flanc senestre. C'est très net. »

Il attira vers lui un tabouret qu'il avait amené et s'assit dessus, le menton appuyé sur la main. Il fixait le coffre de chêne et tentait de comprendre comment et surtout pourquoi du sang avait pu arriver à cet endroit, et seulement le long de cette arête. Herbelot le tira de sa contemplation.

« Vous n'avez plus goût à chercher ces documents ? »

S'apercevant qu'il s'était arrêté au milieu de sa tâche, le chevalier trouva la bonne clef et sortit les quelques feuilles rangées là. Il les examina rapidement, vite déçu par leur contenu.

« Simples reconnaissances de dette, pour beau montant, mais sans valeur pour larron. Il les aura laissées.

— Cela voudrait dire que le murdrier[^murdrier] peut lire ? »

Régnier jeta de nouveau un coup d'œil à la liasse qu'il avait gardée en main, l'air dépité.

« Ou que, ne sachant pas, n'avait aucun goût pour les documents écrits. »

Presque malgré lui, ses yeux revinrent sur le coffre. Il s'était passé plusieurs choses intrigantes dans cette cabine après le meurtre. Le corps avait été bougé et ce satané coffre avait joué un rôle. Mais lequel ? La réponse à ce point précis ferait certainement germer la solution à tous ces mystères.

### Au large du golfe de Patras, matin du 4 octobre

Ernaut ne savait pas trop comment aborder sa mission. Il était très heureux de participer enfin pleinement à l'enquête. Bien sûr, il n'avait pas tout expliqué à son frère, craignant que celui-ci lui reproche une nouvelle fois de ne pas être raisonnable et de ne pas rester à sa place. Mais le chevalier avait cru bon de lui attribuer une tâche qu'Ernaut ne pouvait imaginer autre que stratégique. Il devait faire parler Ugolino sur son maître, comme il n'oserait certainement pas le faire en présence d'hommes d'importance. Ernaut sentait qu'il était mis à l'épreuve et il ne voulait pas décevoir le chevalier. S'il voulait réussir dans le monde, c'était le moment de prouver qu'il en avait les capacités.

Il était donc venu voir Ugolino dans la cabine d'Ansaldi Embriaco pour l'aider à ranger les affaires du marchand génoiset se proposer pour les tâches les plus fatigantes. Le vieux valet avait semblé ému par la proposition et, un moment, Ernaut eut un peu honte de ne pas être totalement franc. Mais sa bonne humeur avait vite repris le dessus et il roulait les vêtements et plaçait les effets avec enthousiasme dans les coffres, attendant de trouver l'occasion propice pour poser ses questions. Ne sachant pas comment faire avec naturel, il repoussait sans cesse l'instant, se mordant les lèvres par crainte de se dévoiler trop clairement. Voyant que la quantité d'affaires à remiser diminuait dangereusement et que le vieil homme n'avait apparemment pas l'intention de se montrer loquace, il se décida à tenter sa chance.

« Ça ne t'est pas étrange de placer ses affaires en te disant que c'est l'ultime fois ? »

Alors qu'il finissait sa phrase, Ernaut se maudit intérieurement.

« Alleluia ! Belle entrée en matière ! Et pourquoi ne pas proposer au vieux de se pendre d'emblée ? » Mais Ugolino ne sembla pas relever au premier abord. Puis sa voix, fluette, brisa le silence.

« Oc je pensais exactement la même chose. Jamais je n'aurais cru survivre à maître Embriaco, lui qui était encore en jouvence. »

En son for intérieur, Ernaut remercia Dieu et afficha un sourire de contentement coupable qu'il s'efforça de faire passer pour de la compassion dès qu'il s'en rendit compte. Ugolino continua.

« Tu sais, garçon, depuis le temps qu'il me louait, je faisais quelques projets. Oh, pas pour moi ! Ma vieille carcasse n'espère plus guère de la vie ! Pour lui ! Je rêvais aux marmots qu'il aurait un jour, au bel hostel qu'il tiendrait. Je me disais qu'il me faudrait droitement former remplaçant. Pas une fois je n'ai cru qu'il disparaîtrait de prime. »

Ernaut tenta de saisir sa chance et amorça la discussion sur le sujet qui l'intéressait vraiment.

« Il était bon avec toi ?

— Il n'avait pas à l'être. J'étais un sien valet, pas un ami.

— Raté ! pensa Ernaut.

— Il était strict alors ? »

Ugolino tourna la tête vers l'adolescent, le regard un peu dans le vide.

« Non pas. Rigoureux, mais droit. Un très bon maître. Il payait bien et à son service, j'ai pu épargner quelques monnaies pour mes vieux jours. »

Ugolino marqua une pause, les yeux rivés sur le vêtement qu'il rangeait avec soin.

« Il savait sa place et respectait chacun selon son rang. Jamais il n'était familier, pour ça non ! Mais il avait quelque respect envers moi, j'ose croire. Il m'allouait parfois une petite somme à Pâques, Noël ou la Toussaint. Mais n'en attendait certes pas des effusions passionnées. Il trouvait juste de payer à sa valeur la besogne bien faite. C'était aussi pour cela qu'il était si sérieux en ses affaires. Sans labeur, nulle richesse n'est méritée. »

À cette évocation, Ernaut repensa au marchand. Il ne s'était pas du tout fait la même opinion du défunt. Pour lui, le Génois était un parvenu imbu de lui-même voyant le monde comme un territoire qui lui appartiendrait bientôt, jaugeant ses interlocuteurs selon la contribution qu'ils pourraient prendre au succès de ses projets propres, sans aucune charité envers son prochain. Pour résumer son avis, il l'aurait bien volontiers classé dans la catégorie des têtes à claques. Entendant que le vieil homme continuait à parler, il se disciplina un peu, conscient qu'il était là pour une raison précise et pas pour laisser divaguer ses pensées.

« … une haute mission, surtout pour si jeune marchand. De cela, je me sentais fier car j'estimais avoir participé, avec mes modestes moyens, au sien succès.

— De vrai ! » répondit Ernaut, sans la moindre idée de ce que le valet avait dit. « D'ailleurs, de quoi faisait-il négoce ?

— Je ne saurais le dire en détail. Jusqu'à ce jour d'hui, il était surtout chargé de petits montants, pour des amis et des membres de sa parentèle. Adoncques, ce passage était le premier d'importance qu'il faisait. Avise donc ! Il avait persuadé le vieux consul Ingo Della Volta lui-même d'apporter une forte somme… »

Ernaut laissa échapper un sifflement admiratif à la mention de ce nom qu'il entendait pour la première fois.

« On l'aurait occis pour ça, tu crois ? »

Le valet eut un geste de dénégation de la tête.

« Ce serait fort étrange. Moult négoce a lieu chaque jour et on n'en meurtrit pas pour autant.

— Adoncques c'était peut-être juste pour les monnaies. »

Ugolino s'arrêta de ranger un instant et prit une voix angoissée. Il regarda Ernaut, les traits décomposés.

« Comme ce serait malheur qu'un tel prud’homme ait subi malemort pour un petit tas de pièces ! Et même injuste ! Dieu aurait-il permis telle chose ? »

Ernaut souffla, se demandant s'il devait répondre à cette question. Il souleva les épaules dans un geste de désarroi, le visage exprimant son doute.

« Pour moi, le plus bizarre, reprit-il, c'est que personne ne recherche la façon dont ça s'est passé. Même pour simple voleur avide d'argent, il me plairait de savoir comment il a fait pour s'effacer.

— Comment ça ?

— Depuis le passage, il aurait à coup sûr traversé la grand salle commune avec son butin ou l'aurait celé de l'autre côté. Mais là, dans les cales, il aurait déjà été trouvé par les matelots. Adoncques il a dû se faufiler par ailleurs. Quand on saura où et comment, il sera plus aisé de le démasquer… »

Ugolino rangea un dernier effet dans la malle qu'il préparait. Puis il en rabattit le couvercle dans un claquement sec, enclencha le rabat dans la serrure et l'assujettit d'un tour de clef. Il prit alors appui sur le meuble, fixant Ernaut de ses yeux fatigués.

« Mon espoir, c'est surtout qu'on saura le pourquoi. À défaut de relever mon maître, qu'on apprenne que sa mort ne soit pas le fait d'un minable larcin. »

### Côte du Péloponnèse, mi-journée du 5 octobre

Régnier et Herbelot attendaient, impassibles, dos à la porte qu'ils venaient de franchir. Ils étaient seuls dans la partie publique de la cabine de Mauro Spinola. La tenture accrochée d'un mur à l'autre les empêchait de voir ce qui se passait de l'autre côté, mais ils percevaient des bruits indiquant la présence d'au moins une personne. Le clerc suivait des yeux les motifs de l'étoffe, cherchant les éventuelles erreurs de symétrie dans les décors floraux et animaliers. Le chevalier, pour sa part, admirait la vue par la fenêtre arrière, dont le panneau était largement relevé, offrant un panorama magnifique sur la mer. Inspirant l'air à pleins poumons, il appréciait l'endroit, exempt de l'odeur viciée de corps transpirants, de nourriture avariée et d'autres choses plus écœurantes encore.

La portière[^portiere] se souleva à une de ses extrémités et apparut Mauro Spinola, vêtu d'une longue cotte de soie brodée. Il fit un sourire de convenance et invita du geste ses invités à prendre place sur deux tabourets disposés là à leur intention. Lui-même s'installa sur une chaise curiale recouverte d'un beau coussin orné. Il joignit les mains et adopta une attitude attentive.

« Je suis à vous, messires. En quoi puis-je vous assister ? »

Jetant un regard au clerc assis à ses côtés, Régnier prit la parole le premier.

« Comme vous le savez déjà, le *patronus* m'a octroyé le soin de traquer le mécréant. Vu la notabilité de la victime en votre communauté, vous avez peut-être quelques éléments pouvant tracer la piste.

— Vous ne croyez donc pas à l'idée d'un voleur ?

— Je n'exclus rien. De prime, je moissonne les faits et les informations. »

Mauro se recula dans son siège et joignit les paumes de ses mains en un geste emphatique, fermant à demi les yeux pour marquer son attention.

« Je vois. Eh bien, je dois avouer n'avoir guère à dire de ce pauvre bachelier. Il appartenait à une des plus fameuses familles de la cité, mais n'était pas un notable de premier plan. Son père n'a jamais été très habile négociant et n'a pas pris part aux expéditions majeures. Ansaldi avait excellente renommée, comme l'atteste la confiance du consul Della Volta. Malgré sa jouvence, il avait déjà fort voyagé, surtout comme *socius tractans*[^sociustractans], et avait donc nombre de correspondants dans moult riches cités. Il besognait de façon acharnée, mais n'agissait usuellement pas sans avoir amplement pesé le pour et le contre. Voilà la réputation qu'on lui prêtait. »

Il marqua une pause, levant les yeux vers le plafond et cherchant visiblement dans sa mémoire d'autres éléments.

« Ce n'était pas très gai compagnon. Il était fameux pour son manque de fantaisie et plus enclin à calculer et compter qu'à passer une veillée entre amis ou à courtiser de jeunes charmantes. Mais cela, vous aviez déjà pu vous en assurer. Voilà, c'est à peu près tout… »

Régnier opina du chef. Il n'y avait rien de bien nouveau dans tout cela. Il se doutait que le vieux Spinola ne lâcherait pas le morceau si facilement. Il faudrait lui arracher chaque bribe d'information comme on épluche un oignon, couche après couche.

« Et de ses affaires ? De quelles denrées faisait-il négoce ? Avait-il bonne fame ?

— Excellente ! Et, je pense, méritée. Pas un de ses contacts n'avait à se plaindre de lui. Il achetait et vendait adroitement. Il tenait ses informations bien à jour et savait souvent avant quiconque quand les marchandies allaient arriver. En outre, il demeurait discret, ne dévoilant que peu à ceux qui n'étaient pas en affaire avec lui. »

Le chevalier se demanda où le vieux diplomate désirait en venir avec un tel sous-entendu. Il abonda dans son sens, afin de voir où cela les mènerait.

« Vous pensez qu'il pouvait celer des choses ? De malhonnêtes opérations ?

— Je n'ai jamais vu de preuve, seules des rumeurs ont couru. Je ne voudrais médire d'un mort, mais si cela a pu amener ce tragique dénouement, je crois qu'il est bon que vous en ayez connaissance. »

Régnier acquiesça, suivi de Herbelot qui ne perdait pas une miette des échanges, observant avec attention le Génois, comme convenu avec son compagnon. Mauro développa son idée.

« Il se disait qu'il lui arrivait de négocier des marchandises interdites par l'Église. Je n'y ai jamais accordé crédit, cela me paraissait aller à rebours de ce que je voyais de ce jeune homme.

— Pourquoi s'en ouvrir à moi alors ?

— Parce que cela peut être lié à votre affaire. Si, comme je le crois et, je l'espère, vous aussi, il n'avait jamais succombé à de tels actes, d'où venaient tels bruits ? Dans quel dessein ? Le discréditer ? Il n'avait pas d'ambition politique déclarée ! Et pour un marchand, ce n'est pas vous faire insulte que de crier partout que vous êtes prêt à tout. »

Bien qu'intrigué par cette révélation, Régnier ne voulait pas se laisser embarquer par un vieux renard comme celui qu'il avait en face de lui. Il opta pour une question un peu plus directe, afin de voir s'il pourrait le déstabiliser.

« J’admets l'étrangeté du point. Cela aurait-il lien avec les crimes qui ont frappé votre ville récemment ? »

Spinola eut à peine un frémissement, mais Herbelot le remarqua distinctement. Son relâchement ne dura néanmoins qu'une fraction de seconde et il avait retrouvé toute son assurance lorsqu'il répondit.

« Qui sait ? Les trois primes victimes étaient fort plus fameuses et bien plus notables. Comment savoir si un lien invisible ne les reliait pas ? Je ne saurais dire.

— Ils ont tous péri du poignard.

— De vrai, mais sur le *Falconus* il y a aussi des pièces dérobées. Peut-être n'est ce qu'affaire de larron… »

Lassé de jouer au chat et à la souris, Régnier décida de tenter le tout pour le tout, quitte à dévoiler une partie de son jeu.

« N'avez-vous pas été missionné pour résoudre ces ténébreux crimes ? Ne pouvez-vous m'en dire plus ? »

Mauro Spinola eut un étrange sourire et toussa très légèrement. Il assura sa tenue sur son coussin et se pencha en avant, les coudes sur les genoux.

« Comprenez qu'il m'est impossible de répondre de façon aussi directe à des questionnements ainsi formulés. J'essaie de vous aider, dans les bornes du raisonnable. Mais ne me demandez pas de trahir certains principes que je suis depuis moult dizaines d'années. »

Régnier accusa le coup et, se reculant sur son siège, croisa les bras.

« L'affaire est-elle politique ?

— Je vous l'ai dit, rien ne permet d'affirmer ce jour d'hui que ce malheureux crime soit relié aux odieux meurtres ayant ensanglanté ma ville. Si vous visez à résoudre ce mystère, il serait adroit de vous concentrer sur le *Falconus*. L'histoire y commence et y trouve fin, j'en suis assuré. »

### Côte du Péloponnèse, soir du 6 octobre

Peu à peu, Ernaut s'était rapproché de Yazid et Ugolino, pour pouvoir leur poser les questions que Régnier lui indiquerait si nécessaire. Il avait donc été amené à participer aux bruyantes veillées des marins, au grand dam de son frère qui lui avait amèrement reproché ce manque de sérieux dans son attitude de pèlerin. Il était vrai que cette mission secrète arrangeait bien le jeune homme, pouvant s'abandonner à quelques plaisirs coupables avec la meilleure des excuses à offrir au final à Lambert. Mais il s'était révélé étonnamment sérieux dans son attitude, buvant le minimum pour rester lucide et demeurant à l'affût de tous les indices s'offrant à lui. Et si parfois il se laissait déborder par son enthousiasme, pas une fois cela n'avait desservi sa mission, bien au contraire.

Il n'avait pas eu de mal à être accepté des matelots, son naturel jovial et un caractère fier-à-bras le désignaient comme un compagnon évident pour ces hommes à la vie rude. Ses fréquentes incartades et les innombrables remontrances qu'il avait subies de la part du *patronus* n'étaient d'ailleurs pas pour rien dans sa popularité. Ce soir-là, les hommes avaient prévu de se défier les uns les autres à une épreuve de force, où, flanc contre flanc, pied droit contre pied droit, et se tenant par une main, les deux adversaires tentaient de se faire tomber l'un l'autre sans séparer leurs jambes. Au début, ils s'étaient tous esclaffés lorsqu'Ernaut voulut y prendre part, le trouvant bien trop imposant pour qu'on puisse envisager sérieusement qu'il participe. Mais Fulco proposa que tous les membres d'équipage affrontent l'adolescent à la suite, en espérant qu'il finirait par se fatiguer.

Confiant et amusé, le jeune colosse n'eut aucun mal à faire valser ses adversaires les uns après les autres, enchaînant les victoires avec une aisance déconcertante face à des matelots aux muscles noueux. Sa supériorité de taille, de poids et de puissance était telle que certains se retrouvaient à rouler dans les cordages, les tonneaux, voire dangereusement près des trappes de descente. Le seul qui lui donna du fil à retordre fut Octobono. Ernaut ne s'en était pas méfié en raison de son petit gabarit. Mais le marin était solide et surtout, il était vif comme l'éclair et d'une souplesse surprenante, arrivant à retourner la force de son adversaire contre lui. Inquiété de voir sa suprématie remise en cause par une demi-portion, Ernaut finit par trouver la parade : il le souleva de terre d'un seul bras, mettant fin au combat dans un grognement de rage et s'attirant les rires du groupe. Cela entraîna des discussions passionnées pour désigner le vainqueur, un tel cas de figure ne s'étant jamais présenté. Afin de résoudre le problème, ils décidèrent unanimement de nommer les deux adversaires champions de la soirée et ils arrosèrent leur victoire comme il se devait.

L'ambiance retombant un peu, de nouveaux sujets furent lancés dont, bien sûr, le meurtre, présent dans tous les esprits. Les hommes s'inquiétaient aussi de la décision du *patronus* de nommer un chevalier étranger, originaire de France, au service du roi Baudoin, pour « fouiner un peu partout » comme ils disaient. Rufus, un des marins les plus expérimentés, et ancien soldat, était le plus déçu de cette décision de leur commandant.

« Malgré sa ruse de vieux marin, Ribaldo s'est fait parer la châtaigne[^parerlachataigne]. Voilà problème à ne confier qu'à un Génois. Et le seul capable ici, c'est Enrico, pas vrai vieux ? »

Tous les regards se tournant alors vers lui, ce dernier fut surpris alors qu'il était en train de vider à grands traits son godet. De stupéfaction, il manqua d'en renverser une bonne part et s'étrangla en partie. Moitié toussant, moitié crachant, il réussit finalement à retrouver sa voix. Un peu en retrait, Ernaut tendit l'oreille, curieux de la tournure que prenait cette discussion.

« Le soleil t'a brûlé le crâne, Rufus. Moi je suis pas traqueur, mais soldat. On me dit qui occire, quand le faire, et voilà… »

Il frappa sa paume sur son genou d'un geste vif pour illustrer son propos. Gandulfo, le cuisinier, se mit à rire.

« On peut te louer pour meurtrir n'importe qui ? Intéressant, ça ! Peut-être que ça vaudrait même quelques piécettes de récompense de la part de ce chevalier. »

Enrico sourit, de cet air carnassier qui tenait plus du loup affamé que du compagnon amusé. Il donna dans le vide une tape imaginaire à son camarade trop éloigné pour recevoir le coup.

« Me fais pas dire ce que j'ai pas dit, maubec[^maubec] ! Moi, ça m'agrée que le vieux ait choisi ce François. Ça l'occupe et même qu'avec un peu de chance il débusquera le meurtrier. Mais je dois dire qu'en un cas comme dans l'autre je m'en gabe !

— D'aucune façon, il l'a bien cherché, ce type ! insista le cuisinier, visiblement éméché. Il n'y avait qu'à le voir agir. Je serais pas surpris si la moitié des gens qu'il a encontrés voulait le tuer…

— Seulement la moitié ? » ironisa un de ses compagnons.

Ernaut eut une pensée pour le pauvre Ugolino. Lui qui tenait en si haute estime son maître ! Il jeta un coup d'œil furtif vers le valet, mais s'aperçut que ce dernier ne faisait guère attention aux propos tenus, le nez dans son verre. Entretemps, Fulco Bota était intervenu, d'une voix plus calme que celle des autres marins, qui criaient plus qu'ils ne s'exprimaient.

« Je ne vois rien de joyeux à la mort d'autrui. Il m'était inconnu, ce marchand, je n'ai guère eu ni à m'en plaindre ni à m'en féliciter. Mais si on ne tente pas de trouver qui l'a meurtri, c'est comme si on consentait, qu'on trouvait supportable que d'aucuns soient éliminés ainsi, sans raison. »

Il claqua des doigts, achevant sa démonstration d'un air très docte. Piqué au vif, Enrico s'emporta.

« Tu veux quoi, à nous faire sermon ? Nous montrer comme toi tu es meilleur que nous ? »

Octobono, sentant l'ambiance s'échauffer et le risque de conflit monter entre ses deux amis, intervint.

« Il a raison, ce n'est pas deviner qui est plus honnête que l'autre. Mais ne pas punir comme il sied un coupable, c'est au final risquer soi-même d'être percé d'un poignard. »

Fulco hocha la tête.

« Exactement. On dégoise, là, de concert sous la belle étoile. On joue à railler la mémoire du mort, sans chercher malice. Mais je sais qu'au fond, on est tous en accord. Si on oublie ce genre de crime, qu'on ne s'assure pas que le meurtrier reçoit juste châtiment, on ne mérite guère le nom d'homme. »

Cette sentence calme et simple apaisa le groupe et sonna la fin de la veillée. Peu après, les matelots commencèrent à partir les uns après les autres, dans un quasi-silence qui contrastait bizarrement avec la vive agitation qui régnait jusqu'alors sur le pont. Loin au-dessus d'eux, des nuages s'amoncelaient devant la voûte étoilée, achevant d'obscurcir un ciel presque sans lune. Désormais l'un des rares encore présents auprès d'Ernaut, Octobono leva la tête, humant le vent d'un nez connaisseur.

« L'air sent la pluie. Je ne serais pas étonné que nous ayons gros temps d'ici peu. La tempête est sur nos talons. »

### Côte du Péloponnèse, après-midi du 7 octobre

Ja'fa al-Akka était à demi assis contre un coussin posé sur un des tonneaux du pont. Appuyé contre un des poteaux du gaillard d'avant, il regardait le paysage défiler. Le *Falconus* était en train de passer entre une presqu'île rocheuse faisant éperon dans les flots et un îlot aux pentes abruptes recouvertes d'une végétation broussailleuse. De nombreux oiseaux planaient au-dessus d'eux, traversant le bras de mer en quelques coups d'aile puissants. Il en suivait parfois certains du regard, se protégeant les yeux du plat de la main. Il vit arriver Régnier d’Eaucourt, qui le salua d'un geste alors qu'il était encore à plusieurs mètres. Il inclina la tête en réponse. Le chevalier se rapprocha alors, le visage transpirant sous son chapeau de paille.

« Belles contrées n'est-ce pas ?

— Oui, splendide ! J'apprécie fort cette partie du passage. De sûr un des plus magnifiques endroits ici-bas. »

Régnier fit mine de s'intéresser à son tour au panorama, portant son regard de droite et de gauche pour en admirer l'étendue sans vraiment reconnaître ce qu'il contemplait.

« Savez-vous où nous sommes ?

— Nous entrons le golfe de Messénie, au sud du Péloponnèse. La mer Égée n'est guère éloignée. Si nous voguions vers Constantinople, nous remonterions ensuite vers le nord, par Patmos et Chios. »

Le chevalier fit une légère moue, ne connaissant que vaguement les lieux cités.

« Vous semblez fort habitué à ces mers.

— Je suis monté jusqu'à la cité des Romanoï[^romanoi] une fois. Le voyage fut enchanteur et j'en ai goûté chaque journée comme une coupe de bon vin. J'espère y retourner, et plus tôt que tard. »

Régnier opina du chef, plissant les yeux en raison de la forte luminosité du soleil réverbérée sur les ondes.

« Moi, j'espérais joindre une ambassade auprès du basileus Manuel[^manuelcomnene]. Mais lorsque le prince Renaud d'Antioche s'est allié à son ancien adversaire Thoros d'Arménie contre Constantinople, le roi Baudoin a retraité la mission.

— L'assaut a été commenté fort loin, Chypre fut sévèrement bousculée par l'horrible coup de main. Par suite, les Latins ne sont plus guère les bienvenus dans les territoires de Manuel.

— Nous n'y ferons d'ailleurs pas escale. Le roi Baudoin est toujours ami fidèle de Manuel, mais le commun ne ferait peut-être pas la différence entre nous et ceux d'Antioche. »

Ja'fa resta silencieux plusieurs minutes, admirant le jeu des lumières sur les vagues, le ballet des oiseaux de mer dont une colonie était installée aux abords des falaises de roche gris clair. Une brise faisait voler les longues extrémités de son turban, revenant régulièrement lui fouetter le visage et l'obligeant à secouer la tête. Régnier s'était accoudé et essayait d'identifier deux navires près de la côte au nord. Ja'fa rompit alors le silence.

« Adoncques, il vous incombe de faire lumière sur ce mystère ? »

Régnier releva le buste et se tourna face au marchand, laissant son regard s'attarder sur le beau vêtement décoré de bandes brodées sur les bras.

« Le *patronus* œuvre déjà fort pour la bonne marche du *Falconus*… D'ailleurs, je voulais avoir votre avis en différents points de cette affaire. »

Ja'fa leva les yeux au ciel et soupira, l'air amusé.

« Je dois avouer n'en avoir guère. Et même, en toute franchise, je ne me sens guère affecté. Une telle meurtrerie est regrettable bien sûr, mais Embriaco n'était ni un mien ami, ni un associé. Et je ne déplore aucun vol. Pour moi, il n'y a là qu'heurt de voyage, comme il y en a tant. »

Le marchand sourit pour lui-même, sans chaleur, avant de poursuivre. « À l'aller, l'entièreté d'une famille a été emportée en deux jours, par de males fièvres. Un père, son épouse et leurs trois enfançons. Allemans, ce me semble. Là encore, triste histoire, mais dont les récits de marin sont truffés. »

Régnier laissa ses yeux rejoindre le paysage quelque temps, se mordillant les lèvres. Mais il ne désarma pas et, se tournant de nouveau vers le négociant, accrocha son regard.

« En fait, je souhaiterais surtout apprendre de vous sur les opérations marchandes de maître Embriaco. Avez-vous déjà négocié avec lui, ou en avez-vous ouï rumeurs ?

— Je dois avouer que je l'aurais désiré, il y a encore peu. Mais commercer avec Gênes perd ses attraits pour personnes telles que moi. Ils partent surtout de leur propre port, Gibelet, et taxent si fort les étrangers que le bénéfice disparaît. Je pense d'ailleurs à carrément déserter le ponant[^ponant] de la Méditerranée. »

Régnier remarqua le ton amer du négociant. Un tel ressentiment aurait-il pu être à l'origine d'un meurtre ?

« Et de sa renommée, connaissez-vous d'aucuns qui auraient parlé de lui, en bien ou en mal ?

— Très franchement, je ne m'en souviens guère. C'est possible. Mais il n'était pas si puissant qu'il aimait à le croire. Il n'était qu'un commissionnaire, servant plus fortunés et puissants. Guère plus qu'un valet, en somme. Comme je le suis moi-même.

— C'est dans l'ordre du monde de montrer son talent avant d'en user selon ses désirs.

— Il y a de ça, il faut en permanence donner force garanties. Et épargner pour investir en son temps. Et cela demande des années, moult années ! »

Ja'fa baissa le regard, comme attristé à cette pensée, fatigué du poids des ans pendant lesquels il voyageait pour en enrichir d'autres. Régnier se rapprocha insensiblement et hésita quelques secondes avant de poser la question qui lui brûlait les lèvres.

« J'ai pourtant ouï qu'il commençait à avoir puissants associés,  riches négociants, consuls et notables de la *Compagna*.

— Un marchand peu scrupuleux a beau jeu d'amasser rapide fortune. Il lui suffit de choisir d'habiles partenaires et de s'ouvrir aux risques. Si bonne fortune vous sourit… »

Ja'fa haussa un sourcil narquois, qu'accompagna un rictus moqueur. Puis il reprit son sérieux avant de poursuivre.

« Mais ce faisant, vous jonglez avec chaudes braises. Et risquez d'être dévoré dans les flammes à la toute fin !

— Vous voulez dire que sa richesse serait mal acquise ? »

Régnier marqua un temps, réfléchissant à ce qui pouvait bien motiver le jeune négociant à critiquer ainsi le Génois défunt.

« Il me faut vous affirmer que rien ne l'établit à ce jour d'hui. Ses archives ne citent qu'habituelles denrées : étoffes, matières à teindre, papier… Les rares fois où il s'est risqué à de plus délicates marchandies, c'était toujours à destination d'Acre ou de Gibelet, donc honnête et autorisé.

— Êtes-vous à ce point naïf que vous vous pensez qu'on note pareilles males opérations ? Absence de marques n'est pas marque de l'absence. »

Régnier dut reconnaître la pertinence de la sentence, bien que cela ne collât pas vraiment avec le portrait de la victime qu'il s'était fait jusque-là. À moins que, se sentant coupable, Ansaldi n'ait voulu racheter ses fautes par des dons. Il lui fallait vérifier la date des donations et si elles pouvaient avoir été engendrées par un événement précis, un déclencheur, puis retrouver les voyages qui précédaient immédiatement ces versements. C'était un travail minutieux et un peu ingrat, mais il était persuadé qu'un clerc comme Herbelot excellerait dans un tel ouvrage de collationnement.
