## Chapitre 12

### Port de Gibelet, soir du 1er novembre

Ernaut regardait le soleil se coucher par-delà les fortifications du port. Le *Falconus* était entré peu de temps avant que le cor ne soit sonné pour établir la lourde chaîne entre les deux tours à l'entrée de la rade. Les bannières des sires d'Embriac et du comte de Tripoli claquaient dans la brise de mer sous un ciel baigné de lumière jaune-orangé. Tous les passagers étaient sur le pont, maintenant que le navire était amarré, et attendaient pour subir les formalités douanières.

La ville s'établissait sur les contreforts légèrement surélevés menant aux montagnes visibles plus loin vers l'est. Les maisons étaient dominées par une grande église cathédrale, Saint-Jean, et la forteresse des seigneurs du lieu. Celle-ci était située sur le point le plus élevé, un peu au sud des habitations encloses d'une muraille, au milieu de quelques palmiers. Puissant quadrilatère de pierres miel renforcé de tours aux angles, le haut donjon supervisait toute la région et assurait à son propriétaire de faire reconnaître son pouvoir sur le pays alentour. C'était là le cœur de la puissance de la famille Embriaco en Outremer.

Ernaut huma l'air à pleins poumons, déçu de n'y remarquer que les odeurs marines qu'il subissait depuis des semaines. Il avait rêvé qu'il s'exhalerait de ces territoires des senteurs nouvelles, épicées, florales, sucrées, comme s'il se trouvait dans la boutique d'un apothicaire. Mais pour l'instant, il devait se contenter d'admirer les palmiers, les maisons à l'architecture un peu étrange, les tenues exotiques des hommes du guet, nombreux à porter le turban. Voyant qu'un petit groupe s'était approché de leur navire, avec un guerrier en bliaud de qualité, une épée au côté, il espérait, comme beaucoup, enfin être autorisé à descendre du bord.

La passerelle fut mise en place, l'officier monta aussitôt et deux sergents restèrent près de la planche pour bloquer le passage. Ne prêtant pas attention aux visages tournés vers lui, l'homme fut rapidement mené au château arrière, où se trouvait le *patronus*. Quelques curieux s'étaient regroupés aux alentours des deux soldats de faction, d'où chacun essayait de les examiner plus attentivement sans en avoir l'air. Le premier, le visage marqué par les ans, était habillé d'un qabâ[^qaba] turc en laine, avec des bandes à tiraz[^tiraz] de belle qualité. Il était chaussé de bottes arborant un décor floral sur leur partie supérieure. Enfin, il était coiffé d'un turban savamment noué autour de la tête, dont les deux extrémités pendaient longuement. Mais le plus étonnant pour les passagers, c'était qu'au contraire de son compagnon, visiblement européen, il avait tout l'air d'un infidèle. Régnier s'approcha silencieusement de Lambert et Ernaut et s'aperçut qu'ils étaient aussi intrigués que les autres par ce soldat.

« Il est Syrien et chrétien. Il est parfois malaisé de les différencier des musulmans. »

Ernaut se retourna, se frottant la lèvre avec le doigt.

« Alors, comment reconnaît-on nos ennemis ? »

La naïveté de la question arracha un sourire au chevalier.

« Ça, malheureusement, ce n'est jamais aisé. Mais peu importe sa vêture ou son apparence, l'adversaire finit toujours par se dévoiler d'une façon ou l'autre. D'aucune façon, moult guerriers ennemis sont Turcs ou Turcmènes, bien différents des Arabes… »

Une bousculade vers la poupe du navire les interrompit. Les valets de Mauro Spinola tentaient de progresser au milieu de la foule et du pont encombré, portant des coffres et des paniers. L'officier du port interpella ses soldats dans une langue rauque, leur intimant l'ordre de les laisser passer. Le visage d'Ernaut s'illumina.

« On va pouvoir descendre alors ? »

Régnier fit une moue dubitative et s'avança un peu.

« Cela m'étonnerait que ce soit si rapide. »

De fait, l'homme à l'épée écartait les gens du bras pour faire de la place à Mauro Spinola. Celui-ci arborait son air le plus sévère, espérant faire taire par sa seule autorité naturelle toutes les critiques éventuelles. Il descendit rapidement du *Falconus*, attendant quelques instants sur le quai en discutant avec l'officier. De nouveaux gardes arrivèrent et escortèrent le diplomate droit dans les ruelles de la ville. Pendant ce temps, les serviteurs s'employaient à débarquer les affaires personnelles qu'ils fixaient sur des chariots à bras amenés là. Aucune autre personne n'avait été autorisée à passer.

Ernaut, comme toujours au premier rang des spectateurs, vit s'avancer Enrico Maza. Celui-ci portait une grande partie de son équipement, assisté de deux compagnons d'armes. Il avisa le colosse et s'approcha de lui, posant la main amicalement sur son épaule.

« On va se séparer là, garçon. J'ai trouvé louage auprès de Spinola. »

Un peu surpris par ces adieux brutaux, l'adolescent bredouilla, l'air franchement ébahi :

« Vous allez travailler pour… cet homme ? »

Enrico esquissa un sourire forcé.

« Je sais qu'il a mauvaise renommée, mais moi je suis déjà damné comme un serpent. Adoncq… »

Ernaut acquiesça en silence, soudain abattu de perdre celui qu'il commençait à estimer comme un ami.

« Prends soin de toi, gamin. On se croisera peut-être à nouveau un jour. »

Et sans un mot de plus, le soldat descendit en quelques enjambées sur le quai, surveillant le chargement de ses affaires sur un chariot. Puis il suivit le groupe en ville, après un dernier salut de la main en direction d'Ernaut et de ses compagnons.

Soudain la cloche du navire résonna frénétiquement, attirant les regards vers l'arrière. Ribaldo Malfigliastro était sur la dunette, entouré du pilote, du nocher, du notaire de bord et de ses principaux matelots, tous demandant le silence avec force gestes. Impatients d'en savoir plus, les passagers mirent rapidement fin à leur brouhaha.

« Je viens de tenir conseil avec les autorités du port. Le débarquement sera en plusieurs fois. Prime, pèlerins et voyageurs sans marchandise pourront aller dès demain matin. S'ils n'ont personne qui les attend, ils seront accueillis et guidés vers les hospitals d'accueil, aux fins de préparer leur cheminement vers Jérusalem. »

Des vivats accompagnèrent cette déclaration, certains s'embrassant les uns les autres à l'annonce de la fin de leur long et périlleux voyage. Ernaut ne put s'empêcher de grimper sur les haubans, debout sur le plat-bord, pour hurler encore plus fort que les autres. Sautant ensuite sur le pont, il faillit étouffer son frère en l'étreignant et se mêla à l'embrassade générale qui s'ensuivit. Quelques-uns parmi les plus pieux entonnèrent tout de même un *Salve Regina* en remerciement. Il fallut quelques minutes avant que le *patronus* puisse à nouveau se faire entendre.

« Des officiers de la douane viendront également à bord demain, pour préparer le débarquement des marchands et le déchargement de leurs denrées. Ils pourront mettre pied à terre dès ce samedi, après-demain, lorsque les formalités auront été achevées. »

Une voix dans la foule s'écria « Et les taxes payées surtout ! »

Malfigliastro ne put réprimer un sourire, se frottant un sourcil, l'air amusé.

« C'est à peu près ça, oui. Benedetto, le notaire, vous assistera si vous en avez besoin. Rassurez-vous, les personnes qui vous aideront parlent toutes le françois ou le provençal. Si jamais vous venait désir de rentrer chez vous à bord de mon navire, nous mettrons à la voile assez tôt dans l'année, avant Pâques, et naviguerons en *conserva*[^conserva], pour plus grande sécurité. Cela n'intéressera certes pas les fidèles occupés aux célébrations pascales à Jérusalem. Mais je serais heureux d'accueillir quiconque à mon bord. »

La même voix que précédemment l'interpella une nouvelle fois.

« À bon prix ?

— Ça, je ne le décide pas. Nous offrons toujours un honnête prix. Vous pouvez même être loué pour faire le passage, comme rameur sur une des galées si le cœur vous en dit. »

La foule ricana à cette plaisanterie et les conversations privées reprirent tout doucement. Toutefois, Malfigliastro n'avait pas fini. Il adopta un air plus solennel et ramena difficilement le silence.

« Ce soir, comme le temps semble clément, prenons notre souper en commun sur les ponts, et prolongeons la veillée ensemble, une ultime fois. J'aurais aimé que ce soit occasion pour nous tous de prier pour nos quatre défunts compagnons, vu que c'est demain la fête des Morts. »

Un murmure d'approbation, moins débridé que précédemment, entérina ce discours et de petits groupes commencèrent à se former, commentant les informations et préparant le débarquement. Ernaut souriait béatement à son frère, heureux d'être arrivé à bon port. Il voulait proposer au chevalier de se joindre à lui pour la soirée, mais lorsqu'il le chercha du regard, il mit un moment à l'apercevoir. Régnier était monté auprès du *patronus* et discutait avec lui tout en l'accompagnant à sa cabine.

### Matin du 2 novembre, port de Gibelet

Lambert et Ernaut aidaient Gringoire à sortir sa malle du pont passager. Comme le temps était beau de larges trappes avaient été démontées afin de faciliter le déchargement, et des échelles supplémentaires, parfois improvisées, accueillaient un fourmillement de voyageurs pressés de se retrouver sur la terre ferme. Sous le contrôle d'un officier à l'air taciturne, quelques soldats vérifiaient qu'aucune marchandise devant être taxée ne se faufilait sans autorisation. Les pèlerins et leurs affaires étant exonérés du paiement des taxes, certains fraudeurs essayaient de se faire passer comme tels ou de glisser dans les besaces de ces derniers quelques objets précieux qu'ils récupéreraient plus tard.

Le ciel était un peu gris, mais la température était clémente et les hommes en sueur avaient retroussé leurs manches pour manipuler les bagages. Sur le quai devant le *Falconus*, ce n'était qu'un amas de coffres, paniers et sacs, déplacés, triés, réorganisés au gré des différents passagers et des contrôles douaniers. Tout cela se faisait au milieu d'un vacarme de halètements, d'interjections, de cris, de discussions enflammées et d'au revoir poignants, un peu dans toutes les langues. Des portefaix, parmi lesquels se glissaient quelques larrons, tentaient de proposer leurs services ou ceux de leur âne, pour transporter les affaires à la demande. Et même de l'autre côté, dans l'eau, des vendeurs à la sauvette s'étaient approchés en barque du navire et présentaient aux hommes encore à bord des fruits, du pain et de la nourriture fraîche.

Ayant fini de descendre les bagages, le groupe encadré par Aubelet se préparait à se rendre à l'hôpital[^hopital] où il resterait quelques jours, en attendant d'organiser son voyage vers le sud, vers la Terre sainte tant espérée. L'ambiance était euphorique et tout le monde se congratulait joyeusement ou se promettait des retrouvailles prochaines, tels de vieux amis. Ernaut et son frère demeuraient à bord, à la demande de Régnier, désireux d'avoir l'adolescent à ses côtés chez le seigneur du lieu, pour apporter son témoignage. Rendez-vous était donc donné à Jérusalem, où tous se retrouveraient afin d'assister à la célébration de Pâques.

Assis à califourchon sur le plat-bord, Ernaut et Lambert faisaient un dernier salut de la main à ceux qui avaient partagé ce long périple, lorsqu'ils virent s'approcher un cavalier de belle prestance, sur une monture de prix. Sa maîtrise désinvolte en disait long sur son aisance et sa compétence. Il était habillé d'une cotte de soie unie rouge brique, décorée de bandes de broderies sur les bras, les poignets et au cou. Il portait les cheveux courts et sa peau mate indiquait qu'il vivait là depuis longtemps, mais il était visiblement d'origine européenne.

Il descendit de son cheval, lança ses rênes à un garde et vint presque en courant sur le *Falconus*. Il paraissait de fort bonne humeur. À peine sur le navire, il toisa de droite et de gauche et héla un marin à la manière d'un homme habitué à être obéi. Il demandait après Ansaldi Embriaco. Les deux frères se regardèrent, hésitant à aller lui parler. Mais son allure les incitait à la méfiance. Il ne fallut de toute façon pas très longtemps pour que Ribaldo Malfigliastro arrive, l'air ennuyé. Il annonça la mauvaise nouvelle au chevalier, qui fut extrêmement choqué.

Régnier et Herbelot s'approchèrent à leur tour et, après s'être présentés, lui résumèrent l'affaire en quelques mots, sans entrer dans les détails. Sous le coup, le nouvel arrivant s'assit sur un des paquets posés là en attendant, se passant la main sur le visage comme s'il tentait de se réveiller d'un cauchemar. Tout en faisant mine de s'étirer en bâillant, Ernaut avait fait en sorte d'être suffisamment proche pour entendre ce qui se disait.

« J'ai pour nom Guy, chevalier de l'hostel de messire Guillaume d'Embriac, seigneur de Gibelet. Je connaissais Ansaldi depuis l'enfance. Il a passé moult années ici, et nous étions restés bons amis. Il m'avait écrit qu'il devait passager à bord du *Falconus* et arriver avant la clôture des mers[^cloturemer]. Je me faisais si grande joie de le retrouver, après toutes ces années ! »

Il redressa le buste et inspira profondément, les traits du visage soudain tirés.

« Et vous dites que son valet l'aurait poignardé, par sotte vengeance ? Quelle nouvelle ! Si je m'attendais à ça !

— Il nous faut d'ailleurs livrer le fautif au sire de Gibelet. Je donnerai toutes les explications à messire Guillaume avec l'aide de maître Gonteux, que voici, clerc de l'archevêque de Tyr. J'ai demandé au jeune Ernaut, justement occupé à nous épier de peu discrète façon, de demeurer avec nous car il a beaucoup aidé à la traque. »

À la mention de son nom, l'adolescent sursauta puis se retourna, un peu embarrassé, sous les regards désapprobateurs d'Herbelot et du *patronus*. Il fit une espèce de courbette symbolisant à la fois un salut et des excuses. Le chevalier tripolitain le regarda d'un air morne, ne semblant pas vraiment le voir malgré sa masse. Il revint vers Régnier et Ribaldo.

« Je vais porter ces nouvelles à mon seigneur, et il faudra mener le meurtrier en la forteresse. Vous y serez également tous entendus, au plus tôt. Nous vous garderons le moins longtemps possible, vous devez être espéré par l'hostel du roi.

— Et celui de l'archevêque, ajouta Herbelot, mortifié d'être ainsi négligé.

— Oui, veuillez m'excuser. Je suis fort ému. J'en oublie les usages. »

Il se leva de son siège et réajusta son vêtement, puis prit congé poliment. Lorsqu'il redescendit par la passerelle, ce n'était plus le même homme. On aurait dit que le ciel venait de lui tomber sur la tête. Régnier s'approcha d'Ernaut, les sourcils froncés.

« Il va falloir mieux te tenir ! En présence de messire Guillaume, veille à ne pas oublier ton rang. Vu que c'est par moi que tu y seras amené, tout ce que tu y feras me sera attribué. Adoncques ne me fais pas regretter la confiance que j'ai eue en toi jusqu'à ce jour. »

L'adolescent secoua la tête, un peu contrit de cette réprimande sévère. Il ne pipa mot, marquant ainsi l'obéissance qu'il entendait afficher. Le chevalier fit demi-tour, revenant vers Ribaldo et Herbelot, toujours à évoquer les événements à venir. Le clerc, ayant relevé les remarques adressées à Ernaut, arborait le sourire satisfait de ceux voyant les choses évoluer à leur gré. Il semblait particulièrement impatient d'être introduit auprès du seigneur du lieu. Régnier écoutait d'une oreille un peu distraite, se contentant d'abonder silencieusement en hochant la tête lorsqu'on sollicitait son avis. Au bout d'un moment, le *patronus* se décida à lui demander ce qui le tourmentait tant.

« Je ne sais, rien de bien clair. Mais j'ai impression que le chevalier n'a guère été surpris de la meurtrerie de son ami. »

Herbelot prit un visage effrayé.

« Vous croyez qu'il appartenait au complot ?

— Non, certes pas. Un homme de son statut ne s'abaisserait pas à s'acoquiner avec simple valet. Et d'aucune façon, il a été sincèrement frappé par la mort d'Ansaldi. Un instant il m'a semblé qu'il ouïssait une nouvelle… qu'il avait longtemps crainte. »

### Citadelle de Gibelet, midi du 3 novembre

Ernaut se sentait un peu gauche dans ses habits propres. Exceptionnellement, il avait tenté de rester en arrière et de se faire discret, mais on lui avait vite fait comprendre qu'il devait être devant, vu qu'il serait certainement amené à témoigner. On était venu chercher la petite troupe en fin d'après-midi pour se rendre à l'audience du seigneur de Gibelet. Les gardes de leur escorte étaient plutôt décontractés, heureux de n'avoir comme prisonnier que le vieil homme et pas le colosse endimanché qu'ils regardaient en souriant. Sur les conseils de Herbelot, Régnier avait demandé à Lambert de se joindre à eux, estimant que la présence d'un frère aîné pourrait éviter des débordements. Il redoutait toujours une bourde ou une maladresse de la part de l'adolescent, malgré toute l'amitié qu'il lui portait.

La forteresse était très imposante et le monumental donjon rectangulaire dans la cour submergée de lumière en imposait par sa masse. Même Ernaut se sentait tout petit devant le spectaculaire édifice de pierre. Alors qu'ils patientaient, il n'avait pas pu s'empêcher de toucher de la main l'impressionnante architecture. Il n'avait jamais vu de mur pareil, avec d'énormes blocs à bossage en table. La salle d'audience se trouvait à l'étage supérieur, auquel on accédait par quelques degrés aménagés dans l'épaisseur de la maçonnerie, depuis le premier niveau que desservait un escalier de bois extérieur. Partout où il passait, le jeune homme prenait garde à ne pas se cogner dans les portes basses et les couloirs étroits. De plus, il sentait bien que de nombreux regards se portaient sur lui, qui dépassait d'une tête les plus grands des hommes alentour, avec un gabarit de même mesure.

Lorsqu'il pénétra dans la salle d'audience, Ernaut fut impressionné par le décor, tout à la gloire du maître des lieux. Les murs étaient agrémentés de peintures et de tapisseries suspendues et, assis sur une chaise curiale sur une estrade à l'extrémité se trouvait Willelmo Embriaco, Guillaume d'Embriac, seigneur de Gibelet.

Dans la force de l'âge, celui-ci écoutait ce que lui disait à l'oreille un de ses conseillers tonsuré, ouvrant de grands yeux à l'entrée d'Ernaut et le suivant du regard. Il salua d'un signe de tête Régnier, tout en le gratifiant d'un sourire. À sa gauche, debout en contrebas de l'estrade se trouvait Guy, au milieu d'une demi-douzaine d'autres chevaliers en tenue de cour, longs bliauds taillés dans de coûteuses étoffes, réhaussés de décor brodés et de passementeries. Ernaut se sentait déplacé, avec sa cotte aux couleurs passées. Il ne savait que faire de ses bras ni de ses mains, et hésita longtemps avant de se résoudre à les bloquer dans sa ceinture, tout en essayant de n'avoir pas l'air trop crâne.

Ugolino fut amené devant Guillaume d'Embriac et Guy exposa ce dont il était accusé. Le valet ne réagit à aucun moment, se contentant de fixer le sol, comme absent. Soucieux de recueillir le plus d'informations possible, le seigneur interrogea tour à tour Régnier, Herbelot et Ernaut. Ce dernier répondit par monosyllabes et phrases courtes, attentif à ne pas décevoir Régnier. D'un regard, ce dernier le rassura lorsqu'il reprit sa place. Après quelques instants de réflexion où tout le monde resta silencieux, Guillaume d'Embriac prit la parole de façon solennelle.

« Je n'arrive à croire que tu aies meurtri un mien cousin pour si absurdes motifs ! Cela sans compter les victimes faites à Gênes. Je ne sais comment tu es arrivé à croire qu'Ansaldi était à l'origine de tes maux, mais ce ne peut être que par erreur. Chaque homme finit par recevoir son dû ; faire porter le poids de ses échecs à son prochain est le signe du démon. Je méditerai l'affaire, voir s'il est nécessaire que je te renvoie outre la mer, ou si je te condamne céans pour tes crimes. D'ici là, tu seras tenu en la citadelle, en attente de ma décision. »

Puis, faisant un geste définitif de la main, il fit comprendre qu'il fallait emmener le prisonnier.

« Quant à vous, messire Régnier d'Eaucourt et maître Gonteux, je serais aise de vous avoir à ma table ce soir, ainsi que toi, jeune Ernaut. Vous serez mes invités en attendant votre départ pour la sainte ville. »

Les deux premiers, habitués aux usages de la cour, s'inclinèrent en remerciement, bientôt imités par Ernaut, toujours sur le qui-vive. L'entrevue était terminée.

Ils repartirent par là où ils étaient venus, mais sans escorte cette fois. Guy les accompagna jusqu'à la sortie de la forteresse, leur laissant le soin d'organiser le transport de leurs affaires depuis le navire. Ils profitèrent du retour pour flâner un peu dans les rues, où les souks étaient en train de fermer. Les deux frères, qui découvraient l'Orient, s'émerveillaient bruyamment face à la moindre boutique, la plus petite fontaine. Ils s'arrêtaient à chaque place pour n'y admirer parfois que les palmiers entourés de plantes odorantes. Devant tant d'enthousiasme et de naïveté, même Herbelot ne pouvait réfréner un sourire, et l'humeur du groupe s'améliora peu à peu pour finir excellente lorsqu'ils arrivèrent sur le port.

Là, le débarquement des balles de marchandises occasionnait un tumulte encore plus impressionnant que la veille. Des employés des douanes surveillaient le moindre paquet, vérifiaient les sceaux avec le notaire de bord et les négociants, et allaient et venaient jusqu'au bureau où les taxes étaient réglées et enregistrées. Le petit groupe dut patienter quelque temps avant de pouvoir accéder à la passerelle, que les allers et venues des porteurs monopolisaient. Le pont était très largement ouvert et toutes les trappes avaient été démontées pour faciliter le débarquement. Ernaut et son frère s'y engouffrèrent rapidement, impatients de rassembler et de descendre leurs quelques affaires du navire.

Alors que Régnier et Herbelot se coordonnaient pour retourner à la citadelle ensemble avec leurs bagages, ils aperçurent la frêle silhouette de Sawirus de Ramathes qui leur faisait de grands signes depuis le château avant, ses larges manches d'étoffe légère flottant dans le vent comme une bannière. Ils s'avancèrent donc au pied de la plate-forme, interrogeant du regard le marchand.

« Auriez-vous encontré Ja'fa ? Il a débarqué ses bagages tôt ce matin, et je le crois déjà parti. Il m'avait salué hier à la veillée, mais j'aurais aimé deviser avec lui une ultime fois. »

Comprenant qu'il y avait un problème, le chevalier et le clerc montèrent à l'échelle et rejoignirent le vieil homme à l'étage. Il était visiblement inquiet, se grattant le crâne d'un geste nerveux.

« Que se passe-t-il ?

— Je suis fort soucieux pour l'avenir de ce jeune homme. Il risque de suivre un mauvais chemin.

— À quel propos ? »

Le vieux marchand baissa un peu la voix, comme si quelqu'un avait pu l'entendre, dans le vacarme autour d'eux.

« Je compte sur vous pour tenir la chose discrète. Il m'a confié hier soir à mots couverts que Mauro Spinola lui a fait proposition de s'associer pour du négoce avec l'Égypte. Il cherche des partenaires syriens peu scrupuleux. Comme vous le savez, la dogana[^dogana] fâtimide est toujours en demande de certaines denrées, bois, fer et poix ou autre… Mais ce commerce en est défendu pour les marchands chrétiens, à juste cause. Je voulais donc le sermonner une nouvelle fois. »

Herbelot lança un regard furieux vers Régnier, les lèvres pincées.

« Quelle impudence ! Proposer pareil marché à des gens passageant sur le même navire que nous ! »

Régnier resta calme, mais son visage indiquait bien la contrariété qu'il ressentait.

« Ce serait de fait une fort mauvaise idée ! Le roi ne plaisante guère avec ce genre de méfait. Et si Spinola arrive à passer au travers depuis des années, cela risque d'être fort risqué pour un marchand moins rusé et avec moins d'amis. Vous avez eu raison de m'en parler, soyez assuré que j'apprécie cette confiance. Cela demeurera entre nous, n'est-ce pas, maître Gonteux ? »

Le clerc gonfla ses joues de surprise, peu enclin à réfréner son envie de se répandre aussitôt que possible sur ces détestables marchands et leurs ignominieuses habitudes. Se mordillant la lèvre un bon moment, il finit par lâcher du bout des lèvres un oui peu enthousiaste.

« Très bien, alors voilà ce que je vous propose. Sans doute Ja'fa est-il impatient de retourner chez lui. Je pourrais le retrouver en passant par Acre. Et faire en sorte qu'il ne gâche pas son avenir. »

Herbelot, de plus en plus nerveux et dansant presque sur place de colère, ne put se retenir plus longtemps.

« Et Spinola ? Allez-vous le laisser continuer ainsi en toute impunité ?

— Je ne peux rien faire, il a de nombreux alliés. Mais j'espère  avoir occasion de mettre un terme à ses méfaits. Pour l'heure, je dois me contenter d'amasser des éléments et de préparer des témoins, attendant qu'il me soit enfin possible de le démasquer au grand jour. »

Herbelot n’était pas satisfait de cette réponse et se lissait la moustache et la barbe d'une main nerveuse.

« Alors même un chevalier de l'hostel du roi de Jérusalem ne peut mettre fin aux agissements d'un tel… d'un tel… »

Incapable de trouver le mot convenant à la détestable opinion qu'il avait de Spinola, Herbelot partit, furieux. Sawirus le suivit des yeux, l'air las, puis fit un sourire timide à l'adresse de Régnier. Celui-ci comprit vite là où le marchand voulait en venir.

« Vous avez raison. Sans pour autant partager sa colère, j'en apprécie la valeur. Sinon à quoi bon servir comme je le fais ? »

### Gibelet, après-midi du 3 novembre

Régnier était confortablement installé sur un divan bas dans la pièce principale de la demeure de Guy, aux abords de la porte donnant vers la citadelle. Le Tripolitain louait un vaste appartement au premier étage d'un bâtiment qu'il partageait avec plusieurs familles, doté au rez-de-chaussée d'un beau jardin flanqué d'écuries.

L'endroit était spacieux et calme, une fenêtre à volets en claustra de bois, ouverts ce jour-là, apportait une clarté agréable. Le sol était recouvert de tapis et quelques coffres bas et coussins constituaient l'essentiel du mobilier. Apparemment le chevalier était adepte de la vie à l'orientale. Lorsqu'il revint, suivi d'un domestique porteur de boissons et de sucreries, il était vêtu d'un ample vêtement à manches très larges, assez coloré sur le devant, dont les broderies de bras, des bandes à *tiraz*, indiquaient une origine locale. Régnier, pour sa part, s'habillait toujours à l'occidentale, cotte plus ou moins longue et ajustée, selon les circonstances, et chausses de lin ou de laine. Le *patronus* du *Falconus*, assis à côté de lui, partageait ses goûts vestimentaires.

Ils avaient été invités par l'homme de l'hôtel du seigneur de Gibelet parce que ce dernier souhaitait discuter avec eux du cas d'Ugolino. Il était d'ailleurs peut-être en service commandé pour Guillaume d'Embriac, Régnier ayant eu maintes fois l'occasion de s'acquitter de telles missions. Une fois les rafraîchissements servis et les invités confortablement installés, Guy prit la parole.

« Je souhaitais vous entretenir, car je subodore quelque étrangeté en cette histoire. Je ne comprends pas pourquoi le valet a occis Ansaldi. Les autres meurtreries ne me posent pas souci. Mais poignarder mon ami n'a guère de sens au vu des motifs qui animent le vieil homme. »

Régnier croquait dans une pâtisserie à base d'amandes.

« Je suis d'un semblable avis. Il ne s'est guère étendu sur les raisons le conduisant à fustiger son maître des vilenies dont il l'accuse.

— Sornettes de son invention ! Je connaissais fort bien Ansaldi, il n'était pas homme à profiter de son prochain. Certes il avait de l'ambition, mais pas de ce monde. S'il amassait des richesses, c'était en un but précis, pour défendre le Christ et la Croix. Nous avions songé quand nous étions jeunes de rejoindre la Milice du Christ[^templiers] ou les frères de Saint-Jean-de-l'Hôpital[^hospitaliers]. Et je pense que, de nous deux, c'était lui le plus fervent. Il était profondément croyant.

— Nous l'avons découvert en cherchant dans ses archives. Il n'en faisait pas étalage.

— Comme tous les hommes de bien. Mais le fait est qu'il souhaitait œuvrer pour la Foi, même alors qu'il n'était que marchand. Il avait entrepris de faire tomber les corrompus.

— Nous y voilà ! pensa Régnier.

« Ce que je vais vous révéler doit demeurer frappé du sceau de secret… Ansaldi s'était mis en tête de rendre notoire les trafics de Mauro Spinola. »

Le chevalier toussa pour garder sa contenance mais ne put se retenir de se pencher en avant, impatient d'en savoir plus sur le cœur du mystère.

« Comment comptait-il s'y prendre ?

— Il espérait que Spinola lui ferait proposition de s'associer puis aurait tout avoué en place publique. Vu sa parentèle, puissante et respectée, cela aurait eu un retentissement important, et un poids certain auprès des consuls, alors obligés de sévir.

— Diablement dangereuse comme idée !

— Comme la suite l'a malheureusement montré… »

Il sortit de la manche de son vêtement un pli cacheté qu'il tendit à ses invités.

« En cette lettre, Ansaldi m'écrit qu'il a réussi à attirer Spinola à bord de son navire et que le vieil escroc lui a fait une prime proposition, sans donner de détails. Il était perturbé par cette détestable situation et craignait que tous ces mensonges, même pieux, finissent par entacher son âme immortelle. Il souhaitait se confesser, mais hésitait quant au choix du prêtre. »

Régnier jeta un coup d'œil hâtif au courrier.

« Quand vous a-t-il adressé cette missive ?

— Il l'a laissée à Messine à un ami marchand, d'où elle m'est parvenue par galée rapide. Ce qui m'a de suite fait souci, c'est qu'il semble que le sceau en avait été tranché, avec un fil fin, puis recollé. Spinola a des contacts partout, et une compagnie de fidèles mercenaires. Il a très bien pu prendre connaissance du contenu de ce pli alors que vous étiez encore en Sicile. »

Un peu abasourdi par cette histoire, Ribaldo se rencogna sur ses coussins, cherchant à s'appuyer sur un dossier solide, sans succès.

« Une chose m'intrigue. Comment a-t-il attiré Mauro Spinola sur le *Falconus* ? Pour autant que je le sache, ce dernier était fort occupé à traquer le meurtrier des notables.

— Précisément. Ansaldi connaissait cette mission, il en avait entendu parler alors qu'il était au palais Della Volta. Et par suite, il avait découvert que c'était Ugolino, pour l'avoir suivi à de nombreuses reprises. Il l'a donc loué à la mort de Pedegola. C'était surtout pour cela qu'il craignait pour son âme. Couvrir un assassin lui coûtait fort. Tout est dans la lettre. Il me voulait à l'arrivée pour l'aider à arrêter Spinola, ainsi qu'Ugolino bien sûr. »

Ribaldo acquiesça d'un air triste et se tourna vers Régnier.

« Je sais comment il connaissait la corruption de Spinola. Un mien frère est négociant et a pris part à ces contrebandes. La méthode était fort simple : après une petite participation, Spinola forçait ses associés à toujours plus de concessions, menaçant de les livrer s'ils se refusaient à lui. Comme il est entouré de témoins à sa botte et de notaires à ses ordres… Mon frère s'en est ouvert un jour au jeune Embriaco, qui a fort mal accueilli la chose et lui a prédit mille maux s'il n'arrêtait pas. Il l'a contraint à abandonner tous les profits à l'Église pour tenter de sauver son âme. C'est pour ça que chaque fois que vous envisagiez qu'il soit corrompu, j'ai tenté de vous faire comprendre que cela ne se pouvait. »

Les trois hommes demeurèrent silencieux plusieurs minutes, appréhendant enfin l'affaire dans sa globalité. L'horreur de la tragédie du *Falconus* leur apparaissait finalement dans son entier. Régnier s'assit sur le bord de son siège et se pencha en avant, commençant à s'exprimer par gestes avant même d'ouvrir la bouche.

« Il demeure alors encore une chose à découvrir ! Comment Spinola a-t-il fait pour qu'Ugolino poignarde Ansaldi ? Je n'imagine pas qu'il ait pu le payer, le valet n'est pas homme d'argent.

— Il a dû lui faire acroire qu'Ansaldi était lié de près ou de loin à l'expédition d'Espaigne.

— Comme il nous l'a laissé entendre, d'ailleurs ! En ce cas, si le domestique témoigne de ce que Mauro lui a dit, nous pourrions porter accusation ! »

Guy se frotta la joue, l'air dubitatif, les yeux au ciel.

« Vous le croyez vraiment ? Il n'a certes pas mis lui-même le poignard en la main de ce stupide valet ! »

### Forteresse de Gibelet, soir du 3 novembre

Ugolino était recroquevillé dans un coin de la pièce. C'était une sorte de débarras où étaient entassés des restes de tonneaux, des morceaux de meubles cassés, des seaux, le tout empilé en un tas branlant et poussiéreux. C'était un repaire de rongeurs variés et le royaume des araignées, où la seule lumière arrivait par dessous la porte. Les rayons rasants mettaient en relief le sol poudreux et trahissaient le moindre insecte ou la plus petite souris assez brave pour sortir des ténèbres. L’homme restait assis des heures sans bouger, ne prenant même pas garde à ses membres qui s'engourdissaient.

La serrure claqua bruyamment et une luminosité aveuglante  l'obligea à se protéger les yeux de la main. Deux grandes silhouettes portant des épées s'avancèrent, bientôt suivies par un géant. Le garde s'éloigna, laissant la porte ouverte.

« Ugolino, je suis avec messire Guy, officier du seigneur de Gibelet, et Ernaut, que tu connais bien. Nous sommes céans pour ouïr toute ton histoire. »

Le prisonnier balaya la demande d'un geste.

« Je vous l'ai déjà narrée encore et encore, messire. Je n'ai rien de plus à ajouter. »

Régnier s'avança un peu et sa voix se fit menaçante.

« Oh que non, tu n'as pas tout rapporté ! Et tu t'es fait berner, fol que tu es. »

Intrigué, le valet sembla retrouver un semblant de vie et chercha une position moins inconfortable.

« À propos de quoi ?

— De ton maître. Il n'était en rien dans la ruine de ta famille. Qui que soit la personne qui t'en a persuadé, elle a fait mensonges. »

Le visage éclairé par la lumière crue venant de l'extérieur, Ugolino ne pouvait guère se dissimuler. Un peu perplexe, il haleta comme un poisson hors de l'eau, les yeux légèrement exorbités. Guy s'agenouilla près de lui, provoquant un mouvement de recul instinctif de sa part.

« C'est Mauro Spinola. N'est-ce pas ? »

Le valet détourna le regard, tentant de fuir dans les pleurnicheries la terrible présence des deux chevaliers à l'allure martiale, aussi intransigeants que la justice qu'ils entendaient représenter. Il n'espérait par ailleurs guère de commisération d'Ernaut, qui se tenait un peu en retrait, tel un bourreau prêt à frapper. Régnier ne laissa pas Ugolino se réfugier dans le mutisme. Il se pencha à son tour, tapant du doigt la poitrine du captif.

« Veux-tu que nous contions partout que tu n'étais que sbire à la solde de Spinola ? C'est ce qui semblera le plus normal à tout le monde lorsque nous dirons que tu as frappé Ansaldi Embriaco, alors qu'il n'avait rien à voir avec les guerres d'Espaigne. »

Les yeux embués de larmes, Ugolino n'y tint plus et, tout en postillonnant une rare salive, il hurla.

« C'est faux ! Personne ne m'a payé ! Je voulais seulement vengeance, pour mon frère et ma parentèle, et pour tous les Génois volés par ces seigneurs cupides ! »

Guy prit une voix plus amicale.

« Adoncques, avoue qui t'a dit qu'Ansaldi était coupable de tels méfaits. Et ton geste ne sera pas entaché par rumeurs infondées. »

Pestant contre eux à mi-voix et moitié crachant, moitié pleurant, Ugolino finit par lâcher :

« Dans la cabine de maître Spinola ! Il m'avait mandé, car il avait quelque message pour maître Embriaco. J'attendais et j'ai entendu des voix outre le rideau. Le diplomate devisait à propos de maître Embriaco, disant qu'il était fort talentueux, ayant su accroître sa fortune lors des guerres d'Espaigne. Et tout cela, en restant fort discret et sans que cela ne se sache, évitant ainsi la disgrâce que d'aucuns avaient connue.

— Quand cela s'est-il passé ? demanda Guy.

— Alors que nous sortions du détroit de Messine. »

Régnier échangea un regard de connivence avec Guy.

« Qu'as-tu fait alors ?

— J'étais esbaubi… Moi qui l'avais cru loyal et honnête marchand. Je ne pouvais demeurer à son service ! J'ai donc résolu de le frapper au cœur, comme les autres ! »

Ugolino déglutit difficilement, se passant une langue épaisse sur ses lèvres sèches.

« Le meilleur moment était à Otrante, où j'avais possibilité de fuir à terre après l'exécution. Je l'ai occis la prime nuitée, mais ai dû demeurer à bord, avec l'arrivée de ce damné matelot. Adoncques j'ai fait comme si maître Embriaco était toujours vif, le temps de raisonner. J'ai mangé ses repas, bloquant la porte avec la huche lorsque j'étais absent. J'avais pris les pièces et les ai jetées outre  bord pour tracer de fausses pistes. J'ai volé le ciseau de Fulco pour glisser un lacet et fermer derrière moi le second soir. Le reste, vous le savez déjà… »

Guy se releva, l'air buté.

« Sais-tu seulement qu'Ansaldi avait découvert que tu avais commis toutes ces meurtreries ? »

Ugolino prit un masque dépité comme si plus rien ne pouvait l'étonner.

« Non, je n'en savais rien. Mais je comprends alors sa question alors que je le frappais… “Pourquoi ?” »

L'air désolé, le visage de Régnier se fit sévère tandis qu'il se relevait à son tour.

« Eh bien, apprends que ta lame a frappé une âme innocente. Il gênait Mauro Spinola, instruit depuis Messine que ton maître cherchait à le trahir, pour dévoiler le félon qu'il est. Adoncques il s'est servi de toi. Dans ta folie, tu n'as pas accordé une once de confiance à un homme qui te protégeait depuis des semaines. Lui aussi criait justice, mais pas de celle brandie nuitamment en des ruelles étroites, de la vraie justice, en plein jour, lorsque chutent les masques et que gredins sont livrés en pâture à l'opprobre. Tu pensais faire œuvre de justice, Ugolino, mais tu ne fus qu'abject mécréant ! »

### Forteresse de Gibelet, soir du 3 novembre

À leur sortie des bâtiments, Ernaut et Régnier partageaient un semblable abattement. Ils avaient salué Guy du bout des lèvres, soulagés d'enfin savoir la vérité, mais accablés de se voir confrontés à pareille noirceur morale. Puis ils cheminèrent en silence, contournant l'imposant donjon. Au dessus d'eux, les cieux étaient perforés ici et là de quelques points lumineux dans une mer indigo. Dans la vaste cour, les lampes à huile suspendues par endroits dessinaient des ombres dansantes de leur clarté jaunâtre. Le jour touchait à sa fin et le château entrait paresseusement dans la nuit. Pourtant, quelques valets s'affairaient bruyamment, amenant les montures et les mules à un groupe d'hommes, dont certains en armes, apprêtés pour le voyage.

Pressés de retrouver le navire et quelque repos, les deux compagnons se hâtèrent de rejoindre le portail principal, que surveillaient plusieurs gardes à l'humeur badine. Ils se moquaient d'un des serviteurs, fort occupé à tenir un étalon nerveux, à la superbe robe isabelle. Sans même y penser, Régnier s'arrêta pour admirer l'animal, soulagé de voir un peu de beauté apparaître en cette fin de journée. Alors qu'il se préparait à décrire le palefroi à Ernaut, à ses côtés, ses yeux dérivèrent naturellement vers le groupe où la bête était menée. Il lui fallut quelques instants avant de discerner la petite silhouette richement vêtue, qu'il identifia immédiatement. Figé dans son mouvement, il se rapprocha de quelques enjambées, le regard mauvais, la main vite posée sur le pommeau de son épée. Ernaut le suivit, intrigué, jusqu'à ce qu'il reconnaisse Enrico Maza parmi les soldats. C'était la troupe de Mauro Spinola.

Au centre de ce cercle empressé, le diplomate se tenait de dos, debout sur son perron, une nouvelle fois habillé d'une cotte de prix qu'il avait recouverte d'une chappe de drap fin, bordée de fourrure fauve. Il sauta prestement en selle et s'affaira à prendre ses rênes et s'installer confortablement. Pendant ce temps, il laissa sa monture avancer d'elle-même vers la sortie.

Prenant conscience qu'on le dévisageait avec insistance, il finit par relever doucement la tête. C'est là qu'il croisa le regard de Régnier, à quelques pas de lui, flanqué de son inévitable acolyte à stature d'ours. Il allait le saluer aimablement lorsqu'il découvrit une lueur désagréable dans les yeux du chevalier, la main crispée sur son arme. Le vieux Génois ne put retenir une amorce de sourire se dessiner sur ses traits, imperceptible. Il redressa le buste, lentement, savourant longuement la bouffée d'air qu'il prenait. Il se pourlécha avec indolence, petit triangle de chair rose franchissant subrepticement la mince fente de ses lèvres.  Il fut tenté un instant de bousculer les deux hommes en sortant, mais se ravisa.

Il commençait à incliner la tête avec suffisance en guise de salut lorsque son regard s'arracha enfin à celui de Régnier. Le géant venait d'avancer, insensiblement, les poings serrés. Si le chevalier était habité de fureur contenue, celle qui s'emparait du jeune colosse n'était que haine pure, sans retenue. La tension faisait saillir les veines de son cou, dont Spinola semblait percevoir le sang charrié par flots fougueux. Les narines frémissaient, tel un mufle de taureau avant la charge et les prunelles flamboyaient, pareilles à des braises. Instinctivement, le vieux diplomate lâcha prise et abandonna son air fanfaron, soudain inquiet de savoir si ses hommes étaient proches. Puis Ernaut montra les dents, plus qu'il ne sourit, la mâchoire contractée. Il avança encore, jusqu'à se trouver proche du Génois et, dans un souffle, lui lança :

« N'oubliez pas, messire, on ne doit point louer la journée dont on n'a encore vu la nuit… »

En un éclair, toute tension parut alors disparaître de l'adolescent. Le visage soudain illuminé par une joie intérieure, il exécuta une courbette quelque peu irrévérencieuse pour saluer, indiquant le passage d'un geste guilleret. Mauro Spinola semblait abasourdi, même l'étalon sous lui sentait son trouble et commençait à s'agiter. Médusé par le surprenant assaut, le Génois ne parvenait guère à retrouver une contenance. Il finit par claquer de la langue et lança sa monture au galop pour franchir les portes de la citadelle, sans un regard pour les hommes derrière lui.

Lorsqu'à son tour, Enrico Maza s'approcha au trot de la sortie, il hocha la tête, désolé. Mais Ernaut arborait de nouveau son air crâne et lui fit un clin d'œil appuyé, devant un Régnier encore pétrifié de la scène qui venait de se dérouler sans qu'il n'ose intervenir. Le jeune homme lui avait asséné une leçon qu'il n'aurait jamais attendue en pareil instant. Sans un mot, instinctivement, Ernaut avait senti que la bataille était perdue. Il était vain de tenter une action inconsidérée, rapide chemin vers le gibet. Néanmoins, refusant l'échec, Ernaut avait aussi fait clairement comprendre à son ennemi que jamais la guerre ne s'arrêterait pour autant. Assuré de sa force et de son droit, il n'avait qu'à choisir le bon moment pour goûter une complète victoire, inconditionnelle, évidente…, inéluctable ! Spinola l'avait bien senti, et plus jamais son vin n'aurait goût de nectar. La peur avait changé de camp.

### Gibelet, matin du 4 novembre

Un vent humide venu du large poussait des nuages vers la côte, où ils s'accrochaient aux reliefs des montagnes proches. Régnier assujettit le col de sa cotte à capuche, pour se garantir de la fraîcheur et de la pluie qui ne saurait tarder. Il attendait que Ganelon ait fini d'organiser le train d'animaux de bât. Régnier, Herbelot et les deux frères de Vézelay s'étaient réunis en convoi, au moins jusqu'à Tyr où le clerc, le premier, les quitterait. Ils avaient loué des montures, le chevalier ayant sélectionné un étalon nerveux et bien éduqué. L’animal ronflait régulièrement, impatient de dépenser son énergie. Herbelot avait opté pour une mule qu'il estimait être plus digne pour un prêtre. Mais il ne se priva pas d'en choisir une très belle, au poil brossé, avec un superbe harnachement.

Régnier était heureux de se retrouver enfin en selle. Il se sentait bien plus à l'aise sur une monture qu'à bord d'un navire. Faisant tourner son cheval d'un mouvement de hanche, il leva la tête pour admirer le paysage alentour. Un ciel bas mangeait la partie supérieure des sommets à l'est, mais la luxuriance de la végétation qui s'accrochait sur les pentes des monts du Liban était un vrai régal pour l'œil. Il renifla plusieurs fois l'air, content de n'y trouver que de faibles senteurs salines.

Au loin, un troupeau de moutons paissait, quelques bovins étaient menés vers la ville, certainement pour y être abattus. Ici et là, des paysans s'occupaient de leurs arbres fruitiers, de leurs jardins. Quelques voyageurs poussaient un âne fatigué devant eux, la trique à la main. Régnier se sentait bien, de retour chez lui. Impatient d'entamer le voyage, il mit son cheval au pas et le fit déambuler aux alentours du groupe.

Il regardait le jeune homme l'accompagnant, heureux de le voir plus posé, moins écervelé qu'à leur départ. Ernaut s'était avéré un compagnon agréable malgré son jeune âge et sa fougue parfois irritante. Régnier savait qu'il n'aurait pu mener ses recherches à bien sans lui. Il avait également insisté sur l'importance qu'il y avait à ne pas se montrer trop bavard à l'avenir, afin de ne pas s'attirer d'ennuis. Ernaut avait acquiescé avec gravité, se plongeant ensuite dans le mutisme.

En cavalier expérimenté, Régnier jaugeait l'adolescent sans même y penser et le trouvait relativement à l'aise, à l'inverse de sa monture, bien dérisoire entre ses jambes. Voyant que Régnier regardait vers lui, l'adolescent talonna laborieusement pour le rejoindre.

« Selon vous, messire, nous faudra-t-il long temps pour arriver ?

— Vous devriez être à Jérusalem d'ici une semaine à dix journées, guère plus. Nous serons à Beyrouth à la nuit. Nous pourrons y hosteler et pourrons même assister à une messe. Nous repartirons mardi au matin, une journée pour Sayette[^sayette], et une autre jusqu'à Tyr. Je ne sais si nous y demeurerons longtemps. De là, le voyage pour Saint-Jean d'Acre est d'une journée. Arrivé en la ville royale, je vous laisserai continuer seuls. »

Émerveillé, le jeune homme écoutait tout en observant les environs, curieux de découvrir les mystères de ce nouveau pays.

« La contrée sera-t-elle toujours ainsi ?

— Non pas. Les monts descendent jusqu'à Tyr, mais dès Sayette, la côte se fait plus large. Et la végétation plus rase aussi.

— Nous allons suivre la rive ? »

Régnier tendit le bras.

« Exactement. Au long de cette plage, sur le chemin caillouteux droit au midi, où l'on voit quelques gens s'éloigner. »

Ernaut souriait béatement, apparemment satisfait de se trouver là et impatient de parcourir ce nouveau monde.

« Croyez-vous que ce sont nos amis du *Falconus* en chemin vers la sainte Ville ?

— Peut-être, garçon, peut-être. »

Après un bref instant de silence, l'adolescent approcha son cheval de celui de Régnier. Il semblait plus sombre tout à coup.

« Il ne sera donc rien tenté contre maître Spinola ? »

Régnier renâcla un peu à évoquer de nouveau cette histoire contrariante. Il fronça le nez et inclina la tête.

« Il n'y a aucun témoin digne de foi, Ernaut. Ansaldi est meurtri et une lettre ne sert à rien.

— Et Ugolino ?

— Penses-tu vraiment que la parole d'un meurtrier avéré puisse faire balance face à un homme de renom tel que Spinola ? »

Ernaut baissa la tête, déçu de se rendre compte que même près du royaume de Dieu la justice ne se manifestait pas de façon aussi éclatante qu'il l'aurait espéré.

« Alors, que va-t-il devenir ?

— Je ne sais. Si le choix avait été mien, il aurait été pendu. Mais la décision appartient à d'autres. Et la loi génoise ne prévoit jamais d'exécuter un homme pour ses crimes. Il sera certainement banni.

— Vous croyez qu'il demeure une chance pour lui de se racheter malgré ses péchés ? »

Une voix se fit entendre derrière eux. Herbelot avait apparemment suivi leur conversation.

« Bien sûr ! La vie est toujours porteuse d'espoir. Et le pardon n'est qu'en Dieu. Il n'est jamais trop tard pour confesser ses fautes et chercher à s'amender. Il suffit d'être sincère en ses intentions. »

Ernaut se retourna vers Lambert, qui approchait au petit trot, pas très à l'aise sur un cheval visiblement peu confortable. Son frère avait entendu cette dernière phrase et il souriait, les yeux pleins de confiance. L'adolescent se redressa peu à peu, retrouvant son assurance coutumière. Il eut un rictus espiègle et, subitement, il claqua ses rênes sur la croupe de sa monture, la lançant dans un galop débridé qu'il ne maîtrisait pas parfaitement. Il manqua de faire partir tout le convoi en pagaille, inquiétant les autres animaux, y compris le train de bât que le valet venait de finir de constituer. Régnier soupira, mais il ne put retenir un sourire lorsqu'il entendit le jeune homme hurler, tandis qu'il s'éloignait à grande vitesse.

« Que les Infidèles escouillent le dernier sur la plage ! »

Lançant à son tour un regard faussement désolé vers Herbelot et Lambert qui maintenaient avec peine leurs montures, il fut envahi par un sentiment de gaieté. Négligeant les récriminations polies qu'il lisait dans leurs yeux, il éperonna son étalon, claquant de la langue. Il était bien décidé à montrer à ce jeune effronté qu'on ne défiait pas impunément un chevalier de l'hôtel du roi de Jérusalem. ❧

*À suivre…*
