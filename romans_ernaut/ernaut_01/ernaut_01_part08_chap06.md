## Chapitre 6

### Canal d'Otrante, nuit du 27 septembre

Maalot secoua doucement Aubelet par l'épaule. Il arrivait chaque jour à trouver facilement le sommeil et n'avait pas fait exception à la règle malgré l'agitation ambiante. Il ronflait paisiblement, pelotonné sur le côté, l'air boudeur et concentré. Sa femme n'avait pas eu le cœur de le réveiller pour lui annoncer la mauvaise nouvelle. Mais un des matelots était venu les voir, car sa présence était requise auprès du mort. Il sursauta légèrement et ouvrit avec difficulté un œil bien ensommeillé.

« Il faut que tu te descouches. On a besoin de toi. »

Il décolla la tête de son oreiller et se tourna vers elle.

« Ça ne peut attendre ? On est au mitan de la nuit ! »

Il réalisa alors que de nombreuses lumières parsemaient la salle et que son épouse avait enfilé une cotte et posé un voile sur sa chevelure et ses épaules. Il battit des paupières plusieurs fois et se força à ouvrir grands les yeux. Ce faisant, il ne put réprimer un bâillement. Son épouse le regarda d'un air désolé et lui souffla tristement :

« Il y a eu male fortune. Un mort. Occis. »

La phrase eut sur lui l'effet d'une douche glacée et, devant l'émotion visible de sa compagne, il s'assit dans son lit, la prit dans ses bras et la serra contre lui sans rien dire. Ce fut une voix issue du couloir vers les cabines qui les ramena dans l'instant présent.

« Il faudrait s'enhâtir. L'autre, là, y va pas tenir tout seul. Il a déjà vidé ses boyaux dans le passage. »

Aubelet s'écarta doucement de sa femme après l'avoir tendrement embrassée sur la joue. Il saisit la cotte qu'il avait posée sur un de leurs coffres et commença à la revêtir par-dessus sa chemise.

« Il est amorté ou malade ? Je ne comprends guère…

— Le marchand, lui, il est finé de mort, pour sûr, percé par poignard. Non, c'est votre pair, le petit gros, il a pas supporté la scène. Y s'emploie à peindre le pont de ses vomissures… »

Le prêtre attrapa ses chaussures qu'il enfila directement, sans prendre le temps de mettre des chausses. Il passa une chape de laine rapidement tout en finissant de se lever. Il rejoignit à la sortie de la salle un des matelots, occupé à se curer les ongles à l'aide de son canif, visiblement peu ému. L’homme sourit poliment au clerc, replia son couteau et lui fit signe de le suivre, décrochant au passage la lanterne qu'il avait suspendue en attendant.

Ils parvinrent vite à l'endroit du couloir où deux marins passaient une pièce d'étoffe au sol après avoir lavé énergiquement. Une odeur âcre restait décelable malgré le savon utilisé. Aubelet se pinça le nez et sauta par-dessus la flaque humide. Il arriva alors devant la cabine d'Ansaldi Embriaco. Il interrogea du regard le matelot, qui confirma d'un signe de tête. Il poussa la porte doucement et découvrit le spectacle désolant, rendu encore plus macabre dans sa mise en scène avec le drap recouvrant le marchand. Aubelet prit une inspiration et allait entrer quand il se reprit.

« Soyez assez aimable de vous enquérir de l'autre clerc. Prévenez-le que je l'attends céans. »

Il passa alors le seuil les yeux mi-clos, refermant avec soin l'huis derrière lui sans regarder ailleurs. Il prit son courage à deux mains et avança d'un pas vers le cadavre recouvert d'un semblant de suaire. Ce n'était pas la première fois qu'il voyait un mort, mais il n'était pas habitué à ce que ce soit une personne si jeune, en pleine santé et sauvagement assassinée. Il souleva la toile, dégageant ce qui n'était guère qu'une forme jusque-là. Alors qu'il finissait de dévoiler le corps, il entendit la porte grincer derrière lui. Il se retourna et vit Herbelot, plus pâle qu'à l'ordinaire malgré les lumières orangées des lampes. Celui-ci lançait un regard horrifié vers le cadavre, la main devant la bouche.

« Grand Dieu ! Vous n'allez pas l'abandonner ainsi découvert, j'espère ! »

Aubelet lui adressa un sourire apaisé et répondit d'une voix calme.

« Il le faut bien, mon frère. Nous ne pouvons nous occuper de lui à travers une toile. Disposons-le en sa couche avant de faire oraisons pour lui. Donnez-moi la main… »

Herbelot s'avança timidement, s'efforçant de faire bonne figure. Son apparence lisse et détachée habituelle avait complètement disparu et il semblait frappé de stupeur. Agissant comme un somnambule, il se décida à prendre le corps par le bras. Le cadavre était étonnamment raide, ce qui en facilita le déplacement pour l'allonger. Le poids fit transpirer le clerc, peu habitué à de tels efforts et contracté par la tension. Il lui semblait qu'une sueur glacée lui coulait lentement dans le dos tandis qu'il s'efforçait de bouger le défunt avec toute l'attention dont il était capable. En quelques manœuvres, ils réussirent à étendre le jeune marchand sur son lit. Aubelet brisa alors le silence.

« Nous devrions peut-être penser à son meurtrier dans nos priements ? Nous dirons complète messe à l'avenir. »

Herbelot retrouva un peu de contenance à la mention d'une pratique familière. Il hocha la tête sans néanmoins desserrer les lèvres, ni interrompre Aubelet.

« Je m'emploierai plus tard à laver le corps. Si vous le souhaitez, je pourrai le faire seul, j'en ai usage. »

Herbelot regarda le prêtre d'un regard reconnaissant.

« En ce cas, je réciterai quelques psaumes pour demander à la Vierge et aux saints d'intercéder pour lui.

— Parfait ! J'ai vu des chandelles à côté de l'écritoire, enflammons-en une à son chevet. »

Joignant le geste à la parole, Aubelet récupéra sur la table un crochet métallique qui permettait d'y fixer une bougie et le planta au dessus de la tête du défunt dont le visage exprimait une souffrance atroce, les traits figés dans un dernier rictus de douleur indicible.

Aubelet sentit une vague de compassion monter en lui à la vue de cet homme, dont il ne savait pas grand-chose. Il éprouvait néanmoins un grand chagrin, créé par le vide installé là où la vie, les espoirs, les ambitions, les sentiments confus et antagonistes s'épanouissaient jusqu'alors. « Tu n'es que poussière et tu retourneras poussière » songea-t-il, heureux de s'en remettre à la puissance divine qu'il avait choisi de servir, soutien et réconfort dans de telles circonstances. Baissant la tête, il rejoignit Herbelot dans la prière, en communion avec ce petit clerc bedonnant dont il ne savait pas grand-chose non plus en fin de compte.

### Canal d'Otrante, matin du 28 septembre

Lorsque Régnier arriva dans le couloir, il ne restait qu'un matelot de faction devant la porte. Il était assis sur le coffre d'Ugolino, adossé à la cloison latérale, occupé à aiguiser son couteau. Le chevalier demanda s'il pouvait voir le *patronus* dont on lui avait dit qu'il était là. Le marin acquiesça silencieusement et désigna la cabine de la pointe de sa lame, se remettant immédiatement au travail, sans émettre d'autre bruit que le glissement régulier de l'acier sur la pierre. Régnier s'annonça et poussa doucement le battant de la main. Aucune réponse ne vint, mais la porte s'écarta sur un Ribaldo agenouillé, occupé à regarder sous le haut lit.

« Vous arrivez à point nommé, messire, j'ai besoin d'aide pour retraire cette caisse de sous la couche, je ne voudrais la tirer sans la soulever et donc risquer d'effacer utiles indications. »

Régnier s'avança et se mit à genoux à son tour pour dégager l'épais meuble de chêne ferré.

« Qu'a-t-elle à voir avec le meurtre ?

— Le valet m'a dit qu'elle celait importants documents et, surtout, moult monnaies. Plusieurs dizaines de livres. Voilà bonne raison de meurtrir son prochain. Le prêtre m'a donné les clefs trouvées à la ceinture de maître Embriaco. »

Ils posèrent la malle sur le lit et, un trousseau à la main, Ribaldo entreprit de faire jouer la serrure, jusqu'à ce qu'il obtienne satisfaction. Le pêne fut relâché avec un bruit sec et il souleva le couvercle avec empressement. En dehors de quelques documents  au fond, la huche était vide. Ribaldo ne put réprimer un sourire qui tenait plus de la grimace.

« Foutrecul ! Ma main au feu ! Nous affrontons un voleur, un simple voleur ! »

Régnier se passa la langue sur les lèvres et hésita à formuler ce qu'il se résolut pourtant à dire.

« Un *simple* voleur ? Le croyez-vous de sûr, maître ? Nous ne savons ni comment il est entré, ni, surtout, comment il est sorti. »

Le commandant de bord le regarda, interloqué. Il n'avait pas envisagé la situation sous cet angle et ne voyait pas quoi répondre. Il bredouilla un peu, décontenancé par cette remarque inattendue. Puis il balaya tout cela de son esprit.

« Plutôt que disputailler questions de détail, je préfère chasser un lièvre que je peux pister. »

Régnier, ne comprenant pas où il voulait en venir, fronça les sourcils.

« C'est-à-dire ?

— Les pièces ont disparu. Leur grand nombre les rend difficiles à cacher. Nous sommes en pleine mer. Nous pouvons donc tout fouiller jusqu'à découvrir notre larron. Lui faire avouer la façon dont il s'y est pris sera aisé. »

Régnier reconnut le bien-fondé de la méthode du marin. Mais il omettait plusieurs points qu'il pouvait être intéressant de garder en tête.

« Si vous me permettez, nous n'avons pas pu savoir si un passager clandestin était monté ou pas à Otrante. Adoncques pensez-vous vraiment que nous saurons découvrir la cachette de quelques bourses bien garnies ?

— Je ne le sais. Cependant, voilà tâche aisée et facile à organiser jusqu'à réussite. Sans compter que ça occupera les hommes qui cogiteront moins. »

Ribaldo hésita un instant, tordant sa bouche de façon nerveuse. Il se gratta le nez et décida de dévoiler le fond de sa pensée au chevalier.

« Vous savez, je n'exclus pas la possibilité que ce soit le meurtrier frappant les notables influents de Gênes, dont vous avez peut-être ouï parler. À ma grand surprise, je l'avoue fort volontiers ! Mais c'est la première fois qu'il y a aussi vol. »

Régnier hochait la tête lentement, finalement convaincu par la démonstration de son interlocuteur. Machinalement, il lissa le couvre-lit froissé par le coffre et remarqua quelques salissures sombres sur l'étoffe, peut-être du sang. Il lança un regard troublé vers Ribaldo, montrant du doigt les auréoles brunes. Déposant rapidement le coffre à terre tous les deux, ils cédèrent à une sorte de frénésie, tirant les linges avec un empressement fiévreux. Ils découvrirent ce qu'ils craignaient : de larges taches coagulées imprégnaient les draps du lit. Figés devant ce spectacle, ils échangèrent des coups d'œil inquiets et stupéfaits. Régnier osa le premier prendre la parole.

« Il a pourtant bien été trouvé à sa table, n'est-ce pas ?

— Oui, allongé sur son écritoire. Dans une étrange posture d'ailleurs, les bras au long du buste, pas vraiment assis ni complètement debout.

— On a dû poser son corps en la couche après l'avoir poignardé. Cela expliquerait le sang, plus rare qu'au sol. »

Ribaldo remuait la tête, visiblement ému par ce qu'il découvrait et entendait. Régnier continuait à suivre le fil de sa pensée, de plus en plus horrifié par ce qu'il avançait.

« Cela voudrait dire qu'on l'a frappé à sa table ou debout ici même où nous nous trouvons… Puis allongé en sa couchette, pour ensuite être placé derechef à son travail. »

Les deux hommes se regardèrent, indécis quant aux conséquences de ce qu'ils venaient de réaliser. Ribaldo souleva son petit bonnet de feutre et se gratta le crâne dans un mouvement de plus en plus frénétique.

« Mais quel intérêt y a-t-il à mouvoir un corps de ci de là ? Ce ne peut être que labeur de fou ou de démon… »

— Ou d'un monstre sans scrupule, pensa Régnier en son for intérieur.

### Canal d'Otrante, après-midi du 28 septembre

Les traits tirés, le valet tentait de boire un peu de vin allongé. Il était assis sur un lit qu'on lui avait ménagé dans un coin de la salle commune, protégé des regards par des pans de toile. Il n'avait rien avalé depuis le matin, visiblement bouleversé par la découverte. De plus, probablement en réaction au choc nerveux, il souffrait d'une affection aux intestins qui ne lui laissait pas de repos, le faisant se tordre régulièrement de douleur. Lui qui n'aimait pas rester sans rien faire devait s'abandonner à d'autres.

Le *patronus* s'annonça de derrière la toile, souhaitant s'entretenir avec lui. Ignorant le garde, de faction pour éviter que des curieux ne viennent écouter, Ribaldo entra dans la petite cabine, chichement illuminée par une modeste lampe posée sur un tabouret aux pieds du malade.

« Désolé de te troubler, mais il te faut m'aider à y voir plus clair. »

Ugolino eut une toux sifflante, grimaçant à chaque soubresaut, en se tenant le ventre. Il remonta ensuite la couverture sur sa poitrine. Le commandant s'assit au bout du lit, en faisant attention de ne pas écraser les jambes du vieil homme.

« Il m'importe fort de vous aider, maître, bien sûr. Si cela peut dévoiler qui a fait ce… cette… cette horreur ! »

Ribaldo posa une main compatissante sur le valet.

« Calme, l'ami. De prime, je dois apprendre si ton employeur avait des ennemis, ou s'il craignait aucun.

— Cela m'étonnerait. C'était un honnête et franc marchand. Il ne parlait pas mal et se consacrait avant tout à ses affaires.

— Il n'aurait pas, même involontairement, attiré sur lui la haine d'un concurrent ou d'un ancien associé ?

— Pas à ma connaissance.

— Et sais-tu si ses familiers auraient pu avoir des ennemis qui s'en seraient pris à lui, pour venger frère ou cousin ? »

Ugolino leva les yeux, cherchant dans sa mémoire quelques secondes, puis fit claquer sa langue avant d'arborer un visage interrogatif.

« Aucune idée. »

Le *patronus* réfléchit un instant, le front barré. Il lui fallait aborder un sujet qui lui déplaisait.

« Sais-tu si maître Embriaco avait des liens avec Jacomo Platealonga, Lafranco Rustico ou Oberto Pedegola ?

— Il faudrait peut-être regarder ses comptes. Je ne me souviens pas qu'il les ait jamais encontrés. Mais je ne connais pas tous les négociants avec lesquels il traite, ou je peux en oublier. Je n'ai pas usage d'espionner…

— Attitude fort honorable ! Mais repense à ces trois noms tranquillement. Cela pourrait être fort important. »

Le *patronus* joignit les mains dans un geste un peu solennel.

« Narre-moi en détail la veillée d'hier, depuis l'ultime fois où tu as vu ton maître vif jusqu'à ce que le jeune François ouvre la porte. »

Le valet renifla avec un sifflement et, cherchant à s'éclaircir la voix, se mit à tousser doucement. Il déglutit bruyamment.

« J'ai porté son souper à maître Embriaco en fin d'après-midi, comme d'habitude. Aux cuisines, les matelots faisaient goûter moult sortes de mets à Yazid. Chemin revenant, je n'ai pas pris garde si quelqu'un se trouvait dans le couloir. Maître Embriaco se reposait et m'a dit de poser les plats sur la table puis de revenir plus tard débarrasser.

— Il t'a semblé troublé, fatigué, inquiet ? »

Le valet écarquilla de grands yeux.

« Difficile de répondre. Je ne me souviens de rien d'étrange. Je fais ça chaque journée, je ne pourrai guère vous en décrire une  précisément. Elles ne sont que répétition des précédentes.

— D'accord, et après ?

— Quand je suis revenu, il passait une cotte propre. Voilà qui m'a surpris, étant donné qu'il ne se changeait usuellement qu'au matin. Mais ça lui arrivait de mettre une nouvelle tenue pour la veillée. J'allais pour l'aider, mais il a refusé et m'a demandé de l'éveiller dès l'aube. Je suis alors sorti, il a verrouillé derrière moi et je suis allé frotter les écuelles que j'ai rendues à Gandulfo. Après avoir fait un petit tour et pris le frais sur le pont, je suis redescendu prendre repos dans le couloir. C'est là que Yazid m'a trouvé peu après. Comme il avait aussi achevé ses tâches, nous avons fait rouler les dés. Jusqu'à l'arrivée du soldat et du jeune François, avec qui nous avons ouvert la porte.

— C'est un bruit qui vous a surpris, c'est bien ça ? »

Ugolino hocha la tête en silence et sembla ne reprendre qu'à contrecœur.

« Yazid et le sergent faisant sa ronde se groignaient. C'est ce soudard qui avait sorti son poignard voilà peu.. ».

Ribaldo écarta d'un geste cet élément qu'il jugeait sans intérêt.

« C'était quelle sorte de bruit ?

— Comme gémissement mêlé de grognement, mais très faible, suivi d'un son comme deux pièces de bois qui se heurtent. C'était peut-être la charpente du navire, je ne suis plus vraiment sûr. Mais il m'a bien semblé alors que ça sortait de la chambre du maître… »

Ugolino toussa et émit quelques pets, l'air misérable. Ribaldo se releva péniblement, appuyant la main sur ses genoux pour s'aider. Il fit craquer son dos en grimaçant. S'avançant vers le vieux valet, il lui donna quelques tapes amicales sur l'épaule. Puis le *patronus* souleva le pan de toile et sortit de la petite loge. Il s'arrêta un instant, repassant dans sa tête différentes hypothèses. Ce faisant, il regardait sans vraiment les voir les poussières voletant dans les rais de lumière tombant par l'ouverture de la trappe qui menait au pont supérieur. Il fallait qu'il aille donner quelques ordres à Gandulfo, pour que le valet mange des mêmes plats que lui. La subite maladie du serviteur lui paraissait suspecte. Il comptait bien faire en sorte qu'il n'y ait pas un mort par poison en plus d'un poignardé à son bord.

### Côte ionienne, après-midi du 29 septembre

Le petit groupe de pèlerins s'était pelotonné, assis à même le plancher auprès de son pasteur, Aubelet. La mer était grosse et un vent bon plein soufflant par rafales rendait le travail des marins difficile sous les averses de pluie. La pénombre régnait au niveau du pont des passagers car il avait été demandé de réduire le nombre des éclairages pour éviter les incendies. Ballottés en tous sens par la frénésie des vagues qui  secouaient le *Falconus*, les voyageurs s'agglutinaient, inquiets, autour de leur prêtre. La lumière de la lampe accrochée derrière sa tête accentuait l'ambiance surréaliste de la scène, lui faisant une sorte d'auréole. La violence des heurts malmenant le navire l'obligeait à se tenir d'une main à une des poutres, apportant un peu de matérialité à sa présence.

Aubelet avait profité des événements récents pour inciter les pèlerins à réfléchir à leur sort après la prière de l'après-midi. Plusieurs d'entre eux avaient émis des remarques agressives vis-à-vis du marchand et de sa morgue les jours précédents. Certains l'avaient envié, admirant ses belles tenues et son train de vie confortable. Et Dieu l'avait rappelé à lui sans crier gare, sans qu'il n'ait le temps de s'y préparer, ni de mettre de l'ordre dans ses affaires. Tout cela était propice à l'introspection, selon le clerc.

« Mémorez-vous que vous aussi, quelque splendide soit votre atour, vous devrez genouiller devant plus superbe que vous. Quelle que soit votre opulence, vous serez dépouillés de vos fripes devant Celui qui détient la seule vraie richesse. »

Gringoire, croyant que ces remarques s'adressaient à lui, le plus fortuné du groupe, fit une grimace et pencha son buste en avant, manquant de tomber avec le mouvement du bateau. Son ton était un peu agressif lorsqu'il prit la parole.

« Adoncques il ne faut rien faire ? Se débarrasser de tout et espérer du Seigneur qu'Il nous comble de ses bienfaits ?

— Ai-je jamais dit ça ? Rappelez-vous Job. A-t-il douté de Dieu ? Riche ou humble, s'en remettre à Dieu et se reconnaître misérable pécheur est l'unique choix sensé. Faire charité comme vous, Gringoire, rapproche de Dieu, mais chaque jour se pose derechef la question de son Salut. L'exemple de ce marchand prouve que le moment du départ n'est jamais prévisible. Le Malin et ses démons sont toujours à guetter notre âme. »

Une des femmes hésita un instant, effrayée par les perspectives de cette dernière remarque.

« Vous voulez dire que c'est le prince ténébreux qui a meurtri le Génois ? Ce n'est pas Dieu qui a rappelé à lui sa créature ?

— Vu les faits, il me semble patent que l'esprit du mal y a eu quelque part… »

Un des hommes explosa alors.

« C'est pour ça que l'huis était verrouillé. Le maudit est venu le prendre et a disparu en un nuage soufré ! »

Un autre opina et surenchérit.

« Je serais pas surpris que sa richesse soit issue d'un pacte. Et le maître a rappelé son valet lorsque l'accord arrivait à terme. »

Plusieurs des pèlerins se signèrent devant toutes ces imprécations.

Aubelet tenta de ramener le calme de la main, secouant la tête d'un air désolé.

« Amis, quand je parle du mal, je pense à celui lové en nos cœurs. Celui qui nous incite à nous abandonner au péché. Ne croyez pas qu'il va jaillir comme étincelle, devant vous. Il est fort plus malicieux, félon et fourbe… »

Il fut interrompu par un brusque mouvement de roulis, qu'accompagna un sinistre grincement répercuté dans toute la structure et, déséquilibré, il manqua de tomber. Cherchant à recouvrer son aplomb, il assura sa prise sur la pièce de bois. Déconcentré par l'incident, il eut besoin de quelques instants pour retrouver le fil de ses idées. Profitant de ce silence, ses fidèles continuaient sur leur lancée.

« Au final, ce n'est que justice que marchands soient frappés. Ce ne sont certes pas bons chrétiens, je l'ai toujours su.

— D'ailleurs, le fameux invité mystérieux sur le bateau, il n'est guère besoin de se fossoyer la tête bien longtemps pour trouver qui c'est, moi je dis… »

Le dernier interlocuteur se signa, inclinant du chef cérémonieusement pour assurer l'auditoire de son absolue certitude sur le sujet. Les quelques pèlerins à ses côtés l'imitèrent, inquiets à l'évocation de la possibilité d'une présence démoniaque parmi eux.

Aubelet comprit que les esprits risquaient de s'échauffer et qu'il devait ramener un peu de calme. Il était toujours désolé que ses paroissiens ne saisissent pas bien ses sermons. Lui était persuadé que le diable présent à bord se trouvait au sein de chacun d'entre eux. Peut-être était-il plus actif chez un des passagers et l'avait-il poussé à commettre ce meurtre. Mais blâmer la défaillance de l'autre n'était pas la meilleure voie pour le Salut. Seule la prière pouvait les aider. C'était cela qu'il s'efforçait de leur faire comprendre, sans grand résultat.

« Calmez-vous. Ne prêtez foi à de telles sornettes, cela n'avance à rien. »

Prenant le risque de lâcher la main, il écarta un peu les jambes pour s'assurer de son maintien. Puis se mit en position d'orant.

« Prions plutôt pour notre Salut et proclamons notre foi envers le seul capable de nous sauver ! Reprenez avec moi : *Pater noster, qui es in caelis…* »

La foule reprit en chœur, avec plus de conviction que de compréhension.

« *Paternes austères, couillaisses in chez lys*… »

À l'autre extrémité de la pièce, Herbelot avait assisté en silence à toute la scène depuis son lit. Il avait d'abord souri devant les erreurs fréquentes de latin faites par le prêtre, puis s'était scandalisé en son for intérieur de ses raccourcis théologiques audacieux ou frisant parfois l'hérésie, sans qu'il en ait même conscience. Mais il avait réalisé peu à peu que son confrère avait une lourde tâche à mener. Il devait sans cesse ranimer la Foi par instants vacillante, souvent désorientée, d'un groupe complètement inculte.

Bien sûr, en tant que jeune clerc issu des meilleures écoles parisiennes, Herbelot était à l'aise dans le maniement des notions les plus avancées, mais il n'était pas sûr que trouver les mots simples, aptes à toucher le cœur des gens ordinaires, de peu d'esprit, fût chose facile. Lui n'avait pas été formé à cela. Le prêtre de paroisse ne l'avait été guère plus, sans compter que sa formation pêchait manifestement en plusieurs points. En outre, il était lui-même en état de péché permanent avec cette femme à ses côtés. Et pourtant, il s'employait à ramener sans cesse dans le giron de l'Église les brebis confiées à sa garde, sans chercher à en retirer une vaine gloire. Herbelot comprit alors qu'il avait péché par orgueil vis-à-vis d'Aubelet et qu'il ne lui appartenait pas de juger ainsi si sévèrement son prochain. Tout en écoutant la prière familière s'égrener à ses oreilles, il rejoignit les fidèles peu à peu dans leur profession de foi, mêlant sa voix à la leur depuis le recoin où il s'était pelotonné.

### Côte de Corfou, 30 septembre

Installé avec les passagers les plus importants sur la terrasse intermédiaire du château arrière, Régnier suivait la cérémonie funèbre de façon assez distraite. Il ne pouvait s'empêcher d'admirer la vue offerte à ses yeux. Le temps était revenu au beau et, malgré quelques nuages d'altitude, le ciel était splendide. Un vent léger venant du nord, suivant les côtes, apportait des senteurs étonnamment printanières. Le rivage qu'il apercevait au loin était majoritairement constitué de montagnes de verdure plongeant directement dans la mer. Mais le reflet blanc d'une magnifique plage, encadrant d'un trait clair une lagune ou un lac laissait deviner un endroit merveilleux.

Il aurait aimé pouvoir s'arrêter et peut-être offrir là une vraie sépulture chrétienne au marchand qu'ils s'apprêtaient à jeter par-dessus bord. Les voiles avaient été amenées, le temps pour eux de rendre un dernier hommage à un homme passé comme une ombre dans leur vie à tous. Assassiné sauvagement au large des côtes, il ne connaîtrait pas le repos dans un cimetière consacré, mais attendrait la Résurrection dans les profondeurs marines.

Régnier réalisa qu'il n'avait pas vraiment lié connaissance avec Ansaldi Embriaco et pourtant ce qu'il en avait aperçu était suffisant pour qu'il regrettât une si courte échéance. Il avait été touché par la fougue du jeune homme lorsque ce dernier parlait de la Terre sainte et de tous les projets qu'il avait en tête. Tous ces rêves et ces espoirs étaient désormais rassemblés sous une rude étoffe de lin, en surplomb de la coque, prêts à disparaître avec le corps qui les avait créés, chéris, entretenus toutes ces années. Retournés au néant et à la poussière d'où ils étaient issus.

Chassant d'un geste imaginaire ce souffle mélancolique, Régnier se prit à espérer que le criminel serait châtié comme il se devait. Il n'était pas sûr que Ribaldo Malfigliastro saurait trouver l'assassin, occupé qu'il était déjà par la direction du navire. Il s'était proposé, mais pour l'instant, on ne lui avait rien demandé de façon claire. Cela ne lui interdisait néanmoins pas de se faire sa propre idée, de recueillir ses impressions personnelles. Le coupable allait certainement se dévoiler d'une façon ou d'une autre, passager clandestin trahissant sa présence ou voyageur laissant échapper la preuve accusatrice. Et cela pouvait tout à fait commencer par une attitude qui le dénoncerait le jour de la cérémonie à la mémoire de sa victime.

Son regard errant parmi l'assistance, il remarqua qu'il n'était pas le seul à n'être pas très assidu. Le plus évident à voir, le jeune Ernaut, semblait trépigner, sautant d'un pied sur l'autre. Il n'était pas très discret vu la masse qu'il bougeait à chaque fois, mais ne correspondait pas à ce que Régnier s'imaginait du tueur. Ayant pris conscience qu'on le regardait, l'adolescent s'appliqua à prendre une attitude aussi respectueuse et recueillie qu'il lui était possible, avec un succès tout relatif, arrachant un sourire bien peu de circonstance au chevalier. Il se rendit compte qu'il appréciait le jeune homme, malgré sa maladresse et sa curiosité maladive. Il était toujours le premier à rendre service, à répondre à une demande d'aide. D'un autre côté, il était aussi toujours le premier partout, le nez en l'air et les yeux fureteurs. Mais lors de ses entraînements avec le soldat, il s'était révélé pugnace et combatif, ne se reposant pas sur sa force seule. Le chevalier avait toujours pensé qu'on se révélait dans l'affrontement et ce qu'il devinait du jeune Ernaut lui plaisait. Un jeune homme encore mal dégrossi mais prometteur.

Au premier rang du demi-cercle formé autour de l'officiant et du corps se trouvait Herbelot Gonteux. Régnier était un peu surpris de le voir là, aussi concentré dans la célébration, et s'effaçant devant un autre prêtre qu'il critiquait avec fréquence, parfois assez durement. Il avait pensé que ce serait le jeune clerc parisien qui allait s'occuper de la cérémonie, mais le *patronus* avait dit à Régnier qu'il avait conseillé de confier cette tâche à Aubelet. Le chevalier avait été choqué par ce qu'il avait pris pour de la vanité, ce jeune lettré ne souhaitant apparemment pas s'abaisser à faire un office pour un simple marchand, fut-il la victime d'un sauvage assassinat. Mais ce qu'il voyait là démentait sa première impression. Herbelot participait de son être à la célébration, aidant et motivant les répons de sa voix claire et juste et s'assurant que les paroles en étaient distinctes. Plus familier des cloîtres et des salles capitulaires que des scènes de combat et de meurtre, peut-être avait-il été plus bouleversé par le crime qu'il ne l'avouait.

Continuant son tour d'horizon, Régnier fut attiré par Octobono et Fulco Bota, qui discutaient à voix basse, peu concernés par ce qui se passait. Ils participaient de la main sinon du verbe aux répons et aux gestes, mais n'interrompaient pas pour autant leur conversation. Un peu plus loin, parmi les hommes qui s'étaient mis le plus en retrait, Régnier remarqua le soldat avec lequel Ernaut s'était battu. Il était négligemment appuyé contre le mât et semblait amuser les matelots autour de lui par ses remarques, au grand dam d'un des marins qui essayait de se recueillir et tentait de les ramener à un peu plus de respect.

Revenant sur la plate-forme où les passagers les plus importants se trouvaient, il n'aperçut que le crâne de Mauro Spinola, à l'attitude sereine et mesurée, comme on pouvait l'attendre d'un politicien tel que lui. Il était d'ailleurs au premier rang, à côté du *patronus*. Alors qu'il tournait la tête, Régnier croisa le regard de Ja'fa al-Akkar, occupé lui aussi à un petit tour d'horizon. Surpris et perturbé d'avoir été pris en flagrant délit, le chevalier le salua discrètement d'un signe de tête puis tenta de reprendre une posture plus digne des obsèques. Il prit conscience que d'autres risquaient de l'imiter et de se rendre compte qu'il n'était pas vraiment en communion avec le groupe ou qu'il ne se sentait pas concerné par cette mort. Et une telle attitude compromettrait sa réputation, voire attirerait les soupçons sur son innocence, Régnier le savait d'expérience. Certaines rumeurs grossissaient d'elles-mêmes, pour n'être au final que la seule hypothèse visible, retenue et proclamée par tous, oblitérant toutes les autres et finissant par remplacer la vérité.

Il réalisa que les matelots s'étaient avancés pour faire glisser le corps du marchand depuis la planche dans les ondes aux reflets d'azur. Après une ultime bénédiction d'Aubelet, le plongeon retentit à leurs oreilles, assorti d'une colonne d'embruns éclaboussant la coque. C'était ainsi que disparaissait Ansaldi Embriaco, jeune marchand génois plein de promesses, certainement fierté de sa famille. Ce n'était pas la première fois pour Régnier que le Seigneur rappelait à lui une de ses créatures, loin de là. Il ne comptait plus le nombre de personnes qu'il avait accompagnées à leur dernière demeure ou tout simplement vues mourir, de maladie ou lors des combats. Mais c'était une des premières où il ressentait une injustice si criante. Il ne s'agissait pas d'un affrontement l'épée au poing, ce n'était pas une fièvre ni encore moins la vieillesse qui était en cause. Ce n'était pas la main de Dieu la responsable, mais le poignard d'un assassin, qui n'avait laissé aucune chance à sa victime, attaquant de dos de façon lâche. Il se fit la promesse de ne pas permettre qu'une telle ignominie demeure impunie.

### Îles ioniennes, midi du 1er octobre

La chaleur donnait des suées au *patronus*, assis un peu en retrait sous l'avancée du plancher sommital du gaillard d'arrière. Il s'essuyait régulièrement le front avec un mouchoir qu'il avait fini par garder de façon permanente en main. Heureusement, le vent soufflait bien et il pouvait rester le visage à l'ombre sans avoir à se démener à lancer des ordres. Il avait envoyé chercher à boire une nouvelle fois, le vin lui glissant dans le gosier comme l'eau sur un sol desséché. Il était de plus assez ennuyé, car la fouille du navire ne donnait rien. Aucune bourse de pièces n'avait été retrouvée.

Il avait demandé aux différents marchands présents à bord de vérifier avec l'aide de l'écrivain que leurs ballots étaient intacts, les sceaux toujours en place, sans plus de succès. Mais il n'était qu'à moitié convaincu par cette vérification, estimant qu'il était irréaliste de penser que l'argent y aurait été dissimulé. Les agents des douanes de Gibelet auraient eu tôt fait de les remarquer lors des contrôles.

Alors qu'il s'éventait un peu avec son couvre-chef, il vit arriver Fulco Bota. Comme tous les matins, celui-ci lui faisait désormais un rapport sur l'état du navire. Il n'avait pour l'instant heureusement noté aucun sabotage. Il accepta volontiers la proposition du commandant de prendre un verre avec lui et ils attendirent dans le silence le retour du marin avec la cruche. Ce fut Ganelon qui l'apporta, suivi de peu par Régnier. Celui-ci, venant certainement des ponts inférieurs plus frais, arborait une mine réjouie, encore peu éprouvé par la chaleur. Après les saluts d'usage, il s'expliqua sur sa présence.

« Je me suis permis de m'inviter, avec de quoi se rafraîchir tout en palabrant. Je voulais savoir si vous aviez avancé dans votre enquête. La fouille a-t-elle donné attrayants résultats ? »

Il s'assit sur un tabouret que Fulco lui fit passer. Ganelon s'employa alors à remplir les godets. Le simple bruit du vin frais versé rafraîchissait déjà les assoiffés. Tout en tendant son récipient, Ribaldo pinça les lèvres, la mine contrariée.

« De présent, rien de neuf. Pas de pièce, pas de passager clandestin. Rien ! »

Fulco inclina la tête, grimaçant pour marquer son désaccord.

« Avec votre accord, maître Malfigliastro, nous avons tout de même pu vérifier une possibilité : il y peu de chance que quelqu'un soit monté à bord lors de la relâche à Otrante. »

Régnier avala rapidement la gorgée qu'il venait de prendre.

« Comment pouvez-vous l'assurer ?

— Aucune trace, à nul endroit. Pas de vivres qui disparaissent, pas de seau d'aisance dissimulé… À moins qu'il n'ait complices à bord, je ne vois comment un homme pourrait rester tapi ainsi plusieurs jours durant, presque semaine entière. »

Ribaldo remua la tête pour acquiescer.

« Nous croyons donc que le larron est redescendu, s'il est jamais monté à bord. »

Le chevalier plissa les yeux et regarda malicieusement le *patronus*.

« Cela peut aussi indiquer qu'il n'a jamais quitté le *Falconus* ! »

Ribaldo opina de la tête, d'un air las, presque désolé.

« Certes oui. D'où la fouille des bagages de tous les passagers. Mais sans résultat à ce point. »

Fulco finit d'avaler la dernière goutte de vin et reposa son gobelet au sol, sous son tabouret.

« Certains croient qu'il faudrait vérifier du côté de ses contacts de négoce, ou de ses concurrents. »

Ribaldo fronça les sourcils et avança le menton, relevant les poils de sa barbe.

« Que voulez-vous dire ?

— Que, comme moult marchands, il a pu avoir ennemis par les affaires qu'il faisait. Ce n'est secret pour personne que certaines denrées voyagent alors qu'elles sont interdites. Livrées en des lieux défendus… »

Le *patronus* redressa le buste, l'air apparemment contrarié.

« Cela m'ébahirait fort de maître Embriaco qu'il ait jamais trafiqué dans semblable malhonnêteté. Jamais il n'a laissé échapper la moindre allusion à cela devant moi, et toutes ses balles à mon bord ne contiennent qu'honnêtes marchandies, selon les saints préceptes de l'Église. »

Fulco opina de la tête, mais n'avait pas l'air convaincu. Régnier renchérit.

« Je serais de même fort surpris qu'il ait fourni des armes aux infidèles. Car c'est bien de cela que nous devisons, non ? Certes, il avait grandes ambitions, mais manifestait un réel désir de participer à la Guerre sainte. »

Fulco le regarda un peu de travers, l'air mauvais.

« Voilà beau discours par-devant de moult trafiquants. »

Un regard courroucé de Ribaldo l'empêcha de développer son idée. Il accusa le coup puis, se levant brusquement, les salua d'un rapide signe de tête, prétextant quelques tâches urgentes à surveiller. Sans demander son reste, il dévala l'escalier de descente. Régnier demeura quelques minutes silencieux, à regarder le fond de son gobelet dans lequel il faisait tourner le vin. Il finit par l'avaler d'une lampée. Puis il fixa le commandant droit dans les yeux et adopta d'une voix faible.

« Il parle de raison. Vous ne pouvez omettre le fait que nombre de négociants font du trafic. Cela me répugne de l'avouer, mais il est possible que ce meurtre ait servi l'intérêt des royaumes latins de Terre sainte. »

Ribaldo Malfigliastro ne répondit pas, regardant le sol le visage fermé. Il releva la tête et fixa le chevalier d'un air où perçaient l'inquiétude et la colère mêlées, la mâchoire contractée. Il n'ajouta pas un mot quand Régnier le salua pour prendre congé.

### Îles ioniennes, soir du 2 octobre

Yazid avait réussi à convaincre Ugolino de se joindre à la veillée des marins sur le pont supérieur. Profitant du peu de lumière qui restait avant que le soleil ne se cache tout à fait, ils s'étaient regroupés pour quelques festivités comme habituellement. Le vieux valet se sentait beaucoup mieux depuis quelques jours et il s'estimait d'attaque pour une soirée un peu animée. La mer était belle, et si le vent n'était guère puissant, il avait permis d'avancer sans avoir à trop se fatiguer. Les hommes étaient donc de joyeuse humeur, volubiles et pressés de prendre du bon temps.

Un des matelots les plus âgés venait de leur raconter une histoire qu'il tenait lui-même de marins byzantins. Cela parlait d'un formidable navigateur écumeur de ces mers à l'époque de la légendaire Troie. Pourchassé par des démons, il ne rentra pas avant plusieurs années chez lui, obligé de recourir à des trésors de ruse et d'imagination pour se sortir de situations plus périlleuses les unes que les autres. Sa demeure se trouvait d'ailleurs au sein des îles qu'ils abordaient, ce chapelet de terres qu'ils allaient parcourir jusqu'à leur prochaine escale, Rhodes.

Le jeune esclave de Sawirus comptait parmi les plus bruyants et les plus enjoués. Ayant eu le temps de se familiariser avec les chansons, il hurlait à plein gosier avec ses compagnons, le cou tendu comme un loup une nuit de pleine lune. Il avait du reste rapidement acquis une réputation de joyeux drille, surnommé Franc-Godet par les marins, en raison de sa vivacité à faire son affaire à tout récipient rempli de vin qu'on lui présentait. Il trouvait dans l'alcool l'oubli de sa condition à défaut du réconfort qu'il recherchait. Mais grâce à Ugolino, qui le traitait presque comme un fils, c'était la première fois qu'il avait l'impression d'appartenir à un groupe et qu'il n'était pas rejeté parce qu'il était un esclave païen. Il ne savait même plus ce que cela signifiait d'être musulman, depuis le temps qu'il voyageait et vivait en territoire chrétien. En tout cas, l'abstinence des boissons alcoolisées ne faisait pas partie des préceptes auxquels il était toujours fidèle.

Les hommes réclamèrent à cor et à cri qu'il leur refasse le spectacle dont il les avait honorés la veille du départ d'Otrante. Ce soir-là, il avait tellement bu qu'il s'était lancé dans une variante toute personnelle, chantée et mimée, du récit de l'amulette. Il l'avait entendu raconté quelques fois par des membres de sa famille et de son village alors qu'il était tout jeune, avant qu'il ne soit capturé. Il en avait fait une version beaucoup plus ambitieuse, dans laquelle le propriétaire du bijou ne vendait plus contre un dinar la protection contre les éléments, mais demandait à chacun de lui rendre un service ou de lui exaucer un vœu.

Devant l'insistance de ses camarades, Yazid se leva de façon théâtrale et salua son public. Puis, bondissant sur un tonneau posé à côté du groupe, il commença à déclamer ce qui aurait pu être de la poésie s'il ne prenait pas tant de liberté avec les rimes. Mais il y ajoutait des gestes si bien trouvés et exécutés avec une telle conviction que les spectateurs étaient sous le charme. D'ailleurs, l'interprétation de ce soir-là était encore plus animée que la fois précédente. Yazid s'enhardissait dans les acrobaties et les inventions verbales : il grimpait après les haubans comme un singe, caracolait dans les cordages, s'élançait par-dessus les coffres de matériel, se dissimulait dans le canot. Les hommes riaient de bon cœur et encourageaient même le garçon à accomplir divers exploits, aussi inutiles que dangereux, étant donné l'alcool qu'il avalait à grands traits à chaque fois qu'il passait auprès d'eux. Mais le résultat était là : aucun des matelots présents n'avait vu de spectacle aussi drôle depuis des années.

Sawirus de Ramathes et Régnier assistaient à toute la scène, confortablement assis un peu en retrait sur le gaillard d'avant. Un besoin naturel les avait menés l'un après l'autre aux latrines, provoquant une envie ultérieure d'aller admirer le ciel nocturne depuis la passerelle supérieure. Régnier s'était installé en premier, dangereusement allongé sur le plat-bord de la balustrade de proue. La voix amicale l'avait tiré de ses rêveries. Mais ils avaient vite arrêté de discuter : intrigués par l'agitation en contrebas, ils avaient été attirés vers le pont principal. Et là, ils avaient ri de bon cœur devant les pitreries du jeune homme qui leur révélait des aptitudes qu'ils n'auraient pas soupçonnées. Alors que Yazid s'était finalement assis pour trinquer à sa santé avec ses compagnons de boisson, Régnier se tourna vers Sawirus qui, accoudé au comptoir, continuait à regarder le garçon, l'air ravi.

« Eh bien, maître Sawirus, vous nous aviez celé que vous aviez si talenté valet ! Il devrait faire jongleur[^jongleur] ! »

Le vieil homme avait encore les yeux humides des larmes de rire suite au spectacle. Il s'efforçait de les essuyer avec un coin de manche.

« En fait, j'ignorais jusqu'à ce soir que la bouffonnerie lui seyait si bien.

— Bien étrange que cela ! Jamais aucun doute ?

— Non  pas. Il est d'usage très modeste, toujours un peu en retrait. On dirait qu'il a découvert là une vraie voie… »

Le vieux marchand continuait à pouffer silencieusement, tressautant régulièrement de rire en repensant aux facéties de son domestique. Régnier enchaîna.

« Avec pareil don, il pourrait sans problème trouver un protecteur qui l'habillerait et le nourrirait.

— Vous avez raison, il me faudra penser à cela, bien qu'il ne s'agisse guère de ce que j'envisageais jusqu'alors. »

Régnier était étonné devant l'indulgence dont faisait preuve le marchand pour son valet. Il l'étudia attentivement tandis que Sawirus continuait à s'essuyer les yeux et à ressasser les plaisanteries. Puis Régnier leva les yeux au ciel et sourit pour lui-même. Il était habitué depuis trop longtemps à se méfier des gens et ne pouvait s'empêcher de chercher une raison cynique à tout cela : un esclave acrobate valait certainement plus cher qu'un simple serviteur. Mais il essaya de chasser cette pensée peu amène pour le vieil homme qui aurait été alors un monstre de duplicité, vu la franche réaction qu'il semblait manifester.

Le chevalier reporta son attention sur le groupe de fêtards, et vit, moins agité que les autres, le profil éclairé par une des lampes à huile, Ugolino, un peu perdu au milieu de cette agitation. Il se joignait parfois aux manifestations bruyantes, mais son intérêt retombait quasi immédiatement. Le sourire de circonstance qu'il affichait à chaque fois qu'on s'adressait à lui ou qu'il trinquait ne restait jamais très longtemps sur son visage. Régnier se demanda à quoi le vieux valet pouvait bien penser. Alors qu'il n'avait pour seul but que de servir au mieux, il se trouvait désormais sans protecteur, laissé à lui-même. Et comme il s'agissait du voyage aller d'Ansaldi Embriaco, il devait se sentir abandonné, condamné à se perdre à l'autre bout du monde. Il fallait aborder la question avec le *patronus*, afin que le pauvre homme ne finisse pas avec les indigents. Au pire, Régnier pourrait le prendre à son service quelque temps. Ganelon ne verrait certainement pas d'un mauvais œil d'avoir un peu d'aide. Et cela ferait toujours plus sérieux d'avoir plusieurs domestiques dans son hôtel.
