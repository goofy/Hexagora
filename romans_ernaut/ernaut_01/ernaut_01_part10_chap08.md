## Chapitre 8

### Côte sud du Péloponnèse, matin du 8 octobre

Très calme malgré les conditions effroyables, Régnier sentait son ventre crier famine depuis un moment. Dehors, la tempête grondait et le navire secouait ses passagers comme jamais auparavant. Une nouvelle fois, il avait fallu éteindre les lampes pour éviter le feu et chacun s'était pelotonné dans un coin, priant dans le noir et le vacarme des flots en furie pour que la mer se calme enfin. De nombreux voyageurs étaient malades et certains se soulageaient dans la salle commune, où ils le pouvaient, ne se risquant pas à sortir pour aller jusqu'aux latrines du gaillard d'avant. Les rares qui s'y étaient aventurés en étaient revenus trempés. Un des pèlerins fit sensation en décidant de s'y rendre presque nu, simplement enroulé dans un morceau d'étoffe plutôt ridicule, afin de ne pas avoir ses habits pleins d'eau pour le reste de la journée.

La coque craquait de partout et des embruns se répandaient depuis l'ouverture de l'escalier, derniers vestiges des lames retombées avec force sur le pont supérieur. Chaque fente, chacune des plus petites craquelures offrait un passage aux flots écumants. De temps à autre, des voix humaines leur parvenaient d'en haut, cris des marins tentant de maintenir leur navire à flot dans la mer déchaînée. Chaque nouveau tremblement de la structure, accompagnée de grondements terribles, entraînait des cris apeurés des voyageurs tremblants et paniqués. Plusieurs se concentraient pour réciter des prières et des psaumes, gages de leur Foi en ces moments difficiles. D'autres, moins courageux, se contentaient de pleurer plus ou moins bruyamment. Partout, ce n'était qu'angoisse, peur et renoncement.

La faible lumière de la trappe permettait au chevalier de voir son valet agrippé à l'un de leurs plus gros coffres comme à un radeau. Il l'avait entendu quelques minutes plus tôt prier la Vierge pour qu'elle intercède en leur faveur. Certainement seul dans ce cas, Régnier n'était pas vraiment inquiet. Bien sûr, il n'appréciait pas de se cogner contre ses bagages ni d'être tellement secoué qu'il ne pouvait pas se mettre debout. Mais il était d'une nature confiante et avait surmonté trop d'épreuves pour en craindre une nouvelle, sur laquelle il n'avait pas vraiment prise. Il s'en remettait à Dieu, innocemment, presque avec indifférence. Et d'ailleurs, en la circonstance, il était plus ennuyé par la faim qui le gagnait que par la peur d'être noyé.

Une silhouette obscurcit temporairement le passage et un marin de petite taille s'avança, dégoulinant d'eau et éclaboussant à chaque enjambée le chemin qu'il se frayait parmi les voyageurs. Il cheminait en titubant malgré son habitude de la mer, se retenant à tout ce que sa main rencontrait. Voyant que Régnier le regardait, il cria au passage, l'air goguenard.

« Petite pluie de coucou[^pluiecoucou], sans plus ! »

Le chevalier saisit l'occasion et hurla à son tour pour couvrir le vacarme des éléments déchaînés.

« On va être tempêtés encore long temps ? »

L'homme s'arrêta, les jambes écartées pour se tenir debout. Son buste compensait naturellement les mouvements du navire.

« Aucune idée. Ça branle sec là-dehors ! On a déjà deux gars blessés, Dieu merci sans grand mal !

— La nef va-t-elle tenir ?

— On y besogne ! Désolé, je ne peux demeurer, il me faut inspecter le niveau de l'eau en fond de cales. »

Régnier le remercia de la main, mais le matelot était déjà parti. Il se retrouvait de nouveau seul avec son ventre gargouillant. Il se demandait s'il serait bien raisonnable de se lever et d'aller fouiller dans les vivres qu'il avait achetés en risquant de se blesser pour quelques vieux biscuits rassis ou des raisins secs. La majeure partie de sa nourriture était stockée avec celle de l'équipage car il avait payé sa traversée de façon à bénéficier de repas chauds, préparés par le cuisinier de bord. Il en était là de ses réflexions lorsqu'il prit conscience qu'une silhouette était plantée à l'abord de la zone habituellement délimitée pour lui par des toiles, sauf en cas de tempête, précisément comme aujourd'hui. Il écarquilla les yeux et finit par discerner les traits du gros marchand qui accompagnait le principal groupe de pèlerins français, Gringoire la Breite. Ce dernier vit que Régnier l'avait remarqué et, se penchant pour lui parler, il prit appui sur un des coffres.

« Pourrais-je vous entretenir quelques instants ?

— Vous trouvez le moment choisi ? Je ne sais si vous l'avez noté, mais dehors règne le chaos !

— C'est justement ce qui me pousse à vous faire confidence. »

Intrigué, Régnier inclina la tête, signifiant par là à Gringoire qu'il pouvait s'asseoir à ses côtés, de façon à ne pas avoir à hurler pour se comprendre. Brinquebalé en tous sens alors qu'il pliait les jambes, le Français se laissa tomber au sol, grimaçant sous le choc. Il se passa la main sur le visage, les traits contractés par l'anxiété.

« Il me faut absolument décharger mon âme. Je me suis confessé, mais Aubelet m'a dit que cela ne saurait suffire. Si d'aventure on devait sombrer, je ne veux conserver tel poids… Je ne saurais l'affirmer, mais il se peut que j’aie pris quelques parts à la mort du Génois. »

De surprise, Régnier se recula et regarda le visage effaré du marchand, les yeux exorbités et pleins d'angoisse. L'air interrogateur, il se pencha de nouveau en avant, impatient d'entendre la suite.

« Peut-être l'avez-vous noté : je ne le portais guère en mon cœur, ce bachelier hautain et empli de morgue. Il toisait chacun, se prenant pour le roi de France. Adoncques j'ai entrepris de lui donner sévère leçon. J'ai bonne acointance avec le cuisinier, Gandulfo. Il sert aussi comme chirurgien, et connaît quelque peu les simples[^simples]. Alors, on s'est accordé pour mêler à sa nourriture… »

Il remua sur place, comme si quelque chose le démangeait. Il se grattait le nez, lançant des regards de droite et de gauche, ce qui exaspéra assez vite le chevalier.

« Vous avez versé quoi ? Expliquez-vous, par le sang du Christ !

— De la bourdaine !

— Quoi donc ?

— De la bourdaine. On l'use lorsque l'on ne va guère à la selle d'un moment. Ça purge les entrailles. Efficace, mais un peu douloureux. On espérait qu'un moment aux latrines, le ventre en débâcle, ça lui rappellerait qu'il n'est qu'un homme. »

Régnier n'en croyait pas ses oreilles. Il réprima un sourire devant la bêtise et la naïveté de son interlocuteur.

« Vous êtes en train d'avouer que vous avez provoqué des coliques à maître Embriaco ?

— Même pas, il semble que notre plan a failli. Du moins, on n'en a rien connu, il ne sortait jamais de sa cabine. On avait pourtant ajouté force mesures à la fin. »

À sa voix, il paraissait presque déçu. Régnier joignit les doigts de ses deux mains, prenant un air très docte.

« Ansaldi Embriaco a été poignardé, il n'a pas été occis par poison. Votre rigolage d'enfançon, quand bien même je le trouve bien sot pour un homme de votre rang, n'a absolument rien à voir avec tout ça. Retrouvez votre place et grâcez Dieu que j'ai plus graves soucis en tête. Vous auriez mérité sévère leçon pour pareilles stupidités ! »

Le marchand était visiblement contrit, l'air malheureux comme un chat mouillé. Il se releva avec lenteur et repartit d'un pas mal assuré, se retournant de temps à autre, le regard abattu. Quel imbécile, se dit Régnier. Faire de pareilles blagues à son âge ! Il n'avait pas de qualificatif assez fort pour exprimer ce qu'il ressentait. Il faudrait en toucher deux mots au *patronus* afin que le cuistot soit sévèrement puni. Son rôle était particulièrement important et qu'il ne se soit pas montré digne de la confiance qu'on avait en lui était extrêmement grave. Par ailleurs, le chevalier était effaré de voir avec quelle vitesse Ansaldi avait réussi à se faire deux ennemis à bord, prêts à prendre des risques pour des représailles si mesquines. Un esprit moins puéril aurait très bien pu vouloir tirer vengeance de façon moins grotesque.

### Côte sud du Péloponnèse, matin du 9 octobre

Le navire bougeait plus calmement, la tempête semblait passée. Ernaut risqua un œil hors de sa couverture et vit qu'une lumière diffuse se déversait depuis l'ouverture d'accès. Quelques personnes commençaient à se réveiller, toussant et s'étirant. Des bâillements et des chuchotis se répandaient parmi les passagers, soulagés de sentir que la mer avait fini par retrouver un peu de calme. Au-dessus, les marins étaient déjà à la tâche, on entendait la voile claquer, pas encore en tension, et des ordres fusaient d'un bord à l'autre. Ernaut s'assit dans son lit et regarda son frère en plein sommeil, la bouche ouverte. Il fut tenté de lui faire une blague, mais se retint, craignant, certainement à raison, une réaction pas franchement amicale. Enfilant simplement ses braies, il se leva en bras de chemise et se dirigea vers l'escalier, prenant garde de n'écraser personne au passage.

L'air marin le frappa à peine eut-il passé la tête hors de la trappe. C'est là qu'il réalisa à quel point l'odeur était fétide dans la salle qui leur servait de dortoir. Et ces dernières heures n'y avaient rien arrangé, avec la quasi-impossibilité de se rendre aux latrines et les inévitables malheurs avec les seaux d'aisance lors d'une tempête. Tout en bâillant, il saluait les différents matelots qu'il croisait, d'un geste de la main ou seulement d'un signe de tête, appréciant de se voir si bien intégré. S'étirant une nouvelle fois le dos, il s'apprêtait à entrer dans le couloir qui desservait les cabinets quand il entendit un grand bruit venant de l'eau, comme si un gros objet y était tombé. Curieux comme à son habitude, il se pencha par-delà le bastingage et vit un corps inerte s'éloigner, pas très rapidement encore car le navire n'était pas lancé. Brusquement réveillé par cette vision effrayante, il cria « un homme à la mer ! » et, sans s'inquiéter des réactions, il sauta par-dessus le plat-bord.

L'eau fraîche le saisit à la poitrine, il ne s'attendait pas à un tel choc. Mais il reprit bientôt son souffle, se forçant à respirer régulièrement, et s'élança pour rattraper la personne avant qu'elle ne coule. Les vagues étaient encore pas mal formées et il lui fallait s'arrêter de temps à autre pour repérer où il allait. Il avait surtout peur de ne pas réussir à sauver le malheureux.

À bord, les marins s'étaient figés lorsqu'il avait crié, se demandant si c'était sérieux. Ayant vérifié l'exactitude de l'information, ils savaient quoi faire et, la stupeur passée, ils se précipitèrent : Octobono ordonna d'abattre la voile immédiatement et de préparer le canot. Les ancres devaient être remises à l'eau au plus vite, pour éviter de dériver. Il envoya également un matelot prévenir Ribaldo Malfigliastro. Le temps n'était pas encore très beau, mais au moins il ne pleuvait plus. Malgré tout, la situation était dangereuse pour ceux tombés à la mer. Le *Falconus* risquait de s'éloigner pas mal avant que la barque ne soit à flot et il fallait se dépêcher de manœuvrer si on voulait les remonter sains et saufs à bord. Octobono ne savait pas quel autre homme était dans l'eau, ni ses talents de nageur. Le jeune colosse avait l'air d'être à l'aise,  pourrait-il toutefois suffire à la tâche et attendre les secours ?

Au milieu des courants, Ernaut avait l'impression que la mer entière essayait de rentrer à toute force dans sa bouche. Il avait perdu de vue le noyé et s'était arrêté pour souffler un peu. Il se tourna, profitant de ce qu'il était au haut de la vague pour se repérer. C'est là qu'il réalisa que le *Falconus* avait continué sur sa route. La voile n'était plus en place, mais il était à bonne distance, suffisamment pour qu'il lui soit pénible de revenir seul. Refusant d'envisager sa défaite, il ôta de son esprit ces idées pessimistes et appela, au cas où l'autre personne dans l'eau serait consciente. Il finit par repérer une tache blanche, la chemise qu'il avait suivie jusque-là, et reprit sa nage pour s'en rapprocher.

La rumeur s'était propagée comme un feu de paille et la plupart des passagers étaient désormais accoudés le long du navire, tentant d'apercevoir le petit point qui se débattait dans la mer. Un bon nombre l'encourageait, d'autres priaient pour lui, tandis que certains se mordaient le poing d'angoisse. Lambert était de ceux-là. Vêtu seulement de sa chemise, il s'était rué là dès qu'on lui avait dit que son frère était à l'eau, s'efforçant de porter secours à un marin tombé lors de la manœuvre de départ. Octobono encadrait la descente du canot, tentant de faire les choses au mieux sans se précipiter ni prendre des risques inconsidérés pour autant. Lorsque l'embarcation fut lâchée pour les derniers centimètres et heurta les vagues avec un bruit d'éclaboussement, cela fut accueilli par des hourras et des cris de joie. Les matelots désignés descendirent rapidement le long des filins et firent jouer les avirons pour rejoindre les naufragés.

Ernaut était désormais suffisamment près de son compagnon d'infortune. Il tenta de l'appeler avant les ultimes brasses, essoufflé par tous ses efforts. Mais il comprit que le noyé ne pouvait pas répondre, il flottait, le dos à l'air, la tête dans l'eau. Le garçon se lança alors une dernière fois et attrapa l'autre par les cheveux. Il le retourna sans ménagement et eut un hoquet de surprise, avalant par mégarde un paquet de mer : le buste de l'homme était rouge de sang, rosissant la chemise et l'écume qui s'en éloignait. Décontenancé par cette vision, Ernaut s'efforça de rester calme, conscient qu'il lui fallait garder ses moyens s'il ne voulait pas périr noyé. Il vit aussi que la victime commençait à s'enfoncer, il avait dû mettre un peu de temps à succomber et se remplissait maintenant d'eau.

Cherchant du regard autour de lui parmi l'immensité grise dans laquelle il se sentait perdu, Ernaut avisa le canot qui s'approchait de lui. Il se dit qu'il fallait absolument qu'il ramène le corps du pauvre homme afin que l'on puisse constater qu'il avait été assassiné lui aussi. L'attrapant par le cou, il se plaça en dessous et tenta de battre d'un bras pour se rapprocher du navire. Il réalisa vite qu'il avait surestimé ses forces. Il était épuisé et n'avançait pratiquement pas, peinant déjà à simplement se maintenir hors de l'eau à chaque brasse. Le sang écoulé de la poitrine rendait le corps glissant et il devait assurer sa prise continuellement. C'était comme traîner un bloc de métal huilé désireux de l'entraîner vers le fond. Ne sachant que faire, il commença à paniquer et des larmes lui montèrent aux yeux, qu'il essuya d'un mouvement rageur. Il ne voulait pas lâcher le cadavre, mais n'était pas certain d'arriver jusqu'à ses sauveteurs s'il s'accrochait à lui. Il fallait qu'il fasse un choix : tenter de ramener un mort et risquer de ne pas en réchapper ou l'abandonner à son sort et attendre en faisant la planche qu'on vienne le sauver. Il était désespéré et ne supportait pas l'idée d'être vaincu, encore moins de devoir abandonner, de reconnaître sa défaite. Dans un hurlement de rage, il lâcha son compagnon d'infortune, qui coula immédiatement comme une pierre.

### Côte sud du Péloponnèse, midi du 9 octobre

Gandulfo se hâtait de porter une écuelle de brouet qu'il avait rapidement réchauffée. Il l'avait accompagnée d'un pichet de bon vin, de la réserve personnelle de Ribaldo Malfigliastro. Ayant les deux mains prises, il frappa du coude à la porte de la cabine du *patronus*, attendant qu'on lui ouvre. Régnier lui-même fit office de portier et l'aida à poser le tout sur la table, à l'intention d'Ernaut qui était assis sur le lit du commandant de bord, emmitouflé dans une épaisse couverture de laine. Il avait repris meilleure mine, mais gardait un air abattu. Son visage s'éclaira instantanément à la vision du plat fumant.

« Un potage bien eschaudé ! Ça alors ! Je n'en voyais plus qu'en mes songes ! »

Se frottant les mains de plaisir, il attrapa la cuiller et s'appliqua à engloutir le contenu à grandes lampées, ne s'arrêtant que pour avaler des doses proportionnelles de vin coupé. Il ne prêtait guère attention à Régnier et Ribaldo qui échangeaient des regards anxieux, bien que compréhensifs. Son comportement héroïque valait bien un peu de mansuétude. Voyant que le géant en arrivait à la fin de son plat, Régnier estima qu'il était temps de poser les questions qui lui brûlaient les lèvres. Ils avaient déjà appris que la victime était Fulco Bota, le charpentier, mais ils étaient impatients d'en savoir plus sur les circonstances de cette mort et d'avoir le point de vue d'Ernaut.

« Adoncques, garçon, que s'est-il passé au juste ? »

Repoussant son assiette soigneusement grattée, Ernaut assura sur lui la couverture, réprimant un frisson tandis qu'il se réchauffait graduellement. Une ombre passa sur son visage tandis qu'il répondait.

« Ben, j'allais aux latrines quand j'ai entr'aperçu un corps parmi les eaux. J'ai sauté pour porter aide. Mais… avec les vagues, le vent, ça a été bien plus ardu que je ne pensais. »

Des larmes lui montèrent aux yeux.

« Calme, Ernaut, tu as fait ton mieux. Il s'était peut-être aplomé en chutant, personne ne pouvait le ramener sauf. »

L'adolescent secoua la tête en dénégation.

« Certes pas, il était de sûr déjà mort en touchant l'eau. »

Ribaldo lança un regard inquiet au chevalier et se rapprocha de la table, s'y appuyant à deux mains pour regarder Ernaut droit dans les yeux.

« Que veux-tu dire, garçon ?

— Il a été percé d'un poignard. »

Le *patronus* le fixa quelques secondes puis se releva  brusquement, frappé par cette révélation, et entreprit de faire les cent pas dans sa petite cabine. Régnier, pour sa part, s'adossa à la cloison et croisa les bras, la tête baissée. Malfigliastro revint à la charge.

« Tu n'as aucun doute ? Ça ne pouvait être une navrure qu'il se serait faite par accident ? En tombant ?

— Nul doute. Il avait été assailli plusieurs fois parmi la poitrine. Ça ne pouvait rien être que par poignard.

— Tu ne l'avais pas vu avant de saillir en la mer ?

— Non, il ondait sur le ventre. Et si j'avais perçu qu'il était déjà mort, je n'aurais pas pris pareils risques. J'aurais juste appelé. »

Ribaldo attrapa le tabouret et s’y laissa tomber, l'air abattu et contrarié. Régnier n'était pas sorti de son mutisme et continuait à réfléchir, comme insensible à son environnement. Ernaut prit une gorgée de l'excellent vin, le regard fixe, droit devant lui. Après quelques minutes ainsi dans le silence, le chevalier soupira et toussa, comme revenant à la réalité.

« Voilà ténébreuse nouvelle. Le meurtrier n'a pas achevé sa sinistre besogne. »

Ribaldo leva la tête vers lui.

« Les deux meurtreries auraient lien ?

— Comment douter ? Tous deux tués par poignard, à une semaine l'un de l'autre.

— Et serait-ce en rapport avec… Gênes ?

— Je ne sais. Mauro Spinola a peut-être raison, pourquoi chercher à tout assembler ? D'aucunes façons, il faudrait déjà dénicher ce qui unit les deux victimes du *Falconus*. Sans compter que votre déserteur à Messine n'en est peut-être pas un.

— C'était un redoutable ivrogne, on le retrouvera au retour à dormir son vin le chef dans son vomi. Mieux vaut n'en tenir pas compte. »

Les trois hommes replongèrent un bon moment dans leurs pensées puis le *patronus* brisa le silence.

« Il me faut en faire annonce à l'équipage et aux passagers, non ? Je préfère que chacun soit sur ses gardes. »

Ernaut sortit de son mutisme, pensant à ses compagnons de voyage.

« Cela risque de déclencher un vent de panique, moult pèlerins n'ont guère de bravoure ! »

Les sourcils froncés, Régnier se mordit la lèvre et se frotta le nez.

« C'est par malheur nécessaire. À ce point nous sommes toujours dans le brouillard. »

Visiblement abattu, Malfigliastro appuya les coudes sur ses genoux.

« Vous n'avez donc rien de nouveau sur le premier murdriment ?

— Non. Peut-être est-ce lié à ses affaires, finalement.  Quoique cela pourrait parfaitement se résumer à un crime de larron, vu la coquette somme dérobée. Mais le grand soin apporté à l'entreprise, qui nous en cèle certains aspects, ne me semble guère correspondre aux usages de vulgaires détrousseurs. »

Ernaut toussota pour attirer à lui les regards des deux hommes.

« Il se pourrait aussi que le charpentier ait aidé au premier crime et qu'il y ait eu dispute pour le partage. Il est connu que les larrons se chamaillent fort souvent. »

Régnier eut un sursaut, éclairé par cette idée.

« De vrai, Bota savait la besogne du bois. Qui d'autre que lui pouvait aider à entrer et sortir de la cabine sans trace ? Il a bien dirigé la fabrication des cloisons pour ce passage, maître Malfigliastro ?

— Oui. Mais je ne peux croire que Fulco Bota ait été tel que vous le dites. C'était un homme droit, et honnête. Un bon compaignon. Il buvait parfois trop, mais c'était bien là le seul vice que je lui connaissais. »

Écoutant le *patronus* détailler la mentalité du charpentier de bord, Régnier commençait à se demander pour quelles raisons Ribaldo Mafigliostro prenait systématiquement la défense des victimes. En dehors de l'éventualité qu'il dît la vérité, pouvait-il avoir un intérêt à ce qu'on ne s'intéresse pas trop aux affaires et à la personnalité des poignardés ? Si jamais il en savait plus qu'il ne l'avouait, il risquait d'être la prochaine personne à tomber sous les coups de lame du meurtrier. Il lui faudrait veiller à ce qui se passerait autour du gaillard d'arrière dans les jours suivants.

### Côte sud du Péloponnèse, matin du 10 octobre

Ernaut s'apprêtait à allumer une nouvelle mèche à suif avec sa lanterne quand Herbelot lui posa la main sur l'avant-bras, le stoppant dans son geste.

« N'enlumine point trop, mon garçon. Nous allons juste bavarder. »

Peu convaincu, Ernaut fit une moue expressive. Herbelot se frotta le nez, un peu gêné et ajouta, à mi-voix.

« Je ne tiens guère à bien voir tout ce sang, autant demeurer un peu enténébrés. »

Ernaut haussa les épaules puis reposa sa lampe sur les étagères en hauteur. Il s'installa sur la couchette, au fond, laissant de la place à Régnier et Herbelot, le premier en tête du lit, le second sur le tabouret. La cabine qui hébergeait jusque-là Ansaldi Embriaco avait été vidée des affaires du marchand et pouvait donc leur servir librement pour discuter de leur enquête. Dans le couloir, Ganelon veillait à ce que personne ne vienne écouter ce qui devait rester secret.

Nettoyant de la main des poussières imaginaires de sa cotte tandis qu'il s'asseyait, Régnier avait l'air contrarié. Un pli soucieux barrait son front. Une fois bien calé, il prit la parole sans pour autant abandonner sa tâche de nettoyage.

« Il faut pointer notre avancement. Comme vous venez de l'apprendre, Herbelot, une deuxième victime vient de nous apparaître. Il nous faut mettre terme à ces méfaits et donc définir notre plan de bataille. »

Intrigué par ce vocabulaire guerrier, le clerc était néanmoins d'accord sur le fond. Une vague de meurtres risquait d'échauffer les esprits à bord et de révéler le pire en chacun des occupants du *Falconus*.

« Pour ma part, dit Herbelot, je ne suis pas demeuré oisif. Trois jours à remirer les lettres et papiers d'Ansaldi Embriaco ! Il a toujours fait larges dons, depuis moult années. Les montants s'en sont accrus avec le temps, grâce à sa meilleure fortune. Par contre, rien ne permet de déterminer événement ou date à l'origine de si belle générosité. Il était bon chrétien, concerné par les plus démunis. »

De déception, Régnier ne put s'empêcher de protester, le visage fermé.

« Vraiment rien d'étrange ou peu commun ?

— Ma formation n'inclut pas les comptes marchands, alors je ne peux rien vous assurer. Mais je n'ai rien relevé d'étrange, si ce n'est assez colossale fortune pour son âge. Je n'ai pas mesuré au juste, mais je l'estime à plusieurs centaines de livres, à dire le moins. »

Ernaut siffla d'admiration à l'énoncé du chiffre, ne sachant pas vraiment ce que pouvait représenter une telle somme, qui lui paraissait irréelle. Régnier lui-même fut surpris, se frottant la main sur son menton orné d'une barbe naissante.

« Cela seul peut expliquer qu'un frippon se soit intéressé à lui, en omettant sa manie à demeurer tout le temps en sa chambre. »

Impressionné par la mention d'une telle richesse, Ernaut abonda en ce sens.

« Et s'il craignait pour sa vie, à demeurer enfermé ? S'il avait vu d'aucun qu'il craignait fort à bord, un deshonnête ennemi, il aurait été sage de se barricader. »

Herbelot souleva un sourcil amusé, comme surpris d'entendre le jeune garçon émettre une hypothèse intelligente.

« Cela me semble logique. Partons de cela. Il ne pouvait craindre Fulco Bota, car il aurait de sûr appris son embauche lors d'une visite avant le voyage et n'aurait manqué de s'y opposer. »

Dodelinant de la tête pour marquer son accord, Ernaut enchaîna.

« Ni redouter le matelot évanoui à Messine, pour semblables raisons. En outre, après l'escale, il aurait arrêté de se terrer comme un conil[^conil]. »

Régnier suivit le fil de cette idée tout en hochant la tête.

« J'en ferai demande au *patronus*, on ne sait jamais. Il n'a peut-être pas pensé à vérifier les louages ou n'a pas réussi à imposer ses vues. Mais s'il a souhaité s'opposer, cela affermirait nos idées. En outre, Fulco Bota a dit qu'il croyait Embriaco capable de contrebande. Peut-être avait-il des raisons personnelles à dire cela. »

Ernaut sourit, tout content d'être à l'origine d'une piste. On le traitait comme un homme, et pas n'importe qui, un chevalier et un clerc de haut rang ! Peu importait le dénouement de cette affaire, une fois ce voyage achevé, il savait que rien ne serait plus comme avant. Il allait s'engager dans des rêveries sur la vie merveilleuse qu'il aurait en Outremer lorsque la voix d'Herbelot le ramena à la réalité.

« Mais pourquoi craindre Bota ? Jamais son nom n'est cité sur les documents ! Pour que le charpentier ait eu tel pouvoir sur le marchand qu'il l'obligeait à se terrer, cela devait être terrible ! »

Ernaut prit un instant pour se remémorer tout ce qu'il avait pu entendre lors des soirées qu'il partageait avec les hommes du bord. « Par le passé, dit-il enfin, Fulco Bota œuvrait dans les ouvrages de siège. Ils en ont devisé d'aucunes fois devant moi. Avec pareil métier, il a dû faire nombre de voyages et a pu y découvrir quelque honteux secret… »

Régnier souffla de façon lasse.

« Nous voilà derechef à ces rumeurs de contrebande ! »

Herbelot se gratta le nez nerveusement comme souvent lorsqu'il émettait une idée qu'il jugeait audacieuse et pertinente.

« Cela me paraît sot. Quand on détient semblables indications sur quelqu'un, on ne le meurtrit pas, on le fait payer. On ne mord jamais la main nourricière ! »

Ernaut se pencha en avant, faisant de l'ombre sur le clerc. Ce dernier, assis un peu en contrebas, poursuivit.

« Donc Bota voulait tirer vengeance d'Ansaldi Embriaco ! Unique raison expliquant la crainte du marchand.

— Vous négligez que le charpentier a aussi été poignardé », rétorqua Régnier.

Nullement démonté, Herbelot répondit du tac-au-tac.

« Il devait avoir quelque complice, loué pour l'aider dans sa meurtrerie. »

Ernaut, l'air décidé, pointa un index accusateur vers le ciel.

« Ou pour s'en charger ! Et avec lequel il se sera groigné lors du partage du butin, peut-être imprévu. »

Emporté par leur enthousiasme commun, Herbelot paracheva leur démonstration collective.

« Voire que le marin disparu était leur compaing et qu'il y a eu prime dispute à Messine. »

Ernaut et lui partagèrent un bref instant de complicité dans un échange de regards, auquel le clerc mit fin brusquement, un peu décontenancé. Mais l'idée avait convaincu le chevalier.

« De vrai, ça se tient ! Il ne reste qu'à découvrir d'où Bota connaissait Embriaco. Et qui partage ce lien avec eux. »

Les trois hommes s'arrêtèrent un instant, tentant de chercher dans leurs souvenirs la personne qui correspondait le mieux à ce profil. Ernaut s'exprima le premier, réfléchissant en fait à voix haute.

« Fulco Bota connut la ruine, ainsi que toute sa parentèle. Je ne sais comment en détail, mais c'est rapport aux combats en Espaigne, où Gênes mit toutes ses nefs, il n'y a pas dix ans. »

Régnier opina.

« Je connais bien cela, c'était lors de tous les passages pour la Terre sainte. »

Herbelot paraissait sceptique ; il se redressa sur son siège et s'accouda à la table derrière lui.

« Embriaco aurait eu quoi ? À peine sorti de ses jouvences ! Il était trop jeune pour y avoir pris sa part. »

Régnier ne sembla pas s'émouvoir de l'argument :

« Si fait, mais il pouvait déjà tenir négoce pour autrui. N'oublions pas qu'il sort d'une famille riche et puissante. Il a fort bien pu amasser une belle fortune là-bas, au détriment de Fulco Bota, d'une façon ou l'autre. »

Enflammé par l'argument, Ernaut commençait à comprendre ce qui s'était passé et ne put se retenir de conclure l'hypothèse avancée par le chevalier.

« Et les trois autres marchands meurtris ces derniers mois devaient être associés en cette affaire. D'où la crainte d'Ansaldi, et son choix de demeurer en sa cabine. Même s'il n'avait pas reconnu Bota, ou ne savait pas qui était son ennemi. »

Le clerc commençait à adhérer au point de vue de ses compagnons.

« Si vieilles transactions ne sont pas consignées ici, en outre. Il se sentait fautif et faisait depuis lors régulières aumônes pour alléger son âme. En ce cas, qu'avait-il bien pu commettre pour se punir ainsi durant des années et craindre vengeance si tardive ? »

Régnier se leva du lit, excité par les hypothèses qu'ils venaient d'échafauder.

« Mes amis, lorsque nous saurons cela, nous aurons fait lumière sur cette intrigue et démasqué le compagnon ou valet de Bota. Outre, je serais curieux d'apprendre pourquoi le vieux Spinola ne souhaite pas voir ces meurtreries reliées… La *Compagna* aurait-elle quelque part en cette sombre affaire ? »

### Au large de Cythère, après-midi du 11 octobre

Cela faisait deux jours que le navire affrontait des conditions de mer difficiles. La veille, tout le monde avait craint que les averses ne se transforment en tempête une fois de plus. Heureusement, la situation s'était améliorée en fin de journée et à la nuit. Ne demeurait qu'un puissant vent du nord, apte à porter le *Falconus* sans encombre. Mais il s'obstinait à ne se manifester que par rafales, faisant à chaque fois gîter le navire avec insistance et mettant à rude épreuve la patience des marins courant régler les voiles. Le *patronus* était sur le pont, présent presque sans interruption depuis la veille. Il était resté debout une partie de la nuit, accompagnant les hommes de quart, pour s'assurer que les conditions ne se détérioraient pas alors que tout le monde dormait. Les cernes sous ses yeux indiquaient l'anxiété et le sérieux avec lesquels il se consacrait à la tâche.

Accoudé à la rambarde bâbord, Régnier attendait, hésitant à s'imposer au commandant à un moment si difficile. Mais il lui fallait absolument aller l'ennuyer avec des questions intéressant l'enquête. Il n'avait guère pu avancer récemment, les conditions maritimes monopolisant la plupart des hommes qu'il voulait questionner. La situation, bien que délicate, semblait s'être stabilisée, les matelots s'efforçant de gérer de façon autonome les soubresauts du vent. Régnier se dirigea donc vers Ribaldo Malfigliastro, qui le vit arriver et le salua de la main. Lorsqu'il parvint sur la plate-forme, le marin devait avoir compris ses intentions car il l'invita à le rejoindre un peu vers l'arrière, à l'abri des rafales et des oreilles indiscrètes. Le pilote était aux commandes, maintenant le cap déterminé et donnant ses ordres aux matelots maniant les rames-gouvernails.

Régnier prit un siège à côté du *patronus*. Celui-ci s'appuya dos à la paroi, s'en servant comme d'un dossier. Il bâilla ostensiblement et, s'excusant par avance de sa fatigue, demanda au chevalier ce qui l'amenait.

« Je commence à croire en la possibilité d'un lien avec les affaires d'Espaigne. Il me faudrait savoir si vous aviez connaissance de la présence d'Ansaldi Embraco là-bas, ou s’il avait usage d'y commercer. Cette demande concerne d'ailleurs aussi les trois autres marchands meurtris. »

Ribaldo souleva des sourcils broussailleux, dessinant de ses doigts la moustache autour de sa bouche, l'air ennuyé.

« Difficile réponse, messire d'Eaucourt. Comment êtes-vous rendu là ?

— Ce n'est que possibilité, je souhaite moissonner plus de renseignements avant de la présenter, si cela ne vous mésaise pas. »

Ribaldo eut un sourire fugace.

« Oui, d'autant plus que j'ai peut-être quelque part à l'affaire, c'est ça ? »

Régnier répliqua d'un rictus énigmatique, son silence invitant invitant à une réponse.

« Convenez qu'il serait bien sot pour un intrigant de confier les recherches à autrui. Il lui suffirait de bâcler les recherches et d'oublier l'affaire.

— Certes. Mais disons pour l'instant que je ne veux nuire à aucune réputation.

— Fort bien. Avant tout il me faut vous confier, si vous ne le savez déjà, que j'ai en personne combattu à Almeria et Tortosa. J'étais *capitaneus* d'une nave de guerre lors des deux sièges, aux ordres d'Ansaldo Doria. J'y ai encontré le pauvre Fulco. Il faisait partie des blessés que j'ai hébergés quelque temps. Je ne me souviens pas y avoir vu Ansaldi Embriaco. Il aurait été si jeune ! Et de trop bonne lignée pour n'être que soldat ou matelot. Il aurait servi un des consuls, ou quelque notable comme Mauro Spinola. »

Régnier lança un regard intéressé, se mordant les lèvres tandis qu'il écoutait.

« Spinola y a pris part ?

— Certes oui, il est fort lié à Caffaro[^caffaro] ainsi qu'à nombre de magistrats. Il ne chasse pas qu'en Outremer, même si c'est là qu'il se complaît le mieux. Mais je vous le redis, je n'ai jamais encontré Ansaldi Embriaco là-bas, ni même ouï après lui. La prime fois, c'est lorsque nous apprêtions le passage du *Falconus*. On me présente souventes fois comme vétéran d'Almeria et Tortosa, et je ne crois pas qu'il ait relevé. Ce qu'il n'aurait pas manqué de faire s'il y avait pris part. »

Sauf dans le cas où cela ravivait un sombre souvenir, songea Régnier.

Le *patronus* laissa son regard errer quelques instants, rassemblant ses souvenirs et, peut-être, choisissant ceux qu'il choisirait de révéler. Sa barbe et sa moustache dansaient autour de son visage tandis qu'il bougeait sa mâchoire en tout sens, comme s'il remâchait à l'avance ses mots.

« En revanche, les trois hommes meurtris à Gênes étaient puissants marchands, qui avaient prêté monnaies pour l'expédition d'Espaigne. Les bénéfices qu'ils en ont tirés n'ont pas plu à tout le monde. Mais ils n'étaient pas seuls, faisaient bonnement partie du lot. Ils ont fait belle récolte de livres, alors que la plupart ont connu la ruine.

— Avaient-ils bonne renommée par ailleurs ? Étaient-ils sus pour passer outre les interdictions de vente aux infidèles par exemple ?

— Je ne le crois pas. On les a bien sûr accablés de mille maux, mais c'était issu du dépit et du chagrin. Vous pensez que c'est la raison de leur murdriment ?

— Possible. Mais une fois encore, nulle place pour Ansaldi Embriaco. Les pistes mènent vers l'Espaigne, peut-être nœud de trafics. Mais chaque fois, je ne vois comment y placer ce malheureux. »

Ribaldo resta quelques instants silencieux, puis attrapa un gobelet qu'il se remplit de vin. Il le tendit à Régnier et s'en servit un autre.

« Vous savez, Mauro m'a entretenu de vous et de vos recherches. »

Régnier rit pour lui-même, amusé à l'avance de ce que le vieux diplomate pouvait dire sur lui, mais sa curiosité fut éveillée par cette étrange confidence.

« Il n'a guère dû se laisser à complimenter, je le crains.

— Ce n'est pas homme à s'ouvrir franchement de ses opinions, surtout inamicales. Il m'a simplement fait comprendre que vous suiviez mauvais chemin et cherchiez du sens entre éléments sans lien. Que la preuve en était que Fulco n'était pas riche négociant membre d'une influente parentèle, sans même parler du matelot manquant. Il voulait que je vous en parle, quoique de sûr moins franchement. »

Le *patronus* assortit sa dernière phrase d'un grand sourire et inclina amicalement son godet vers le chevalier. Régnier rendit la politesse machinalement.

« C'est vrai, mais Bota était en Espaigne. Et il a pu participer à quelque affaire, ou en être témoin. »

Le *patronus* se leva en rangeant son gobelet, signifiant par là qu'il mettait fin à l'entretien. Il allait s'éloigner lorsqu'il se figea, se retournant vers Régnier, le visage encore dans la pénombre de l'abri.

« La seule chose que je puisse proclamer, c'est qu'il avait toute ma confiance. Je l'ai toujours pensé brave, loyal et franc. Si jamais on l'a meurtri en lien avec pareille affaire, cela me désolerait que vous le croyiez coupable de vilenie. Je suis persuadé qu'il n'est qu'innocente victime, autant que peut l'être un homme. »

Régnier hocha la tête sans conviction. Il comprenait l'opinion du commandant de bord mais ne pouvait se contenter d'impressions et d'opinions. Il devait se fier à sa raison, avec méthode.

« Une ultime demande, si vous le permettez. Ansaldi Embriaco s'est-il opposé au louage de Fulco Bota pour ce passage ?

— Certes pas. Il encourageait plutôt l'enrôlement de tous ceux qui s'étaient illustrés au combat contre les musulmans. »

### Au large d'Antikythira, matin du 12 octobre

Régnier trouva Gringoire le Breite en train de jouer avec son épouse, confortablement installés sur leurs matelas dans la petite zone qu'ils s'étaient réservés au sein de la salle des passagers. Dehors, le temps était encore frais et la jeune femme était légèrement fiévreuse depuis deux nuits, derniers avatars de la tempête qui les avait tous éprouvés physiquement et nerveusement quelques jours plus tôt.

Ils s'adonnaient à une partie de mourre, Ermenjart restant bien à l'abri sous ses couvertures. Régnier se demandait comment on pouvait choisir de demeurer dans cet endroit dont la moins nauséabonde des odeurs exhalait comme une saumure en voie de fermentation. Pour sa part, dès qu'il le pouvait, il sortait sur le pont supérieur ou les châteaux avant et arrière, selon les autorisations accordées par le *patronus*, préférant le goût des embruns salés à cette atmosphère de latrines. Il s'approchait du couple en essayant de ne pas trop marcher sur les différents paquets, colis et corbeilles remplissant pratiquement tout l'espace au sol. Heureusement, le navire ne bougeait guère et, tout en se raccrochant aux poutres, il pouvait avancer sans avoir à trop jouer les acrobates. Le voyant arriver vers eux, les deux conjoints arrêtèrent leur partie et la jeune femme tira sur sa tête la couverture qui l'emmitouflait, de façon à s'en faire une sorte de voile. Malgré la promiscuité du voyage, elle conservait quelques réflexes de pruderie.

« Le bonjour à vous, maître et maîtresse la Breite. Je suis désolé de vous déranger alors que vous souffrez de fièvres. Mais vous détenez, maître, peut-être quelques informations pouvant me faire avancer dans mes recherches. »

Le marchand se rengorgea de se voir accorder une telle importance, surtout devant sa jeune épouse qui posa sur lui des yeux amoureux, bien que fatigués. Il se tassa sur sa couche, faisant un peu de place au chevalier. Régnier s'assit avec plaisir.

« Vous êtes marchand d'expérience et il est possible que vous ayez ouï rumeurs sur l'Espaigne lorsque le roi Louis prit la croix. L'opinion d'un négociant étranger à la *Compagna* peut m'aider fort.

— Oh, pour ça oui, je peux vous en parler ! C'était plus que rumeurs ! Les affaires s'essoufflèrent cruellement de cette histoire. Plusieurs compaings et amis, habitués à encontrer Génois lors des foires du Lendit, de Lagny, Provins ou Troyes ont dû chercher nouvelles voies pour leurs marchandies. En outre, ils ne trouvaient rien à leur acheter. Par bonne fortune, il demeurait Pisans et  Vénitiens avec lesquels les plus sages avaient gardé bons contacts, mais j'en sais plus d'un qui a mangé son chapeau plusieurs années durant. »

L'air satisfait qu'il arborait alors en indiquait long sur l'excellente opinion qu'il avait de lui-même et de son flair en affaires. Dépité que le marchand ne comprenne pas à demi-mot ce qu'il recherchait, Régnier entreprit d'être plus précis.

« Savez-vous en détail ce qui s'est passé lors de leur assaut sur Tortosa et Almeria ?

— C'est fort simple, ils ont envoyé leurs nefs au loin pendant trop longtemps, le commerce à longue distance en a été durement touché. Force négociants ont vu leurs affaires ruinées, faute de transport. Les taxes ne sont plus rentrées comme auparavant et il leur a fallu emprunter pour payer les dépenses publiques. »

Régnier hocha la tête, réalisant que c'était en définitive assez évident. La flotte, au nom de la défense des intérêts de Gênes, avait en fait servi à quelques familles pour s'enrichir. Parmi ces dernières se trouvaient les gens qui étaient dans l'entourage des consuls à cette époque, revenus au pouvoir depuis lors, après le règlement de la crise politique et financière.

« Est-ce que vous connaissiez Lafranco Rustico, Jacomo Platealonga, Oberto Pedegola ou Ansaldi Embriaco à cette période, ou aviez-vous ouï parler d'eux ? »

Le Champenois réfléchit quelques minutes, l'air circonspect.

« Non, ça ne me dit rien. Par contre, je me suis souvenu que j'avais bon compaing de Bar sur l'Aube qui avait usage de négocier avec la famille Bota. »

Après avoir lâché cette dernière phrase avec un sens très personnel du suspens, il marqua un temps d'arrêt, afin de vérifier l'impact de cette nouvelle sur Régnier. Mais le chevalier attendit impassiblement la suite, obligeant Gringoire à continuer son histoire sans pouvoir jouer plus avant de ses talents de conteur.

« Ils étaient plusieurs de cette parentèle, bien que je ne croie pas qu'il y avait le charpentier parmi eux, je ne sais d'ailleurs même pas s'ils étaient cousins ou pas. Ils ont presque tout perdu dans cette histoire d'Espaigne. Des denrées gâtées et gâchées ! Ils s'en étaient ouverts à mon ami la prime année, où ils ont quand même fait chemin jusqu'en Champagne. Par contre, à la saint Barnabé suivante ils ne sont pas venus pour le marché du Lendit. On ne les a plus jamais revus. Complètement ruinés ! »

Appuyant son discours, il fit un geste théâtral de la main, fendant l'air d'un mouvement définitif avant d'enchaîner d'un air empressé :

« Et ce n'est pas tout ! Un de leurs coursiers, que je connais fort bien, m'a même conté par la suite que l'un d'eux se serait donné la mort, n'ayant pas accepté sa disgrâce. »

Régnier songea qu'il tenait probablement là le mobile de Fulco Bota, ainsi que le lien qui unissait les crimes de Gênes. Il ne voyait néanmoins pas ce qu'Ansaldi venait faire dans toute cette affaire. Mauro Spinola avait peut-être raison de mettre cet assassinat à part. Toutefois, qu'au moins deux meurtres sans rapport l'un avec l'autre aient pu se commettre sur le même navire, à quelques jours d'intervalle, lui semblait inconcevable. Tandis qu'il était plongé dans ses réflexions, Gringoire continuait son récit, qu'il illustrait de gestes souvent inappropriés, bien que singulièrement démonstratifs.

« Cette malheureuse histoire a duré longtemps. Ils avaient de telles dettes qu'on ne les voyait guère. Mais j'ai impression que tout va repartir pour eux. C'est d'ailleurs pour ça que j'ai tenté d'en discuter avec le jeune Embriaco, pour pouvoir profiter… »

### Abords de la côte crétoise, matin du 13 octobre

Ingo était assis sur un tabouret au centre de la petite loge où les outils étaient rangés. Il lui arrivait souvent d'y dormir, lorsqu'il trouvait que les marins autour de lui faisaient trop de bruit. Pourtant, depuis la mort de son maître et ami Fulco, il n'avait plus vraiment de cœur à l'ouvrage. Pour l'heure, il écrasait avec soin du biscuit dans une écuelle, pour en faire une bouillie allongée de vin et d'eau, son déjeuner habituel. Le ciel gris n'apportait qu'une lumière plate et correspondait assez bien à son humeur. Il vaquait à ses tâches, assistant et parfois prenant le pas sur un matelot que le *patronus* avait délégué pour remplacer le défunt charpentier. Mais il avait perdu son entrain et s'acquittait de ses devoirs machinalement. La perspective d'un nouveau professeur pour le voyage retour ne l'enchantait guère, car c'était plus le compagnon que l'enseignant que le jeune homme regrettait.

C'est ainsi qu'Herbelot et Ernaut le trouvèrent, la tête penchée au-dessus de sa gamelle. Le clerc frappa à l'huis pour signaler qu'ils souhaitaient s'entretenir avec lui, leur arrivée ayant forcément été remarquée par l'apprenti.

« Nous venons t'importuner quelques instants, garçon. Nous avons plusieurs demandes à propos de ton maître. »

Herbelot avait insisté auprès de Régnier pour accompagner Ernaut dans cet interrogatoire car, contrairement à Régnier, il ne se fiait toujours pas aux capacités de raisonnement de l'adolescent, qu'il estimait naturellement limitées. Le chevalier lui avait rétorqué que sa seule présence risquait d'effrayer quelqu'un d'aussi simple qu'Ingo, peu habitué à fréquenter des gens importants. Mais surtout, cela désignait Ernaut comme participant officiellement à l'enquête, ce qui pouvait les desservir. Le clerc objecta que tout le monde s'en doutait désormais, vu leurs fréquentes réunions dans l'ancienne cabine d'Embriaco. En s'entêtant dans sa décision, Herbelot finit par rallier le chevalier à son idée. Bien évidemment, ils ne dévoilèrent ni l'un ni l'autre cette discussion à Ernaut. Et le clerc était décidé à montrer qu'il était le responsable de cet entretien et garant de son bon déroulement.

Ingo leva la tête et les toisa en silence puis se mit debout, s'effaçant pour les laisser entrer dans le petit espace. Il s'assit sur un des coffres, au fond, continuant à égrener son biscuit.

« Que voulez-vous savoir, maître ? »

Cherchant un lieu pas trop rempli de poussière et de sciure où se poser, Herbelot lui répondit.

« Ce que Fulco Bota avait fait comme voyages jusqu'à ce jour, les lieux où il aurait pu se rendre, les cités et contrées où il aurait pu s'attirer  ennemis… Tout ce qui te vient en tête susecptible de nous éclairer sur la piste de son meurtrier. »

Ingo lança un regard intrigué à Ernaut, comme s'il l'interrogeait sur la fiabilité du petit bonhomme trépignant sur place, incapable de trouver un siège digne de recevoir son fessier. Ernaut fit un discret signe affirmatif de la tête, perché sur une table étroite servant d'établi.

« Ça ne faisait guère de temps qu'il m'avait pris à son service. J'étais chez un autre maître qui besognait uniquement les coques, à terre. Maître Bota m'a conté qu'il s'était fait soudard moult années, mais qu'il avait cessé par crainte des dangers.

— L'Espaigne, c'est ça ?

— Oui, il a œuvré comme *ingenior* à plusieurs sièges, c'est là-bas qu'il a connu Octobono et Enrico Maza. Le prime était matelot, sur une des galèes. Et maître Maza se louait comme arbalétrier.

— Ils étaient déjà bons compaings ? »

Le jeune homme haussa les épaules, l'air interrogateur, et avala rapidement une bouchée, comme un animal craintif.

« Et que sais-tu de sa maison ?

— Il est marié, avec plusieurs enfançons, tous à Gênes. Il appartient à une famille ancienne de la cité et certains de ses cousins sont notables et assez riches. Mais la plupart connus de moi ont été ruinés voilà peu, justement avec l'expédition d'Espaigne. »

Herbelot lança un regard entendu à Ernaut. Ce dernier enchaîna.

« Il a été personnellement touché ?

— Je ne sais. Il avait gagné belle somme au combat. Il a acheté une demeure pour sa famille, avec une fenêtre apercevant le port. Par contre, son espoir de diriger sa propre nef, qu'auraient payé ses cousins les plus fortunés, est tombé à l'eau.

— Il te contait souvent Almeria ou Tortosa ?

— Sans plus, il disait qu'il y avait encontré vrais héros. Il ne tarissait pas de compliments sur maître Maza, un lion au combat… »

Herbelot laissa à l'apprenti le temps d'avaler un peu de nourriture, tout en jetant un coup d'œil vers l'intérieur du navire. Son regard croisa celui d'Ernaut, le visage interrogatif, apparemment impatient de reprendre la discussion. Mais le clerc ne souhaitait pas brusquer les choses et attendit d'accrocher l'attention d'Ingo avant de poursuivre d'une voix amicale.

« Avait-il eu quelques liens avec aucun notable de la *Compagna* ?

— Certainement. Il avait de hautes fonctions lors des assauts et prenait ses ordres de haut-placés. Peut-être pas les consuls eux-mêmes, mais au moins grands officiers et capitaines.

— Une ultime question, avant de te laisser. Est-ce que tu vois quelqu'un à bord qui aurait pu haïr ton maître ?

— Je ne lui connaissais aucun ennemi, et certainement pas sur le *Falconus*. Tu as dû t'en rendre compte, Ernaut, tout le monde l'appréciait fort.

— Et au sein des passagers, négociants, pas seulement parmi les matelots ?

— La plupart me sont inconnus, du moins l'étaient avant le départ. Maître Bota ne faisait pas négoce, en dehors de quelques petites balles personnelles, comme d'usage.

— Fort bien, mille grâces pour ton aide. »

Herbelot vérifiait tout en parlant qu'il n'avait pas amassé trop de saleté sur son vêtement, et en ajusta les plis tout en prenant congé. Alors qu'il était devant la loge, il se figea brusquement, manquant de peu d'être bousculé par Ernaut, qui marchait une nouvelle fois sans regarder là où il allait. Prenant un air très sérieux, il interpella une dernière fois Ingo.

« Surtout, si quoi que ce soit te revient en mémoire, même de peu d'importance à première vue, rapporte-le nous, à Ernaut, messire d'Eaucourt ou moi, d'accord ? C'est fort important ! »

Ils avancèrent de quelques pas et Herbelot fit signe à Ernaut de se pencher vers lui.

« Nous tenons la crapule. Le soudard, en toute évidence ! Avec son caractère rageux, il a dû vouloir plus ou s'échauffer pour quelque broutille, et il lui aura fait son affaire. Nous venons de dénouer cette intrigue, garçon ! »

Il eut un sourire entendu, retroussant son nez fin. Tandis qu'il reprenait son avance, il réfléchissait à la façon dont il allait présenter ses conclusions à Régnier. Ernaut, l'air plus soucieux qu'habituellement, était moins convaincu. Il avait passé pas mal de temps avec Enrico Maza et, s'il reconnaissait que l'homme n'était pas sans défaut, il n'arrivait pas à convenir que c'était un assassin calculateur. Il pouvait tuer n'importe qui sous l'influence de la colère mais, selon Ernaut, préparer et exécuter une série d'assassinats, cela ne collait pas au personnage. Herbelot allait un peu trop vite en besogne.
