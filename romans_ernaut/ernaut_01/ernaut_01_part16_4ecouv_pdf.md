
Gênes, 1156. Le Falconus quitte le port. À son bord, pélerins, marchands, membres de l'équipage… et un crime.  
« À ainsi te mêler de tout, tu sais que tu t’attires des ennuis ».  
Le jeune Ernaut mènera l'enquête et l'exaltation des débuts laissera place à la maturité. Et si le voyage vers la Terre Promise était aussi un cheminement spirituel ?

Dans cette première enquête, à mi-chemin entre polar médiéval et roman d'aventure, Yann Kervran nous embarque dans un huis clos à la fois haletant et érudit. Sur cette inquiétante nef des fous, c'est aussi une partie de nous-mêmes que nous retrouvons, comme si nous étions compagnons d'Ernaut. En maître du genre, l'auteur ne fait pas que nous envoûter, il nous transporte dans ce monde tourmenté des croisades.

Ancien rédacteur en chef de magazines historiques (Histoire Médiévale, Histoire Antique, L'Art de la Guerre...), Yann Kervran se consacre désormais à l'écriture de fiction.
Photographe et reconstitueur historique enthousiaste depuis la fin de ses études universitaires, il trouve dans ces passions l'occasion de concilier goût pour la recherche et intérêt pour la mise en pratique et en scène de savoirs théoriques.
