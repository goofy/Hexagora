## Remerciements

Lors de la première édition de cet ouvrage, j’avais remercié mes parents Michel et Marie-Claude, sans qui, au sens le plus évident, rien n’aurait été possible pour moi. Je peux apprécier chaque jour à quel point c’est vrai, encore aujourd’hui. Puissé-je être un guide aussi aimant et avisé qu’eux pour les miens.

Je tiens par ailleurs à remercier l’équipe de Framasoft, Christophe Masutti en tête, qui a accueilli mon projet avec chaleur, enthousiastes qu’ils étaient à recueillir un spécialiste rare, de ceux qui se consacrent au troisième quart du XIIe siècle des Croisades. Les membres de Framabook, qui ont relu avec minutie mes écrits pour en tirer le meilleur, m’ont témoigné une attention appréciée, en particulier Mireille dont le soin à traquer mes virgules baladeuses égale la gentillesse d’abord. Un grand merci aussi à Sandra qui en a détecté la moindre silhouette inopportune tout en y ajoutant une rigueur bien appréciée sur la typographie.

Et, enfin, je souhaite inclure à cette liste Jean-Louis Marteil, de La Louve éditions, qui, le premier, a cru suffisamment en mes textes pour les intégrer à son catalogue. Qu’il soit doublement remercié de m’avoir redonné par la suite la liberté d’expérimenter ailleurs de nouvelles formes d’édition.

## Plan du Falconus

![Plan du navire](ernaut1_falconus.jpg)

## Trajet du Falconus

![Le trajet du Falconus](ernaut1_trajet_falconus.jpg)

## Liste des personnages

**Gênes**

+ Frère Federico, moine hospitalier  

**Pèlerins**

+ Aubelet, prêtre  
+ Ermenjart la Breite, épouse de Gringoire  
+ Ernaut de Vézelay  
+ Gringoire la Breite  
+ Lambert, frère d'Ernaut  
+ Maalot, épouse d'Aubelet

**Marchands et voyageurs**

+ Ansaldo Embriaco, marchand  
+ Ganelon, valet de Régnier d'Eaucourt  
+ Mauro Spinola, marchand et diplomate  
+ Herbelot Gonteux, prêtre  
+ Ja'fa al-Akka, marchand  
+ Régnier d'Eaucourt, chevalier  
+ Sawirus de Ramathes, marchand  
+ Ugolino, valet d'Ansaldo Embriaco  
+ Yazid, esclave de Sawirus de Ramathes  

**Équipage du Falconus**

+ Benedetto, notaire du bord  
+ Bertolotus, matelot  
+ Bonado, matelot  
+ Enrico Maza, arbalétrier  
+ Fulco Bota, charpentier  
+ Gandulfo, cuisinier  
+ Ingo, apprenti charpentier  
+ Octobono, matelot  
+ Ribaldo Malfigliastro, patronus  
+ Rufus, matelot  

**Messine**

+ al-Manzil al-Qamah, marchand  

**Gibelet**

+ Guillaume d'Embriac, seigneur de la ville  
+ Guy, chevalier
