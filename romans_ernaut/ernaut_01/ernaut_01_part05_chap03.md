## Chapitre 3

### Golfe de Salerne, journée du 13 septembre

Gringoire la Breite se mordait le bout de la langue de façon automatique, sans même y penser. Cela devait l'aider à viser. Il lança son palet avec délicatesse et ce dernier glissa doucement jusqu'à une saillie du plancher qui l'arrêta. Il en échappa un grognement de déplaisir. Les matelots à l'entour, pour leur part, étaient plutôt satisfaits. Chacun était contre le lanceur au moment où il devait placer sa pièce, dont le but était d'être au plus près de la coque, mais sans la toucher. Malgré son application, Gringoire n'égalait pas les marins habitués à jouer sur un navire en mouvement. Il laissa donc sa place sans regret au joueur suivant.

Tandis qu'il regardait les performances des autres participants, tout en s'essuyant le front transpirant par cette belle journée, il vit qu’Ansaldi Embriaco s'était décidé à sortir de sa cabine. Il se résolut alors de tenter sa chance. Abandonnant la partie, il avança droit vers le gaillard d'arrière où le marchand se dirigeait apparemment. Il le retrouva alors qu’il installait son tabouret sur la plate-forme sommitale, dos au soleil.

« Le bon jour, maître Embriaco.

— Le bon jour à vous…

— J'ai nom Gringoire la Breite, négociant champenois. »

Le jeune homme semblait ne pas être vraiment décidé à discuter et s'assit sur son siège, les épaules appuyées à la rambarde. Nullement découragé, Gringoire enchaîna.

« Je suis aise de vous encontrer, vous ne sortez que peu hors votre cabine.

— Hmm.

— Vous avez eu désir de goûter ce beau soleil ? La vue sur ces monts est tout bonnement somptueuse ! Je ne pensais pas qu'il pouvait s'en trouver de si hauts en bord même de mer. On se demande comment cités ont pu y prospérer.

— Hmm.

— C'est un peu comme Gênes. Quelle splendide réussite, si éclatante prospérité en un lieu si difficile d'accès ! C'est pour sûr le fruit de talent pour le voyage et le commerce. »

Amusé par cette introduction laborieuse, Ansaldi prêta un peu plus d'attention à son interlocuteur : de bonne taille et la panse rebondie, le cheveu blanc se raréfiant, il était vêtu de belle étoffe de laine, d'un bleu profond de qualité, mais il n'avait pas le port d'un homme habitué à de tels atours. Ses larges mains étaient usées par le travail et ses doigts épais indiquaient qu'il n'avait pas toujours vécu dans l'aisance. Un parvenu, estima le jeune Génois.

« Je vous sais gré de tous ces compliments, maître la Breite. Par malheur, je ne connais pas la Champaigne, si ce n'est de renommée pour ses foires, je ne peux donc vous faire part de mes sentiments à son endroit.

— Aucune importance. Il est amusant que vous évoquiez les foires, car j'escomptais justement vous faire proposition d'affairer ensemble. »

Nous y voilà ! pensa Ansaldi.

« Vous commercez quels produits, maître ? Demanda-t-il.

— De la laine de mouton, de la meilleure qualité, donnant de souples étoffes agréables à vêtir, prenant bien la teinture. Je ne crains pas d'être comparé aux toisons angloises.

— Je ne m'y connais guère en laine brute, je n'en fais habituellement pas négoce.

— Justement, cela vous ouvrirait nouveau marché. Il me siérait de vous initier en ses arcanes.

— C'est très aimable à vous, mais je ne commerce que peu avec le nord, je suis plutôt spécialisé dans les productions syriennes et romaines, voyez-vous : étoffes de soie, épices, papier… » Ne laissant pas à Gringoire le temps d'objecter de nouveau, il enchaîna, d'un ton très docte : « Dans le grand commerce ainsi que je le pratique, il faut se consacrer à unique région si on veut être efficace. Je crains fort que vous n'encontriez guère d'oreille attentive auprès de marchands habitués à travailler en Méditerranée. Cela serait  erreur à mon sens de collaborer avec un vendeur en laine brute comme vous. »

Vexé que semblable exposé lui soit fait par un si jeune homme, et de façon aussi hautaine, le vieux Champenois prit un air outré. Puis, ne sachant quoi répondre, il salua sèchement d'un signe de tête et bredouilla :

« Je vois. Faites excuse de vous avoir ennuyé. »

Et il tourna les talons sans demander son reste, abandonnant là le marchand génois, qui eut un sourire pour lui-même devant la naïveté de son interlocuteur. Énervé, Gringoire manqua de bousculer Aubelet auprès de l'échelle, occupé à redescendre vers le pont principal. Ce dernier lui sourit amicalement.

« Attention, mon fils, vous allez renverser quelqu'un si vous ne prêtez garde où vous posez les pieds.

— Désolé, mon père, j'étais perdu en mes pensées.

— Vous m'avez l'air contrarié, en effet.

— Oui, c'est ce jeune négociant génois, empli d'une telle arrogance !

— Il vous a fait insulte ?

— Tout comme, il m'a sermonné comme enfant un peu simplet. Tout ça parce que je lui faisais proposition de travailler avec moi. Il a trouvé bien vilaines excuses pour refuser ! »

Aubelet écarquilla les yeux.

« Vous avez parlé négoce avec lui ? »

Comprenant qu'il venait de se trahir, Gringoire prit un air médusé et bougea les lèvres, sans qu'aucun son sorte pour autant. Le visage d'Aubelet devint plus autoritaire.

« Je me répète, maître la Breite, ne mêlez pas labeur et pèlerinage ! Vous devez vous dédier aux oraisons et à la remembrance des saintes Écritures. Nous ne sommes pas simples voyageurs, mais devons cheminer aussi en notre cœur. »

Gringoire fut tenté de lever les yeux au ciel, mais se retint au dernier moment.

« J'y ai vu occasion à saisir. Il n'y a pas malice…

— Souvenez-vous, vous avez prêté serment devant monseigneur l'évêque, devant Dieu lui-même, pour ce cheminement ! Oubliez donc ces broutilles, tout cela n'a guère d'importance et n'aurait même pas dû arriver. »

Gringoire n'était qu'à demi convaincu.

« Comprenez, mon père, j'ai tout de même dépensé sans mesure pour parpayer ce pèlerinage. D'aucuns n'auraient pu venir sans moi. Il n'y a donc pas mal à tenter de récupérer quelques monnaies. Si c'est pour de telles œuvres.

— Si fait. Mais il est aussi juste de dire qu'à chaque chose son temps. Ces jours, pensez à votre âme. Suivez exemple de votre épouse, elle saura vous soutenir.

— Vous devez parler vrai, comme toujours, je m'enrage trop vite.

— Accompaignez-moi, je commençais à grouper les ouailles pour sermonner à propos des Évangiles. »

Gringoire, dépité comme un enfant morigéné par son précepteur, suivit de mauvaise grâce le jeune prêtre sur le pont inférieur. Mais avant de descendre par la trappe, il lança un dernier coup d'œil mauvais vers la dunette, où se trouvait l'insolent Génois.

### Côte campanienne, matin du 14 septembre

Fulco Bota prenait les outils dans ses différentes caisses et les tendait à son apprenti, Ingo, pour qu'il les mette dans la corbeille. Une fausse manœuvre avait entraîné la chute d'une ancre un peu violemment sur le pont, près du gaillard d'avant, et une planche s'était profondément fendue. Il fallait la changer avant que l'endroit ne devienne dangereux.

Depuis qu'ils étaient repartis le matin, ils affrontaient un vent debout les obligeant à naviguer au près. Et le *Falconus* avait une bonne gîte malgré une lourde cargaison, ce qui rendait Ingo malhabile, peu habitué pour l'instant à la vie au large. Pour sa part, Fulco se déplaçait avec la même aisance que s'il était à terre. Mais il n'aimait pas de telles conditions de mer. Les vagues venaient presque de face et des embruns soulevés à chaque rencontre les frappaient tandis qu'ils étaient à découvert. Le sol était de plus glissant et, dans ces conditions, il n'était pas rare que des hommes se blessent ou, encore plus fréquemment, des passagers présomptueux désireux de ne pas rester à l'abri.

« Ne redoute pas, gamin, c'est normal, le vent s'ébroue toujours un peu mi-septembre. Ça devrait ronfler fort avant à l'entour la saint Michel, outre le canal d'Otrante.

— Le navire ne craint pas de verser ?

— Aucun risque avec le vieux Malfigliastro. Il sait la mer. La gîte ne me fait pas souci tant que le gars à la manœuvre s'y connaît. Mire, la voilure a été abrégée pour éviter les problèmes. Et — tout en montrant l'endroit du doigt — le *patronus* est sur la dunette, guettant tout.

— Ce n'est guère moment de faire erreur en ce cas.

— Et encore, en notre office, il nous est possible d'en commettre de légères. Pas les matelots ! J'ai déjà vu d'aucuns se desrompre un bras par un bout lors d'une rafale ou être balancés à l'eau alors qu'ils prenaient des ris[^ris]. Crois-m'en, tu ne regretteras pas d'être charpentier ! »

Tout en parlant, Fulco s'enfonça sur la tête un bonnet de feutre pour se protéger un peu des embruns. Puis, en indiquant de la main à son apprenti de le suivre, il sortit de la loge pour se rendre, le long de la coursive tribord, à l'endroit à réparer. En dehors des matelots sur le qui-vive, prêts à se ruer à la tâche dès que l'ordre leur en serait donné, il n'y avait personne sur le pont malgré un temps assez ensoleillé. Octobono, concentré sur la tension de l'écoute dont il avait la charge, ne fit même pas un signe de tête à leur passage. C'était lorsque les conditions en mer devenaient difficiles que des marins braillards et soûlards comme lui dévoilaient leurs compétences et leur courage.

Fulco et Ingo arrivèrent à l'endroit où ils devaient intervenir, situé juste devant l'accès à la resserre où les ancres étaient rangées. Le charpentier examina attentivement l'éclat et sortit son couteau pour tâter de la pointe l'état des fibres. Il s'en servit rapidement pour délimiter une zone qu'il décida de changer et en montra les dimensions à Ingo.

« Le bois est fort corrompu, mais en une faible étendue, adoncques on va le remplacer en partie. Va me quérir une pièce dans la cale, épaisse de deux pouces. »

Le jeune garçon bondit alors et disparut par la trappe d'accès. Assis sur le pont humide, Fulco commença à tailler au ciseau la partie qu'il souhaitait enlever, chantonnant pour lui-même tandis qu'il arrachait les copeaux. Ingo revint rapidement, la planche en main. Fulco approuva d'un signe de tête le choix fait.

« Tu vas me la planer en sorte qu'elle soit à peine plus large que l'endroit, je l'y placerai moi-même. »

Au fur et à mesure qu'ils avançaient dans leur ouvrage, Fulco prenait le temps d'expliquer ce qu'il faisait ou déléguait les tâches les moins complexes. Il avait à cœur de former des apprentis compétents et n'hésitait pas à montrer des tours de main ou des astuces pour faciliter le travail. En cela, Ingo avait de la chance d'avoir un si bon maître pour apprendre le métier.

Tandis que le charpentier finissait d'assujettir la cale de remplacement, Ingo rangeait les outils et nettoyait l'endroit. Ce dernier demanda :

« Cela fait longtemps que vous besognez en navire, maître Bota ?

— Plus ou moins. J'ai pris la mer lorsqu'on m'a proposé de rallier un groupe en charge des engins d'assaut.

— Vous avez guerroyé ? Vous étiez soudard ?

— Oc, je supervisais la mise en place de structures de protection, le plus souvent. Je gouvernais la construction de tunnels protégés pour assaillir murailles.

— Vous avez bataillé où ?

— J'étais de la campagne des Baléares et de la prise d'Almeria. Mais ça a été mon ultime assaut, j'ai reçu méchante navrure, une flèche qui a failli me coûter le bras. Je me suis alors dit que ça ne faisait pas tout, d'amasser butin et toucher bonne solde, si c'était pour aboutir en linceul. De retour à Gênes, j'ai quéri de l'embauche comme charpentier de bord.

— Ça vous fait pas faute, toutes ces aventures ?

— Un peu, parfois. Mais Octobono est alors là pour me remembrer comme c'était dur. Lui est demeuré matelot bien qu'il sache l'arbalétrie. »

Tandis qu'ils parlaient, ils achevèrent leurs tâches et remballèrent toutes leurs affaires. Le charpentier se leva et posa son maillet dans la corbeille qu'il avait sous le bras.

« Tu repasseras escurer et frotter le pont, que ce soit bien propre. Mais va d'abord remiser les chutes de bois en la loge. »

Tout en avançant, ils regardaient les hommes manœuvrer. Le navire devait louvoyer pour progresser malgré ce vent de face et les matelots passaient beaucoup de temps à régler la tension des voiles à chaque changement de cap. Il s'arrêta près de son ami Octobono et attendit que celui-ci ait fini de superviser son équipe. D'un naturel amical, ce dernier savait motiver ses hommes pour qu'ils travaillent de leur mieux. Il aimait mettre la main à la pâte, se posant en exemple, même lorsque ce n'était, comme là, que pour rouler proprement l'extrémité d'un cordage autour d'un cabillot. Vêtu de braies qui lui battaient les mollets, il ne portait sur le torse qu'une cotte de laine fatiguée, détendue aux coudes. Il la serrait à la taille par un méchant bout de corde où il suspendait l'étui de son couteau. Ses cheveux noirs, frisés avec l'humidité, lui tombaient dans les yeux, lui donnant un air espiègle, presque infantile malgré sa peau brune ridée. Il sentit qu'on l'observait et se retourna vers Fulco, le regard interrogateur.

« Il y a souci ?

— Je voulais juste savoir si la manœuvre continuerait longtemps ainsi.

— On n'avance guère depuis Gênes, adoncques pour ne pas trop dévier du cap il nous faut louvoyer. Ça fait de la besogne, pour sûr, mais moi, vu qu'on me loue à la traversée, j'aime autant tôt arriver. »

Il agrémenta sa dernière remarque d'un clin d'œil complice et d'un claquement de langue puis se concentra de nouveau sur ce qu'il faisait.

Fulco fut vite de retour à sa loge où il avait aussi à faire. Une des poulies ayant lâché la veille, il voulait en préparer une nouvelle pour conserver son stock d'avance. Avant de rentrer dans son petit appentis ranger ses affaires, il leva le nez pour apprécier le ciel. Malgré le vent, le temps s'annonçait assez beau, quelques nuages moutonneux en altitude n'arrivaient pas à obscurcir un bleu d'azur. Il décida donc de s'installer à la porte de son atelier, où il ne gênerait pas, serait à peu près abrité des bourrasques, et pourrait profiter des rayons de soleil.

### Côte calabraise, journée du 15 septembre

Ernaut s'appliquait à avancer avec précaution, s'appuyant de la main à chaque poteau et chaque cloison qu'il pouvait rencontrer. Il n'avait pas envie de renverser le seau d'aisance dans le passage, là où tout le monde était installé. Heureusement pour lui, le bateau n'était plus incliné comme la veille et un bon vent de travers arrière les poussait avec douceur. Il parvint ainsi sans encombre jusqu'au plat-bord, vers l'avant, près des latrines, à un endroit où les rafales sauraient éloigner le contenu de son récipient de la coque. Il le vida, en tapota le fond consciencieusement et se retourna, l'air satisfait et soulagé d'avoir mené à bien sa périlleuse mission.

Il aperçut alors Enrico Maza, qui l'observait depuis le château avant, accoudé au garde-corps. Ayant réussi à l'éviter depuis plusieurs jours et ne trouvant rien de mieux à faire, Ernaut décida de le regarder crânement, montrant par là qu'il ne le craignait pas. À sa grande surprise, le soldat lui sourit, de façon énigmatique, et lui fit un signe amical de la main, quittant son emplacement pour descendre par l'échelle jusqu'au pont. Circonspect, Ernaut attendit, les poings faits, certes déçu d'avoir peut-être vidé un peu vite son seau.

Malgré tout, l'air affable, presque amusé, du guerrier le surprenait et il se demandait si ce sourire était une moquerie ou une tentative de conciliation. L'arbalétrier portait encore sur la pommette les séquelles du coup reçu, plus d'une semaine après qu'il eut été frappé. Il s'arrêta sous la plate-forme, sans s'avancer trop près du jeune colosse.

« Salut à toi, Ernaut ! Je voulais justement t'entretenir…

— Pour quoi ?

— Rien de bien méchant. Simplement te garantir que je ne te tenais pas grief de cette stupide histoire. J'avais trop pinté de leur méchant vin et ça me rend parfois un peu…

— Ouais, j'ai vu ça.

— Tu m'as empêché de graves embarras, donc je devrais t'en rendre grâces. Vu les circonstances — il se tâta la joue endolorie — je n'irai pas si loin. Mais pour moi, nul besoin de se haïr. »

Joignant le geste à la parole, il tendit la main vers Ernaut, attendant de son interlocuteur qu'il fasse la seconde partie du chemin. Ernaut fit mine d'hésiter un instant, s'approcha lentement puis plus franchement et conclut cette offre de paix énergiquement.

« En un sens, je préfère ça, il aurait été malaisé de s'esquiver tout le reste du voyage.

— Si je devais garder rancune à tous ceux qui m'ont contusé… » Le soldat secoua la tête en dénégation, soulevant un sourcil d'un air narquois. « N'en devisons plus, pour ma part, c'est déjà mis en oubli, ou ça le sera lorsque ma joue sera retournée comme avant. » Il toucha machinalement sa blessure, plissant les yeux sous la douleur. « Ceci dit, bravo, tu sais mailler. Il y en a peu à pouvoir se vanter de m'avoir fait brouter la boue.

— Vous étiez enivré.

— Ça oui ! Je n'ai même su qui m'avait assommé que bien après. Enfin, bon, quand tu auras le temps, j'aurai bon gré à te narrer quelques mémoires où j'ai eu le dessus. Je connais des trucs si tu as besoin d'affronter quelqu'un autrement que par-derrière. »

Il agrémenta sa dernière remarque d'un clin d'œil complice, arrachant un sourire à Ernaut qui répondit.

« Avec grand plaisir ! Mon seul souci, c'est mon frère, il me rebat sans cesse les oreilles que j'aime trop batailler. Alors s’il a soupçon que je m'entraîne…

— On lui dira que tu m'aides à m'exercer et que j'ai métier à adversaire costaud. »

Ernaut se grattait la tête, l'air indécis malgré des yeux brillants de plaisir.

« M'ouais, je ne sais s'il consentira, mais après tout…

— De vrai, garçon, agrape toutes les occasions. Moi j'ai toujours œuvré ainsi. Et à ce propos, je dois te laisser… »

Tout en parlant, il regardait en direction du château arrière, d'où était sorti Mauro Spinola en compagnie de Sawirus de Ramathes et de Ja'fa al-Akka, tous trois en grande conversation. Il fit un dernier signe de tête amical à Ernaut et se dirigea lentement vers le groupe, s'efforçant de ne pas trop se faire remarquer. Les trois hommes s'exprimaient avec passion, Mauro notant de temps à autre quelques éléments sur une tablette de cire qu'il rangeait dans une bourse à sa ceinture. Comme ils s'étaient insensiblement rapprochés du centre du navire, Enrico put s'appuyer sur le mât pour entendre tout en donnant l'impression de regarder ailleurs.

« Comprenez-moi bien, maître al-Akka, je serais fort aise de m'associer à certaines de vos traversées depuis l'Outremer, mais j'ai surtout nécessité de partenaires œuvrant dans les ports égyptiens, en particulier Alexandrie. J'ai possession d'un navire qui pourrait y charger marchandises comme soie ou épices des Indes.

— Je crois savoir que Génois y sont fort mal accueillis, en raison de l'assistance que vous apportez souventes fois au roi de Jérusalem à l'encontre des places fortes du calife al-Faiz.

— Ne vous souciez de cela, Al-Salih Tala'i‘ ben al-Ruzzayk[^alruzzayk] est homme de finesse. J'ai toujours eu autorisation à commercer avec les Égyptiens. Mais il est vrai qu'ils aiment à nous voir le moins.

— D'où l'intérêt pour vous d'avoir des associés comme le jeune Ja'fa ou moi-même.

— Exact ! »

Tout en devisant, Mauro jetait des coups d'œil aux alentours, car il avait la désagréable impression qu'on l'épiait. Il aperçut l'épaule du soldat dépassant du mât et pensa avoir trouvé la source de ce qui le dérangeait. Il décida donc d'écourter cet entretien.

« Oyez, repensez cela tout votre soûl. Nous demeurons à bord encore moult semaines et ensuite j'escompte hosteler en Outremer jusqu'après Pâques prochaines. Nulle hâte. »

Les deux marchands le saluèrent poliment et se déplacèrent vers l'avant du navire, continuant à deviser entre eux. Pendant ce temps, Mauro fit le tour du poteau pour retourner à sa cabine, afin d'identifier l'indiscret. Ne s'attendant pas à être abordé si directement, Enrico décida d'assumer. Il ne feignit pas de faire autre chose et fixa le vieil homme, les bras croisés. Il le salua néanmoins de façon marquée, en inclinant la tête. Mauro Spinola s'avança vers lui, l'air contrarié, les yeux lançant des éclairs.

« Qui croyez-vous tromper ? J'ai horreur des espies ! »

Enrico arbora un visage sincèrement surpris puis une mine tout à fait désolée.

« Je n'escomptais pas être indiscret, maître Spinola. Il m'a semblé que vous escortiez jusqu'au revoir ces deux hommes. J'attendais juste la fin pour demander audience. »

L'expression de Spinola n'était guère engageante et il ne faisait rien pour cacher sa contrariété, feinte, ou réelle.

« Eh bien, votre trait a touché, je suis tout ouïe ! »

Le soldat fut décontenancé d'une telle approche et se tordit les mains, un peu confus.

« C'est-à-dire que… je voulais savoir si vous ne cherchiez pas à louer ou si vous n'aviez pas connaissance d'aucun intéressé par un bon arbalétrier, vétéran.

— Ce n'est que pour cela que tu as le front de m'aborder ?

— Euh… oui. Il n'est guère usuel pour soudard comme moi de pouvoir approcher notable tel que vous. Je ne vous ai toujours avisé que de fort loin, même en Espaigne. J'y ai bataillé sous la gouverne du consul Balduino, à Almeria. J'y ai même gagné cette cicatrice. »

Mauro Spinola demeurait impénétrable, son regard d'aigle parcourant le soldat comme s'il devait en estimer la valeur. Il fit claquer sa mâchoire plusieurs fois, doucement, puis plissa les yeux.

« Je vois. Tu es vaillant, ça se devine. Mais qu'es-tu prêt à faire pour celui qui te louera ?

— Je le servirai droitement, comme j'ai toujours fait pour Gênes et la *Compagna*. »

Mauro semblait un peu apaisé et lançait des regards autour de lui, les lèvres constamment en mouvement comme s'il s'efforçait de mordre quelque chose.

« Je vais y penser. Il y a en permanence à quoi occuper des soudards de valeur. Tu viendras indiquer ton nom à mon escrivain.

— Mille grâces, maître Spinola, j'espère pouvoir vous repayer de votre aide.

— Ne t'en soucie donc pas, de sûr nous trouverons moyen. D'ici là, contente-toi d'obéir et nous aviserons. »

Sans plus de cérémonie, le vieux marchand fit alors volte-face et repartit vers le château arrière où se situait sa cabine. Tandis qu'il s'éloignait, Enrico laissa échapper un soupir de soulagement. Son voyage s'annonçait bien. Le vieux Spinola était un homme influent, dont le réseau de connaissances était réputé. Quand il se retourna, il vit Ernaut assis sur la plate-forme du gaillard d'avant, les pieds pendant dans le vide au-dessus du pont à travers le garde-corps. Enrico ne put s'empêcher de lever le pouce vers lui en signe de satisfaction, en l'accompagnant d'un rictus victorieux.

### Côte calabraise, soir du 15 septembre

Empoignant le tabouret, Ugolino tenta de le poser à l'envers sur la lourde pile de meubles qu'il portait déjà. Heureusement que le navire, à l'ancre pour la nuit, ne bougeait pas trop, car il avançait de façon mal assurée. Il ne voyait pas devant lui et était obligé de cheminer en crabe, la tête de biais, pour ne pas risquer de se prendre les pieds dans un cordage ou de heurter d'autres passagers. Parvenu aux abords de l'escalier qui plongeait depuis la plate-forme du gaillard d'avant, il chercha du pied l'emplacement de l'ouverture pour y descendre à l'aveugle. Octobono, en pleine inspection des haubans, arriva à ce moment-là et vit le vieil homme le pied en avant, tâtonnant dans le vide.

« Arrêtez, malheureux, je vais vous assister ! »

Et, joignant le geste à la parole, il attrapa le coffre et le tabouret d'un mouvement vif. Surpris du poids de l'ensemble, il comprit instantanément pourquoi le valet peinait comme un forçat. Même pour lui, cela représentait une belle charge.

« Grand merci, jeune homme. J'avais peine à voir mes pieds je dois avouer….

— J'ai connu mulets moins bâtés que vous, sauf votre respect, l'ancien. »

Le valet sourit et commença à descendre doucement, de façon à ne pas glisser sur les marches très raides.

« Oh, il n'y a pas outrage. Bien aimable à vous de me prêter le bras ! Vous avez déjà tant à faire avec ce navire.

— Pas vraiment, j'ai achevé mon labeur pour l'instant. Je ne suis pas de quart. »

Ugolino se mit à trottiner pour faire mine de se dépêcher. Octobono ne put s'empêcher de s'esclaffer devant le spectacle comique du vieux valet glissant sur le pont de bois, dans ses vêtements trop grands pour lui.

« Patientez, l'ancien ! Il semblerait que vous avez mille diables de l'Enfer qui vous lancinent le cul ! »

Ugolino se retourna, un sourire confus sur le visage.

« Je me hâte car il va bientôt être temps pour maître Embriaco de souper. Il a besoin de ses meubles en sa chambre, ainsi que de certains documents de ce coffre.

— Il n'avait qu'à pas les sortir, voilà tout ! Il peut bien rester quelques instants sans sa paperasse. »

Le vieil homme le regarda, d'un air mi-sévère mi-professoral.

« Ce n'est pas à valet tel que moi de jauger le bien-fondé des commandes du maître. Si je me mettais à jauger l'intérêt de chacune de ses demandes, c'en serait fini de tout ! Si vous n'êtes pas en accord, remettez-moi les affaires, merci pour votre aide et le bonsoir ! »

Ébahi par une telle véhémence, Octobono en écarquilla les yeux.

« Coi, l'ami ! Je disais ça comme ça, sans y mettre malice envers vous ou votre maître ! »

Devant l'évidente sincérité de son interlocuteur, Ugolino parut se calmer un peu et se racla la gorge.

« Il me faut vous dire, quand on est vieil homme, sans bien ni avoir, sans famille, on est aise d'avoir un toit et maître pas trop sévère. J'ai connu d'aucuns valets bastonnés matin et soir, ou qui soupaient de brouets rallongés d'eau. Maître Embriaco me nourrit bien, me donne chauds vêtements et me paie d'honnêtes gages. Alors cela ne m'enjoie pas de le voir raillé, ni d'être incité à le servir moins prestement. »

Le marin réalisa qu'il avait touché une corde sensible et prit un air contrit.

« Je vois, excusez, l'ancien. J'ai suffisamment navigué sous  mauvais patrons pour savoir comme on peut être aisé d'en avoir un correct. »

Le vieil homme souffla, se mordant les lèvres puis reprit son chemin, un peu moins vite néanmoins. Arrivé au pont inférieur, il avança jusqu'à la chambre d'Ansaldi Embriaco et frappa sur l'huis, s'annonçant à son maître. Le loquet de bois glissa derrière les panneaux et la porte s'ouvrit. Ugolino recula un peu et, de la tête, fit signe au marin de rentrer le premier. Celui-ci s'exécuta sans rien dire et ressortit immédiatement, le temps pour le valet de ranger son chargement. Pendant tout ce temps, le marchand demeura assis sur son lit, sans un mot, la lumière des lampes posées sur les étagères en hauteur laissant son visage dans une quasi-obscurité.

« Je vous porte votre souper à présent ou vous préférez espérer un peu ?

— Plus tôt sera mieux !

— Fort bien, dès que le cuisinier a fini de le préparer, je vous le porte. Je peux patienter aux cuisines ou vous aurez nécessité de moi d'ici là ?

— Tu peux t'occuper comme bon te semble ! »

Le valet salua et se retira dans le couloir, où Octobono le laissa passer. Ils avaient à peine fait quelque pas que le bruit du loquet se fermant claqua dans le petit espace.

« Sacrément prudent ton maître !

— C'est homme important et donc discret.

— Un peu étrange tout de même de demeurer enclos dans sa chambre.

— Il tient à ce qu'il n'y ait homme qui fouille ses affaires, voilà tout ! »

Comprenant qu'il s'engageait encore sur une voie dangereuse, Octobono préféra abandonner et changer carrément de sujet, pour un autre qu'il affectionnait plus particulièrement.

« Allez, puisque ta besogne est achevée, partageons un godelot de vin, histoire de s'eschauder avant la nuit. »

Le vieil homme s'arrêta et, se retournant, fit un large sourire au matelot.

« Enfin parole sensée sort de ta bouche ! »

### Golfe de Gioia, midi du 16 septembre

Régnier d’Eaucourt était tranquillement allongé sur son matelas, qu'il avait installé au soleil sur le pont, contre la paroi du château avant. Les yeux mi-clos, il regardait tantôt les jeux d'ombre sur la voile tendue sous l'effet d'un vent soutenu, tantôt les nuages cotonneux dans le ciel lumineux. Il sentait la plus petite oscillation du navire sur les vagues, le moindre grincement des pièces de bois entre elles. Il venait de prendre son dîner, en milieu de matinée, et appréciait de pouvoir s'octroyer un peu de bon temps à lézarder pour la sieste. Il était juste bien, pas trop échauffé par le soleil, ni trop refroidi par la brise marine qui les propulsait avec tant d'efficacité. N'eut été cette odeur de sel omniprésente, il aurait presque pu trouver l'endroit parfait. Une voix le tira hors de ce cocon.

« Il semble que nous ayons eu semblable idée, messire d'Eaucourt ! »

Il releva à peine la tête et reconnut Ansaldi Embriaco, une épaisse couverture sur l'épaule, un sourire aux lèvres, la main au-dessus des yeux pour se protéger de la luminosité.

« Vous permettez que je me couche à l'entour ? Ou préférez-vous demeurer tranquille ?

— Non, faites, quelque compagnie onques n'est désagréable. »

Le marchand s'installa à côté du chevalier et s'étendit à son tour.

« Quand Ugolino m'a indiqué la bonne chaleur et le ciel d'azur, j'y ai vu occasion de me décloîtrer.

— De vrai, vous ne départez de votre cabine que peu. Vous avez tant de besogne ?

— J'en avais quelque peu, si fait, qu'il me fallait impérativement parclore avant Messine. Voilà désormais chose faite !

— Vous avez grand courage d'être resté tapi alors que les contrées défilant sous nos yeux étaient tant magnifiques. J'avais déjà goûté le voyage aller, mais celui-ci me dévoile nouveaux lieux, vraiment superbes.

— Je navigue depuis moult années et connais bien tous ces pays. Mais pour moi rien n'égale l'arrivée en Outremer, le port englouti de Tyr, les tours de Saint-Jean d'Acre, la rade de Césarée, le phare d'Alexandrie. Enfançon, j'en rêvais. Ma parentèle y possède puissantes attaches, et père avait de nombreux associés syriens. »

Le chevalier ressentait une sympathie naturelle pour le jeune marchand. Lui aussi s'était pris de passion pour les terres chaudes de l'Orient.

« Pour ma part, dit Régnier, je n'y ai jamais songé avant que le roi Louis ne prenne la croix[^secondecroisade2]. J'ai fait choix de le suivre sans vraiment savoir. J'avais déjà pèleriné à Saint-Michel au péril de la Mer[^montsaintmichel], mais n'avais jamais passé outre les Flandres au nord et Bourges au sud. Et au final, je suis demeuré, estimant que ma tâche n'était pas achevée. En à peine dix années, j'ai bonne accointance de tous ces endroits que vous évoquez, sans jamais m'être douté avant que c'étaient vrais lieux, et pas seulement noms en les Saintes Écritures. Cités et hameaux, avec laboureurs dans les champs, comme dans ma Picardie natale. »

Ansaldi l'interrompit, le sourire aux lèvres.

« Sauf que bœufs y ont bosses sur le dos et ne s'abreuvent de plusieurs jours. »

Cette remarque amusa Régnier, qui ne s'attendait pas à une telle plaisanterie du commerçant, si sérieux habituellement. Il hocha la tête, les yeux rieurs.

« J'en oublie presque ce genre de détails… »

Les deux hommes se tinrent cois quelques instants, profitant du calme du navire qui grinçait sous la poussée du vent. L'apathie avait gagné les rangs des matelots et quelques ronflements sonores apportaient la preuve de la sérénité de l'équipage. Un bref moment de félicité, le vent travaillant pour eux, les poussant sur une mer amicale. Quelques rires d'enfants, occupés à jouer sur le pont, égayaient l'atmosphère. Ce fut Ansaldi qui rompit la magie de l'instant, presqu'à contrecœur.

« Je suis heureux que des hommes tels que vous aient fait le passage. Il me plairait de voir les ports d'Outremer accueillir grand nombre des nôtres. Même les plus réservés négociants de la *Compagna* ont compris que là réside l'avenir. Nul besoin d'attendre que les marchandises arrivent à nous, il faut y aller sus, encontrer les Damascènes, les Aleppins en leurs cités. J'espère fort contribuer à cet élan.

— Qui, en outre, renforcera nos conquêtes. C'est certain.

— Si fait, plus de navires mèneront plus de pèlerins et un sentiment de voisinage accru. Vous disiez tantôt que vous n'aviez guère conscience de tels endroits. Avec force colons, comme ce jeune homme à l'allure de Goliath, qui auraient parentèle au loin, cela affermirait les liens par-delà la mer, la bien nommée, la Mer intérieure. »

Tout à l'enthousiasme qu'il mettait dans son discours, Ansaldi avait relevé le buste et accompagnait ses paroles de gestes démonstratifs. Régnier prenait conscience à quel point le marchand aimait son métier et l'avait en haute estime. Il ne s'était jamais vraiment intéressé à cette profession, se résumant selon lui à se contenter d'acheter au moins cher pour revendre au plus offrant. Mais il se rendait compte qu'une passion commune pouvait les habiter.

Régnier allait répondre à Ansaldi lorsqu'un maillet tomba entre les deux hommes, avec un bruit sec qui les fit sursauter. Immédiatement, ils regardèrent au-dessus d'eux et virent le visage d'Ingo, le jeune apprenti, souriant d'un air confus.

« Je suis désolé, maîtres, je l'avais placé à mes pieds et il a roulé. J'espère qu'il n'y a pas eu mal. »

Ansaldi fronça les sourcils.

« Non, par chance. Tu aurais très bien pu occire l'un d'entre nous. »

Fulco Bota se pencha à son tour par-dessus la balustrade.

« Excusez mon apprenti. Il n'a pas encore compris que sur un navire, tout branle. Il se pense toujours à terre.

— Je ne vous avais pas ouï besogner, d'où notre grand effraiement.

— J'arrive juste, avec l'outillement, c'est en préparant que le maillet a chu. »

Ansaldi soupira bruyamment, se frottant le visage comme s'il avait été extrait trop brutalement d'un sommeil enchanteur.

« Il va donc nous falloir oublier notre projet de reposée…

— Non pas, demeurez ! Rien ne presse en ce labeur. Je n'avais pas noté votre présence. Je vous prie de nous excuser pour le trouble. »

Les deux têtes des charpentiers disparurent alors de leur champ de vision. Coupé dans son élan, Ansaldi s'allongea de nouveau, en souriant à Régnier, et ferma les yeux. Voyant que son compagnon s'était décidé à faire une vraie sieste, Régnier estima qu'il ne devait pas être en reste. Il s'installa confortablement et se replongea dans ses rêves éveillé.

### Golfe de Gioia, soir du 16 septembre

Le vin s'écoulait rapidement du pichet de céramique grise et remplit le gobelet en quelques secondes, manquant de peu de le faire déborder. Enrico Maza leva son godet pour redresser le broc.

« Stoppe-là, vérolé, tu va en renverser ! »

Octobono arrêta de verser et regarda dans la cruche d'un air espiègle.

« Il est encore bien empli, sois rassuré…

— Maigre motif pour en gâcher ! Je le préfère en mon gosier qu'au sol. »

Les deux hommes s'étaient joints à la veillée, sur le pont principal, sous les étoiles. Plus familiers les uns des autres, passagers et matelots se cotoyaient un peu plus et envisageaient de partager les mêmes espaces, à défaut de s'amuser ensemble. La nuit était assez fraîche, mais la voûte constellée était splendide. Un mince croissant de lune s'esquissait à peine, laissant apparaître la Voie lactée dans toute sa splendeur. Les marins les moins frileux et les plus chaudement habillés s'étaient donc retrouvés près du canot pour discuter un peu avant d'aller se coucher.

La mer était presque d'huile et reflétait capricieusement les étincelles scintillant dans le ciel. Le clapotis des vagues qui remuaient lentement le navire s'entendait par intermittence et même le *Falconus* demeurait quasi silencieux, les bois au repos après une journée à subir des tractions et des compressions, gémissant continuellement par craquements, chuintements et grincements. La seule activité au loin était celle du Stromboli, faible lumière rougeoyante dont les accents de colère rehaussaient l'éclat de temps à autre. Mais son courroux n'inquiétait pas les hommes, en sécurité si loin de sa portée.

Buvant à grands traits, Octobono vida rapidement son vin et se reversa de quoi tenir la conversation.

« Qu'est-ce qui t'a décidé à risquer ta chance vers l'est ?

— Plus d'aventures possibles ! Répliqua Enrico Maza. Jusqu'à ce jour d'hui, dame Fortune m'a souri, j'espère qu'elle m'ensuivra au centre du monde[^centredumonde]. J'ai reçu  nouvelles d'un mien compagnon, le fils Marzonus…

— Je vois, son père était cordier de grand talent !

— Voilà ! Il s'est repatrié à Gênes après la capture de Tortosa. Mais guère, car peu après il a mené femme et enfants avec lui en Outremer. Il s'est loué un temps au comte de Tripoli. Bonne solde, mais rudes mêlées qui lui ont pris une jambe. Désormais, il commerce un peu et goûte la vie.

— Ça doit être bien agréable…

— D'aperdre jambe ? »

La remarque arracha un sourire à Octobono.

« Mais non, fils à moine ! Avoir paisibles journées, sans craindre pour sa peau…

— Certes ! Il m'a fait écrire une lettre où il me disait faire régulièrement visitance à Damas et Homs, malgré les guerres. Il s'est même mis à parler la langue des Syriens.

— Comme quoi tout arrive ! D'aventure, tu te retrouveras à combattre du côté des païens. »

Maza fut tenté de lancer un regard mauvais mais il savait qu'Octobono aimait à lancer ses saillies sans trop réfléchir ni aux conséquences ni à leur véracité. Il aimait plaisanter et provoquer, sans penser à mal. Le soldat se contenta donc de hausser les épaules.

« Je crois pas, non. Trop des miens ont été occis par pirates maures. Après l'Espaigne[^espagne1148], et ces mêlées ensanglantées, je ne me vois pas passer de l'autre côté. »

Le regard fixé au fond de son gobelet, comme si la vérité se trouvait dans la mixture de mauvais vin coupé d'eau qu'il ingurgitait, le marin maugréa quelque peu son accord avant de continuer de façon plus compréhensible.

« Sacré gâchis quand même cette histoire !

— Pourquoi ? On est revenus emplis de butin et deux cités furent forcées…

— Ouais, mais à quel prix ? Prends Fulco : lui a tout perdu ! Plusieurs membres de sa parentèle avaient négoces en cours et ils n'ont pu les achever, avec tous les bateaux à guerroyer. Certains oncles et cousins, avec qui il devait s'associer pour construire un navire, ont été complètement ruinés. Sans cette guerre, il serait *patronus* d'une nef et œuvrerait à la prospérité des siens au lieu de continuer à servir. »

Sans conviction, Maza opina silencieusement avant de s'offrir une gorgée. D'une moue, les sourcils relevés, il fit mine de balayer l'argument de son compagnon, qui s'entêta pourtant.

« Dans cette histoire, il se trouve plus de perdants que de gagnants. Même la *Compagna* a beaucoup souffert. Ceux qui avaient du bien et qui ont pu prêter à bon taux pour renflouer ont encore une fois gagné. Mais les autres ont été laissés au bord du chemin. »

Un matelot resté muet jusque-là ne put s'empêcher d'intervenir, la voix rendue forte par l'abus de vin.

« Ils n'avaient qu'à se défriper le cul ! Moi j'étais avec la flotte ! Et je ne le regrette pas ! Les tard couineurs, ils me font bien rigoler ! Mon vieux disait qu'à faire sa poule, on finit croqué par goupil[^goupil]. »

Un peu gêné par un soutien aussi vindicatif, Enrico continua plus posément.

« Si fait, c'est toujours le loup qui gourmande la biche, jamais l'envers. Moi j'ai eu ma part du butin, en monnaies sonnantes et bon poivre[^poivre]. J'essaie de faire grossir le tas, pour mes vieux jours. »

Choisissant d'ignorer l'ivrogne dodelinant de la tête, le nez dans son gobelet, Octobono se frotta le visage, un peu embarrassé.

« Bien sûr que toi et les autres soldats méritez vos récompenses. Mais ce me semble, et pas qu'à moi, qu'on a oublié que Gênes, c'est pas seulement les consuls et familiers, les Pedegola, della Volta et autres Embriaco. »

Enrico fronça le nez, comme gêné par la mention de ces noms.

« Attention, coquin, tu vas t'attirer soucis ! Le jeune Embriaco rôde comme chatte en ardeur autour du vieux Spinola. S'ils t'entendent…

— Peuh ! Je ne m'enfriçonne guère ! Un bon marin trouve toujours de l'emploi.

— Parce que tu penses que tu es un bon marin, toi ? »

Le soldat choqua son gobelet contre celui de son compagnon puis en avala le contenu avec entrain, s'essuyant la bouche sur le revers de sa manche. Il tapa amicalement le matelot sur l'épaule, de façon bourrue, en souriant. L'équipage avait entonné un chant de marin, qui aurait horrifié n'importe lequel des pèlerins à bord. Mais seuls les hommes de mer étaient désormais sur le pont et ils célébraient avec joie le plaisir d'être ensemble, d'avoir tenu un jour de plus. Octobono et Enrico mêlèrent finalement leurs voix à cette chorale indisciplinée, plus enthousiaste que mélomane.

### Détroit de Messine, fin d'après-midi du 17 septembre

Annonçant son coucher, le soleil commençait à se rapprocher des reliefs abritant Messine. Mais le pilote était déjà à la barre et le *Falconus* serait amarré dans le port avant le crépuscule. Depuis leur arrivée dans le détroit, tous les marins étaient sur le qui-vive, attentifs aux instructions. L'un d'eux, à l'avant, lançait régulièrement une sonde tandis qu'ils avançaient pour s'assurer qu'ils n'auraient pas de problème et suivaient les bons chenaux. Les courants étaient particulièrement traîtres dans cette zone, et si la manœuvre nord-sud était facilitée par un fort vent arrière très fréquent, elle demandait une réactivité sans faille des membres d'équipage.

 Gringoire la Breite était accoudé sur la rambarde du gaillard de proue, admirant le paysage. À ses côtés, Ja'fa tournait le dos à la Sicile, écarquillant les yeux pour voir la côte de Calabre sous les derniers feux du soleil. Guilleret, le solide Champenois tapa impatiemment du poing sur le plat-bord.

« Quel plaisir de bientôt pouvoir marcher sur sol ferme ! Quand je pense que nous n'escalerons plusieurs semaines durant, après Otrante !

— Si seulement les affréteurs avaient quelque négoce en cours dans d'autres ports…

— De sûr, je ne vois que ce motif pour les inciter à s'arrêter un peu plus : s'enrichir. Mais ils ne commercent pas avec n'importe qui. Si on n'appartient pas à leur petit monde… »

La dernière phrase avait été lancée avec morgue, ce qui incita le jeune Syrien à se retourner vers son interlocuteur. Il s'accouda à ses côtés, interrogateur.

« Vous avez eu maille à partir avec eux ?

— Si seulement ! J'ai fait proposance au jeune Embriaco de travailler de concert. C'est tout juste s'il ne m'a pas ri au nez, cette hommasse !

— Étonnant, je le pensais à guetter toutes les bonnes affaires. »

Ja'fa se frotta le crâne de la main et réfléchit quelques instants, penchant la tête d'un air concentré.

« Nul mal dont bien ne vienne, vous encontrerez sans doute meilleur partenaire… »

Gringoire releva le buste, l'air enthousiaste.

« Vous pourriez être intéressé ? »

Le jeune négociant comprit l'imprudence de sa précédente remarque. Il fit un sourire forcé et répondit en appuyant son discours de hochements du chef.

« J'aurais été bien aise de vous acheter vos laines, mais je ne viens jamais plus à l'occident que Gênes. Même, je pense fort à me dédier à la Méditerranée orientale. »

L'enthousiasme du Champenois retomba aussi vite qu'il était venu. Il arbora une moue inexpressive, entre la grimace et le sourire, puis s'appuya de nouveau sur les coudes. Les deux hommes se tinrent quelques instants silencieux. Gringoire inspira bruyamment puis étendit le bras de façon démonstrative vers la côte.

« Malgré le prodigieux ennui à bord, le passage égaye bien l'œil. Mon curé, Aubelet, ne cesse de me dire que je ne suis pas pèlerin assez recueilli, mais comment rester insensible devant pareille vue ? »

Ja'fa inspira à grands traits l'air frais, acquiesçant de tout son corps. Continuant à dévider ses pensées à voix haute, Gringoire prit soudain un air ennuyé.

— Le souci qui me pèse tant à bord, au final, ce sont ces maudits biscuits ! »

La remarque arracha un rire à Ja'fa, qui ne pouvait qu'être d'accord.

« Surtout avec de la viande séchée ou du hareng salé !

— Qu'est-ce que je ne donnerais pas pour une oie bien apprêtée, soigneusement poivrée !

— Lorsque vous serez au royaume, goûtez donc le *hays* ! »

Gringoire fit des yeux ronds et une moue interrogative, mais Ja'fa ne lui laissa pas le temps de poser sa question.

« Des boulettes de dattes, à la pistache et aux amandes. Ça se conserve assez bien pour en prendre un peu en voyage.

— Parlant mangeaille, nous devrions interroger Gandulfo, le cuisinier, s'il sait ce qu'on pourrait acheter à Messine demain.

— Vous croyez qu'il sera bon conseil ? Certains passagers n'en sont guère satisfaits.

— Parce que ce sont des souillards, voilà tout. C'est un gentil gars. Certainement pas le plus grand queux ici-bas, mais il connaît la différence entre ail et oignon. »

Disant cela, Gringoire, le regard amusé, tapota sa narine d'un index malicieux. Un des matelots monté sur la plate-forme les interrompit alors, car ils avaient besoin de tout l'espace pour amener les voiles et apponter en toute sécurité. Obtempérant de bonne grâce, les deux voyageurs rejoignirent rapidement la petite cuisine installée à la proue du navire, dans le château avant. Lorsqu'ils y entrèrent, ce fut pour y trouver Ernaut et Ganelon qui avaient eu apparemment la même idée. Voyant que tout le monde ne tiendrait pas dans un espace aussi confiné, Ja'fa abandonna les gourmands et descendit sur le pont des passagers. Il retrouva sa modeste loge de toile et s'allongea sur son matelas, en attendant que le bateau stoppe.

Il était impatient de revoir les siens, à Saint-Jean d'Acre. Il était parti depuis plusieurs mois et revenait des idées plein la tête. Leurs partenaires habituels à Gênes ne semblaient plus être vraiment fiables, et il était désormais convaincu qu'il devrait aller voir ailleurs, dans de nouveaux ports et des cités inconnues. Pressé de rentrer chez lui, il se projetait pourtant déjà dans ses voyages futurs.
