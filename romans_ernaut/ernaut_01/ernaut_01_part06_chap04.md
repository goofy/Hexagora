## Chapitre 4

### Messine, matin du 18 septembre

Ugolino refaisait tant bien que mal le lit de son employeur, malgré l'exiguïté de l'endroit. Déployant des trésors de souplesse, il tentait de ne pas déranger Ansaldi qui rangeait des documents dans un petit sac de cuir brodé. Ayant apparemment terminé, le marchand se leva, repoussa son tabouret sous la table lui servant de bureau et prit le couvre-chef de feutre bleu qu'il affectionnait tout particulièrement. Récapitulant une dernière fois sa préparation mentalement, il lissait en même temps la cotte en laine qu'il avait revêtue par-dessus sa tenue habituelle. D'une teinte rouge profond, elle était décorée de broderies fines et d'allure splendide, annonciatrice de son importance ou, du moins, de celle qu'il voulait s'accorder.

« Je te laisse, Ugolino. Veille à bien clore la porte et n'ouvre à personne, en aucun cas. Avec cet arrêt au port, un fripon peut être tenté de venir rober l'un ou l'autre passager.

— Partez tranquille, maître Embriaco, je ne bougerai pas de là et ne glisserai la chevillette que pour vous. »

Ansaldi allait répondre quelque chose, pourquoi pas remercier ou féliciter son valet, mais ne trouva rien d'opportun à dire. Il fit donc claquer ses gants dans sa main gauche, se redonnant une contenance, puis déverrouilla le loquet et sortit dans le couloir sans un mot. Ugolino bloqua la porte aussitôt, fermant la targette bruyamment afin de bien montrer qu'il avait pris ses instructions au sérieux. Ce qu'Ansaldi ne vit pas, c'est que son serviteur se jeta alors sur le lit et s'installa confortablement sur la couche, les bras sous la tête, prêt à une sieste de plusieurs heures.

Le négociant monta avec vivacité l'escalier menant sur le pont et s'arrêta auprès de la trappe, regardant de droite et de gauche. Avisant un des marins assis près de la passerelle d'accès, il s'approcha de lui tout en enfilant ses gants. L'homme s'efforçait péniblement de recoudre une de ses chausses, sans grand talent malgré un soin évident.

« Dites-moi, ami, avez-vous vu passagers descendre du bord déjà ?

— Pour sûr, dès tôt ce matin. Un petit groupe accompagné de Gandulfo pour se ravitailler. Et puis moult pèlerins ont suivi, qui voulaient ouïr messe en la cathédrale.

— Et le clerc, pas très grand, rond de visage ?

— Je ne pourrais en jurer, mais je crois que oui… »

Attendant une nouvelle question, le matelot finit par comprendre que l'entretien était terminé. Il se pencha donc derechef sur son ouvrage, tirant la langue tandis qu'il s'appliquait à planter l'aiguille. Sans un mot de plus, Ansaldi descendit du bord en quelques enjambées. Au moment de quitter la passerelle, il enfila des socques de bois pour ne pas salir ses chaussures de cuir fin dans la boue des rues. Avant de s'éloigner, il regarda dans toutes les directions pour voir si Herbelot n'était pas visible.

Avec sa petite taille, il était peu probable qu'il l'aperçoive, mais Ansaldi voulait s'assurer qu'il n'était pas dans les environs. Il lui semblait également sentir sur lui un regard insistant, mais il n'arrivait pas à mettre un visage sur cette impression. Il hésita un instant à remonter sur le navire pour demander à un marin de l'escorter. Richement habillé comme il l'était, il pouvait intéresser un détrousseur, même en pleine journée. Mais il détestait devoir s'encombrer d'un domestique pour aller et venir à sa guise. Il se décida finalement à prendre la route.

La matinée était encore jeune et les quais étaient envahis des odeurs du poisson frais ramené par les pêcheurs. Heureusement, le *Falconus* avait abordé un peu à l'écart du port de pêche et il put ainsi avancer sans devoir affronter la marée humaine criante et malodorante qui déchargeait, préparait et vendait les prises. Il bifurqua très vite pour monter dans la ville haute où il savait pouvoir rejoindre l'office d'un de ses contacts commerciaux. Le parcours dans les rues parallèles à la mer était moins difficile que sur les quais embouteillés, bien que la plupart des voies fussent encombrées d'étalages de boutiques envahissantes, de chalands peu pressés et d'ânes croulants sous des paniers.

Lorsqu'il déboucha sur une place dégagée où se tenait un marché de fruits et légumes, il aperçut des silhouettes familières. Le jeune colosse avait l'avantage d'être facilement repérable, dépassant d'une tête auréolée de blond très clair les gens qui l'entouraient. Transportant un nombre incroyable de sacs et de paniers, il était apparemment en train de faire des achats. Ansaldi se faufila au milieu des ménagères et des badauds jusqu'à s'approcher de l'étal de maraîcher examiné par les voyageurs. Il les interpella d'un salut sonore.

Ernaut, et Lambert à ses côtés, se retournèrent d'un même mouvement et marquèrent un temps, surpris de se voir ainsi abordés par ce marchand ordinairement si discret et quelque peu hautain. Il arborait pourtant un sourire amical, quoique peut-être légèrement forcé. Ils rendirent le salut puis attendirent, un peu interloqués. Ansaldi regarda les sacs sur Ernaut comme s'ils lui apparaissaient soudainement.

« Vous nouvelez vos provisions ? »

Poli malgré sa surprise, Lambert fut le plus vif à répondre.

« Oui, voilà rare occasion avant la longue traversée. Mais nous avions espoir de dénicher plus de fruits. La saison commence à être trop avant… »

Ernaut ne voulait pas demeurer en reste et, tout en assujettissant un sac sur son épaule, ajouta d'une voix forte :

« Nous aidons les moins vaillants, achetons pour d'autres passagers… »

Le Génois semblait également vaguement mal à l'aise.

« Je vous conseille les dattes, ça se conserve très bien. »

Les deux frères opinèrent, ne sachant que répondre pour entretenir la conversation. Ce fut d'ailleurs Ansaldi qui reprit le premier, ne laissant pas un silence pesant s'installer.

« Auriez-vous encontré le jeune clerc de Paris, maître Gonteux ?

— Le petit gros ? » demanda Ernaut avec naturel, horrifiant son frère au passage.

Ansaldi approuva de la tête, les yeux rieurs, amusé de la désinvolture de la remarque. Lambert n'offrit pas à Ernaut l'occasion d'une nouvelle impolitesse et répondit le premier.

« Nous avons débarqué de concert avec maître Gonteux, messire d'Eaucourt et son valet. Ils nous ont quittés voilà peu…

— Savez-vous ce qu'ils escomptaient faire ?

— Non, ils ne m'ont pas semblé bien certains eux-mêmes. Nous nous sommes départis à l'entour du marché aux poissons. »

Visiblement déçu de n'en savoir guère plus, le marchand remercia et salua les deux pèlerins. Puis il repartit d'un pas rapide, pressé de remettre ses lettres et d'expédier ses opérations commerciales. Il suivait le chemin sans vraiment prêter attention à ce qui l'environnait, plongé dans ses réflexions. Il entendit sexte[^sexte] sonner à un clocher proche alors qu'il se présentait à l'entrée de la boutique de son contact, ce qui le fit sortir de son monde intérieur.

Il frappa sur le chambranle de la porte entr'ouverte, s'annonçant d'une voix forte. Tandis qu'il penchait la tête et plissait les yeux pour tenter de voir dans l'obscurité, le battant s'ouvrit sur un visage souriant. Le vieil homme paraissait hors d'âge tant il était ridé. Une barbe mitée lui ornait la mâchoire et une sorte de bonnet qui tenait plus de la crêpe que du couvre-chef s'étalait de travers sur un crâne aux cheveux épars. Mais le regard était alerte et le sourire amical.

« Ça alors, jeune maître Embriaco ! Quelle joie de vous revoir !

— Le bonjour, maître al-Manzil al-Qamah ! »

Le vieil homme levait et agitait les bras en tout sens pour bien marquer son enthousiasme. Il s'effaça afin de permettre à son visiteur de pénétrer dans la petite pièce, surchargée de papiers, de corbeilles entassées et de produits divers recouverts de la poussière des ans. Il alla ouvrir une porte à l'arrière par laquelle il appela un valet pour qu'on leur apporte de quoi boire. Pendant ce temps, Ansaldi s'était avancé jusqu'à la grande table le long du mur extérieur.

Une belle fenêtre obstruée de volets à claire-voie laissait passer suffisamment de clarté pour travailler dans le confort. Des documents de toute taille, sur tous supports, étaient posés dans un désordre apparent, s'étalant sur plusieurs nécessaires à écriture, des calames et des plumes, et même un abaque dont les jetons étaient éparpillés ici et là. Un des plateaux d'une balance renversée dépassait de sous une pile de petites boîtes et de chiffons, accrochant la lumière de la baie et la reflétant sur le mur.

Le vieux marchand revint, d'un pas traînant mais rapide, et prit place sur un fauteuil à dossier rempli de coussins, tout en indiquant un siège pliant à Ansaldi. Après quelques assauts de courtoisie, le Génois ouvrit sa sacoche.

« J'ai moult courriers à votre intention, ainsi que gages d'amitiés de plusieurs associés génois. Et aussi quelques documents que j'aimerais faire acheminer…

— Nous verrons tout cela plus tard. Faites-moi le bonheur de partager mon souper. Je vais faire prévenir quelques amis qui seraient heureux de vous encontrer ou de vous revoir. »

Ansaldi sourit, un peu contrarié, car il ne pouvait pas refuser une telle invitation. Son hôte était un contact important et un brave homme, deux raisons de ne pas l'offenser. Mais il aurait aimé pouvoir se libérer plus rapidement. Ce fut malgré tout avec un sourire fort amène qu'il répondit.

« Vous me faites grand honneur, maître, j'en serai très honoré. »

### Messine, nuit du 18 septembre

Le marin sentait qu'il avait trop forcé sur la boisson. Mais pas plus qu'à son habitude. Avec l'alcool, il avait l'impression que le sol tanguait comme un navire et croyait moins peiner dans sa progression. Mais alors qu'il se lançait dans la rue, à la sortie du tripot où il s'était adonné à nombre de ses vices, il eut l'impression que le vaisseau avait de la gîte, ou qu'un fort vent de travers s'efforçait de le rabattre dans les escaliers le long des façades.

Impuissant à s'opposer à cette force irrésistible, il tentait péniblement d'enjamber les écueils désireux de le mettre sur le flanc. L'odeur plus forte de la mer le guidait néanmoins et il espérait bien être à bord au plus vite. Il avait abandonné l'idée de lever la tête pour se repérer aux étoiles, inquiet de se retrouver incapable de maintenir le cap efficacement si la vigie regardait ailleurs qu'à la proue.

Il crut un moment qu'une déclamation à voix forte des plus imaginatives chansons de marins qu'il connaissait lui donnerait du cœur au ventre mais il n'obtint pour toute récompense qu'un crachin à odeur de pissat venu d'une fenêtre au-dessus de sa tête, accompagné d'une bordée de jurons dont certains étaient une découverte pour lui. Il était temps de faire une pause. Tandis qu'appuyé au mur, il s'efforçait de retrouver ses esprits en admirant ses pieds, il entendit une voix à l'accent familier, légèrement narquoise.

« Alors, le faucon s'est posé ? »

Il éructa péniblement et se laissa tomber sur une marche, l'air hagard, cherchant à identifier son interlocuteur.

« Hein ? Qu'est-ce vous dites ? »

L'inconnu s'avança, mais en restant dans l'ombre des maisons, noirceur dans les ténèbres.

« Je voulais savoir si tu es à ton aise, désormais à terre, aussi frêle que les poulets que tu moques tant. »

Le matelot se recula, la mine renfrognée, la main se dirigeant naturellement vers le gros couteau qu'il arborait à la ceinture.

« Foutrecul ! Qu'est-ce que vous m'voulez ? Pis qui vous êtes ? »

La forme s'avança encore, brandissant un cruchon.

« Je cherche compagnon du *Falconus* pour achever avec moi ce vin, je crois que j'en ai ma suffisance. »

Le marin hoqueta puis, soudain rassuré, eut un sourire carnassier.

« Et bah voilà, là je comprends, assieds-toi donc compère. Je déserte jamais compaing en détresse. » Puis, se ravisant, il écarquilla les yeux pour dévisager son nouvel ami, cherchant à l'identifier. « Et qu'est-ce que tu fais dans l'coin, à la nuit ? Y'a danger ici, tu sais. »
Il se pencha, le souffle lourd de relents de vinasse.

« Méfiance, en de tels lieux ! Toujours !

— Toi, tu vas seul pourtant !

— Bien forcé ! Les autres sont allés et m'ont oublié. »

Il s'envoya une généreuse rasade pour noyer son chagrin.

« Mais je suis pas esseulé, un fidèle me suit toujours… »

Disant cela, il tapota affectueusement le coutelas dans sa gaine. Son compagnon fit mine de s'y intéresser.

« Belle lame ! Je n'ai pas pensé à m'armer de si belle façon. Elle semble bien roide !

— Et elle l'est ! Mire donc, compagnon ! »

Il brandit la lame, frappant les airs comme un damné, manquant de s'entailler le visage à plusieurs reprises, le bras à peine canalisé par son compagnon de beuverie.

« Attention, tu vas nous occire tous deux, ami.

— Aucun risque, c'est un fidèle serviteur. Depuis que je suis mousse. »

Son camarade se leva soudain, lissant inconsciemment ses habits.

« Allez, il est temps de quitter l'endroit. Le *Falconus* doit lever l'ancre au plus tôt. Je ne voudrais arriver trop tard.

— Je te suis, mon vieux ! déclama le marin, tout en s'efforçant de rentrer la lame dans son étui. Aide-moi donc à partir ! »

Son acolyte se pencha, mais au lieu de lui prendre le bras et de le soulever, il lui appuya le coude, guidant le couteau dans les entrailles de l'ivrogne.

« Tu vas quitter ce lieu, certes, mais n'escompte pas que nous cheminions de concert… ami ! »

Brutalement dégrisé, le matelot ne semblait pas comprendre et implorait de ses yeux humides. Il hoquetait doucement, comme un poisson hors de l'eau, la salive s'écoulant lentement sur le bras de son assassin. Ce dernier attendit que les clignements ralentissent, tandis que du sang remplissait la bouche de sa victime. Puis il se pencha à son oreille et lui susura « Avec le salut du goupil. »

Lorsqu'il poussa de côté le corps désormais sans vie, le meurtrier ne put s'empêcher de lui cracher dessus de rage. Du pied, il le poussa de l'entrée de la maison devant laquelle ils avaient bu. Le tirant à l'écart, il le renversa finalement sur un tas d'immondices dans le caniveau de la ruelle adjacente. Puis il recula de quelques pas et admira le corps, se repaissant intérieurement de la vision de la dépouille. Inquiet qu'on ne découvre le corps trop tôt, il entreprit alors de le recouvrir de déchets variés. Sa besogne achevée, il vérifia que cela pourrait suffire quelque temps et ne put retenir un sourire féroce.

« Dommage de trouver sa vraie place ici-bas seulement outre sa mort. »

Après avoir jeté un regard rapide dans la rue chichement éclairée par un croissant naissant de lune, il prit d'un bon pas le chemin des quais, rasant les murs pour tenter de rester discret. Lorsqu'il disparut de l'endroit, une forme émergea de l'encoignure d'une boutique puis entreprit à son tour de rejoindre le *Falconus*. Malgré le meurtre auquel elle avait assisté sans intervenir, l’ombre marchait d'un pas léger : elle ramenait à son maître plus que la moisson espérée.

### Détroit de Messine, fin d'après-midi du 19 septembre

Les nuages aidant, l'obscurité commençait à gagner sur la lumière, mais la température était encore clémente. Les derniers rayons rougeoyants embrasaient la mâture du bateau, contrastant avec le navire en lui-même, plongé dans les ombres bleues. Ernaut s'étirait comme un chat après une sieste, bâillant à s'en décrocher la mâchoire. Il venait seulement d'avoir l'autorisation, comme les autres passagers, de retrouver le grand air. Pendant toute la traversée du détroit de Messine, le *patronus* avait préféré que les matelots ne soient pas gênés par des voyageurs désœuvrés et les avait consignés au pont inférieur. Les deux frères en avaient profité pour faire un long somme dont ils commençaient à peine de se réveiller. Ils se dégourdissaient les membres dans ce qui ressemblait fort à une danse mystérieuse. Ganelon, venu jeter par-dessus bord quelques ordures, se prit à sourire en les voyant. Il s'approcha d'eux, le visage ouvert.

« Vous vous apprêtez pour une soule[^soule], ou quoi ?

— Hé hé, non pas. C'est juste histoire de s'activer un peu après avoir dû rester reclos en bas. »

Ganelon acquiesça gravement.

« Lors du voyage aller, nous avons subi tempête deux jours et deux nuits entières. Tout ce temps enserré dans l'obscurité, balloté en tout sens comme étoffe chez le foulon, voilà horrible souvenir ! »

Ernaut finit alors de se décontracter le dos en s'appuyant sur le plat-bord. Ganelon l'examinait attentivement tout en se tournant vers Lambert.

« Je ne voudrais pas importuner, mais avez-vous envisagé de servir le roi, une fois à Jérusalem ? Mon maître saurait vous trouver du service à tous deux. »

Ernaut se retourna, les yeux brillants, mais Lambert fit une moue, apparemment peu convaincu.

« Nous n'avons pas formation de guerrier, ni Ernaut ni moi. Je sais que tu penses surtout à lui en disant cela, mais je doute que cela soit bonne vie pour lui…

— Soyez assurés qu'être sergent du roi recèle nombre d'avantages… »

Accoudé de dos au bastingage, Ernaut regardait en direction du pont, face à son frère et Ganelon. Il coupa brutalement la parole à ce dernier.

« En aucun cas je ne veux finir simple valet !

— Pourquoi dis-tu ça ? » demanda Lambert, surpris de cette interruption.

Ernaut tendit le bras et montra le domestique d'Embriaco qui sortait du château arrière, visiblement fort occupé. Lambert et Ganelon se retournèrent. Ce dernier s'élança immédiatement pour rejoindre Ugolino et salua ses compagnons de la main en s'éloignant.

« J'ai justement message à lui faire passer. À plus tard ! »

Ganelon tenta de faire signe au domestique, mais celui-ci était plongé dans ses pensées et ne semblait pas faire attention à ce qui l'environnait. Il manqua d'ailleurs de basculer en avant en se prenant les pieds dans un cordage. Il se décida à l'appeler pour éviter de devoir courir derrière lui sur le pont encombré et occupé par les matelots au travail, tout à la mise à l'ancre. Après plusieurs appels, le  vieil homme, l'air absent, se retourna enfin vers Ganelon.

« Mon maître veut inviter le tien à une partie d'échecs, pour une veillée prochaine, de son choix. »

Ugolino regardait dans le vague, par-delà Ganelon, et ne semblait pas prêter attention à ce que ce dernier lui demandait. Il acquiesça d'un souffle à peine audible. Ganelon s'étonna que le valet ne fît pas montre de son empressement et de sa précision habituels.

« Ça ne va pas ?

— Si, si. Je suis juste faiblet. J'ai dû manger trop de fruits à terre.

— Tu porteras message ?

— Oui, oui, sans souci. »

Ugolino reprit son chemin, l'air aussi perdu qu'auparavant. Ganelon se dit qu'il ferait bien de vérifier par la suite que le message était correctement transmis. Regardant comment le vieil homme se comportait, il eut un moment d'angoisse en se demandant s’il allait finir ainsi, fatigué et un peu sénile, mais devant toujours travailler pour un employeur exigeant. Régnier était très familier avec lui, et peu autoritaire. Mais en tant que chevalier, il risquait régulièrement sa vie et pouvait tout à fait être tué lors d'une mission. Et le prochain maître pourrait être plus dur. Ganelon réalisa soudain qu'Ernaut avait raison, et qu'au lieu de pousser les jeunes à saisir les  opportunités, il ferait bien de s'en inquiéter aussi un peu pour lui.

### Détroit de Messine, soirée du 19 septembre

Régnier et Herbelot avaient installé une petite table de jeu à l'écart de l'espace central où tout le monde se retrouvait à l'abri, sur le pont passager. Ils avaient pris place dès leur souper avalé pour pousser le pion aux mérelles puis aux échecs. Quelques spectateurs s'attroupaient parfois autour d'eux, estimant silencieusement la valeur d'un déplacement d'un signe de tête approbateur ou d'une moue dubitative, commentant ou suivant simplement l'enchaînement des coups.

Tandis que Régnier finissait de replacer ses pièces pour la partie suivante, Herbelot s'employait à redisposer les mèches des lampes pour avoir selon lui une meilleure lumière sur le plateau. Alors qu'il s'apprêtait à poser le dernier chevalier, Régnier eut envie de changer de jeu. Il proposa donc de passer aux tables[^tables]. Herbelot ne leva pas la tête, concentré sur sa tâche. Il répondit lentement, absorbé par la minutie de ce qu'il était en train de faire.

« Dès l'instant où les dés roulent, je n'approuve pas. J'ai l'impression de mettre Dieu à l'épreuve de ma volonté à chaque lancer. Beaucoup ignorent ce sacrilège, mais un clerc de mon rang ne peut décemment pas négliger pareille chose. »

Régnier sourit en lui-même devant l'air sérieux et professoral qu'Herbelot prenait à chaque fois qu'il évoquait sa situation. Il avala une gorgée de vin le temps que son adversaire finisse d'arranger les lampes à sa convenance sur la table. Regardant aux alentours, il vit que d'autres groupes s'étaient rassemblés pour jouer aux dés, aux devinettes ou aux charades. Parmi ceux-ci, Régnier entendait la voix rugissante d'Ernaut hurlant à tort et à travers toutes les hypothèses traversant son esprit, sans trop réfléchir. Cette attitude bouffonne déclenchait des salves de rires des différents participants.

« Voilà, c'est en place ! Qui ouvre ? »

Régnier revint au jeu, souriant poliment à Herbelot et l'invitant de la main à jouer. Du coup, ce dernier appuya le menton sur son poing, commençant d'ores et déjà à cogiter longuement, comme toujours, sur la meilleure ouverture. Lorsque le chevalier tourna à nouveau la tête vers les joueurs de charade, il vit Ernaut s'avancer vers eux, avalant des dattes dont il crachait bruyamment les noyaux dans un gobelet. Il salua un peu outrancièrement, les yeux rieurs, et s'assit auprès d'eux, ponctuant sa présence de mâchonnements qu'entrecoupait le tintement sec du godet.

Après quelques coups, il s'aventura à demander quelques éclaircissements sur ce qui se passait sur le plateau. Comprenant qu'Herbelot ne se montrerait pas très disert, il se contenta de questionner Régnier sur la signification de chacun des déplacements. Ses commentaires fréquents allaient de pair avec une exaspération croissante du clerc qui du coup jouait de façon précipitée, commettant de plus en plus d'erreurs. Il faisait de son mieux pour garder bonne figure, mais n'arrivait pas à se concentrer dans de telles circonstances. Finalement, il se résolut à abandonner.

« Je n'ai plus guère la tête au jeu, ami. Il est temps de me retirer. »

Ernaut sauta sur l'occasion.

« En ce cas, seriez-vous opposé à me montrer le jeu, messire d'Eaucourt ?

— Avec grand joie, mon garçon. Mais êtes-vous sûr, maître Gonteux ?

— Certain ! Je ressors sans regret. Il m'est temps de prier un peu avant le coucher.

— À votre guise ! Adoncques prends place, Ernaut, je vais m'enquérir de mon valet, j'ai grand soif ! »

Tout en s'installant, Ernaut présenta à Herbelot le sac dans lequel il avait un tas de fruits. Ce dernier s'apprêtait à répondre sèchement puis se radoucit un peu devant l'évidente simplicité du garçon. Il piqua une branche copieusement garnie.

« C'est bien gracieux, jeune homme. Je goûte fort ces friandises. »

Avalant les fruits sucrés avec un enthousiasme manifeste, Herbelot détachait soigneusement chaque datte comme un objet précieux.

« Étonnant que tu aies connaissance de ce fruit, d'ailleurs. N'est-ce pas ton premier passage en Outremer ?

— Si fait. C'est le jeune marchand, maître Embriaco, qui nous a donné conseil hier au marché. »

Herbelot souleva un sourcil étonné, et déglutit rapidement.

« Vous avez fait emplettes communes avec lui ?

— Oh non ! Pas du tout ! Ça m'a même esbahi qu'il nous parle. J'ai mieux compris quand il nous a indiqué qu'il vous cherchait.

— Moi ? »

Ernaut repensa à sa remarque sur son physique. Le voyant en train de déguster les dattes comme un lapin dévorant un pissenlit, il réfréna un sourire.

— Non. C'est étrange, je ne l'ai guère croisé depuis notre départ fors Gênes. »

Saluant distraitement d'un léger signe de tête, les sourcils désormais froncés, Herbelot s'éloigna à pas lents, plongé dans ses réflexions. Il ne remarqua même pas Régnier qui revenait de sa chambre et le gratifiait en passant d'un sourire amical. Lorsque ce dernier s'assit en face d'Ernaut, il lui demanda :

— Que se passe-t-il ? Maître Herbelot me semble bien pensif tout à trac. »

— J'en suis cause » répondit l'adolescent, entre deux mastications. « Je lui ai rapporté que maître Embriaco l'espérait hier. Ça l'a étonné. Il ne le connaît que guère et ne l'a pas vu depuis. »

Le chevalier haussa le menton, relativement indifférent à l'anecdote.

« Bizarre en effet ! De toute façon, cela ne nous touche pas. Par contre, savoir échequier dignement, voilà qui nous intéresse de présent, n'est-ce pas ? »

Pour toute réponse, Ernaut fit un large sourire, acquiesçant de la tête en engouffrant un nouveau fruit dans la bouche qu'il avait largement ouverte pour l'occasion.

### Golfe de Tarente, soirée du 22 septembre

Tandis que son adversaire réfléchissait à son prochain coup, Régnier laissait traîner son regard dans la cabine. Tout y était rangé avec un grand soin. Les étagères étaient remplies de linge délicatement plié, de coffrets de rangement divers, ainsi que de vaisselle. Plusieurs lampes à huile apportaient une lumière conséquente étant donné le petit espace, mais creusaient des ombres tranchées, s'accrochant sur les bords des objets et les recoins de la charpente du navire. Ils étaient confortablement installés sur le lit, calés contre des coussins, et le valet avait disposé la table le long de la couche pour qu'ils puissent y poser leur verre et y piocher quelques fruits secs tandis qu'ils s'affrontaient aux échecs.

Ansaldi déplaça un de ses comtes[^echeccomte], puis, avec une moue de satisfaction, regarda Régnier.

« Joli coup, maître Embriaco ! On voit que vous êtes accoutumé… »

Le marchand approuva de la tête alors qu'il avalait une gorgée.

« Mon père me rêvait patricien. Il a donc veillé à une complète formation.

— Savez-vous également escrimer ?

— Bien sûr, mais sans adresse. Pas assez batailleur, répétait mon professeur. »

Régnier déplaça son roc[^echecroc]. Satisfait de son coup, il leva un menton provocateur.

« J'ai peine à vous croire l'épée en main. Je vous pensais plus occupé de comptes, chartes et diplômes.

— Oh, c'est le cas. J'ai tôt compris que je ne serais jamais vaillant guerrier. Je le regrette d'ailleurs.

— Vous auriez préféré la voie des armes ? »

Ansaldi se rencogna, les yeux toujours fixés sur le plateau mais le regard lointain. D'évidence la question lui importait.

« Préféré, je ne sais… Ma parentèle comprend moult talentés guerriers. Le grand Testadimaglio[^embriaco] lui-même est un mien père. Après avoir aidé à la prise de Jérusalem, il a ramené des troupes au roi Baudoin[^baudoinI]. Sans lui, cités telles que Arsuf, Jaffa ou Césarée ne seraient pas nôtres. Tout cela alors qu'il était fort âgé, plus de soixante ans ! »

Le chevalier sentit que le marchand avait envie de se confier à une oreille amie, de s'épancher auprès de quelqu'un qui saurait l'écouter sans moquer ses rêves d'homme encore jeune.

« Cela vous laisse encore temps pour suivre votre voie alors…

— Peut-être… Je suis parfois fort las de notre renommée de marchands impénitents. Les Pisans, que voilà négociants à la petite semaine ! Les Vénitiens aussi ! Mais nous, à Gênes, nous avons sans faillir participé aux guerres contre les infidèles. Nos nefs passent les combattants outre la Méditerranée saison après saison et nos amiraux joutent, souvent avec succès, les embarcations égyptiennes. »

Ansaldi s'arrêta un instant, comme agacé par les pensées qui l'assaillaient.

« Alors oui, de vrai, nous avons gagné moult concessions des puissants d'Outremer, quartiers, cités entières parfois ! Mais avons-nous démérité aux côtés des fiers guerriers qui ont pris la croix ?

— Le roi Baudoin n'est pas homme à oublier cela. Et personne à Jérusalem n'a mauvaise opinion des hommes de Gênes.

— Les barons d'Outremer sont de sûr nos fervents alliés. Mais les François, Anglois, Allemans nous accablent, comme si nous n'étions guère plus que larrons. J'espère que cela va changer. »

Régnier ne s'était jamais posé la question en ces termes. Pour lui, les Génois étaient de bons navigateurs, de solides charpentiers de guerre et de féroces négociants. Qu'ils aient des états d'âme n'était jamais entré dans l'équation.

« Vous aurez certainement de prochaines occasions. La prise d'Ascalon offre un nouvel havre sûr dans le sud.

— Oui, je pense d'ailleurs prendre part, au moins par monnaie, à la prochaine expédition majeure de la *Compagna*. À défaut d'être à bord glaive en main et casque en chef, j'apporterai mon écot de la façon la plus efficace. »

Il sourit puis déplaça un de ses chevaliers[^echecchevalier], garda le doigt dessus quelques secondes pour enfin le lâcher, apparemment à regret.

« Et qui sait ? Peut-être qu'un jour nous bataillerons flanc à flanc pour conquester Alexandrie ou Damiette. »

Invitant Régnier d'un mouvement de menton à jouer à son tour, il prit son gobelet de vin et but à petites gorgées. Son adversaire réfléchissait à ce que le marchand venait de dire. Un partisan loyal dans la *Compagna* de Gênes pourrait être un atout intéressant pour son souverain. Il faudrait peut-être l'appuyer, pour qu'il gravisse au plus vite les échelons. Cela pourrait s'avérer payant au final. Régnier se demanda si Ansaldi cherchait à le manipuler pour obtenir des soutiens. Il paraissait sincère, mais cela pouvait être feint. Il était néanmoins indéniable qu'il avait, malgré son jeune âge, déjà pas mal de contacts avec les hommes les plus puissants de la ville, d'anciens consuls et leurs proches.

« Tantôt, j'ai repensé à votre souhait d'accroître commerce et  échanges. Peut-être pourrais-je vous faire encontrer quelques personnes d'influence, officiers de la maison royale, peut-être même le sénéchal.

— C'est fort aimable à vous. Il m'encharmerait de faire montre que la *Compagna* est assemblée de bons chrétiens, impatients d'achever leur devoir. S'enrichir est honnête labeur, j'y consens, mais il y a des préséances. Honorer sa foi en fait partie. »

### Golfe de Tarente, matin du 23 septembre

Fulco regardait pensivement la côte, au loin, avec les plaines rocheuses qui avançaient dans une mer couleur lapis-lazuli. Un soleil resplendissant annonçait une chaude et agréable journée et le vent soufflait pour une fois dans la bonne direction, sans à-coups. Dès les ancres levées et la voile mise en place, l'activité était retombée sur le navire. Les marins profitaient de ce répit pour accomplir des tâches annexes d'entretien, de rangement ou de réfection. Plusieurs d'entre eux frottaient le sol avec rythme, chantant à tue-tête une comptine qu'ils avaient aménagée selon l'humour matelot. Fulco se surprit à siffloter à l'unisson lorsqu'ils attaquaient le refrain.

La bonne humeur s'était répandue aussi vite que la lumière et l'affluence des passagers sur le pont renforçait cette impression de bien être à bord. Même Ugolino s'était octroyé une pause, appuyé contre un tonneau, les yeux fermés face au soleil. Fulco descendit de la dunette et se rapprocha du vieil homme. Arrivé à ses côtés, il s'assit à son tour et adopta une posture identique, profitant de la chaleur des rayons. Il se hasarda à briser le silence, n'ayant pas remarqué la respiration régulière qui aurait pu indiquer le sommeil chez son voisin.

« Tu t'alloues tout de même repos ? »

Il attendit quelque temps une réponse puis, alors qu'il n'en espérait plus, Ugolino rétorqua d'une voix indolente.

« C'est surtout qu'enfin le ciel est clément. De plus, on est dimanche, non ?

— De vrai. J'aurais aimé assister aux offices du matin, mais j'avais labeurs en cours, alors…

— Un peu comme moi donc ! » Ugolino eut un sourire fugace, mais n'ouvrit pas les yeux pour autant. « Quoique tu as certainement plus grandes responsabilités que simple valet comme moi. Si d'aventure la coque faiblit, ça peut mettre en danger tout le monde. Sauf quand je rase mon maître, il est rare que je tienne la vie de quelqu'un en mains.

— Je n'y ai jamais pensé. J'ai appris la charpente avec père. Enfançon, tu étais plus libre. Par la suite j'ai vu à mes dépens que je n'étais pas habile combattant. Adoncques me voilà œuvrant sur navires. »

Il conclut sa dernière phrase d'une tape amicale sur le pont du navire, comme pour marquer le lien qui l'unissait au *Falconus*. Ce fut Ugolino qui enchaîna.

« Et au final, tu te loues à bon prix, meilleur que moi…

— Je n'ai guère à me plaindre. Mais comment tu sais ça ?

— Par mon maître. Ses associés ont affrété le navire pour ce voyage, il doit donc regarder pour eux aux dépenses.

— Je vois. C'est peut-être pour ça que le vieux Spinola s'est enquis d'Embriaco et toi. Il est également des financeurs si j'en crois le *patronus*. »

Intrigué, Ugolino ouvrit les yeux, qu'il dut garder plissés, ébloui par la vivacité de la lumière.

« Qu'est-ce qu'il voulait savoir ?

— Un peu tout, place dans la famille, fortune, relations connues, valets à son service, voyages déjà effectués, rien que de très habituel… Ce vieux goupil est féroce négociant et il fait commerce d'informations, m'a-t-on dit. Il a dû être intrigué par ton maître. »

Ugolino ne put se retenir et se releva tout à fait, le rouge lui montant aux joues.

« Maître Embriaco est notable de bonne fame[^fame] tout de même !

— Soit, mais il agit de bien étrange façon, admets-le. C'est  la première fois que j'encontre un marchand emmuré en sa cabine ou la faisant garder lorsqu'il en sort. »

Le charpentier se rapprocha insensiblement du valet, le regard inquisiteur.

« Alors, d'aucuns se demandent : il cache quoi en son bagage ?

— Oh, rien d'interdit, sois rassuré. Il est simplement fort prudent, pour la première grande association où il s'est porté garant de fortes sommes confiées à lui par prud'hommes. Il ne veut pas les décevoir en exposant leurs monnaies à tous vents.

— Alors, il faudra lui rapporter qu'au final il arrive au résultat à rebours : tout le monde se demande ce qu'il peut bien celer en sa chambre. »

### Otrante, fin d'après-midi du 25 septembre

Herbelot ne se sentait pas en grande forme. Il était un peu nauséeux et craignait d'avoir pris froid. Il était resté emmitouflé toute la journée sous ses couvertures, n'osant pas affronter l'air du large. On avait accosté depuis peu, à Otrante, et il avait entendu de nombreux passagers s'empresser de descendre. Il attendit encore quelque temps ainsi, seul, à regarder la flamme de sa lampe danser et projeter des ombres tournoyantes sur les parois de toile de sa chambre.

Il finit par se décider à sortir afin de profiter de la dernière occasion avant longtemps de marcher sur un sol stable. Il s'habilla chaudement et enfila par-dessus son épaisse cotte de laine un manteau dont il lança les pans sur ses épaules, de façon à s'en faire un cache-col. Prêt à affronter le froid, il monta les escaliers et fut surpris par la douceur de la température.

Le soleil n'était pas encore couché et l'atmosphère embaumait les fleurs et les plantes. D'invisibles bandes d'étourneaux piaillaient dans les environs. La ville s'offrait au regard en étendue de plaine, au contraire des cités qu'ils avaient admirées récemment. Bien que de belle importance, comme l'indiquait son port empli de navires en partance ou de retour de l'Outremer, Otrante paraissait plus calme et plus sereine que Messine. Non loin des docks, une majestueuse cathédrale surplombait les bâtiments alentour, imposante masse de pierres aux reflets roux.

Herbelot s'approcha de la passerelle et avisa le *patronus* sur le quai, occupé à discuter au sein d'un petit groupe. Il attendit quelques instants, mais vit que la discussion semblait s'éterniser. Pendant ce temps, le clerc nota que des marins commençaient à débloquer les taquets maintenant les trappes d'accès aux soutes à marchandises. Il réalisa soudain que cet arrêt n'avait pas d'autre raison que de permettre aux marchands de faire leur petit commerce, sans égard pour les voyageurs comme lui qui avaient payé leur place, parfois fort cher, pour arriver au plus tôt en Terre sainte.

Il poussa de la main le garde sur la passerelle et descendit sur le quai en grandes enjambées, interpellant le *patronus* tout en avançant. Il s'inquiétait de savoir combien de temps durerait l'escale. Malfigliastro se retourna, occupé qu'il était à régler certaines formalités administratives avec un officier du port d'Otrante. Il comprit qu'une question avait été émise par le petit homme à visage rond qui se tenait devant lui emmitouflé dans un épais manteau de laine. Surpris de cette interruption malpolie, il marqua une pause et regarda droit dans les yeux le jeune clerc qui semblait devenir plus rouge à chaque instant. Il l'examinait ainsi qu'un chat le ferait de sa proie. Le clerc finit par répéter sa question.

« Je vous demandais si nous allions relâcher ici pour long temps. Il me fait impression que nous stoppons en tous ports d'Italie. »

Ribaldo demeura silencieux un bon moment, voulant montrer à son interlocuteur qu'il ne répondait à ses questions que par correction, sans y être tenu.

« Cette escale était prévue. Demain en la matinée, d'aucuns produits seront déchargés et autres seront hissés à bord, nous pourrons reprendre la mer dès après mercredi. »

Le clerc tapa ses pieds sur le sol de terre battue du quai, apparemment dans l'intention de les réchauffer alors qu'il s'échauffait manifestement tout seul à chaque seconde passée.

« Tôt sera le mieux. Quelle pitié de nous arrêter pour aussi viles tâches que le commerce !

— Désolé de vous contrarier, maître, mais c'est utile aux denrées pour circuler. Et cela paye les nefs qui vont et viennent entre  France, Empire et Outremer…

— Je n'ignore pas cela, mais je le déplore tout de même. Supporter de telles contingences mondaines pour le profit d'un petit groupe de marchands du Temple, cela m'exaspère, voilà tout ! »

Vexé par cette attaque brutale, le navigateur se redressa brusquement, tentant d'impressionner son interlocuteur par sa masse. Il fronça les sourcils et fit une mimique qui eut pour effet de tendre à l'horizontale les poils de sa moustache. Si l'allure était comique, son regard ne l'était guère.

« Je vous trouve plutôt strict en vos opinions, maître. Beaucoup de ces négociants sont aussi bons chrétiens. »

Herbelot secoua la tête en dénégation. Il commençait à perdre contenance et ses bras avaient tendance à s'agiter en tous sens, cherchant à mimer ce dont il parlait, sans grande limpidité.

« Le poids de leur or pèsera lourd dans la balance au Jugement dernier, soyez-en assuré ! Et pas du bon côté ! »

N'y tenant plus, le *patronus* se mit à élever la voix, s'exprimant comme s'il s'adressait à un jeune enfant qu'il réprimandait.

« Comment croyez-vous que Ugo Embriaco a obtenu la cité de Gibelet ? Parce qu'il a vaillamment combattu ! Et comment a-t-il payé ses passages en Outremer ? Mais grâce au négoce, maître Gonteux, grâce à ce commerce que vous haïssez tant. Sans des hommes comme lui, la terre de Jérusalem ne serait pas ce qu'elle est. Et vos tenues liturgiques ne seraient pas de soie fine, mais de laine brute et de lin grossier ! »

Herbelot se dressa sur la pointe des pieds, l'index visant de façon agressive le *patronus*. Il chercha quoi rétorquer pendant quelques secondes mais, ne trouvant rien, se contenta de souffler, rouge comme une pivoine. Il se résolut alors à faire demi-tour, se drapant dans son manteau comme un Romain dans sa toge et repartit majestueusement à bord, sans plus un regard. Le commandant du navire le suivit des yeux, remâchant sa colère et prêt à hurler une nouvelle fois si le clerc était tenté de s'arrêter pour lui répondre. Il souffla à plusieurs reprises puis se détourna, revenant vers ses interlocuteurs portuaires. Il s'excusa auprès d'eux et, se calmant peu à peu, continua de présenter ses différents documents, assisté du scribe du *Falconus* qui avait eu la présence d'esprit de se faire oublier.

Appuyé à la rambarde de la plate-forme du château arrière, Mauro Spinola buvait à gorgées lentes son verre de vin épicé. Il avait assisté à toute la scène et l'avait prise avec amusement. Il n'avait pas si souvent des occasions de rire ainsi. La candeur du jeune clerc lui plaisait et il se demanda s'il n'allait pas l'inviter à sa table, juste pour voir jusqu'où cette naïveté s'étendait. Il avait justement quelques contacts à Otrante, toujours ravis de le recevoir, et ils ne feraient pas de difficulté à ce qu'il y ait un invité supplémentaire. Il appela un de ses valets, il fallait qu'il écrive un message en ce sens dès ce soir.
