## Chapitre 1

### Gênes, soir d'orage, été 1156

La foudre déchira le ciel, illuminant de l'intérieur les nuages ventrus qui recouvraient la ville. La pluie furieuse s'abattait dans la ruelle en un rugissement assourdissant, éclaboussant les fines chaussures de cuir ouvragé du marchand. Seul sous les gouttes glacées, il fulminait et enrageait silencieusement. Il avait frappé en vain à la porte principale pendant un bon moment, mais avec l'orage, personne ne l'avait entendu.

Il escomptait juste que, pour une fois, ses instructions n'auraient pas été suivies à la lettre et que le petit guichet de la cour arrière ne serait pas verrouillé. Chose étonnante que d'espérer que son garçon d'écurie ait une fois de plus négligé de pousser le verrou. Ruisselant d'une eau grasse, il n'était guère satisfait de salir ses vêtements coûteux dans la boue des ruelles. Tenant d'une main délicate les pans de sa cotte[^cotte] de soie brodée, il s'efforçait de sautiller de façon à éviter les flaques les plus boueuses et les plus profondes. Agacé, il essuyait périodiquement d'un revers nerveux la goutte d'eau au bout de son nez. Il n'aimait pas se dépenser ainsi, comme en attestait l'imposant diamètre de son ventre.

Un nouvel éclair et le tonnerre à sa suite le firent sursauter. Frémissant, il se rattrapa d'une main au mur de la venelle, les pieds de chaque côté du caniveau nauséabond. Il savait qu'il aurait dû prendre ses socques[^socques] avant de partir ! Mais comme il avait confié de nombreuses tâches à son valet, ainsi retenu en son hôtel, il n'avait pas eu le courage de les porter lui-même. Il se maudissait intérieurement et se promit de ne pas succomber à la paresse la prochaine fois.

Au moins, tout ceci n'aurait pas été en vain. Ses rendez-vous avaient été fructueux et la saison en Méditerranée s'annonçait assez bien. Sans s'en rendre compte, il sourit pour lui-même, heureux de ces perspectives et du bruit de la porte cédant sous sa poussée. Le loquet n'en avait pas été tiré ! Il franchit le seuil rapidement et jeta un coup d'œil vers les fenêtres d'où filtrait à travers les volets un peu de lumière, annonciatrice de douce chaleur et prometteuse d'un abri contre la pluie.

Il s'apprêtait à refermer l'ouverture lorsqu'une brutale poussée dans son dos le propulsa dans une flaque de boue au centre de la courette. Il n'eut que le temps de lancer ses deux mains en avant pour ne pas s'y étaler de tout son long. Grassement arrosé, le visage crotté, il recracha tout en essayant de se relever l'infâme mixture qu'il avait gobée. Avant même qu'il puisse accomplir son geste, un violent coup de pied dans les reins l'aplatit dans un amas bourbeux, en une gerbe d'eau et de terre qu'accompagna un juron bien choisi. La bouche remplie, il ne put réprimer un haut-le-cœur lorsqu'il goûta les cailloux qui crissaient sous sa langue. Abasourdi par cette agression, il se retourna, s'efforçant sans succès de discerner son assaillant. La silhouette s'approcha de lui et posa le pied sur sa poitrine, appuyant de plus en plus fort pour le plaquer à terre. Sa résistance ne fut que symbolique, il rencontra le sol dans un éclaboussement sonore et sentit son crâne heurter douloureusement plusieurs pierres. Affolé, légèrement choqué par la douleur, il leva la main pour se masser la tête et tenter de se nettoyer le visage. Il roulait des yeux en tous sens, incapable de hurler. Tout ce qu'il aperçut fut une ombre le toisant, un pied appuyé sur son imposante panse, sans qu'il n'arrive à la reconnaître.

Lorsque son assaillant se pencha vers lui, il cligna des paupières, pour en évacuer la boue et l'eau, mais aussi de stupeur. Les yeux, pourtant familiers, s'affichaient distants, animés seulement d'une fureur animale. Le gros marchand voulut le questionner, mais il ne réussit pas à parler avant que la lame ne pénètre dans sa gorge. Seul un gargouillis de sang parvint à franchir ses lèvres. Il essaya de retrouver son souffle, paniqué et jetant des regards ici et là comme une bête aux abois. Des bulles écarlates commençaient à se répandre sur ses mentons, rejoignant les flaques boueuses en de minces filets. Soudain, il sentit une douleur se propager au plus profond de ses entrailles, d'abord doucement puis plus sauvagement à chaque nouveau coup de couteau. Tandis qu’il opérait, un large sourire se dessinait sur les traits de son assassin. Le gros homme sentait la vie s'écouler de son corps et ne réussit qu'à cracher à la face de son meurtrier avant de succomber, dans un dernier borborygme sanglant.

Lâchant alors sa victime, le tueur attrapa un des pans des vêtements du défunt et s'essuya le visage. Puis il entreprit d'ôter toute trace de sang de son couteau, méthodiquement, sans se presser, aidé par la pluie battante qui continuait de s'abattre avec la même violence. Il savourait le moment, comme un chasseur repu de la mort d'une proie longtemps traquée. Penchant la tête en arrière, il ouvrit la bouche pour laisser les gouttes tombées du ciel le désaltérer. Il ressentait l'amour de Dieu couler sur son corps en cet instant de sauvage félicité.

À l'entrée de la cour, un individu s'effaça silencieusement après avoir assisté à l'agression, rejoignant l'obscurité discrète du passage. Le témoin muet restait là, à admirer la macabre scène, comme envoûté, oscillant entre plusieurs sentiments. Se frottant le visage en un geste inutile pour évacuer l'eau serpentant sur ses joues, il hocha le menton lentement, comme pour convaincre un invisible interlocuteur. Un appât, songea-t-il, un appât offert par Dieu !

### Gênes, matin du 31 août 1156

Les volets de bois de la galerie avaient été levés et le soleil pénétrait largement, égayant l'atmosphère. La grande salle de l'hôpital du monastère bruissait de l'agitation de voyageurs empressés. Comme tous les matins, beaucoup d’entre eux s'employaient à préparer sacs et baluchons, impatients de reprendre la route ou de s'embarquer sur un des navires menant par-delà la mer. Certains étaient assemblés vers le petit oratoire, au levant, unis dans une prière au latin incertain menée par un vieux prêtre au visage rubicond. Au milieu des voyageurs allaient et venaient, dans leur tenue de bure fatiguée, des frères convers affairés portant des couvertures, assistant les plus faibles ou balayant le sol. Devant l'entrée principale, les mains sur les hanches, le frère hospitalier, Federico, couvait d'un regard satisfait la ruche désormais bien éveillée. Une agréable odeur de pain chaud et de brouet parvenait à ses narines depuis le couloir des cuisines. Il souriait, autant pour lui-même qu'à l'intention des marcheurs qui se répandaient en remerciements et bénédictions pour lui et son établissement. Il était en charge du bien-être des pèlerins et mettait un point d'honneur à les bénir individuellement lors de leur départ.

Soudain, venu d'une des courettes et rompant avec fracas la quiétude ambiante, un bris résonna dans tout le bâtiment, s'achevant en un déluge persistant peu sonore. Dans la grand salle des pèlerins, chacun se figea un instant puis tourna la tête de droite et de gauche, cherchant du regard l'origine du bruit. Frère Federico fut le premier à comprendre. Le visage défait, marmonnant des imprécations pour lui-même, il s'élança vers la zone des cuisines, d'où se faisait désormais entendre une discussion animée. Il passa sans ralentir devant la grande cheminée où les repas des voyageurs étaient préparés, n'accordant pas un seul regard aux cuistots et marmitons, dont la plupart étaient massés à la fenêtre, se chahutant du coude en ricanant. Arrivé à la porte arrière, il stoppa brutalement, les bras ballants, comme si les quatre cavaliers de l'Apocalypse l'y attendaient.

La scène offerte à ses yeux était conforme à ses craintes. Tout au long de l'escalier, jusqu'au sol de la cour, les fragments d'un cuvier étaient répandus, au milieu des restes d'écuelles en bois fracassées, des cuillers rompues et des gobelets brisés. Et à mi-chemin, assis au milieu des vestiges de ce qui avait été la vaisselle de l'hôpital, occupé à tenter de rassembler les morceaux, se tenait le jeune géant français à la chevelure blonde, Ernaut de Vézelay, ce pèlerin plus dévastateur que les plaies d'Égypte. S'il n'avait été que maladroit, il aurait été facile de s'en accommoder, mais Dieu l'avait créé semblable à un colosse antique, avec une force que lui envieraient bien des bœufs. Et surtout, dans son humour infini, Il l'avait doté d'une bonne volonté pour aider son prochain qu’égalaient seulement son insatiable curiosité et son incommensurable maladresse. Le frère hospitalier porta la main à son front et poussa un cri à fendre le cœur du plus endurci des tortionnaires.

« Mais que fait ce sanglier en la courette ? »

Deux valets, le regard penaud, étaient occupés à rassembler les débris au bas de l'escalier et à les ramasser dans des seaux. L'un d'eux prit sur lui d'affronter la tempête.

« Il voulait aider, frère, en descendant le grand cuvier de vaisselle. »

Ernaut crut bon d'intervenir.

« Je me suis dit que nous serions plus aises de porter le vaissellement sale à l'eau et non l'inverse. »

Le frère faillit s'en étouffer, les yeux lui sortant littéralement de la tête.

« Où vois-tu de l'eau chaude en cette cour ? Tu crois que nous chauffons nos citernes pour escurer les plats ? »

Le jeune colosse fit mine de répondre, puis baissa le regard, l'air contrit. On aurait dit que le sol venait de se dérober sous ses pieds. Mais l'hospitalier ne désarma pas pour autant, pointant un doigt accusateur.

« Apprends à mieux penser, garçon ! Je sais que tu veux bien faire, mais… mais… Accomplis choses plus dans tes cordes : fends des troncs, tire charrois ou fracasse de méchantes murailles ennemies avec ce qui te sert de crâne. Et, par tous les saints, cesse de nous aider, arrête avant que nous ne soyons irrémédiablement ruinés ! »

Puis, incapable de supporter plus longtemps une telle vision, le moine disparut dans le bâtiment, sans même laisser d'instructions précises quant au nettoyage. Ernaut fit mine d'aider un peu, sous les regards franchement amusés des présents. Peu à peu, les sourires se firent plus nombreux et certains gloussèrent même, toujours prompts à se distraire avec n'importe quel événement les sortant de leur routine. Le jeune Français finit par souscrire à l'humeur ambiante et se prit à rire, sans trop savoir pourquoi. Mais une voix autoritaire bien connue stoppa ses vélléités d'amusement.

« Ernaut ! Quelle catastrophe as-tu encore commise ? »

Lambert, son aîné de nombreuses années, se tenait dans l'encadrement de la porte, plus dépité qu'énervé. Ses épais sourcils bruns noyaient son regard et sa mâchoire, déjà volontaire, s'avançait, prête à mordre.

« Quand sauras-tu donc faire attention, frère ? »

L'adolescent ne répondit rien, balbutiant sans vraiment faire de phrase.

« À ainsi te mêler de tout, tu sais que tu t'attires des ennuis. Nous sommes pèlerins et n'avons rien à faire en cuisines.

— Je me disais que ce serait chrétien d'ainsi remercier.

— Certes, voilà honorable sentiment, mais en ce cas, sois plus éveillé à ce que tu fais. Dieu du Ciel, tu n'es plus enfançon ! »

De fait, le garçon faisait plus d'une tête que le plus grand des valets, avait des bras dont beaucoup se seraient satisfaits comme jambes et, n'était le duvet blond ornant son visage et ses traits toujours juvéniles sous une coupe au bol, il avait tout de l'homme fait. Mais demeurait en lui la fougue, la naïveté et la balourdise de ceux qui n'ont pas encore pris conscience de leur maturation.

Lorsque Lambert et Ernaut se présentèrent à la porte de l'hôpital, frère Federico avait déjà oublié l'incident. Il aimait sincèrement sa tâche et voyait en chaque pèlerin le visage du Christ, ainsi qu'on le lui avait enseigné depuis son enfance. Ce fut donc sans aucune rancune, et certainement avec un grand soulagement, qu'il donna sa bénédiction aux deux frères, leur souhaitant un bon voyage aller et retour. Lambert se fit un devoir de le détromper.

« Grand merci de votre sollicitude, frère hospitalier, mais nous ne reviendrons pas. »

De désaccord, le moine hocha la tête, adoptant un ton professoral.

« Voyons, ne soyez pas si bileux, nombreux sont les voyageurs qui traversent de nos jours. Et la plupart reviennent saufs.

— Nous ne sommes pas simples pèlerins. Nous nous rendons au royaume de Jérusalem pour nous établir. »

Un vaste sourire illumina le visage aux traits fatigués de frère Federico, dévoilant des dents gâtées.

« Grand' merveille ! Vous êtes si rares à faire ce choix. »

Des étoiles scintillant dans les yeux, le moine savourait l'instant, inquiet de rompre ce doux moment de félicité. Ce fut après un petit moment qu'il osa demander où les deux frères comptaient s'installer.

« Un moine de Charroux m'a expliqué que plus belles offres s'encontraient en Judée, près Ascalon, la grand ville capturée voilà peu, précisa Lambert.

— Ascalon ? C'est aux confins des terres sarrasines du Soudan de Babylone[^soudanbabylone]. Que voilà terrible endroit !

— À dire le vrai, nous ne savons guère ce qui s'offrira à nous. Père nous a largement doté, de quoi suivre les pas du Christ quelque temps, au moins jusqu'à Pâques. Nous aviserons alors. »

Le moine arborait un visage de bienheureux et, si la matinée avait mal commencé pour lui, il garderait certainement ce doux souvenir plusieurs jours durant.

« Vous embarquez sur quelle nef ?

— Le *Falconus*, aux bons soins du *patronus*[^patronus] Malfios…ro, s'il me souvient de ce qu'a dit le notaire de bord.

— Vous voulez dire Malfigliastro, pour sûr. Excellent ! Vous serez entre de bonnes mains. J'ai rencontré moult voyageurs ayant traversé avec lui. Savez-vous quand vous devez partir ?

— Pas encore. Nous devons être à bord ce jour d'hui. Nos vivres ont été chargés hier. J'espère que nous n'attendrons pas à quai trop longtemps.

— Juste le temps que vos cœurs soient prêts. »

Le moine ajouta un sourire malicieux.

« Que vents et courants soient propices ! Partez en l'amour de Dieu, mes frères !

— Amen ! »

Attrapant leurs sacs de cuir huilé, les deux Français descendirent l'escalier en direction du portail de l'abbaye. Federico les suivit du regard, un peu attendri de les savoir en route pour s'installer sur les terres que le Christ avait foulées. Il ne put réprimer un fugitif sourire en pensant à tous les maux que le sympathique colosse allait pouvoir semer en ces lieux saints. Avec un peu de chance, il serait capturé par les Infidèles et il sèmerait la désolation dans leurs territoires. Conscient du caractère bien peu charitable de cette pensée, le moine se réjouit à l'avance de la réaction amusée de ses frères lorsqu'il battrait sa coulpe au prochain chapitre.

### Gênes, après-midi du 31 août

Ragaillardi par l'air frais et l'animation ambiante, Ansaldi Embriaco sifflotait pour lui-même, à peu près satisfait de la tournure que prenaient les événements. Son voyage était fin prêt, les marchandises chargées sur le *Falconus* et les accords conclus. Il déambulait tranquillement le long des voûtes de Sottoripa, plus calmes en cette fin de saison qu'au plus fort du trafic maritime. C'était là que les marchandises débarquées des navires étaient stockées dans un premier temps, sous les arcades, avant de rejoindre les entrepôts et les foires lointaines. Là, il avait grandi, à l'ombre des balles transbordées par les *camalli*[^camalli] transpirants, aux côtés de son père, négociant comme lui. Ce dernier lui avait expliqué qu'il était né pratiquement au même moment que ces arcades le long des quais, ce qu’il voyait comme un signe de la prospérité à venir de son fils. De fait, il ne se trompait guère car à peine un quart de siècle plus tard, Ansaldi incarnait la nouvelle génération de négociants, travailleurs et ambitieux. Sa tenue soignée, de fine laine anglaise teinte d'écarlate, ses chausses indigo et son bonnet de feutre brodé au fil d'or montraient à tous son aisance. Toujours très soigné, il était généralement rasé de près, le cheveu brun coupé très court, pour éviter la vermine lors des voyages. Et si certains moquaient son jeune âge, la voix calme et le regard noir impressionnaient ses interlocuteurs, bien forcés de céder à ses arguments.

Derrière lui, trottinant dans son sillage, on trouvait un vieil homme au visage ratatiné comme une vieille pomme, occupé à porter en soufflant un petit coffre lourdement ferré. Les yeux globuleux un peu éteints, le cheveu désormais rare, il semblait flotter dans des vêtements éternellement trop grands. Ce vieillard dégingandé répondait au nom d'Ugolino et servait d'homme à tout faire au jeune marchand. Extrêmement dévoué, il lançait des regards mortels à tous les portefaix qui menaçaient de bousculer son maître. Et vu l'agitation coutumière sur le port d'une des plus grandes cités maritimes de Méditerranée, il pouvait maudire tout son soûl. Partout on ne voyait que des ballots, des sacs, des tonneaux et des jarres. Des étoffes de soie, de coton ou de lin arrivaient, des fibres à travailler étaient déchargées, accompagnées de colorants pour les teinter ; de lourds paquets de céramiques fines, en provenance de Sicile ou de Grèce, trônaient dans des paniers, enrobés de  paille. Les paquets d'épices, dont l'air était embaumé, avaient connu des voyages de plusieurs mois et ne seraient pas sur les étals avant encore un long moment. De lourdes cargaison de bois, de métaux ou de bruyants chargements d'armes étaient destinés aux États latins, de l'autre côté de la mer. Ceux-ci envoyaient en retour les délicats produits d'Orient, si prisés des plus riches, comme les tapis aux couleurs chatoyantes ou les étoffes de soie aux motifs complexes. C'était là, sur ces quais poussiéreux, accablés de soleil, que toutes les marchandises du monde passaient un jour, entre les mains rugueuses de travailleurs voûtés, sous les yeux calculateurs d'opulents négociants.

À côté de cette plaine fertile s'étendait la forêt de mâts des navires, gros vaisseaux et frêles esquifs, pourvoyeurs de la frénésie commerciale. Des tarides siciliennes, des galées[^galée] grecques, des saiètes rapides et de lourdes nefs ventrues, gorgées de marchandises, attendaient, les voiles abattues, que leurs entrailles soient vidées et rechargées avant de s'élancer vers de nouveaux horizons. À bord, toutes les langues résonnaient, se répondaient, criées par des voix rudes aux accents exotiques. Bordant les vaisseaux, disséminés parmi la foule d'anonymes sobrement habillés de vêtements aux couleurs passées et aux manches rapetassées, on reconnaissait les notables aux couleurs vives de leurs tenues, aux belles coupes de leurs vêtements et à leur allure autoritaire. « __Genuensis, ergo mercator__[^genuensis] » disait l'adage, et leurs princes n'étaient pas loin de penser que le monde leur appartenait, à travers ses marchandises.

Aux abords des quais, Ansaldi devait se contorsionner pour avancer. La presse était telle qu'il était pratiquement impossible d'avancer en ligne droite, une balle ou son porteur se trouvant immanquablement en travers. Sans compter les collègues et partenaires marchands, qu'on ne pouvait croiser sans au moins les saluer selon le rang qu'on leur accordait. Les Malloni, Platealonga, Buronus, Vento, Embriaco, Spinola, Roza, Pedegola, Della Volta… tous avaient des liens de parenté plus ou moins distants. Le jeune Embriaco lui-même appartenait à un réseau familial très soudé, dont certains percevaient des revenus issus des anciens temps. De naissance, il faisait partie de l'élite génoise, ceux qu'on appelait les Visconti, les descendants d'Ydo Vicecomes. Mais pas un seul instant, il n'imaginait que la magnifique promotion sociale par lui connue, alors qu'il n'avait guère plus de vingt-cinq ans, était due à autre chose que son propre talent. Depuis plusieurs années il s'efforçait de monter une *societas* à destination de Gibelet[^gibelet]. Là-bas, il saurait faire fructifier ses intérêts et ceux de ses financeurs. Il y avait déjà accompagné plusieurs membres de sa famille, ainsi que des Buronus et des Ususmaris, dans toute la Méditerranée orientale. Lorsqu'il y avait résidé, il avait eu des contacts avec plusieurs personnes du royaume de Jérusalem, de lointains cousins qui pourraient l'assister le moment venu. Mais depuis qu'il avait réussi à convaincre Ingo della Volta d'investir chez lui, il ne se sentait plus de joie. Le consul était un homme extrêmement influent et il avait fallu tout l'appui de la famille pour décider le vieux patriarche à confier une somme à quelqu'un d'autre qu'Opizo Amico Clerico ou Ingo Nocenzio. Il était responsable d'une énorme somme, presque 300 livres, dont une petite partie seulement était en numéraire, précieusement gardée dans un solide coffre de chêne qu'il glisserait sous son lit.

Les abords du *Falconus* étaient plus calmes, le chargement enfin terminé. En haut de la passerelle d'accès, l'imposante silhouette du *patronus* Ribaldo Malfigliastro défiait quiconque n'était pas autorisé à monter. Son impressionnante barbe déployée sur son torse puissant, sa tignasse frisée comme une couronne autour d'un crâne tanné par les années en mer, ses yeux profondément enchâssés, tout en lui inspirait l'autorité. Il devisait avec l'un des passagers, un marchand levantin qu'Ansaldi avait rencontré lors des préparatifs, Sawirus de Ramathes. Comme l'homme était bien plus âgé que lui, le jeune négociant le salua le premier, sans se permettre d'interrompre la conversation. Ce fut finalement le *patronus* qui l'interpella.

« Auriez-vous question à me poser, maître Embriaco ?

— Nenni, je reviens juste avec mes dernières affaires. Me voilà fin prêt à embarquer. »

Le vieux marchand lui sourit amicalement, opinant du chef de façon débonnaire.

« Le *patronus* me déclarait justement que nous devrions mettre à la voile dès demain. Ne manquent que deux passagers notables.

— Les cieux semblent nous sourire, nous rejoindrons Naples prestement. Avec de la chance, nous serons Outremer bien avant la Toussaint. »

Ansaldi eut un sourire poli. Il regrettait de n'avoir pu trouver un navire plus grand, plus lourd, plus rapide. Mais ses contacts étaient pour la plupart au comté de Tripoli, au royaume de Jérusalem, et pas à Alexandrie vers laquelle les trajets étaient plus directs. Il lui faudrait donc supporter le cabotage le long des côtes, le navire ne s'aventurant en haute mer que pour la traversée depuis Otrante. Naviguer ainsi obligeait à s'arrêter chaque soir, à la nuit tombée, pour ne pas risquer de heurter un écueil ou de s'échouer sur un haut-fond. Et il bouillait intérieurement, impatient de pouvoir révéler ses talents, ses projets, ses ambitions. Il avait soigneusement préparé ce voyage et il devait au final admettre qu'il ne pouvait tout contrôler, malgré son immense talent et toute sa détermination. Enjambant la passerelle en trois foulées, il marqua un temps devant le *patronus* qu'il gratifia d'un geste amical. Il acceptait de s'en remettre à Dieu en fin de compte, et à Ribaldo Malfigliastro pour l'instant.

### Gênes, début de soirée du 1er septembre

Accoudé au plat-bord côté quai, Ernaut admirait la ville. Avec le crépuscule, le ciel s'empourprait à l'ouest et les reliefs enserrant la ville rougeoyaient comme des braises ardentes. Les abords des navires étaient désormais calmes, les hommes de peine étaient rentrés chez eux et les commanditaires fêtaient leurs succès dans les palais émergeant des quartiers le long des pentes. Sur le *Falconus*, au contraire, c'était l'effervescence. Tous les passagers s'efforçaient d'organiser l'espace qui leur avait été alloué, symbolisant les frontières par des sacs, des coffres ou tout autre objet à même de délimiter leur espace vital. Musardeur, Ernaut avait laissé son frère Lambert se charger de la corvée et s'était enfui vers la passerelle. Rares étaient les passagers autorisés à quitter le bord, et certainement pas les pèlerins. Il s'était donc rabattu sur un poste de vigie tout à fait acceptable, d'où il pouvait surveiller les allées et venues aux alentours du navire, satisfaisant à peu de frais son insatiable curiosité. Sur le quai, un vieux négociant aux vêtements orientaux faisait ses adieux à un groupe d'amis, venus en nombre. Arrivant au petit trot, deux cavaliers s'approchèrent. Celui de tête était à n'en pas douter un chevalier. Pas très grand, le front haut surmontant un nez aquilin, d'un port trahissant l'habitude de commander et d'être obéi, il menait d'une main experte sa monture. Sur sa longue cotte de drap orange, suspendu à un baudrier décoré, on pouvait admirer un fourreau et une poignée d'épée dont la valeur à elle seule aurait nourri une famille de nombreux mois durant. Lorsqu'il démonta, il s'entretint quelques instants avec le second cavalier,  certainement son valet, un homme rondouillard à l'impressionnante toison frisée que complétait une barbe épaisse. L'apparente complicité qui les unissait étonna Ernaut, peu habitué à voir des nobles frayer ainsi avec des serviteurs. Les deux hommes confièrent leurs rênes à un domestique qui les avait suivis à pied puis grimpèrent à bord en devisant joyeusement. Le jeune homme fut surpris qu'ils s'exprimassent en français, le valet avec une voix de tête un peu irritante. Le notaire de bord, occupé à superviser quelques matelots près de là, s'approcha respectueusement, s'exprimant avec un accent chantant.

« Je vous souhaite la bienvenue à bord, messire d'Eaucourt. Le *patronus* serait heureux de vous inviter à partager son souper. »

Le chevalier hocha un crâne dégarni à la peau mate.

« Ce sera pour moi grande joie ».

Puis, se tournant vers son domestique :

« Ganelon, vérifie que nos affaires ont été correctement chargées. Je ne voudrais pas avoir égaré d'importantes lettres. Et vois si maître Gonteux est là. »

Le notaire fronça les sourcils un court instant.

« Le jeune clerc ? Il est arrivé dans la matinée, comme la plupart. Il doit se trouver en bas, sur le pont passager. »

Tandis qu'ils devisaient, Ernaut suivait du regard les deux matelots occupés à déplacer à grand peine un tonneau, lorsque soudain ils heurtèrent la passerelle. Un cri les fit tous se retourner. Un jeune valet devançait son maître sur la passerelle, portant un gros coffret de bois ouvragé. Le choc l'avait dangereusement déséquilibré. Avant que quiconque n'ait eu le temps de réagir, Régnier d'Eaucourt s'était élancé et avait puissamment agrippé le bras du garçonnet. Mais le coffre tomba, dans une impressionnante gerbe d'eau. Depuis le quai, portant les mains à sa tête, Sawirus de Ramathes ne put retenir un cri désolé.

« Seigneur Dieu, pas mes *daftar*[^daftar] ! »

Sans prendre le temps de réfléchir, Ernaut sauta et fendit l'eau dans un éclaboussement sonore. Il ne lui fallut que deux brasses avant d'empoigner la lourde cassette que les eaux sombres commençaient à avaler. Il s'accrocha ensuite du mieux qu'il put à un poteau de bois planté le long du quai, servant à l'amarrage des navires.

Peu après, il était de nouveau à bord, sous d'épaisses couvertures, au centre d'un petit cercle de curieux. Le vieux marchand levantin ne tarissait pas d'éloges.

« Je ne sais comment te mercier mon garçon. Sans toi toutes mes archives auraient péri, et ma mémoire n'aurait guère suffi !

— Je rends grâces à Dieu d'avoir été utile. Si je peux aider… »

Le *patronus*, au centre du petit groupe, prit un air désapprobateur.

« Tes intentions t'honorent mon garçon, mais tu devrais prendre garde. Sauter ainsi à l'eau dans un port peut être fort périlleux. Je ne voudrais pas qu'un de mes passagers se noie avant même le départ.

— Je nage fort bien, maître. J'accompagnais souvent le mien père en Normandie et aimais à m'y baigner.

— J'en suis aise, mais abstiens-toi de ce genre d'exploit à l'avenir, je préfère égarer caisse que pèlerin. »

Le chevalier et son valet, qui avaient assisté de loin au repêchage du héros et à l'entretien, se décidèrent finalement à rejoindre le pont inférieur. Alors qu'il s'apprêtait à descendre à l'échelle, Régnier d’Eaucourt ne put s'empêcher de commenter, après un regard vers Ernaut.

« Sacré morceau, digne fils de Samson ! Je me demande ce qu'il donnerait en grand haubert, lance en main et heaume en chef, sur un destrier. »

Ganelon afficha un petit sourire candide.

« Ça donnerait une pauvre bête étouffée sous le poids ! »

Ce fut en s'esclaffant que le chevalier rejoignit sa petite cellule, délimitée par des tentures au centre du navire. Tandis que Ganelon s'occupait d'enflammer la mèche de la lampe à graisse auprès d'un voisin, il jeta négligemment sa chape sur le lit et ouvrit en fredonnant le coffre où il celait ses biens les plus importants. Mais sa bonne humeur retomba immédiatement lorsqu'il réalisa que ce qu'il espérait y trouver en était absent. Agenouillé devant le petit coffre appuyé au pied de son lit, il ne mit pas longtemps à s'énerver, remuant tous les objets patiemment rangés, les jetant rageusement sur sa couverture en maugréant. De retour derrière lui, l'air désolé, Ganelon ne savait que faire, dansant d'un pied sur l'autre. La tempête finirait bien par se calmer, comme toujours. Il fallait juste en repérer les phases les plus dangereuses. Par l'ouverture du rideau levé, les passagers aux alentours regardaient, troublés ou simplement curieux de tout ce remue-ménage.

« Incroyable que cela puisse disparaître ! Es-tu seulement certain de les y avoir mis ? »

Ganelon sentit que l'orage était en train de s'approcher dangereusement de sa personne et déglutit bruyamment.

« Oc je, messire, rangés près vos autres documents lorsque vous m'avez demandé de préparer tout ce qui avait trait à finances et mission. Je m'en souviens fort bien, je les ai pliés en un portfolio de cuir beige, décoré de feuillages, tâché, long assez mais peu haut. »

Régnier s'assit, l'air dépité. Il soupira.

« Adoncques réfléchissons. Revoyons en notre tête tout ce que nous avons fait depuis que tu les as pris. En l'hostellerie, où étaient-ils ?

— Avec d'autres pièces sur votre écritoire, près la fenêtre dans votre chambre.

— Fort bien ! Et ensuite ?

— Je les ai mis à la place des billets de change génois dans ce portfolio que j'avais à la main. Je l'avais sorti du coffre ferré.

— Qu'as-tu fait à ce moment-là ? Précisément ?

— Euh…, je l'ai posé… Non ! Attendez ! J'allais le ranger dans ce coffre comme d'accoutumée lorsque vous m'avez dit de vous les porter…

— … Pour les placer dans mon havresac, que je puisse les garder vers moi ! »

Un sourire triomphant sur les lèvres, Régnier s'empara de la lampe et se leva. Il poussa de la main gauche la chape posée sur sa couche et attrapa un sac de lin dont il vida vigoureusement le contenu. Et apparut parmi ses menues affaires une pochette de cuir plié que Ganelon ramassa et lui tendit.

« Mirez ! Le portfolio, messire !

— Ordre et méthode ! Voilà qui permet de se sortir outre toutes situations, ami ! »

Tandis que Régnier écartait sans ménagement le fatras accumulé pour se faire une place sur le lit et commençait à compulser les lettres, Ganelon jeta un coup d'œil dépité sur le couchage, recouvert de documents froissés, d'habits pelotonnés et d'ustensiles éparpillés. Le coffre béant présentait un ventre dans lequel on aurait dit qu'une tempête avait soufflé toute la journée. Et pour ne rien arranger, le roulis du navire incitait les objets à se répandre encore plus avant vers les autres passagers, sous les toiles faisant office de paravent. Avant même qu'il n'ait eu le temps de se pencher sérieusement sur l'épineux problème du rangement de tous ces accessoires en un si petit volume, dans une quasi-obscurité, le chevalier le sortit de sa réflexion :

« Plutôt que de bayer aux corneilles, tu devrais t'inquiéter de l'invitation du *patronus*. Va t'enquérir de ce qu'il en est. »

Sans même prendre garde à la réaction ou à la réponse de son serviteur, Régnier se replongea dans ses différentes lettres. Il rentrait d'une tournée en France, à laquelle plusieurs diplomates du royaume de Jérusalem avaient participé. Ils tentaient de convaincre des guerriers de revenir en Outremer pour continuer le combat. Mais les nouveaux projets du roi Baudoin[^baudoinIII] l'obligeaient à s'en retourner avant les autres. Des assauts vers le territoire damascène étaient prévus et les solides connaissances de Régnier sur cette région allaient être bien venues. Il ramenait tout de même quelques courriers d'intention, ainsi que des fonds qui seraient utiles. Mais l'échec de l'expédition prêchée par Bernard de Clairvaux lui-même quelques années plus tôt pesait sur les consciences[^secondecroisade]. Donc peu de nobles de qualité et aucun d'importance n'envisageaient pour l'instant de prendre la croix. Régnier n'était pas porteur de bonnes nouvelles pour son suzerain le roi de Jérusalem. Il avait beau lire et relire les feuillets, il n'y voyait aucun élément qu'il pourrait présenter de façon positive et cela l'angoissait depuis plusieurs semaines.

Nouveau venu dans l'administration de Baudoin, il craignait pour sa situation de manière générale et son fief de besants[^fiefdebesant] en particulier. Il s'en était ouvert à Herbelot Gonteux, un jeune clerc avec lequel il avait voyagé quelque temps en Europe. Ils avaient pris le même bateau, l'ecclésiastique dans le but de rejoindre l'office de l'archevêque de Tyr une fois sa formation terminée dans les plus grandes écoles d'Europe. Ce dernier l'avait un peu réconforté, estimant qu'un rappel précoce pour une nouvelle mission le dédouanait du peu de résultats de l'expédition en cours.

Les cris d'un enfant proche sortirent Régnier de ses réflexions. Il avisa soudain le désordre qu'il avait créé et commença à tout rassembler, en tas hétéroclites. Enfournant avec vigueur les objets en vrac dans la huche, il lui fallut appuyer de tout son poids sur le couvercle pour arriver à la refermer et la verrouiller. Il récupéra alors sa chape et l'enfila avant de franchir le pan de toile ouvrant sur la zone centrale occupée par les voyageurs et pèlerins les moins fortunés. Quelques lampes éclairaient la salle commune en direction de l'escalier abrupt  montant sur le pont. La nuit semblait être tombée, l'ouverture ne révélant plus guère de lumière venant d'en haut.

Il avança prudemment, s'appuyant régulièrement sur les poteaux afin de ne pas basculer dans les emplacements que les autres passagers s'étaient appropriés. Couvrant les craquettements des structures, des voix s'exprimaient dans différentes langues, chuchotis et éclats accompagnant les jeux d'ombres sur les parois et les piliers. Des regards furtifs se fuyaient les uns les autres, cherchant à s'assurer un peu d'espace vital dans ce vaste endroit sans intimité. Quelques relents de cuisine, masquant les effluves de bois humide et d'odeurs confinées, vinrent chatouiller les narines du chevalier, qui commençait à avoir faim. Il monta sur le pont et aspira une large bouffée d'air marin. Les odeurs salines le mettaient en joie. Il repartait enfin vers les terres qu'il affectionnait tant, le foyer qu'il s'était choisi, sous le soleil éclatant de Palestine.

### Gênes, soirée du 1er septembre

La taverne, encombrée de tables, de bancs et d'escabeaux[^escabeau], était vaste mais de faible hauteur, chichement éclairée par des lampes à huile dispersées. De toute façon, on ne venait pas là pour se voir, mais pour s'y abreuver de tout ce qui pouvait alléger les cœurs. Les femmes présentes préféraient ne pas voir trop clairement les clients qui versaient quelques piécettes contre des caresses haletantes dans un recoin  plus sombre que les autres. L'endroit dégageait une odeur aigre, mélange de vieille sueur, de senteurs salines, de vin renversé et d'haleines toxiques, ajouté aux remugles arrivant d'une petite porte donnant sur la cour qui servait de lieu d'aisance. Mais c'était un lieu gai, bruyant et animé, où les marins beuglaient leurs refrains entraînants, riant de leur bonne fortune d'avoir survécu à une traversée de plus, palpant les chairs de compagnes éphémères, cherchant à éprouver la vie par tous leurs sens avant qu'elle ne leur échappe irrémédiablement.

Seuls deux hommes semblaient indifférents au tapage ambiant. Ils appréciaient aussi l'ombre bienveillante de l'endroit mais parlaient affaires, et entre eux il n'était question d'aucune intimité. Le plus imposant, dos à la salle, était un chauve corpulent qui cachait son compagnon encapuchonné. Il parlait le visage en avant, son gros nez luisant touchant presque le visage de son interlocuteur. Sans cesse, sa main nerveuse parcourait son crâne, comme pour le gratter.

« Tu sais, je n'ai pas toujours été simple palefrenier. C'est ce damné *Agnus dei* qui m'a foutu dedans. Cette satanée coque de noix a coulé, avec toutes mes denrées. Dont trois magnifiques étalons ! Tu t'en serais merveillé, si tu les avais vus… »

Son compagnon ne bronchait pas tandis qu'il bavassait, énumérant ses malheurs, ses échecs, expliquant pourquoi il en était là, au fond du  trou. Le seul moment où sa logorrhée s'interrompait, c'était quand ses lèvres embrassaient goulument le gobelet de terre pour arroser son gosier, trop sec pour continuer. Puis le monologue reprenait. Ce ne fut que lorsque son propre godet fut vide que son camarade l'interrompit, sans même s'inquiéter de savoir où il en était dans ses explications.

« Pourquoi tu as tenu à me voir ? Je confesse être là par simple curiosité et je n'arrive toujours pas à m'expliquer ce que je fais séant. »

L'obèse se recula, visiblement embarrassé, il massait désormais toutes les peaux flasques de son visage, l'air ennuyé.

« Tu n'as pas compris ? Je sais que c'est toi le tueur… Au moins pour le gros Oberto. »

Il avala une gorgée, ménageant son effet, puis ajouta :

« Mais je te comprends, pour sûr. Ce n'était qu'une outre pansue, emplie de vent, un riffardeur[^riffardeur], sans aucun égard pour personne. Pour ma part, je dirais : bon débarras, et que les démons lui rôtissent le cul jusqu'à la fin des temps. »

Il frotta ses deux mains l'une contre l'autre, tel un Pilate au rabais.

« Ce que j'escompte juste, c'est d'en croquer un peu, tu vois ? Je suis prêt à me salir les mains, pas de souci. J'étais assez doué au couteau en ma jouvence. »

Son taciturne compagnon hocha la tête, reniflant son gobelet et lâcha, comme à regret :

« Tu aurais des projets en tête ?

— À foison ! Je pourrais me louer facilement chez un riche marchand. Nous pourrions le saigner puis le dépouiller. »

Le gros valet ne put s'empêcher de s'esclaffer, se pourléchant à l'avance.

« Pour ma part, j'aurais profité de la meurtrerie de Pedegola pour m'ébattre un peu avec sa jeune épouse, si tu vois ce que je veux dire… Je lui aurais montré que je n'étais pas une mauviette, un ribaud qu'on toise comme elle le faisait. Oh oui, pour ça, je suis prêt à te suivre, compaing. Jusqu'en Enfer s'il le faut ! »

Son interlocuteur lança un regard alentour, comme s'il craignait de voir des hommes de la milice se dévoiler parmi les consommateurs avinés. Mais il fut rassuré par ce qu'il voyait, la plupart étaient occupés à brailler et postillonner de la piquette, pour les moins éméchés d'entre eux.

« Il me faut aller, mon absence risque de soulever des questions.

— Pas de souci, je t'accompagne un bout de chemin, c'est pas si loin ! »

Joignant le geste à la parole, les deux hommes se levèrent et prirent la direction de la sortie. Dehors, le ciel était couvert et la lumière rare. Il leur fallait prendre garde où ils marchaient, les ruelles étant encombrées de déchets variés, d'écoulements suspects, et d'une faune grouillante qui s'en repaissait. Après un petit moment à déambuler dans les ruelles, longeant les travaux d'amélioration des défenses de la ville, le moins loquace reprit la parole.

« Comment m'as-tu découvert ? »

Le gros homme pouffa.

« Je t'ai vu, tout bonnement. Il m'avait semblé entendre un bruit à la porte mais lorsque j'ai desclos le guichet, il n'y avait personne. Je suis allé voir à la porte de derrière, par la cuisine. Les souillons œuvraient dans les chambres. Du coup seules quelques braises couvaient et quand j'ai ouvert, aucune lumière n'a passé. Je t'ai bien aperçu tandis que tu t'ensauvais après avoir saigné le vieux, à la lueur d'un éclair.

— Il y en a d'autres qui voudraient m'accompagner ? »

L'idée parut amusante à son compagnon.

« La moitié des valets de cette ville seraient prêts à te filer un coup de main, mais je ne l'ai encore proposé à personne. Ce sera à toi de voir. »

Le ventripotent valet stoppa, arrêtant son compagnon de la main.

« Que les choses soient bien claires, je veux pas prendre ta place. J'approuve et je pense pouvoir t'aider à gagner plus. Et si jamais la bande s'engraisse, ce sera toujours toi le chef, sur la tête des Saints !

— As-tu bien pesé les risques ?

— N'importe quoi plutôt que demeurer valet ! Je m'en suis ouvert : je suis prêt à te suivre en Enfer !

— Parfait… ! » susurra l'homme encapuchonné.

Puis, d'un mouvement vif, il dégaina un long couteau de sous sa cotte, qu'il planta dans le ventre de son camarade, lequel ne réussit qu'à prendre un air étonné, les yeux exorbités fixant la plaie sanguinolente, la bouche béante. Arrachant sa lame des entrailles, l'assassin la planta d'un geste dans la gorge, satisfait de réduire au silence l'insupportable pachyderme. Le palefrenier s'effondra dans un dernier râle grotesque.

Essuyant sa lame comme d'accoutumée dans les vêtements de sa victime, le meurtrier s'inquiéta du poids du corps. Il avisa les mœllons entassés pour les travaux, à quelque distance de là, et alla en récupérer un. Il ne fallait pas qu'on puisse reconnaître le visage de sa victime ni que personne ne puisse faire de lien. Tandis qu'il s'évertuait à fracasser le visage de son compagnon de soirée, il ne put s'empêcher de laisser sa rage exploser. Il était outré qu'on ait pu croire qu'il dépouillait ses victimes et ne tuait que pour s'enrichir. La fortune, le prestige, tous ne songeaient donc qu'à ça. Et que faisaient-ils de la justice ? Pensaient-ils vraiment qu'elle n'était que du ressort de Dieu ? Ou s'en moquaient-ils complètement ?

Lorsqu'il eut achevé sa sordide besogne, il contempla la masse de chair informe qui avait été le visage de l'infortuné Giovanni, palefrenier et marchand ruiné.

« En Enfer, t'y voilà, compaing ! Have[^haver] Pedegola de ma part ! »

### Côte ligurienne, matin du 2 septembre

Il semblait à Aubelet que le navire était secoué en tous sens, mais qu'il était le seul à s'en rendre compte. C'était la première fois qu'il voyageait par bateau sur la Mer intérieure et il regrettait de s'être embarqué. Il n'arrivait à garder aucune nourriture, se sentait faible et continuellement nauséeux. Ce matin-là, il avait décidé de tenter de prendre un peu l'air. Avec l'aide de sa jeune épouse, Maalot, il était monté sur la galerie extérieure longeant le bord du navire. Accroché à la rambarde comme un moineau à sa branche, il humait les embruns à pleins poumons, les yeux fermés, se concentrant sur la difficile tâche consistant à ne pas vomir par-dessus bord les quelques bouchées qu'il avait avalées. Il s'essayait à ouvrir les paupières de temps à autre, regardant au loin les côtes du paysage émilien.

Deux marins portant un long cordage passèrent à côté de lui, l'air rigolard. L'un d'eux apostropha la jeune femme :

« Il n'a pas bonne tête votre mari, maîtresse !

— Je le sais bien. Il n'y a donc rien que je puisse faire pour l'aider ? »

Les hommes d'équipage s'arrêtèrent et l'un d'eux réfléchit un instant.

« Donnez-lui à manger des pommes de paradis[^pommeparadis]. Mais ça sera dur à trouver à bord. Pour votre retour, achetez-en avant de partir.

— Ça prévient des flux de ventre ?

— C'est surtout que ça a le même goût quand ça sort que quand ça rentre ! »

Sa réponse fit pouffer son camarade et adopter un visage fermé à Maalot. Le marin se sentit désolé pour elle et voulut se montrer compatissant.

« De vrai, maîtresse ! Sinon, tenez-le chaudement, donnez-lui à manger, menez-le respirer le grand air, et ça devrait aller mieux. »

Assujettissant de nouveau sa prise sur les cordes qu'il avait posées, il hocha de la tête vers son compagnon pour l'inciter à reprendre leur avance, laissant le couple agrippé au bastingage. La jeune femme se tourna vers son époux.

« Peut-être aurions-nous dû pèleriner par les terres, comme ceux qui ont libéré le sépulcre du Christ[^croisade1a].

— Ne t'angoisse pas, je me sens mieux. Dieu ne m'abandonnera pas tandis que mes fidèles ont besoin de moi. Je dois surmonter cette épreuve, manigance du malin.

— Crois-tu vraiment qu'il y a là force maléfique à l'œuvre ?

— Ce n'était que façon de parler… »

Une nausée le réduisit au silence. Les yeux emplis de larmes, il hoquetait sans pouvoir se retenir. Il lui fallut quelques instants avant de pouvoir continuer.

« Mais il peut s'agir d'un signe, il faut nous purger de nos péchés avant d'aborder en Terre sainte. J'y ai pensé toute la nuit, cela ne peut être autrement. Le groupe doit se recueillir plus avant lors des prières. »

Un petit homme d'aspect replet, jusqu'alors un peu à l'écart, s'avança vers le couple, l'air sérieux et concentré.

« Vous devriez peut-être leur faire réciter le psaume clamant la divine miséricorde, mon père. »

Aubelet tourna la tête et découvrit un visage rond au nez pointu comme une étrave, surplombant une courte barbe s'élançant aussi  vigoureusement que l'appendice nasal. Au-dessus de cet étrange assemblage brillaient des yeux vifs profondément enchâssés. Il battit des cils, peu convaincu de la réalité de ce qu'il observait.

« Pardon ?

— *Miserere mei Deus secundum magnam misericordiam tuam* etc.[^miserere] Cela ne vous dit rien ?

— Si fait. Il m'appartient de savoir cela. Mais à qui ai-je l'honneur de parler ? J'ai vu tantôt que vous portez la tonsure des clercs. »

Le petit homme renifla d'un air suffisant et se gratta délicatement le nez, se mettant en scène comme s'il devait assister à son propre couronnement. Et il bomba le torse fièrement en se présentant.

« J'ai nom Herbelot Gonteux. J'œuvre à la chancellerie de monseigneur l'archevêque de Tyr, surtout pour des missions diplomatiques. »

Tout en parlant, il tendit la main pour montrer une bague frappée d'un sceau rappelant celui du prélat. Aubelet écarquilla ses yeux embués pour regarder le bijou, soucieux de marquer sa déférence envers si important personnage. Il fit un sourire maladroit à son interlocuteur.

« Quant à moi, je suis chargé d'une assemblée de fidèles, prêtre du village des Essarts Neufs en Vexin. On me nomme Aubelet. Et voici, euh…, mon amiete[^amiete], Maalot. »
Surpris, le clerc lança un œil horrifié non dénué d'animosité vers la jeune femme et répliqua d'un ton glacial.

« Ignorez-vous que dès le concile organisé par Sa Sainteté feu Innocent II[^innocentII], il vous est interdit d'avoir commerce avec la femelle ? Comment osez-vous faire visitance au royaume du Christ en tel état de péché ? »

Ne laissant même pas le temps à son interlocuteur de répondre autre chose qu'un bredouillis, il tourna les talons et s'éloigna, dans un bruissement théâtral d'étoffes.

« Quel goujat ! » s'exclama Maalot.

Aubelet soupira.

« Encore un de ces bacheliers à grosse tête, qui pensent mieux savoir le monde que le Créateur. À peine diplômés ès écoles de Paris, ils donnent leçon à tous, ponctuant de latin leurs phrases…

— Je plains ses ouailles en tout cas. Sauf s'ils sont pareils à Gringoire, bien sûr ! Auquel cas je serais fort curieuse de les voir discuter. »

La remarque de son épouse arracha un sourire à Aubelet. Gringoire la Breite était du voyage, un solide gaillard ayant fait fortune dans le commerce de la laine de ses moutons, et qui « ne s'en laissait pas aconter par les hommes du bourg » comme il le disait. Il avait d'ailleurs parfois la main un peu leste, oubliant que ses interlocuteurs n'étaient pas tous des valets à son service. C'était néanmoins un brave homme, désireux de s'amender, il avait d'ailleurs financé une large partie de ce voyage pour leur petit groupe. Et sa jeune épouse Ermenjart avait barre sur lui, arrivant à le raisonner et le persuadant de se comporter selon des préceptes plus chrétiens. Il oscillait donc entre crises de colère homériques et repentances tout aussi spectaculaires.

### Côte toscane, matin du 3 septembre

La frappe régulière du maillet sur le ciseau finit par réveiller le garçon pelotonné dans un coin du petit atelier. Il bâilla bruyamment tout en s'étirant puis souleva le bord de son bonnet de feutre, qu'il avait jusque-là ramené sur son visage pour dormir. Le charpentier occupé à tailler un cap de mouton[^capmouton] s'interrompit et l'interpella, l'aspect sévère de ses sourcils froncés contredit par un sourire aux lèvres.

« Tu en as mis du temps à desclore les yeux, Ingo ! Ton sommeil est un peu lourd pour faire bon gardien. Cela fait long moment depuis qu'on a hissé les ancres et déployé la voile. »

D'humeur taquine, il lança joyeusement une chute de bois à l'adolescent qui peinait manifestement à se réveiller et le regardait d'un air absent. Le garçon ne prit même pas la peine d'essayer d'esquiver le projectile et le reçut sur le torse.

« Il est temps pour toi de te rendre utile. Va nous quérir le manger et le boire. »

À peine plus dynamique qu'un mort, Ingo obtempéra. Il  manqua de tomber en heurtant du pied l'encadrement bas de l'ouverture dont le rideau avait été relevé pour laisser la lumière entrer. Regardant s'éloigner son jeune apprenti, le charpentier sourit et se remit à l'ouvrage. Il n'avait pas frappé dix coups qu'il fut interrompu par une personne venant s'interposer entre sa pièce et le soleil. Levant la tête, il vit l'imposante silhouette de Ribaldo Malfigliastro. Lorsque leurs regards se croisèrent, le *patronus* l'apostropha de sa voix de ténor.

« Nous encontrons un petit problème avec un poteau sur le pont médian, maître Fulco. Des balles mal attachées l'ont fort cogné.

— Très bien. J'aviserai dès mon apprenti revenu, qu'il puisse surveiller les outils en mon absence. »

Saluant de la main et sans plus de cérémonie le capitaine du navire s'éloigna tranquillement. Il traversa le pont en observant les hommes du coin de l'œil et n'hésitant pas à vérifier d'un geste assuré la bonne tenue des cordages et le rangement correct des voiles. Fulco se remit alors à la besogne. De nouveau, il n'eut pas le temps de frapper plus de quelques fois que le soleil lui fut derechef caché. Il releva la tête et vit un de ses compagnons, Octobono, qui le regardait, un large sourire sur le visage, une main sur les hanches et l'autre dans le dos.

« Toi, tu t'appareilles encore pour quelque mauvais coup ! » dit, tout sourire, Fulco à son ami.

Niant de la tête, Octobono brandit alors en gloussant une gourde de céramique sous le nez du menuisier.

« Je l'ai déjà fait, mon male coup ! Une bonne rincée de piquette du vieux Malfigliastro ! Ça te tente ?

— Si jamais il l'apprend, ça va nous coûter fort.

— Crois-tu ! Je l'ai gagnée aux dés hier à la veillée, contre Gandulfo, le queux. Je lui ai effacé sa dette contre quelques lampées de ce nectar. »

Puis il avala une large gorgée de son breuvage, avec un certain sens de la mise en scène, avant de passer le contenant à Fulco. Tandis que l'artisan goûtait avec délice l'excellent vin et approuvait d'une moue, le marin continua.

« Le nocher[^nocher] a indiqué qu'on relâcherait à Rome finalement, à deux jours d'ici. Quelques marchandises et une grosse huile à charger.

— Il me déplaît que des escales soient ainsi décidées, sans nous aviser. »

D'un claquement de langue, Octobono marqua son accord.

« Le vieux a eu l'accord du béjaune[^bejaune], de la famille du vieux consul…

— Celui qui s'aime comme pape ? C'est Ansaldi Embriaco. Moi j'ai ouï qu'il était familier du sire de Gibelet.

— Peut-être, je n'y entends guère en riches hommes. Enfin bon, celui-là même qui se voit tel le Messie. Il a donné son agrément. Apparemment il agoûte fort la nouvelle. »

Tout en parlant, Octobono reprit la gourde et avala une rapide gorgée avant de poursuivre.

« En espérant que ça va le rendre plus tranquille. Il est après Gandulfo, justement.

— Par ma foi, il ne sait pas les risques.

— De vrai. Figure-toi qu'hier soir, on y est allé à quelques-uns et on s'est arrangé pour mélanger à son manger… »

Il n'eut pas le temps de finir sa phrase et, de goguenard, il devint rapidement livide lorsqu'il entendit une voix l'apostropher depuis la plate-forme du château arrière.

« Octobono, vous ne croyez pas que vous seriez plus utile à besogner aux voiles ? Vous n'avez pas senti que le navire a lofé[^lofer] ? »

Profitant de ce que le capitaine ne pouvait voir ses mains depuis le balcon, Octobono lança le récipient de terre au charpentier et repartit en trottinant après un rapide clin d'œil. Fulco tenta furtivement de regarder vers le château arrière, mais le *patronus* avait déjà disparu.

### Côte du Latium, après-midi du 5 septembre

Tranquillement installé contre une caisse sur le pont supérieur, Enrico profitait du soleil. Il s'employait à graisser ses chaussures tout en sifflotant gaiement. Il avait disposé son grand bouclier comme un dossier et musardait autant qu'il s'activait en cette belle après-midi.

Ernaut était assis à distance respectueuse du soldat et faisait semblant de regarder les matelots opérer dans les gréements. En fait, il ne perdait pas une miette de ce que faisait cet homme au regard sombre, au visage barré d'une large cicatrice. La silhouette trapue et le torse puissant, son activité guerrière se manifestait dans le moindre de ses gestes. Son visage, le nez cassé surmontant une bouche sans lèvres ornée de dents brisées, ne laissait guère de doute. Enrico avait remarqué le manège d’Ernaut et appréciait d'attirer ainsi l'attention. Mais il était un peu circonspect, car le jeune garçon qui l'observait devait peser une fois et demie comme lui et mesurer un à deux pieds de plus en hauteur.

Le géant se décida à venir le voir, l'air curieusement enfantin. Il le salua de la tête et l'interrogea, tentant tour à tour de lui parler en français et en provençal, en montrant le grand bouclier.

« Bonjour, vous êtes soudard ? »

L'homme hocha la tête sans pour autant desserrer les mâchoires.

« Et possédez terres comme chevaliers ? »

Cette remarque fit rire Enrico.

« Oh non, sinon je croquerais plus meilleure pitance pour salaire. Suis arbalestrier et j'œuvre pour qui peut payer ma solde. »

Ernaut s'accroupit auprès du guerrier, l'air visiblement emballé à l’idée des confidences qu'il allait pouvoir tirer de son interlocuteur.

« Vous avez déjà bataillé Sarrasins ? Rencontré l'ost[^ost] du Soudan ?

— Non pas. Encore jeunot, j'ai suivi l'expédition à Minorca puis j'ai combattu en Espaigne pour le comte Ramon Berenguer[^ramonberenguer] ou l'empereur Alfonso de Castille[^alphonsecastille]. J'étais présent lorsque la sienne bannière a été placée en les tours de Tortosa !

— Jamais entendu parler. C'était où ? »

Le soldat arrêta son ouvrage et commença à s'exprimer avec plus de passion tandis qu'il égrenait ses souvenirs.

« Voilà bientôt dix années, quand le roi Louis et l'empereur Conrad ont pris route pour la Terre sainte. J'estais d'un groupe de fiers guerriers génois, alors tous guère plus âgés que toi ! Mordiable, quelle aventure ! Les bannières claquant sous le vent, les voiles ventrues, poussées par les chants des hommes impatients d'en découdre ! Imagine, plus de deux cents vaisseaux ! »

L'homme sourit devant l'expression ébahie de son auditeur, modulant sa voix pour animer son récit et y adjoignant de nombreux gestes démonstratifs.

« J'obéissais au consul Balduino, sur une des quinze galées pour Almeria, ce nid à pirates. J'ai suivi le valeureux Guglielmo Pellis, qui balisait le chemin avec les têtes des Sarrazins qu'il abattait par lance ou par épée. Un vrai lion ! La cité fut tôt prise, grâce aux engins de siège apportés. »

Réfrénant le sourire qui lui venait naturellement, le soldat se calma et fit mine de reprendre son ouvrage.

« Je suis demeuré avec Ottone de Bonovillano pour défendre l'endroit. J'ai tout de même été de la prise de Tortosa, histoire de ne pas perdre la main. Mais après plusieurs années de repos, j'ai eu désir de me frotter à ces féroces Turcs d'Outremer, tant vantés. »

Ernaut se frotta le nez, les yeux encore pointés vers ces lieux inconnus, la tête emplie de merveilleux combats qu'il s'imaginait avoir admirés.

« C'est incroyable, jamais je n'avais entendu parler de ça ! Peut-être parce que c'était en même temps que la campagne du roi Louis. »

Tout en discutant, le guerrier finissait distraitement l'application de graisse sur sa seconde bottine. Il glissa d'une voix tenue :  « Sauf que nous avons conquêté, nous. »

Ernaut fit claquer sa mâchoire, le visage soudain fermé.

« Ce n'est pas faute du roi. Son épouse l'a trahi[^alienor] et les Allemans n'ont guère aidé !

— Tu me parais bien jeunot pour avoir aussi sûre opinion. Tu aurais connaissance de secrètes informations ? »

L'adolescent toisa un instant son interlocuteur, pinçant des lèvres comme s'il répugnait à s'adresser à un homme de si vile origine, malgré son admiration récente.

« Ma parentèle et moi voyageons fort pour notre commerce et père a bons amis, notables en notre région, et même jusqu'à Paris, cité du roi. »

Le soldat s'esclaffa sans qu'un sourire ne s'affiche néanmoins sur ses lèvres, les yeux réduits à une fente.

« Mille excuses, je ne savais pas avoir affaire à si haute famille. »

Cette dernière remarque surprit Ernaut, qui se demandait s'il devait y voir de l'ironie ou pas. Il chercha à lire les intentions du guerrier, en le regardant fièrement dans les yeux, mais il n'obtint en réponse qu'un sourire énigmatique, accentuant sa confusion. Enrico posa sa deuxième chaussure et rangea avec soin son chiffon de graisse dans un pot à couvercle de bois.

« Je suis désolé, mais je dois te laisser, jeune ami. Nous aurons encore le temps de parler batailles, rassure-toi.

— Le merci, maître…

— Maza, je me nomme Enrico Maza.

— Et moi Ernaut de Vézelay.

— Bien aise de faire ta connaissance, Ernaut de Vézelay. »

Le soldat se leva prestement et lui tendit la main, un sourire franc sur les lèvres. Après une poignée rapide en guise de salut, il attrapa d'un bras vigoureux son bouclier et rejoignit ensuite la trappe qui menait, à l'avant, vers les quartiers des voyageurs.

Ernaut resta seul un moment, les yeux dans le vague, rêvant des hauts faits d'armes de son compagnon, dont il inventait la plus grande part. Puis il retourna vers son frère, sur le pont inférieur, se déplaçant comme un sergent vétéran au plus fort de la bataille, se gardant à droite, se protégeant de la gauche et taillant de coups habiles tous les adversaires imaginaires assez téméraires pour croiser son chemin.
