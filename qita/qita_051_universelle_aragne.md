---
date: 2016-03-29
abstract: 1156-1158. Enrico Maza, arrivé récemment en outremer en réponse à l'invitation d'un vieil ami, décide de repartir à l'aventure. Comme nombre de soudards, il espère trouver la fortune à la pointe de ses armes. Ou aurait-il d'autres motivations à suivre les troupes ?
characters: Enrico Maza, Mauro Spinola, Guilhem Torte, Willelmo Marzonus, Mestre Clarin, Chicot, Paylag
---

# Universelle aragne

## Tripoli, demeure de Willelmo Marzonus, fin de journée du samedi 10 novembre 1156

Willelmo Marzonus se trémoussa sur son siège, comme s'il y découvrait soudain une punaise. Il soupira longuement et tourna son regard vers la mer, où les franges écumeuses se teintaient de fauve sous un soleil déclinant.

" J'aurais eu espoir de te voir à mes côtés pour mes voyages au printemps.

- Comprends, je suis encore jeune et en pleine santé, il me semble que je pourrais amasser butin avant de te rejoindre.

- J'ai entretenu le même rêve et me voilà avec pilon de bois en guise de mollet !

- Tu as fait le soudard bien assez auparavant, avoue-le. Je veux juste te rejoindre comme associé et pas comme valet. "

Enrico Maza avala une gorgée de l'excellent vin que son ancien compagnon d'armes lui avait servi. Il avait envisagé que l'entrevue serait difficile, mais il ne pensait pas que cela l'affecterait autant de décevoir celui qui l'avait soutenu toutes ses jeunes années. Il se pencha en avant, coudes sur les genoux, le verre à la main, tentant de prendre un air engageant.

"Je vais te confier mon pécule dès maintenant. Il n'y a nul intérêt à le laisser dormir et je ne vais pas le traîner sur les champs de bataille avec moi.

- C'est de ta lame et de ton bras que j'avais grand besoin. Il est difficile de trouver bons soudards par ici.

- En effet, c'est ce qui m'a convaincu de rejoindre l'ost de Raymond le Jeune[^raymondIII]. Il aura sûrement goût à l'aventure, à venger son père ou conquérir nouvelles terres sarrazines.

- Son père a été assassiné par des fanatiques adversaires des Turcs, et était parfois l'allié des Damascènes, sais-tu ? "

Maza en écarquilla les sourcils de surprise. Il s'attendait à trouver une situation comme en Espagne, lorsqu'ils avaient conquis et pillé nombre de villes musulmanes : un terrain offert à la rapine et au saccage. Une excellente occasion de s'enrichir. Il plissa la bouche, vexé de se voir pris en défaut.

" Il y aura bien cliquaille et richesses à récupérer. On parle de prendre des cités aux Mahométans. Lors de ma venue depuis Gibelet, je n'ai entendu que ça.

- Le sac vaut mieux que le trousseau. Damas a échappé au roi.

- Et Ascalon ? Voilà belle prise, tu ne peux le nier ! "

Quoique de mauvais gré, Marzonus hocha lentement la tête. Il avait largement profité de la prise de ce port, véritable antichambre de l'Égypte. Il y faisait généralement escale lorsqu'il se rendait à Alexandrie pour ses affaires. Il veillait à ne pas commercer de marchandises prohibées, plus par souci d'éviter les complications que par crainte des interdictions officielles. Il négociait surtout des cordages et toiles de mers, et diverses denrées agricoles, en complément de cargaison.

Il soupira longuement, n'ayant nulle envie de se chamailler avec Enrico le jour même de son arrivée. Malgré sa déception, il comprenait le point de vue de son ami, ayant entretenu semblables espoirs un temps. Jusqu'à ce qu'il soit blessé, mettant un terme définitif à sa carrière guerrière.

Il tendit le bras, attrapa le pichet et invita d'un geste Enrico à approcher son gobelet.

" Tu as déjà quelques contacts auprès le castel comtal ?

- J'arrive à peine, fraîchement débarqué de la nef, et suis venu directement ici. J'espérai trouver quelque appui auprès de la Compagna[^compagna].

- Nul besoin. J'ai encore bons contacts dans l'ost. Des gens que tu pourras aller voir en citant mon nom. Ils te feront bon accueil, les arbalétriers sont fort appréciés, car bien efficaces pour repousser les archers turcs. Tu as gardé tout ton fourniment ?

- Harnois de guerre au complet, briqué et entretenu comme dot de pucelle depuis les campagnes d'Espaigne. "

Marzonus échappa un sourire. Enrico était bien tel que dans son souvenir, inconséquent et fanfaron. Il aurait aimé qu'il se montre aussi bon compagnon qu'autrefois. Il se leva avec lourdeur, toujours handicapé par sa jambe de bois.

" Allez, viens-t'en, compère. Nous allons te trouver grabats pour la nuit. Et avant cela, tu vas goûter à l'excellence de ma table ! "

Les deux hommes abandonnèrent la terrasse alors que la fraîcheur nocturne s'avançait depuis les côtes. Autour, l'agitation de la ville tombait et le ressac s'arrogeait la préséance, entrecoupé du bruit des navires gîtant à l'ancre. La mer s'éveillait à l'amorce de la nuit.

## Shayzar, abords de la citadelle, matin du lundi 21 octobre 1157

La matinée avait été fraîche, mais le soleil radieux avait fait monter la température rapidement. Les hommes s'étaient vite mis en bras de chemise, comme paysans en moisson. Depuis plusieurs semaines, les armées combinées de tous les princes chrétiens du Levant, plus celle du comte Thierry de Flandre, de nouveau outremer, tentaient de s'emparer de Césarée sur l'Oronte[^cesareeoronte]. La cité avait été aisément prise, mais l'arrogante citadelle, puissamment bâtie sur un éperon rocheux surplombant le fleuve et son pont, résistait encore.

Enrico encadrait un petit groupe d'hommes du comte de Tripoli affectés à une bricole[^bricole]. Il s'était vu confier la responsabilité du montage, habitué qu'il était aux subtils mécanismes de l'arbalète. Le plus compliqué pour son équipe était d'approcher des murailles adverses qui les dominaient, sous le feu intermittent des archers musulmans, et leurs projectiles les plus divers, parfois aussi répugnants que dangereux.

Essoufflé d'avoir porté plusieurs lourdes pièces de bois, Enrico aidait à assujettir le pied des mantelets destinés à les protéger. Il établissait une sorte de redoute qui permettrait de s'associer au pilonnage faible, mais régulier de la porte d'accès principal.

" Il faudrait tenir puissamment l'étai arrière, être sûr qu'ils ne sauraient l'abattre en projetant caillasse ou pierraille. Planissez donc le sol pour notre bricole et remblayez-lui autour avec ce que vous ôterez là. Je vais aux nouvelles et demander à ce qu'on nous porte de quoi boire. "

Assez autoritaire, il était malgré tout apprécié de ses hommes. C'était un vétéran qui devait sa place à son expérience, attestée ostensiblement par la cicatrice qui lui barrait le visage. Mais il avait aussi coeur à prendre leur parti lors des décisions difficiles et de s'assurer qu'ils ne manquaient de rien. Il avait trop souvent eu la gorge sèche et le ventre creux. Sans sensiblerie aucune : il était conscient que cela sapait le moral et coupait les jambes. Trop bien traité, le soldat était mou, mais trop mal, il n'était bon à rien. Il fallait le maintenir ainsi qu'une corde d'arbalète aimait-il à dire : assez tendue pour projeter puissamment le carreau, mais pas trop pour ne pas briser l'arc.

À l'écart de la forteresse, la vie de camp se déployait parmi les ruines de la cité. Ce n'était nullement leur oeuvre, les tremblements de terre ayant durement touché la région durant l'été. Les souverains francs s'étaient contentés de profiter de l'opportunité. D'autant que leur plus irréductible adversaire, Nûr al-Dîn, gisait moribond sur son lit, murmurait-on avec espoir.

Enrico arriva dans le quartier où plusieurs forgerons s'étaient établis. Ils frappaient sans discontinuer sur le fer pour chemiser de neuf les outils, pics et tranchants de pelles de sape, pour fabriquer les clous des mantelets et chats, pour les pentures, crochets et clavettes des engins de siège. Enrico aimait l'odeur du métal chauffé, le tintement des marteaux sur les enclumes, les vapeurs soufrées de certains charbons de terre, malheureusement fort rares en cette région.

Il reconnut la langue tonitruante de Paylag, l'Arménien responsable de son secteur. Pas très grand, mais poilu comme un ours, il semblait de fort gabarit en raison d'une barbe impressionnante, d'une propension naturelle à occuper l'espace et d'une voix de stentor. C'était pourtant un homme malingre, dont le physique sec ne laissait que peu présager de la puissance et l'endurance qu'il manifestait lorsqu'il mettait la main à la pâte. Il était maçon de profession, formé à tailler des pierres depuis son enfance, et dirigeait ici les tirs des engins sur les portions de muraille qu'il pensait les plus fragiles.

" Ah, maître Paylag, je suis aise de vous encontrer. Nous allons commencer le montage et je voudrais que vous veniez placer l'axe du trébuchet. Je ne sais quel ouvrage nous devons viser.

- Il me faut aussi vous faire porter les boulets de pierre. Le prince Renaud a fait venir grands renforts de carriers, nous en aurons plus qu'assez pour abattre leur porte. Outre, le maître engingneur Milos est de retour, avec les cordages et des cuirs bruts espérés. J'encrois qu'il fera visitance pour estimer notre travail. "

D'un geste de la main, il l'invita à venir sous l'auvent de branchage qui lui servait de bureau, de réserve et de chambre. Il décrocha une peau de chèvre, avala quelques gorgées et la tendit à Enrico. Avant de la prendre, celui-ci lui demanda :

" Quelles nouvelles des princes, s'il y en a ?

- Ils attendent, comme nous. Nous ne savons si leurs celliers sont bien garnis. Auquel cas ça risque d'être long. Ici, la sape n'est pas possible, les échelles peu aisées. Il nous faudra miner peu à peu de nos engins une courtine pour envisager l'assaut. Mais ils ont largement de quoi étayer au dedans et nous repousser.

- Lorsque la ville s'est si vite rendue, j'ai eu espoir qu'il en soit de même pour la forteresse.

— Ce n'est pas l'ost du Turc qui tient le castel, par malheur. Car nous aurions pu leur proposer honorable retraite. Ce sont des fanatiques de Masyaf, de ceux qui ont tué le père du comte. Nulle échappatoire pour eux. Ils n'en sont que plus résolus. "

Enrico avala une lampée de l'infâme mixture, vin passable mâtiné d'eau croupie dans une peau mal tannée. L'ingurgiter lui coûta une grimace, sous l'oeil indifférent de l'Arménien. Il lui rendit son outre et demanda s'il était possible de faire porter à boire à ses hommes. Et, avant de repartir, il posa la question qui lui brûlait les lèvres :

" Êtes-vous acertainé que ce sont des fanatiques au-dedans ? N'est-ce pas la cité d'un grand prince parmi les Mahométans ?

- Nul doute. Toute la famille du fameux prince est morte, ensevelie sous les décombres de l'été dernier. Les Bâtinites comme ils disent ici, ont été plus rapides que nous pour s'installer. Et maintenant nous devons les en déloger. "

Enrico avait déjà appris par d'autres que la famille régnante avait été décimée lors du tremblement de terre, mais il préférait en avoir confirmation. Il savait que tout ce qui avait trait au fameux Usamah Ibn Munqidh était sujet épineux. L'ancien ambassadeur de Damas auprès de la cour de Jérusalem était un homme aussi retors qu'imaginatif. Il était capable de récupérer la forteresse familiale au nez et à la barbe des Francs, l'occasion pour lui de se venger des affronts subis lors de sa fuite d'Égypte quelques années plus tôt. Mais était-il assez sournois pour requérir à une alliance aussi dangereuse ?

## Pont du Fer, midi du mercredi 22 janvier 1158

Un vent intermittent rabattait la poussière sur le convoi étiré dans la plaine. Chevauchant sur ses abords, projetant des graviers, une vingtaine de cavaliers progressait, l'air las, menés par un jeune homme à l'allure impétueuse. Ils avaient passé les murailles d'Antioche alors que le soleil n'avait pas encore dépassé le Staurin et avançaient à marche forcée en direction d'Harim par le chemin le plus court, d'une douzaine de lieues.

Les mules et chameaux, durement conduits par les caravaniers, portaient les pièces d'engins de siège supplémentaires pour assaillir la place forte. Autour des muletiers, quelques soldats, l'arme à la main complétaient la protection. Mais nul n'était inquiet. Ils étaient dans la vaste plaine de l'Amik, dont les richesses agricoles seraient à eux s'ils arrivaient à prendre Harenc une nouvelle fois. De plus, le tell d'où s'élançait la forteresse permettait de surveiller très loin les accès depuis Alep, via Azaz ou al Atharib. Une position stratégique pour se prémunir d'attaques futures contre la principauté, mais aussi un excellent point de départ pour tenter de s'emparer du coeur de la puissance turque.

Parmi les piétons, l'arbalète sur l'épaule, Enrico avançait d'un pas lourd. Il n'avait pas bien dormi la nuit précédente, ayant profité fort tardivement des charmes de la grande cité du Nord. Il regrettait un peu la fatigue, mais nullement les abus. Les prostituées du camp, lavandières à l'occasion, n'étaient pas si nombreuses et il détestait devoir attendre son tour. Comparativement, Antioche accueillait suffisamment de lieux de plaisir pour contenter toute une armée.

Il leva la tête des pieds du soldat devant lui et aperçut l'agglomération proche du Pont du Fer. Récemment repris, il commandait le meilleur passage sur l'Oronte dans la région. On racontait que ses solides arches de pierre avaient été édifiées par un grand empereur romain, celui qui avait doté Antioche de ses formidables remparts. Enrico n'y voyait qu'une halte bienvenue, pour abreuver les bêtes et faire le point sur le chargement. Ils avançaient tellement vite qu'il était difficile de ressangler un sac mal fixé, de réparer un lien rompu. En outre, cela voulait dire qu'ils avaient accompli à peu de choses près la moitié du chemin.

Les quelques soldats demeurés là en faction pendant la campagne les accueillirent de plusieurs forts coups de trompe, saluant la bannière de Raymond de Tripoli. Enrico devait reconnaître qu'il avait de la sympathie pour le jeune homme. Âgé d'à peine vingt ans, il semblait infatigable, toujours à aller et venir sur son superbe coursier alezan. Lui aussi était très confiant dans leur mission, car il avait dédaigné son lourd destrier à la robe sombre, et ne portait pas de haubert. Ses longs cheveux bruns volaient lorsqu'il remontait la colonne en un rapide galop. Il n'hésitait pas à encourager les hommes, attendant de chacun qu'il se ménage aussi peu que lui.

Sauf qu'à la nuit tombée, il aurait droit à un vrai lit dans une tente aux motifs bariolés, tandis que les plus humbles se contenteraient une fois de plus d'un matelas de terre et d'un oreiller de pierre. Enrico regrettait le siège de Shayzar, car il avait pu s'y dénicher un appentis, un coin bien à lui, au sec, et avec un tas de paille. Un peu poussiéreuse, certes, mais c'était mieux que souvent.

Réalisant que le comte arrivait dans son dos, il se rapprocha d'un des caravaniers et l'encouragea d'une voix forte. Il n'espérait pas tant s'attirer les bonnes grâces du valet que l'attention de son seigneur.

" Nous voilà déjà en vue du pont, compère. Encore quelques lieues et nous verrons les murailles d'Harenc. "

Il accompagna sa remarque d'une bourrade sur l'épaule et ajouta, goguenard :

" Il ne faudrait long temps avant que nous les mettions bas avec ce que nous leur amenons ! "

Tandis qu'il déclamait avec force, Raymond passa à sa hauteur, le sourire aux lèvres, sans leur accorder guère plus d'attention qu'aux autres. Il était habitué à plus habiles manifestations et plus chevronnés courtisans. Mais Enrico savait attendre, une trop rapide ascension était toujours suspecte, et il était plus sage de ne pas brûler les étapes. Il s'était déjà vu attribuer l'encadrement de soldats plusieurs fois, et avait montré qu'il tenait assez bien en selle pour prétendre à faire partie de la cavalerie. Ainsi, il pourrait se voir confier plus d'hommes, ou accéder à des rôles plus ambitieux. Il lui fallait également donner des gages à ceux qui trouvaient que les Génois étaient trop présents, trop puissants, dans le comté. Raymond n'était encore qu'un enfant, entouré des fidèles de son père, et ne se constituerait que peu à peu une garde personnelle, promouvant ses amis et se dotant d'un réseau qu'il aurait choisi. Ce n'était qu'une question de temps.

Lorsqu'ils abordèrent le petit hameau, il semblait inhabité. Les fellahs qui s'entêtaient à vivre là voyaient aller et venir les armées depuis longtemps, et se méfiaient. Ils profitaient de la situation de passage obligé pour commercer, comme l'attestaient le modeste caravansérail et le souk attenant. Mais quand les épées et les lances remplaçaient les ballots de marchandises, ils se claquemuraient chez eux, attendant que l'orage se calme, une fois de plus. Une large portion de la ville, bâtie de terre, était à l'abandon. Le Pont du Fer était autant malédiction que bénédiction.

## Tripoli, quartier du port, fin de journée du jeudi 27 février 1158

Installée sous les arcades d'un entrepôt portuaire, la petite troupe de soldats fêtait joyeusement le retour dans la capitale du comté. Ils avaient tous la besace pleine du butin et des gages amassés pendant la campagne hivernale, et le coeur léger d'avoir une fois de plus survécu. Parmi les habitués du groupe occupé à festoyer, se goberger et chanter, il en manquait trois sur les douze, dont Cressin, qui attendait entre la vie et la mort chez les frères, à Antioche. La fureur avec laquelle les survivants se jetaient dans l'alcool et la débauche n'égalait qu'à peine les angoisses qu'il leur avait fallu surmonter. Mais aucun ne l'aurait jamais avoué.

Mestre Clarin, qu'on appelait ainsi sans que personne ne sache pourquoi, s'affairait à leur rejouer une des péripéties de ses combats, à grand renfort d'interjections, de grimaces et gestes outrés, et ils appréciaient fort ses talents de conteur. Ils étaient de toute façon tellement imbibés de vin que le moindre prétexte à rire leur était bon. Chicot, dont le surnom était, pour sa part, évident, se rapprocha d'Enrico, son gobelet à la main :

" Tu sais si on va demeurer long temps ici ?

- Comment le saurais-je ? Nous sommes en les murs depuis cette dernière nuitée.

- Avec le comte Thierry présent, je me disais qu'on allait peut-être reprendre les chemins d'ici peu.

- Et alors, tu t'es fait soldat pour quoi ? Rester au chaud à besogner ta mégère ? "

L'autre lâche un rire gras, qui se finit en rot.

" Si j'en avais une avec le tétin aussi fier que la Coleite, je saurais m'en contenter ! "

Les hommes étaient las des mois passés à marcher, installer le camp, assaillir des murailles. Ils n'avaient pas eu à subir les flèches des cavaliers turcs et s'estimaient chanceux pour cette fois. Ils saisissaient malgré tout la moindre opportunité offerte de se sentir vivant, même et surtout dans l'excès.

" Profitez donc de votre soirée, et des jours qui viennent. Si le ban est derechef appelé, nous le saurons tôt assez. Je vous le ferais assavoir."

En se levant, Enrico lâcha quelques pièces d'argent noirci sur la table, accueillies avec joie et célébrées de nombreuses bénédictions fort peu orthodoxes.Il laça son épée à sa hanche et attrapa le baluchon qui ne le quittait jamais, ainsi que son arbalète, dans sa housse de cuir, et son carquois. Il lança le tout crânement sur son épaule et s'engagea dans une ruelle proche, sur un dernier salut de la main.

Il serait bien allé se faire laver dans un établissement de bains, autant pour le plaisir de se décrasser que pour profiter des services des jeunettes espérées là. Mais il avait une mission impérieuse auparavant. Il était dans un bien triste état, avec ses souliers aux coutures usées, ses chausses pendantes et sa chevelure grasse. Mais il savait que cela n'était que de peu d'importance. L'accueil qu'on lui ferait ne serait jamais conditionné à son apparence, mais à ce qu'il avait en mémoire.

Il se soulagea dans une venelle, s'attirant un regard de désapprobation d'un gros notable accompagné de deux valets. Il rota à leur intention, leur offrant ainsi une occasion de tourner la tête, encore plus offensés. Son mépris pour ceux qui n'avaient jamais eu à défendre leur vie par le fer allant grandissant avec les années.

Il déambula un long moment dans les rues tortueuses qui montaient depuis le port vers les parties les plus opulentes de la cité comtale. Là-haut, dans le château, le roi Baudoin et le comte Thierry devaient célébrer leur réussite.

Le vieux maître de la Flandre était un homme cher au coeur d'Enrico. Il semblait être plus souvent en outremer que dans ses états, et il n'appréciait rien tant que chevaucher, lance au poing, parmi les lignes adverses. Le jeune prince d'Antioche était pareil, impétueux et colérique, mais téméraire comme un lion. Enrico aurait aimé le servir, celui-là. Mais il avait reçu d'autres ordres.

Il stoppa devant une porte discrète et sortit une modeste clef de fer, qui fit jouer sans bruit la serrure. Au-delà, c'était une petite cour, avec des écuries, un cellier, un atelier abandonné. Des lueurs vacillaient sur les bords des volets clos de la demeure. D'un pas assuré, il alla frapper à l'huis donnant dans les cuisines, un peu en retrait. Il n'eut pas longtemps à attendre, et le jeune homme qui lui ouvrit faillit échapper le barda qu'il lui lança presque au visage.

" Fais dire que je suis là, et fais moi donc porter de quoi adoucir ma gorge. Elle est si sèche que je crache de la poussière."

Puis il s'abattit sur un banc disposé près de l'entrée, étendit ses jambes. Des odeurs épicées venaient titiller ses narines, tandis que plusieurs domestiques préparaient le repas. Ils n'osaient guère lever la voix, intimidés par la présence du soldat. Le jeune revint rapidement et demanda à Maza de le suivre.

Une telle rapidité l'étonnait. Habituellement, on le faisait patienter au moins symboliquement. Pour bien marquer qui était le maître et qui était le valet. Il fronça les sourcils, inquiet, et se racla la gorge. Il plissa les yeux lorsque le gamin souleva la portière d'épaisse étoffe qui donnait dans la grande salle.

" Tiens donc, mais il semblerait que mes deux moineaux aient eu coeur de me rejoindre le même jour !", lança, d'un ton narquois le vieil homme qui se tenait au centre de la pièce, les mains dans le dos. En quelques mois, sa posture semblait s'être affaissée, peut-être empâtée, mais le regard de Mauro Spinola demeurait toujours aussi ardent.

Mauro rejoignit une silhouette installée sur un escabeau, près du feu. À côté de lui, Enrico aurait presque pu paraître riche tellement celui-ci avait l'air misérable. Ses hardes puaient à plusieurs mètres alentour. Des yeux fatigués dévisagèrent Enrico un instant avant de plonger de nouveau dans un gobelet de vin. Le vieil homme, indifférent à son aspect de mendiant, vint lui tapoter amicalement l'épaule.

" Guilhem, je te présente Enrico, qui nous sert fort loyalement auprès du jeune comte. " ❧

### Notes

Les simples servants d'armes sont rarement évoqués dans les sources, si ce n'est pour en décrier la barbarie et la vilenie dans les textes célébrant les hauts faits des puissants. À l'occasion certains chroniqueurs en déplorent le massacre lors des batailles, sans s'en émouvoir autre mesure. Il est pourtant évident qu'ils avaient su se rendre indispensables pour les sièges, fournissant la main-d'oeuvre pour mettre en place tous les équipements. En plus de cela, lors des affrontements en terrain découvert, ils constituaient une muraille mouvante de boucliers et d'armes de tir d'où s'élançaient les charges dévastatrices de la cavalerie. L'un sans l'autre n'avait que peu de chance de triompher des forces musulmanes.

Le royaume de Jérusalem était malgré tout régulièrement en demande de ces hommes de sac et de corde, surtout pour mener ses campagnes au-delà des frontières et s'emparer des forteresses. Une partie était issue des levées féodales, fournie par les contingents territoriaux, comme les chevaliers. Mais il est fréquemment mentionné le recours à des soldats rémunérés occasionnellement, qui coûtaient d'ailleurs fort cher au trésor royal. La pénurie chronique de combattants ne fera que s'accentuer, avec le recul de l'esprit de croisade, et ce sera une des causes qui expliquera la rapide conquête du royaume de Jérusalem par Saladin au lendemain de la bataille de Hattîn en 1187.

### Références

Cobb Paul M., *Usama ibn Munqidh. The book of Contemplation. Islam and the Crusades*, Penguin Books, Londres : 2008.

Élisséef, Nikita, *Nûr ad-Dîn, Un grand prince musulman de Syrie au temps des Croisades* (511-569 H./1118-1174), Institut Français de Damas, Damas : 1967
