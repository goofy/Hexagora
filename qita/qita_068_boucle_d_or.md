---
date : 2017-06-15
abstract : 1153-1158. Enfant de colons des environs de Saint-Jean d’Acre, Moryse aspire à un meilleur avenir que valet au service de son aîné. Il décide de partir au loin chercher fortune, de façon à fonder sa propre famille.
characters : Moryse le Bouclier, Guillaume de Lunel, Robert de Retest, Frère Rostain
---

# Boucles d’or

## Casal de Mimas, manse familial de Moryse, matin du lundi 9 février 1153

Le soleil déjà vaillant s’engouffra violemment dans la petite pièce dès que la porte s’ouvrit en grinçant. En retrait, plusieurs personnes portèrent la main à leur visage tout en fronçant les sourcils. Tenant la clenche, un baluchon à l’épaule, une fine silhouette découpait d’ombre la lumière. Ses cheveux frisés brillaient d’or, dessinant sur sa tête une couronne. Malgré ce signe de bon augure, l’ambiance n’était pas à la fête. Moryse avait décidé de partir contre l’avis de tous.

« Me bénirez-vous, père et mère, avant mon départ ?

— Pour ce que tu tiens en honneur mes avis, que vaudra ma bénédiction ? grogna son père. Prends quand même garde à toi, la terre d’ici est pas tendre. »

Il leva le bras, comme pour faire mentir sa déclaration, mais sans aller jusqu’à se dédire devant sa parentèle. Il soupira et fit un geste de dépit. Hésitant à s’opposer à son époux, la mère de Moryse s’avança en lançant des regards à la dérobée. Elle embrassa malgré tout son fils et traça du pouce le signe de croix sur son front avant de marmonner quelque prière à la Vierge. Le jeune homme sourit et offrit sa bénédiction en retour.

« Ne soyez pas tristes, dès que j’ai trouvé bonne place où m’établir, je vous le ferai savoir ! »

Malgré ses fanfaronnades, il n’était guère rassuré, bien moins qu’il ne le laissait entendre. Il n’était que peu allé au-delà du casal, si ce n’était à Saint-Jean[^acre]. Cette fois, ses pas ne s’y arrêteraient pas. Il avait prévu de se rendre plus au sud, d’abord à Jérusalem puis là où les offres seraient le plus intéressantes pour un colon comme lui. Il avait serré dans un sac un peu de linge de corps, un canif et une petite vache en bois qu’il conservait depuis l’enfance. À cela s’ajoutaient une calebasse en guise de gourde et quelques provisions et c’était tout. Il comptait néanmoins bâtir là-dessus sa fortune, dont il ne doutait pas qu’elle l’attendait quelque part.

Il n’était que le benjamin d’une longue fratrie et n’avait comme avenir que de servir son aîné s’il demeurait dans le manse familial. Il avait un temps espéré devenir clerc, apprenant auprès du prêtre de la paroisse quelques rudiments de lecture et d’écriture, voire de latin. Il en tirait grande fierté, mais avait rapidement revu ses ambitions à la baisse quand on lui avait parlé du célibat et des contraintes liées au clergé.

Il se tourna pour partir quand son frère, désormais en charge de famille, se manifesta d’un bougonnement, avançant sa silhouette pataude. Il sentait la sueur et le mauvais vin, les cheveux perpétuellement en bataille. Sa voix habituée à porter dans les champs résonnait sous la voûte.

« Je dis pas que t’as raison, et je pense comme le père que tu fais grosse erreur, mais t’es un homme désormais, et que je sois damné si je m’oppose à ça. »

Il fouilla sous sa chemise et détacha de son braïel une petite bourse, guère ventrue. Immédiatement ses proches s’esclaffèrent, particulièrement son père, qui voyait là une manifestation de désaveu bien cruelle.

« Te bénir serait pas honnête envers le père, d’autant que je suis d’accord avec lui. Tes bras seraient bien plus utiles sur nos terres que chez des étrangers. Mais ce serait aussi grand hontage que de laisser partir comme un mendiant un des nôtres. »

Il lui claqua le sachet dans la paume.

« Fais-en bon usage, c’est le fruit du labeur des tiens, arraché maille à maille au sol d’ici. T’avise pas de le boire ! »

Ému par cette marque de solidarité, Moryse hocha la tête, avant d’empoigner son aîné pour une bourrade maladroite. Ils n’étaient guère familiers des embrassades.

« Vous entendrez bonnes nouvelles de moi avant la Noël, soyez acertainés ! »

Puis, après un dernier sourire à ses neveux et nièces, cachés dans les jupes des femmes, il leva la main en un adieu. Il ne tourna pas la tête avant d’avoir passé la croix Saint-Jean, à l’orée du village. Là, malgré toute sa détermination, il avait les yeux embués en apercevant une ultime fois son monde d’enfant. « Satanée poussière » maugréa-t-il en se frottant le visage.

## Jérusalem, réfectoire des indigents de Saint-Jean de l’Hôpital, milieu de matinée du lundi 9 mai 1155

Des cuillers choquant et frottant le bois des écuelles, des suçotements édentés et des raclements de gorges emplissaient l’espace, en contrepoint de la voix forte d’un moine replet, ahanant quelque livre saint. Nul n’osait parler et les arrivants traînaient leurs savates en douceur pour ne pas déranger la pieuse lecture qui leur était faite, quoiqu’aucun d’entre eux n’y entende rien. Ils venaient pour le brouet, la piquette et le pain et s’accommodaient fort bien d’un incompréhensible latin vrombissant dans leurs oreilles tandis que leur panse se remplissait.

Aux extrémités des longues tables patrouillaient des frères en robe noire, l’œil sourcilleux et la bouche pincée, s’assurant que nul ne volait le voisin ou ne se permettait le moindre abus. Parfois, ils indiquaient d’un signe de tête aux valets à l’entrée qu’ils pouvaient laisser pénétrer un certain nombre de nécessiteux. Chacun recevait une gamelle emplie d’un épais gruau et une tranche de pain, ainsi qu’un gobelet de vin.

Une fois leur pitance avalée, les hommes s’esquivaient par une petite porte, déposant couvert et écuelle dans des paniers affectés à cela. Périodiquement, des serviteurs venaient emporter le tout à la vaisselle, à travers les méandres de salles et de passages du grand Hôpital.

Se dirigeant vers la sortie tout en pourléchant une dernière fois sa cuiller, Moryse salua de la tête un frère posté près des corbeilles, peut-être pour éviter les vols. Comme ils étaient dans une galerie surplombant une courette, il s’estima autorisé à parler.

« C’est belle charité que la vôtre, frère. Je n’ai pas idée de comment vous faites pour emplir tant de panses.

— C’est là devoir de tout chrétien, nous n’en tirons nulle gloire ! »

Tout en répondant, il ne quittait guère des yeux les paniers dont on aurait pu croire qu’il craignait qu’ils ne disparaissent à tout instant. Il se risqua néanmoins un bref instant à dévisager le jeune homme devant lui, reconnaissant des traits familiers.

« Tu n’es pas pérégrin, il me semble avoir connaissance de ta face.

— Si fait, je suis en la Cité de temps à autre depuis des mois. Je cherche de l’ouvrage, mais bien peu ont de quoi payer manouvrier. Je n’ai suivi aucun apprentissage et une fois moisson et fenaisons achevées, il n’est guère de patrons prêts à lâcher monnaie.

— Pourquoi donc chercher à amasser de la cliquaille ? Es-tu sans parentèle, sans feu ni lieu ?

— Le manse n’était guère étendu, là d’où je viens, au nord. Alors je suis parti chercher fortune. »

Le religieux hocha la tête, tout en supervisant l’arrivée de paniers vides en remplacement de ceux qui débordaient. De la cour leur montaient des voix paillardes, accompagnant le roulement d’un lourd charroi. Des vidangeurs, à n’en pas douter, vu l’odeur méphitique qui vint rudoyer leurs narines.

« Sais-tu que nous recherchons toujours des bras ici, à Saint-Jean. Surtout en cette période, entre Ascension et Carême. Il y a tant de marcheurs de la Foi en nos murs !

— C’est que… N’y voyez pas offense, mais me jurer valet, ça ne me permettra jamais d’amasser de quoi avoir mien hostel.

— Diantre, et si c’est là seul labeur qui s’offre à toi ? Y vois-tu déshonneur ? Nous ne payons pas bien cher, de certes, mais tu auras héberge et pitance et, même, deux fois l’an, linges de neuf. »

L’idée, pour séduisante qu’elle puisse être, choquait les convictions de Moryse. Il craignait avant tout de s’enfermer dans un travail subalterne, qui ne lui offrirait jamais la possibilité de fonder sa famille. Il avait trop croisé de ces infortunés, réduits à vivre dans un galetas en attendant leur mort, à laquelle personne ne prendrait garde. L’hospitalier, voyant que ses arguments ne faisaient pas mouche, persista d’un air bonasse.

« Tu peux te jurer pour un temps, libre à toi d’ensuite courir les chemins de nouvel, fort du pécule amassé.

— C’est donc possible de ne rester qu’un temps ?

— Qu’encrois-tu donc, gamin ? Que tous les valets et commis ont fait pareil jurement que frère profès ? »

Acceptant de lâcher ses précieuses écuelles de vue pour se garantir d’oreilles indiscrètes, il s’approcha de Moryse et lui souffla.

« Outre, tu es bon Chrétien latin, ce serait misère de te voir affecté à tâche ingrate !

— C’est vrai que, vu comme ça…

— Nous pouvons même te garder en sûreté ton trésor, en attendant le jour de ton départ. J’ai dans l’idée que tu as creusé quelque terrier aux alentours où celer tes noix ! se gaussa-t-il. Chez nous, les coffres sont bardés de fer, remisés en solide donjon, sous la protection de bras puissants. Ton magot y sera sauf ! »

Ce dernier point enthousiasma Moryse. Il n’avait guère fait prospérer le tas de pièces confiées par son frère, malgré sa sévère discipline, mais il redoutait toujours de se le voir dérobé. Il en avait cousu une partie dans ses vêtements et le reste réparti entre différentes cachettes, dont une faille d’un vieux mur, dans les ruines d’une maison, à l’ouest de la cité. Il craignait sans cesse que de riches commerçants décident d’y rebâtir, lui interdisant l’accès. Il afficha finalement son plus large sourire.

« Qui dois-je voir pour me faire valet vôtre ? »

## Jérusalem, salle des malades de Saint-Jean de l’Hôpital, matin du mardi 9 juillet 1157

Frère Rostain gratta sa vieille tonsure de ses doigts tachés d’encre. Il récupéra un papier des mains de Moryse, face à lui, de l’autre côté du plateau encombré de tablettes, de feuilles et de documents. Celui-ci hocha la tête, tout sourire. Il avait fière allure, avec ses cheveux fous bien taillés à l’écuelle, sa cotte à peine ravaudée et son menton rasé de frais. L’hospitalier sortit une bourse d’une petite cassette ferrée qu’il avait rapidement ouverte.

« Ton bien est là-dedans, rien n’y manque, si tu veux vérifier. En monnaies du roi, besants blancs pour la plupart.

— Nul besoin, frère Rostain. J’ai toute confiance. »

Le jeune homme se hâta de nouer le cordon de la petite bourse à sa ceinture de braies et recouvrit le tout de sa chemise et de sa cotte.

« As-tu compère pour ne pas aller dans les rues avec pareille fortune sur toi ?

— J’ai solide bourdon qui saura éloigner les malandrins.

— Si tu le manies aussi lestement que le balai quand tu tenais la porte de l’hospice, je n’ai nulle crainte, s’amusa le vieux moine. »

Il soupira longuement, une nouvelle fois ému du départ d’un de ses valets. Il n’était pas toujours tendre avec eux, mais leur conservait une affection réelle, surtout avec les plus jeunes. Il avait vu Moryse gagner en confiance et devenir un homme fait, du jouvenceau qu’il était en arrivant. Il se leva pour accompagner son protégé jusqu’à la porte, profitant d’ultimes moments avec lui.

« TU demeureras en mes prières. Et si jamais tu passes par ici, aie la bonté de passer me saluer. »

Il lui pressa le bras, en un geste d’affection pour celui qui prenait son indépendance.

« Es-tu bien acertainé que c’est le moment de partir sur les routes ? Le roi lui-même a failli tomber aux mains des Mahométans…

— Je ne peux surseoir encore, mon frère. J’ai de quoi payer ma balle depuis plusieurs mois déjà. Vous m’avez convaincu de demeurer aux Pâques, puis à l’Ascension…

— C’est qu’il m’est cruel de me séparer de toi, gamin. »

N’ayant jamais eu la moindre démonstration d’affection paternelle, Moryse était toujours mal à l’aise lorsque le vieil homme s’épanchait ainsi. Il préféra ne pas relever et pointa un pèlerin qui venait d’entrer. La démarche chaloupée et l’épaule puissante, il avançait d’un air buté.

« Regardez donc ce pérégrin du Beauvaisis ! Il n’en faudrait guère comme lui pour enfrissonner tous les mécréants du Soudan. »

Le moine s’amusa de ce jeu, qu’ils affectionnaient tous deux. Moryse savait déduire une foule de choses de petits détails et excellait à déterminer l’origine des visiteurs rien qu’à leur aspect. Frère Rostain leva le menton en une interrogation muette, invitant Moryse à s’expliquer.

« Il a broderie de fil sur sa besace avec la croix à l’envers, comme celle de saint Pierre^[Dédicace de la cathédrale de Beauvais.]. Outre cela, il porte des savates à nœuds, comme j’en ai souventes fois encontré chez eux. Enfin, de nombreux Picards aiment à orner leur bourdon de décors ainsi qu’il l’a fait.

— Je ne sais si je retrouverai portier plus habile que toi ! Tu as le regard acéré de l’aigle. Dieu t’a fait grand don avec cela, apprends à le chérir.

— J’y compte bien, frère. Il m’a d’ailleurs été fort utile pour choisir les éléments de ma balle. Les gens au loin de leur hostel ont toujours besoin de boucles et d’épingles. Et savoir proposer la bonne à celui qui a noué laborieusement son cordon brisé saura m’apporter fortune.

— C’est tout ce que je te souhaite, gamin. Tu as bon cœur et frais minois, puisse la Sainte Vierge veiller sur les deux. »

Il pinça la joue de Moryse avant de le serrer contre son cœur. Celui-ci se trouva un peu dépourvu, ainsi plaqué contre la bedaine molle de l’ecclésiastique, dans les relents de camomille et de savon à raser.

« As-tu déjà choisi où aller en premier ?

— De prime, je vais prendre ma balle et payer ce que je dois, puis je vais m’enquérir d’un éventuel appel du ban. Le roi aura peut-être désir de venger l’affront récent. Les soldats ont toujours grands besoins pour leur fourniment. »

Frère Rostain lui adressa un sourire empreint de tristesse. Après une dernière embrassade rapide, il se hissa sur la pointe des pieds pour un baiser de paix en signe de bonne fortune. Puis il disparut dans l’ombre du bâtiment, abandonnant Moryse sur le perron avec son remplaçant, un jeune homme affligé d’une dentition magnifique, accordée par un créateur facétieux en réparation d’une cervelle défaillante. Fébrile à l’idée que son prédécesseur reprenne son poste, il se plaça au milieu du chemin, jambes écartées, pouces à la ceinture.

Moryse ne lui adressa pas même un regard, à son grand dam. Il dévala les marches avec entrain. Il recouvrait sa liberté, avait amassé un pécule pour lancer son propre commerce de colporteur. Il ne doutait pas qu’il arriverait à s’établir avant que l’année ne s’achève. Ce fut en fredonnant qu’il obliqua à gauche en direction de Malquisinat, bien décidé à s’accorder un bon repas pour fêter la journée.

## Montagnes de Samarie, matin du vendredi 26 juillet 1157

Moryse s’était levé bien avant l’aube afin de marcher avant les grandes chaleurs de mi-journée. La poussière s’accrochait à ses jambes déjà collantes de sueur et il suçait depuis plusieurs lieues un caillou de façon à économiser son outre d’eau. Les sangles de sa balle lui meurtrissaient les épaules. Il s’était chargé autant qu’il l’avait pu lorsqu’il avait entendu parler de la réunion de l’armée dans le comté de Tripoli. Il était à ce moment dans le sud, inquiet de la défaite récente du roi, et pensait se contenter de faire une tournée sans craindre d’éventuelles incursions turques. Quand il apprit que l’arrivée du comte de Flandre avait décidé Baudoin III à se lancer dans des conquêtes au nord, il s’en était voulu de cette idée imbécile. Il en était réduit à avancer à marche forcée, seul, loin de la caravane habituelle. Il espérait juste rejoindre les troupes avant qu’elles n’atteignent le territoire ennemi. On lui avait garanti qu’en tant que marchand, il lui suffirait de s’affranchir des taxes pour éviter les ennuis, mais il n’avait nulle confiance dans les assurances des musulmans.

Il longeait la fin d’une interminable restanque abritant des oliviers à l’allure impeccable. Le sentier s’affaissait ensuite brusquement, certainement en direction d’un oued. Moryse espérait y découvrir ne serait-ce qu’un filet d’eau pour se rafraîchir un peu. S’appuyant un instant sur son bâton, il tenta de déplacer les lanières sur ses épaules. Tout en se déhanchant pour gérer le poids de ses paquets, il laissait son regard vagabonder sur les environs. Ce qu’il aperçut le figea soudain.

À une trentaine de pas à peine se tenait un lion. L’animal, le pelage collé de poussière grise du chemin semblait lui aussi assoiffé, haletant, la langue sur le côté. Stoppée dans sa course, il balançait doucement sa queue avec indolence, le dévisageant avec intensité. Moryse sentit son bas-ventre échapper à son contrôle. Il avait entendu parler de ces bêtes, mais elles étaient fort rares sur la côte où il avait grandi. Il en percevait désormais les effluves lourds, acides. Il serra machinalement son bâton, bien incapable de savoir ce qu’il devait faire. Lorsqu’il vit les puissants jarrets se tendre et le fauve prendre son élan vers lui, il n’eut que l’instinct de se jeter au sol, ramassé sous ses paquets.

Le poids de l’animal lui coupa la respiration, lui écrasant la face sur les graviers. Il sentit une déchirure aiguë dans son épaule avant d’être violemment projeté en travers du chemin, raclant la caillasse et les rochers avec douleur. Il tenta de brandir son bâton, mais il lui fut arraché lorsque la bête vint chercher à le saisir de ses griffes. Une intense brûlure consuma sa joue tandis que sa vision se brouillait. Il se débattait comme il le pouvait, secouant ses membres sans arriver à repousser les assauts du prédateur.

Puis tout s’arrêta brusquement, son attaquant le libéra et disparut aussi vite qu’il était apparu. Moryse se retrouva aveuglé par le soleil, le visage cuisant de douleur, désarticulé sur ses paquets répandus sur le sol. Il se toucha la joue et découvrit sa main emplie de sang avant de perdre connaissance.

Lorsqu’il se réveilla, il était nauséeux, migraineux, allongé sur de la paille dans des senteurs familières d’animaux. Les effluves puissants du crottin et des chevaux lui semblèrent une douce fragrance après l’effrayante odeur du lion. Il pesta de douleur en tentant de se tourner, caressa un bandage sur son visage. Il était dans un fenil, une cruche à ses côtés. Il essaya de se soulever, mais la souffrance fut plus forte, il retomba sur sa couche et se rendormit peu après, accablé de tourment.

Ce ne fut qu’après plusieurs réveils et repos qu’il se sentit capable d’échanger quelques mots avec un gamin qui furetait dans les environs. Plusieurs valets lui apportèrent du bouillon quand il se déclara en appétit. Régulièrement un clerc changeait les linges posés sur sa face, lui administrant par la même occasion un breuvage destiné à apaiser les douleurs. On lui disait de dormir, qu’il était sauf. Il perdit le fil du temps.

Vint enfin le jour où il put sortir et faire quelques pas au dehors. Il s’aidait de son bâton, qu’il avait retrouvé. Il y avait découvert plusieurs entailles dues aux griffes ou aux crocs du lion. On l’invita à se rendre jusqu’à la tour centrale, au milieu de la cour, où il monta dans la grande salle. Le seigneur du lieu, Robert de Retest, à qui il devait la vie, tenait à le voir.

La pièce était vaste et richement décorée, en particulier la cathèdre multicolore de l’estrade. Celui qu’on lui désigna comme le maître était, une badine à la main, occupé à entraîner au mordant un imposant chien de chasse au poil court. Les mâchoires rappelèrent de douloureux souvenirs à Moryse. Bien que fatigué, il trouva la force de s’incliner devant le chevalier. De taille moyenne, le noble était aussi puissant qu’on pouvait l’imaginer d’un homme destiné à la guerre. Son visage rond, attifé d’une barbe argentée miteuse, n’arborait qu’un crâne couleur vieux cuir en guise de cheveux. Des sourcils épais, couleur acier, surmontaient deux yeux globuleux sans cesse en mouvement tandis que la voix grondait, aboiement plus que paroles.

« Voilà le miraculé ! J’ai bien cru que nous devrions te porter en terre sans même savoir ton nom.

— Moryse, sire. Je suis des environs de Saint-Jean. »

Tout en claquant sa cuisse pour intimer l’arrêt du jeu à son mâtin, Robert de Retest s’avança, un sourire proche de celui du lion sur les lèvres.

« Je te dois grand merci, jeune Moryse. Tu as bien failli en mourir, mais grâce à toi, j’ai pu enfin occire ce maudit félin qui assaillait mes bêtes et mes gens depuis des mois. »

Il alla se servir un verre de vin, qu’il avala en une longue et silencieuse rasade, avant de se planter de nouveau devant Moryse.

« J’ai décliné le ban du roi pour la chevauchée, car il me fallait mettre le holà à ses méfaits. Sa dépouille ornera bientôt ma chambre, ainsi qu’il sied. Pour te remercier de ton aide, bien qu’involontaire, je t’ai fait soigner et tu seras mon hôte aussi longtemps que tu en auras le besoin. »

Tout à la gestion de la douleur qui lui battait dans les tempes, Moryse arriva à peine à répondre, mais s’inclina en bredouillant un merci confus.

« Dès que tu te sentiras d’attaque, tu pourras prendre place au bas bout de ma table. Tu y goûteras bonne cher, prompte à redonner vigueur et entrain à un jeune tel que toi. »

Puis il fit claquer sa verge sur ses cuisses et se détourna. L’entretien était terminé.


## Harim, marché du camp Latin, midi du mardi 7 janvier 1158

Avisant le ciel obstrué de nuages aux ventres lourds de pluie, Moryse hésitait à sortir du petit appentis dont il avait fait sa demeure. Rencogné à l’angle d’une ruine et d’une écurie, il y avait installé ses quartiers depuis plusieurs jours. Le lieu lui avait paru propice, car il ne se trouvait pas loin des chevaux. Les palefreniers de Tripoli venaient d’ailleurs faire boire les bêtes dans un proche bassin où le jeune homme avait pris l’habitude de se ravitailler en eau. Espérant que l’averse était finie, il se leva et assujettit sa balle sur ses épaules. Bien qu’il n’ait pas à se plaindre de ses ventes, il avait encore un lourd bagage.

Prolongeant la rue autour d’un bosquet de palmiers, une vaste cour avait été attribuée aux artisans en rapport avec la sellerie. On y trouvait même plusieurs maréchaux ferrants et un charron, plus souvent au travail sur les engins de siège que sur des attelages. Il avaient pris la place des commerçants autochtones, priés manu militari d’abandonner leurs magasins le temps de l’assaut. Ils avaient néanmoins solidement barré leurs devantures afin de rendre le pillage difficile. Chacun s’était donc aménagé une cabane de son mieux, avec une bâche pour les plus riches, sous des feuillages ou des planches de récupération pour les plus modestes. Certains avaient même négocié avec les locaux la location d’une échoppe.

Chaque jour, Moryse déambulait parmi les bourreliers, selliers ou chapuiseurs[^chapuiseur], proposant à tous ses boucles. Il en avait une large provision, de cuivre, de bronze ou de fer simple, parfois d’acier, voire à décor et faisait l’article à tous ceux qui levaient les yeux vers lui. Plutôt que passer son temps à démarcher les soldats pour réparer leurs baudriers et sangles, il estimait plus efficace de vendre par lots à des professionnels. Il n’avait néanmoins pas le droit de s’installer à demeure, car il n’était pas fabricant. Le mâalem[^maalem] était intraitable sur ce point : il n’acceptait que des artisans dans son souk. Moryse avait malgré tout tenu à lui verser un écot, bien qu’il n’y ait nulle obligation. Il avait bien compris qu’il serait fort utile de s’en faire un allié en cédant de bonne grâce quelques deniers. Si d’aventure un client ne lui payait pas son dû, il savait qu’il trouverait une oreille attentive à ses problèmes.

Alors qu’après une vente, il remballait de la marchandise, soigneusement roulée dans des chiffons huilés, il prit conscience d’une présence dans son dos. Se retournant, il découvrit la face joviale de Guillaume de Lunel, maréchal du comte de Tripoli. D’une taille modeste accentuée par un physique musculeux et ventru, son nez cassé et sa longue mèche de cheveux étaient connus de tous. Il était réputé pour son caractère affable, ses blagues douteuses et sa familiarité avec tous.

« Alors, le boucléeur ? On fait la retape ?

— Faites excuses, sire maréchal, je n’avais pris garde à votre présence, je vous laisse la place.

— Doux, l’ami, c’est toi que je piste ! »

Il sortit d’une besace une paire de boucles de bel aspect, étamées, avec un mordant décoré de guillochis rehaussés de cabochons de verre coloré. Moryse en fit une moue d’appréciation. Même brisées, elles conservaient une forte valeur.

« Aurais-tu semblable parure dans tes fourniments ? Le lormier[^lormier] n’a rien qui puisse plaire au jeune sire comte… »

Moryse déglutit lentement. C’était la première fois qu’il avait l’occasion de fournir une personne aussi prestigieuse. En concluant cette vente, il pourrait à l’avenir faire valoir la qualité de ses produits à chacun. Il bredouilla tout en remuant désespérément son ballot de façon à le lacer. Il avait justement ce qu’il fallait, un investissement un peu fou en vue d’une telle opportunité. Deux boucles dorées, avec des motifs en émaux. Il y avait laissé une vraie fortune, mais estimait que c’était aussi une façon d’épargner. Par prudence, il ne les rangeait pas avec les autres objets, mais dans les plis d’une étoffe qu’il se roulait autour du ventre. Il répugnait à se dévoiler ainsi sur la place du marché.

« Si vous acceptez de m’attendre un court moment, à peine quelques *Pater*, je reviens avec ce qu’il vous faut, sire maréchal !

— Voilà qu’il me fait soupirer comme bachelier après pucelle. Hâte-toi donc et ne me fais pas attendre en vain !

— Je vous donne mon gage que cela sera du plus bel effet sur le filet du sire comte. Je reviens de suite. »

Brinqueballant son sac en tous sens, il manqua de s’empêtrer dedans et de tomber à plusieurs reprises tandis qu’il s’empressait de dénicher un recoin tranquille. Là, il dénoua fébrilement sa ceinture pour aller fouiller dans sa cachette. Lorsqu’il revint, il tenait dans ses mains ses trésors, qu’il frottait pour en faire ressortir la brillance. Le maréchal ne put s’empêcher de siffler.

« Dis donc, gamin, tu as dépouillé évêque ou abbé pour tenir pareilles beautés ?

— Je les ai achetés à bon artisan de Jaffa, j’en fais serment. Lui-même…

— Paix, gamin, je t’asticote. C’est exactement ce qu’il me faut ! »

Il s’empara des deux boucles, provoquant un haut-le-cœur dans la poitrine de Moryse. Ils n’avaient même pas parlé du prix. Il se sentait incapable de faire une proposition tellement cela se bousculait dans sa tête. Il déglutit, cherchant comment aborder le sujet. Mais le maréchal, tout en hochant le menton, ne lui laissa pas le temps de s’inquiéter.

« Tu passeras voir mon trésorier pour te faire payer. Je le préviendrai. »

Puis, goguenard, il frotta la tête de Moryse.

« Qui aurait cru qu’un nez crotteux comme toi cachait pareil trésor ? Je crois que tu mériterais bien du surnom de Boucles d’Or, avec ta tête en guise d’enseigne. »

Le sourire qui s’afficha sur le visage de Moryse accentua la profonde cicatrice qui déchirait sa joue de l’oreille aux lèvres. Sa chance avait tourné, il le sentait. Tout irait pour le mieux à présent. ❧

## Notes

La distribution des biens se faisait par le biais de nombreux intermédiaires et ce n’était pas chaque fois directement au fabricant qu’on achetait ce dont on avait besoin. C’était d’autant plus vrai lors des campagnes militaires. Les armées étaient suivies d’un marché où les négociants proposaient leurs produits contre le fruit des rapines ou des soldes. Pour autant, l’argent n’était pas toujours remis à l’instant même et il n’était pas rare qu’on soit réglé à échéance. Parfois les ardoises s’accumulaient jusqu’à certains montants, surtout pour les achats les moins onéreux, étant donné le fort pouvoir libératoire de certaines monnaies.

Les échanges avec frère Rostain, pour ambigus qu’ils puissent paraître, n’ont pas forcément de connotation sexuelle. L’univers affectif de nos ancêtres médiévaux est différent du nôtre. Leur vision de l’amour, de l’amitié et le rapport charnel que ces deux notions impliquent n’est pas simple à appréhender pour nos esprits contemporains. L’univers affectif est un sujet relativement peu connu de l’historiographie bien qu’étudié depuis que de grands noms comme Lucien Febvre ou Marc Bloch s’y sont penchés.

## Références

Boquet Damien, *L’ordre de l’affect au Moyen Âge. Autour de l’anthropologie affective d’Aelred de Rievaulx*, Caen : Publications du CRAHM, 2005.

Carré Yannik, *Le baiser sur la bouche au Moyen Âge. Rites, symboles, mentalités. XIe-XVe siècles*, Paris : Le léopard d’or, 1992.
