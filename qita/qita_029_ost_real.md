---
date: 2014-04-15
abstract: 1144-1150. Tandis que les puissants et leurs serviteurs armés vont et viennent, s’emparant des territoires et des cités comme s’il s’agissait de fruits mûrs, les populations subissent. Alors que le comté arménien d’Édesse n’en finit pas de se désagréger sous les assauts turcs, un jeune homme, fils de croisé latin et d’une arménienne, tente de survivre malgré la tourmente.
characters: Eudes Larchier, Adam de Baysan, Daimbert, Guillaume de Galilée
---

# Ost réal

## Baisan, soir du vendredi 30 mai 1158

Au loin se dessinait la modeste fortification, entourée de palmiers et de sycomores, noyée parmi les maisons du casal et les ruines antiques. Un tel amas de pierre taillées était toujours une aubaine, et on ne se privait pas d'y prélever tout ce qui pouvait être utile. En deçà se dessinait la butte plongeant dans la vallée du Nahal Harod. Eudes ralentit un peu l'allure et chercha un meilleur confort en selle.

Le cri d'un rapace lui fit tourner la tête en direction du fleuve, dont il apercevait la ligne verte sinuant au milieu des plantes desséchées à l'approche de l'été. Un lent convoi soulevait la poussière du chemin, s'éloignant en direction de l'ouest, sans qu'il ne soit possible de savoir si c'étaient des voyageurs, des commerçants ou des soldats. La plaine qui s'étendait entre Samarie et Galilée, bordée par le Jourdain, était parsemée de villages et de champs, de cultures dont il ne restait désormais plus que les traces poussiéreuses. Les zones iriguées par la main de l'homme arboraient un vert insolent ponctuant l'étendue roussie.

Malgré une position peu éminente à la Haute Cour, Adam de Baisan était un homme riche et puissant, qui devait le service de vingt-cinq chevaliers. Jouxtant la zone frontière, il maintenait l'ordre dans la région et protégeait un accès possible vers le cœur du royaume.

Dépassant sans ralentir le pas les maisons qui se ramassaient autour de la fortification, dans l'ombre des bosquets, Eudes avaient hâte de délivrer son message. Il avait chevauché depuis le matin, quittant Jérusalem aux aurores pour parvenir ici avant la fin de journée. Voyant des valets occupés à ranger et nettoyer une vaste zone aménagée pour la cavalerie, il ne put retenir un sourire. Le seigneur de Baisan maintenait ses troupes en excellente forme et les faisait s'entraîner régulièrement. Recevoir une rente de ses mains n'avait pas que des avantages. On passait plus de temps à user les cuirs de selle qu'à profiter de ses besants et des bliauds de soie rebrodée. On le disait également grand amateur de chasse, n'hésitant pas à aller chercher des proies et du gibier à son goût par-delà le Jourdain.

À peine Eudes eut-il le temps de s'approcher de la porte de la forteresse qu'un court son de trompe résonna et plusieurs têtes s'affichèrent entre les merlons. Une voix, avec un fort accent levantin, lui demanda de se faire connaître.

« Je suis Eudes, sergent le roi Baudoin, en service pour mander message au sire Adam. »

Un sifflement rapide fit ouvrir la porte et le cavalier entra. Des valets vinrent lui tenir la bride et l'un d'eux apporta même un pichet de vin coupé. La cour, empierrée grossièrement, résonnait de l'activité d'un domaine bien tenu. Plusieurs hommes allaient et venaient depuis les écuries, menant eau et foin aux montures qui avaient participé à l'exercice. Un bâtiment encombré d'échafaudages et d'appentis accueillait plusieurs maçons et la loge des tailleurs de pierre, qui devaient adapter les blocs arrachés aux ruines romaines voisines. Un nouveau logis prenait forme le long du mur est, dans le bruit régulier des poinçons frappant la pierre. Enfin, sous les arcades proches d'un haut bâtiment pourvu d'une cheminée, les cuisines certainement, des femmes s'employaient à plumer des volailles. À cela s'ajoutait des cohortes de serviteurs qui allaient et venaient d'un logis à l'autre, les bras encombrés de bois, de vêtements, d'outils, ou dans l'intention d'en quérir. Plusieurs enfants s'amusaient autour d'un arbre où ils avaient aménagé une cabane comme siège de leurs exploits.

Guidé par un jeune serviteur d'une douzaine d'années, Eudes monta à l'étage de la tour. Là, dans une vaste pièce aux murs chaulés et ornés d'un décor sommaire de faux appareil, le seigneur Adam de Baisan discutait avec plusieurs hommes, le teint aussi hâlé que le sien, en chemise et chausse, ainsi que le font des compagnons d'aventure. Ils étaient assis autour d'une table, avec une collation légère et plusieurs pichets de vin, qu'ils buvaient dans des verres ornés de pastilles colorées.

À l'écart, auprès d'une fenêtre, plusieurs femmes habillées de tenues élégantes discutaient tout en admirant des étoffes. Le valet abandonna Eudes près des soldats, qui se turent, curieux. Le seigneur du lieu avait une voix douce, agréable, mais la bouche était façonnée pour mordre, pour diriger les hommes dans la bataille. Il fronça les sourcils, habitué à plisser les yeux pour se prémunir du soleil et examina en détail la tenue d'Eudes tandis qu'il parlait. Ce faisant, il oscillait de son large buste.

« Quelles nouvelles portes-tu de notre biau sire Baudoin roi de Jérusalem[^baudoinIII] ?

– Sire Adam, le roi vous mande à son ost, pour chevaucher sus le turc Noradin[^nuraldin], qui assaille présentement une forteresse royale.

– Mes lances sont prêtes, j'ai ouï parler de la campaigne des infidèles. Où le roi nous veut-il ?

– Il cheminera au plus tôt, pour la cité de Tibériade avec le sire Thierry, comte de Flandre. Le connétable fera le conroi de là. »

Adam hochait la tête doucement, inventoriant en silence ce qu'il lui faudrait préparer. Il ne sembla reprendre conscience de la présence d'Eudes qu'un petit moment plus tard. Celui-ci n'avait pas osé bouger.

« As-tu autres missions à accomplir ?

– J'ai messages pour le sire prince de Galilée, et puis je dois joindre Saint-Jean[^acre] où il se peut encontrer quelque seigneur porteur de croix désireux de batailler avec le sire roi.

– Tibériade est trop loin pour que tu y sois ce soir, passe-donc la nuit en mon hostel, je te ferai lever tôt à la matinée, ce sera mieux.

– Grand merci, sire. »

Le chevalier fit un sourire poli et se détourna de Eudes afin de discuter de la campagne à venir avec ses hommes. Le sergent sortit de la salle et retrouva la chaleur de la fin de journée.

Le ciel n'était plus d'un bleu uniforme, des lueurs orangées commençaient à le ronger du côté de la mer. Eudes héla un valet et demanda un coin où s'installer pour la nuit. Puis il rejoignit un petit groupe de curieux qui tapait des mains et chantait. Une troupe de bateleurs présentait un rapide aperçu du numéro destiné à animer le souper du seigneur du lieu. Ils sautaient et jonglaient, accompagnés d'une chalemie[^chalemie] et d'une cornemuse. Une veillée de plus avant que la guerre n'arrive.

## Tibériade, midi du samedi 31 mai 1158

Le soleil faisait scintiller les eaux du lac sous un ciel orné de rares nuages d'altitude. Eudes chevauchait sur un chemin ondulant le long de la berge étroite, parmi les palmiers et les broussailles épineuses. Des canards disputaient à une bande de foulques le contrôle de la zone qu'il venait de quitter, où des touffes de roseaux s'accrochaient à la berge rocailleuse. Il avait chevauché un temps avec un cavalier au service de l'Hôpital, qui rapportait des nouvelles à l'intendant local. La préparation de la guerre touchait aussi les ordres religieux, qui avaient à nourrir et équiper de nombreux hommes. Mais un faux-pas de sa monture en avait arraché un des fers, la rendant boiteuse. Eudes avait donc dû laisser son compagnon de voyage en arrière, ne pouvant se permettre le moindre retard.

Il était soulagé d'entrer enfin dans les faubourgs, au sud de la cité, parmi les antiques rues laissées à l'abandon ou transformées en jardin. Il ne venait pas souvent dans la principauté, et appréciait d'autant plus le lieu. Sur les berges d'un vaste lac, où le Christ et ses disciples avaient vécu, la ville offrait un abord moins austère que Jérusalem, perdue au milieu des reliefs rocheux et poussiéreux. Les jardins étaient luxuriants et on disait que les bains, réputés depuis l'époque romaine, alimentés par des sources chaudes, garantissaient santé et vigueur.

Il chevauchait tranquillement parmi les ruines de l'ancienne cité le long d'un aqueduc, parmi les dalles oubliées, les murs éventrés et les colonnes tronquées. Certaines rues étaient encore habitées, souvent autour des édifices religieux ou de certaines zones thermales, mais la plupart avaient été transformées en verger, pâture ou simplement abandonnées aux herbes folles et à la végétation. Peu avant d'arriver à la muraille, il entendit résonner le marteau sur l'enclume, dans un bâtiment de belle taille qui devait abriter un établissement d'importance. Une forge, en dehors de la ville elle-même, pour éviter le bruit et minimiser les risques d'incendie.

Le sceau royal lui permit de passer rapidement dans les rues, où il demeura en selle. Tibériade n'était pas une cité très animée. On y croisait plus de pêcheurs que de négociants. Dépassant un entrepôt où des enfants et quelques femmes s'affairaient à enfourner justement des poissons dans des tonneaux de saumure, il fit un détour pour éviter un passage encombré. Une caravane de chameaux déchargeait une impressionnante cargaison de sel dans un bâtiment du port, sous le contrôle d'une poignée d'hommes en riche tenue colorée, abrités sous un auvent de toile sur une terrasse à l'étage supérieur du bâtiment.

Il parvint à l'entrée du château, au cœur de la ville et descendit enfin de selle, les jambes raides, le postérieur endolori. Il se fit connaître comme envoyé du roi, porteur d'un sceau royal pour sa mission. On le mena sans tarder parmi des couloirs frais, blanchis à la chaux, traversant des salles emplies d'une cohorte de domestiques. Par les arcades d'un passage où on l'abandonna un petit moment, il admira la cour centrale. De nombreux hommes discutaient, allaient d'un bâtiment à l'autre dans un ballet bien réglé. C'était une véritable ruche.

« Le sire prince va vous entendre »

L'appel le fit sursauter et il rejoignit le valet jusqu'à une porte qui ouvrait directement dans une vaste salle dont les larges fenêtres sculptées offraient une vue saisissante sur le lac. Il demeura interdit, surpris par la magnificence du lieu. Les murs, peints de tons chamarrés, présentaient une fresque aux motifs géométriques en partie basse tandis qu'au-dessus, des figures s'animaient. Le sol, en terre cuite, était émaillé de carreaux vernissés.

Un groupe d'enfants et de femmes était installé près des fenêtres, occupé à écouter un jongleur déclamer une histoire ou les nouvelles de lointains royaumes. Sur une estrade, à main gauche, un imposant siège sculpté accueillait des coussins et des fourrures de fauves. Devant, un groupe d'hommes discutait autour d'une table, un verre à la main. Tous étaient habillés de tenues de prix, mais sans ostentation. Le domestique l'abandonna près d'eux. Voyant que tous les regards se portaient sur lui et reconnaissant la figure altière du prince Guillaume au sein des chevaliers, il s'inclina respectueusement et se présenta.

Le seigneur de Tibériade et de Galilée avait un physique un peu empâté, mais de longues saisons la lance à la main avaient buriné son faciès mou, pour y tracer celui d'un visage fatigué mais passionné. Ses yeux s'agitaient en profondeur, sous les replis des paupières épaisses et les sourcils perpétuellement froncés.

« Quelles nouvelles le roi Jérusalem, sergent ?

– Je suis mandé pour vous informer de la venue prochaine du roi en vos terres pour y assembler l'ost réal. Le sire Baudoin court sus la troupe de Norredin qui assaille la forteresse des Caves de Suet[^suete].

– Le coureur est passé par mes terres. Mes féals sont déjà en route pour la cité. J'ai usage d'hoster le roi pour aller férir en terre sarrazine, il ne sera pas dit que j'ai failli à mes devoirs. »

Eudes inclina la tête. Sa mission n'était parfois que formelle, les seigneurs les plus importants, comme le prince de Galilée, ayant leurs propres informateurs, il était fréquent qu'ils prennent leurs dispositions avant même de recevoir les demandes officielles. Il hésitait à préciser la venue du comte Thierry, mais c'était certainement superflu. Il allait se retirer quand le prince l'interpela.

« Tu fais retour en la Cité ce jourd'hui même ? J'aurais message pour le sire roi.

– Non, sire, je dois encore aller à Saint-Jean, dans l'espoir d'y trouver quelque baron porteur de la croix désireux de se joindre à l'ost.

– En ce cas, tu peux aller. »

Il le congédia d'un signe de main, portant son attention à un des chevalier à sa droite. Eudes reprit donc le chemin vers la porte qu'il avait empruntée mais un jeune gamin au visage dévoré par une marque lie-de-vin l'intercepta et le mena par un escalier qui donnait directement dans la cour.

« Vot' roncin a été brossé et vous attend, sergent le roi.

– J'aurais bien aimé croquer collation avant de remettre mon cul sur la bête. Tu ne saurais pas me dénicher un peu de pain et de quoi mettre dessus ? »

Le gamin acquiesça et partit en trottinant. Pendant ce temps, Eudes vérifia ses sangles, inspecta les pieds de sa monture. Il resserrait un lacet maintenant ses fontes quand il vit plusieurs hommes s'approcher de lui. Le plus grand avait une bedaine bizarrement posée sur les allumettes lui servant de jambes. Un long nez pointait hors de sa face, encadré d'yeux globuleux.

Arborant un sourire courtois, il était escorté de deux solides gaillards, dont les muscles saillaient sous les vêtements tandis qu'ils portaient des sacs au contenu cliquetant. Il s'arrêtèrent près d'Eudes et lâchèrent leur butin dans un fracas métallique.

« Salut l'ami, on dit que tu viens de Jérusalem, de l'hostel le roi. Et qu'on te nomme Eudes.

– On dit vrai. Que me veux-tu ?

– J'ai nom Daimbert. On a là quelques rapines de nos derniers combats avec les païens. Il paraît que tu saurais quoi en faire. Ici, pas moyen de trouver à qui vendre un bon prix. Si tu nous trouvais des acheteurs, on serait prêt à partager avec toi les cliquailles. »

Tout en parlant, il dévoila le contenu de ses sacs : une demi-douzaine d'épées musulmanes, en assez bon état mais sans fourreau, des têtes de masse en bronze, en forme d'étoile ou de concombre, un casque d'origine turque. Et surtout, un large lot de pointes de flèches. Eudes se pencha pour les examiner.

« Ce sont des traits turcs, pas évident à emmancher pour nous autres…

– Ouais, je sais bien, mais ce serait dommage de les vendre à fonte.

– Le truc, c'est que je n'ai rien sur moi, là.

– Aucun souci, mes deux compères seront témoins de ce que tu prendras. Je serai à la Cité pour la Toussaint, si d'ici là tu ne m'as pas payé. »

Eudes examina une fois encore les armes, hésita. Pendant ce temps, le gamin était revenu avec un morceau de pain, des dattes et plusieurs lanières de poisson séché.

«Je te prends les épées, ça je sais que je pourrai leur trouver acheteur.

– Bien, je vais te les nouer dans un de ces sacs. »

Rapidement, les armes furent emballées dans un morceau d'étoffe grossière. Eudes jura devant les deux hommes qu'il acceptait le marché et conserverait un tiers du prix de la vente pour ses efforts. Daimbert et ses acolytes l'accompagnèrent à la porte de la cité et le saluèrent lorsqu'il lança sa monture.

En ce début d'après-midi, le soleil régnait sans partage sur les collines de Galilée, dont les teintes vertes du printemps roussissaient chaque jour un peu plus. Eudes sortit son vieux chapeau de paille et s'en coiffa, le fixant avec un morceau de tissus tandis que sa monture entamait la montée des reliefs conduisant vers l'ouest.

« Foutu moment pour prendre la route », pesta-t-il entre ses dents.

Il n'obtint pour toute réponse que le chant des cigales et des grillons.

## Saint-Jean d'Acre, soirée du samedi 31 mai 1158

Le ciel commençait à rougeoyer, projetant des raies enflammées sur les crêtes écumeuses des vagues. Une brise de mer apportait des senteurs salines et iodées, repoussant la puanteur des quartiers du port. La sueur des portefaix s'ajoutait aux effluves des mille produits déchargés là chaque jour, en plus des relents issus des ruelles, des latrines et des tas d'ordures que les porcs fouaillaient avec des grognements de plaisir.

Eudes avalait quelques beignets à la saveur épicée, aux rares morceaux de viande. Assis sur un muret face au port, il admirait la danse des mâtures au repos. Une armada de barques et de barges environnait les grosses nefs ventrues, les galées racées. Des lanternes accrochées aux châteaux étaient allumées peu à peu. Déjà, les voix se déplaçaient depuis les quais vers les rues environnantes, où les dos fatigués et les mains calleuses cherchaient le réconfort dans le jus de treille.

Il sortait à peine de la Cour de la chaîne, à la pointe sud de la cité, où il avait trouvé le châtelain royal. Les fortifications n'étaient pas si confortables que les vastes salles du bâtiment des douanes, même s'il était agité d'une activité bourdonnante du matin au soir. Il avait pu porter la convocation royale et n'avait plus qu'à se rendre à l'Hôpital, près de la muraille, au nord. La plupart des voyageurs fraîchement débarqués se retrouvaient là le temps de s'organiser. Un excellent endroit où rencontrer des chevaliers en quête d'une cause à laquelle vouer son épée le temps de son séjour en Terre Sainte.

Jetant quelques miettes aux mouettes, il se leva et remonta la grande rue qui menait droit vers l'église Saint-Jean Baptiste. Les minces échoppes s'alignaient, proposant à la fois les délices de l'orient aux nouveaux venus et les souvenirs de l'occident pour les exilés. Les devantures se fermaient tandis qu'il avançait, les paniers étaient rentrés à l'abri, les éventaires relevés et les sacs rangés. Des lueurs orangées se répandaient depuis les ouvertures, claustras et fenêtres des étages tandis que des voix se faisaient entendre sur les terrasses. Des chiens à l'allure misérable furetaient dans les ordures abandonnées par les marchands de nourriture. Une carriole emportait une vaste carcasse de viande enveloppée d'un linge humide vers la demeure d'un riche bourgeois ou le fondaco[^fondaco] d'une des opulentes cités italiennes. Acre se préparait pour la nuit.

Dépassant la vaste église d'où provenaient des voix célestes chantant les psaumes pour Vêpres, Eudes pénétra par un passage voûté dans la cour de l'Hôpital. Un portier affalé sur une chaise bancale ronflait consciencieusement dans un recoin, bénissant les arrivants de ses borborygmes. De nombreuses personnes se tenaient là, le long du bâtiment à l'ouest dont la vaste salle de l'étage servait de dortoir. Mais beaucoup dormaient dans l'église ou aux abords. La plupart étaient tellement perturbés après le long voyage en mer qu'ils se contentaient de dormir sous les arcades, profitant de l'air frais après des semaines, voire des mois, enfermés dans une cale malodorante. Quelques silhouettes habillées de bure noire s'occupaient de distribuer des couvertures, de répartir les couchages.

On entamait la saison où les arrivées se faisaient plus nombreuses chaque jour et les frères de l'Hôpital étaient habitués à gérer ce ballet incessant d'allers et venues. L'un d'eux, en tenue grossière et salie, mais avec une tonsure récente et un sourire épinglé sur sa face ronde s'avança vers lui et le bénit.

« Êtes vous marcheur de Dieu, mon frère ?

– Non pas, frère hospitalier, je suis sergent le roi Baudoin. »

Le moine sourit d'autant plus largement et l'invita à le suivre en direction du bâtiment à l'est.

« En quoi notre modeste maison peut porter aide au sire roi ?

– Je suis là pour voir si d'aucun chevalier aimerait se joindre à l'ost réal pour chevaucher sus Noreddin.

– Nul homme d'épée n'est ce jour en nos murs, je suis désolé. On dit que le *Luna*, parti en même temps que le *Peregrina* de Gênes, arrivé voilà plus de dix jours, ne devrait pas tarder. Il s'y trouvera peut-être ce que vous espérez. »

Eudes acquiesça en silence, un peu déçu, et fatigué de ses deux journées à chevaucher. Le clerc le dévisagea et lui posa une main amicale sur l'épaule.

« Auriez-vous désir de prendre votre souper avec nous ? Nous allons entamer la distribution sitôt vêpres achevées, au retour du frère hospitalier.

– Je vous mercie mais je préfère retourner avant la nuit en mes quartiers, à la forteresse le roi. »

Le moine hocha la tête en silence et le bénit, avant de le laisser pour aller prendre en charge un autre pèlerin. Eudes lança un dernier coup d'oeil circulaire à la vaste cour, bordée de bâtiments industrieux et surplombée par le fin clocher qui s'élançait vers les cieux, à l'est.

« Bon, d'abord, trouver bonne taverne où je pourrai trouver acheteur pour ces lames… »

Enjoué à l'idée de lamper quelques gorgées d'un bon vin importé, en la cité des mille plaisirs, principale porte du royaume, Eudes prit la direction du quartier du port en chantonnant pour lui-même

>« Dormir après souper et faire lardon
>Engloter poulets, canards et chapons… » ❧

## Notes

La composition des armées royales se basait sur les troupes des vassaux en plus des hommes directement rattachés à la maison du souverain. Il pouvait s'y ajouter des croisés désireux de mettre leur force au service de la chrétienté ainsi que de simples mercenaires, payés à la tâche. À cela s'ajoutaient les hommes entretenus par les ordres religieux indépendants, comme le Temple et l'Hôpital. La réunion de tout ceci demandait donc un certain temps, et l'armée n'était rassemblée que le temps de mener des opérations, de façon à éviter le difficile approvisionnement d'un groupe aussi nombreux, avec une grande quantité de montures et d'animaux de bâts. Contrairement à ce qui était la règle en Occident, le roi de Jérusalem pouvait en théorie maintenir son armée perpétuellement en état de guerre, mais le coût de telles dispositions incitait à se contenter de convocations ad hoc.

## Références

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge : Cambridge University Press, 2009.

Smail R. C., *Crusading Warfare, 1097-1193, Second edition with a new Bibliographical introduction by Christopher Marshall*, Cambridge : Cambridge University Press, 1995.
