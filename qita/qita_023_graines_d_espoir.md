---
date: 2013-10-15
abstract: 1157. L’installation de nouveaux colons européens dans les territoire des Terre sainte se faisait sous le contrôle des autorités, des propriétaires, qui offraient généralement des conditions avantageuses pour l’installation. Des villages furent ainsi créés ex nihilo ou à l’emplacement de lieux désertés en raison d’un état de guerre quasi-permanent depuis des dizaines d’années. Cela entraîna l’installation d’une nouvelle population, catholique à côté des musulmans, des orthodoxes et des obédiences chrétiennes orientales. Parmi ces derniers arrivés, on trouve dans les chartes un certain Lambert le Bourguignon.
characters: Ernaut, Lambert, Pisan, Saïd
---

# Graines d'espoir

## Jérusalem, vendredi 26 avril 1157

Au sortir du Saint-Sépulchre, Ernaut et Lambert s'étaient dirigés droit vers Malquisinat où ils savaient trouver de quoi se restaurer. Tout en fendant la foule, ils parlaient d'une voix forte, enjouée, ce qui n'était pas fréquent chez le plus âgé des deux frères. Aussi espiègle qu'un enfant, il faisait de grands gestes et riait d'un rien, attirant l'attention sur lui.

L'entretien avec l'intendant descendu de la Mahomerie s'était fort bien passé. Des terres et un logement l'attendaient désormais dans le casal au nord de la cité, pour peu qu'il prêtât serment et accepte à l'avenir de verser champart et payer cens. Rien que d'habituel en ce cas. La perspective le mettait en joie, même s'il savait qu'il s'y installerait seul, et pas avec Ernaut, comme cela avait été envisagé à l'origine. Pourtant, son entrain demeurait intact.

« Il va me falloir trouver semences pour le prochain automne. Le frère Pisan m'a dit que les semailles se font dès après les premières pluies, on récolte fort tôt ici.

– C'est qui ce Pisan ?

– L'intendant du casal, avec qui je parlais en sortant du cloître. Il m'a invité à faire visite aussi tôt que possible, que je voie les terres. »

Ils contournèrent un groupe dense d'ouvriers transportant des planches et des clayonnages, les vêtements collés de mortier. Lambert les suivit du regard tandis qu'ils les dépassaient.

« Il me faudra aussi trouver oustillement en suffisance. Mieux vaut l'acheter ici, j'aurais plus grand choix.

– Tu parlais de semences, mais as-tu terre à vigne aussi ?

– Certes, quelques maigres arpents, et je pourrai planter mes propres ceps en nouvelles pentes, des champs seront tôt ouverts. Il me faudra tonnels pour garder ma vendange et paniers à ramasser les grappes.

– Tout doux, frère, tu n'as pas encore le moindre cépage ! »

Lambert se mit à rire.

« Tu parles de raison. Il faut prévoir et dépenser droitement le pécule de père. Il ne s'agirait pas d'être à court lorsqu'il faudra œuvrer. Seulement alors j'aurai besoin d'avoir paniers, houe et serpe…

– Je pourrai t'aider en cela. Je n'ai guère usage de ma solde.

– Il te faut penser à ton établissement aussi, tu ne demeureras pas sergent du roi. »

Ernaut haussa les épaules, souriant, mais ne répondit pas. Il n'avait prêté serment que depuis peu et découvrait à peine le travail. Il consistait avant tout à surveiller les portes de la ville la journée, non pas pour en repousser d'éventuels attaquants mais afin de taxer les denrées y entrant. Des patrouilles la nuit, et des rondes sur les murailles, voilà ce qu'on lui demandait. Aucune besogne bien difficile, rien d'exaltant non plus. En fait, il était susceptible de remplir n'importe quelle tâche pour le roi, et il avait juré de le servir fidèlement. Certains de ses collègues lui avaient raconté qu'ils portaient des messages, escortaient des ambassades, accompagnaient l'ost royal lors des combats. À ce titre, il avait le droit de porter une épée au côté dans la cité sainte. Il faisait d'ailleurs partie des rares à s'entraîner, sinon régulièrement, du moins honnêtement.

Ils débouchèrent dans la rue aux Herbes, après avoir dépassé le change syrien. La foule était plus dense et le tohu-bohu des langues et sabirs se mélangeait aux bruits de cuisine, aux aboiements des chiens quémandeurs. Chaque pas faisait découvrir une nouvelle senteur, sucrée, saline ou acide, flattant le nez ou piquant les yeux. Lambert indiqua du doigt un vendeur de pain occupé à trancher une large miche, projetant des miettes et des mies sur le sol aux alentours.

« Bientôt je ferai moudre mon propre grain. Quoi de mieux que se nourrir de son bien, mon frère ?

– Se nourrir dès à présent je dirais, rétorqua Ernaut, moqueur. Il me faudrait fief de moûtier pour contenter mes appétits ce midi.

– Achetons donc gras pâté de viande, fêtons ces bonnes nouvelles ! » approuva Lambert, enthousiaste.

## Jérusalem, matin du mardi 7 mai 1157

Ernaut déposa le sac sur le sol, laissant à son frère le soin de l'assujettir sur la mule.

« Voilà tes dernières affaires ! »

Ils se tenaient au milieu de la rue, au bas de l'immeuble dans lequel ils s'étaient installé à leur arrivée, plusieurs mois avant cela. Saïd, leur voisin, était présent. D'un naturel serviable, il se proposait toujours pour aider, détaillant ses actions en un baragouinage que lui seul comprenait. Il flattait l'encolure de la bête, lui sussurant des mots aimables.

L'aube était encore proche et le ciel pas tout à fait bleu, la rue ombragée exhalait l'humidité et la fraicheur. Lambert se gratta le front, soulevant son chapeau de paille, tout en laçant les dernière courroies.

« De toute façon, tu pourras toujours m'apporter ce que j'aurai oublié. Tu ne seras pas si long à me faire visitance.

– Certes pas. Le casal n'est qu'à une poignée de lieues, avec un des chevaux d'Abdul Yasu le voyage sera tôt fait. »

Lambert s'accouda sur les paquets, un sourire forcé sur le visage.

« Eh bien voilà, cette fois, je crois que nous y sommes !

– Paysan en la terre du Seigneur, qui l'eût-cru ? glissa Ernaut dans un sourire.

– Tant de mois à patienter, et c'est désormais là. Il frappa de la main sur ses affaires, comme s'il carressait un animal. Père serait si fort enjoyé de savoir cela !

– Si d'aucuns Bourguignons passent, j'essaierai de lui faire porter nouvelles.

– Je me demande ce qu'il fait en ce moment.

– M'est avis que si le temps a été, il a fini le liage et s'est attaqué aux gourmands, serpe en main. »

L'évocation des travaux familiers rendit Lambert nostalgique et il approuva, ému.

« J'espère pouvoir faire de même bientôt et te donner à goûter le jus de ma treille.

– Si tu n'en fais pas vin aigre, je veux bien m'y risquer ! »

Lambert tapa amicalement sur le bras de son frère et concéda un rire forcé, mal à l'aise. Plus âgé de pas mal d'années, il s'était toujours senti responsable d'Ernaut, d'autant qu'ils avaient traversé le monde pour venir dans le royaume de Jérusalem. Bien qu'il soit très heureux d'enfin s'établir, il réalisait désormais qu'il le ferait seul, maintenant que son frère était entré dans la sergenterie du royaume. Le jeune homme sentit le silence s'installer et serra à son tour le bras de son aîné.

« Tu es sûr que tu ne veux pas que je te compaigne ?

– Nul besoin. Je dois juste mener ces dernières affaires et commencer à m'installer. Tu viendras me voir quand je serai prêt à te faire bonne héberge.

– J'apporterai quelques rissoles et oublies de Margue si tu veux bien, ce sera moins risqué !

– Je ne dis pas non, je n'ai guère espoir de trouver pareils délices en le casal. Mais j'ai déjà encontré d'aucuns colons de chez nous, de Bourgogne. Ils m'ont fait bon accueil. »

Ernaut sourit.

« C'est toujours bon d'avoir compaings avec qui rompre le pain. Je pense me rapprocher un peu du palais pour ma part, et dénicher meilleur logis que celui-là. Droart m'a parlé d'une chambre au près de la rue du Temple. Je pourrai mieux y accueillir mes compères.

– Je te ferai visite en ce nouveau logis à ma prochaine venue en la cité alors.

– Certes ! »

Lambert acquiesça plusieurs fois, lentement, tourna les yeux vers ses affaires, se remémorant rapidement le contenu des balles et des paniers, se passant le pouce autour des lèvres tout en énumérant les objets. Puis il expira longuement, presque un soupir.

« Il est temps… Le soleil va chauffer ce jour et j'aime autant marcher avant qu'il ne m'arde trop avant. »

Il s'avança et enlaça le géant qui lui servait de frère, dérisoire silhouette face à la montagne de muscles qui l'enserra tel un ours.

« Prends soin de toi, Ernaut, et évite les bêtises et les ennuis.

– Je suis de ceux qui les empêchent, désormais ».

Après une longue accolade, ils se séparèrent doucement et Lambert récupéra la longe de la mule que lui tendait Saïd. Il le salua d'un signe de la main et s'avança, invitant la bête à le suivre d'un claquement de langue. Le regardant s'éloigner puis disparaître au coin de la rue, Ernaut se figea un moment, les yeux dans le vague et, d'un geste, interpela Saïd :

« Ça te dirait d'aller vider quelques godets ? C'est moi qui rince. »

## Casal de la Mahomerie, midi du mercredi 29 mai 1157

La chaleur était écrasante et les hommes s'étaient réfugiés sous l'ombre des caroubiers et des alliboufiers[^aliboufier], en grappes resserrées. Le mitan du jour était abandonné au soleil, aux cigales et aux reptiles.

Pour les moissonneurs, le repas puis la sieste permettaient de reprendre des forces avant de retourner s'échiner, faucille en main, sur les tiges et les épis dorés. Ernaut mâchonnait de la sève de pistachier, alangui, les mains sous la tête. Il ne portait que sa chemise et des braies retroussées au plus haut sur les cuisses. Lambert avait remonté ses manches et lacé ses jambières sous le genou. Il était assis le dos contre un tronc, dévorant à belles dents un morceau de pain et du fromage dur. Il contemplait la vaste parcelle qu'ils avaient attaquée au matin, où les gerbes s'empilaient désormais en faisceaux couleur miel.

Des meules bâties la veille sur les terres alentours ponctuaient les champs aux tiges désormais rases. Ernaut soupira d'aise, s'appuya sur un coude, s'empara d'une outre, y but à la régalade une longue gorgée. Une brise évanescente avait séché leur sueur et ils étaient désormais prêts à goûter un peu de repos. Lambert s'allongea à son tour, souriant.

« Nous aurons bien tôt fait la récolte, à ce rythme !

– M'oui. On va bon train.

– Dire qu'au pays, certains voient à peine les pousses sortir. Avec si beau temps, les chaumes sont bien secs…

– Ce n'est pas tant le soleil qui pose souci ici, mais les pluies automnales, à ce qui se dit. »

Lambert tourna la tête vers son frère et sourit.

« Certes. Chacun a les yeux rivés au ciel dès la saint Martin passée. Ce n'est pas comme chez nous…

– Je croyais que c'était ici, chez toi, maintenant » répliqua Ernaut, taquin.

Lambert lui lança une tige de blé.

« En tout cas, j'aurai de quoi semer mes terres à la fin de l'an, comme ça.

– Le gars t'a donné un tarif pour les grains ?

– Non, ça dépendra de la récolte, mais vu les jours passés à l'aider, je suis acertainé d'avoir bon prix.

– Tu n'as pas si grand de terres à emblaver. Tu veux y mettre quoi ?

– Blé et orge. De ce dernier, il a déjà nombeux silos bien pleins il m'a dit. Je ferai aussi du sésame aux fins de ne pas affaiblir la terre. Outre, j'ai un bosquet pas loin, tu as vu. Les champs gastes[^gaste] commencent au-delà. Il se pourrait que j'y mette plusieurs paniers pour les mouches à miel. »

Ernaut grogna en acquiescement, bougeant légèrement afin d'éviter l'ankylose.

« J'y suis allé tantôt me soulager, d'aucuns y prélèvent la résine.

– J'ai vu ça. Il faudra que je me renseigne sur les droits. On peut en faire bon commerce, en plus des cultures. Mais je vais avoir déjà fort ouvrage, avec les vignes.

– Le vin qu'on nous a donné n'est pas mauvais d'ailleurs. »

Lambert hocha la tête en assentiment.

« Pas de quoi le vendre bien loin mais autant de soleil, ça ne fait pas de mal, certes. Seulement, celui qui taillait avant moi avait la main un peu lourde et l'entretien n'est pas à mon goût.

– Les meilleures parcelles sont prises depuis longtemps je crains. »

Lambert s'éclaircit la gorge, se massant les cuisses tout en parlant.

« Ce n'est pas tant la terre qui manque que les hommes. Mes fils m'aideront quand ils seront en âge.

– Tes fils ?, se moqua Ernaut. Tu n'as même pas épouse, sais-tu que cela est utile pour en avoir ?

– Maintenant que j'ai du bien, ce ne sera guère difficile à trouver, frère. » ❧

## Notes

La société latine moyen-orientale au XIIe siècle n'était pas exclusivement urbaine ainsi qu'on l'a longtemps cru. De vastes zones géographiques accueillaient des colons, incités à s'établir par les taxations plus légères offertes par les propriétaires terriens, souvent des établissements religieux.

Les implantations de villages de colons étaient planifiées, organisées, parfois sur les lieux mêmes d'anciennes localités abandonnées les siècles précédents. Ces agglomérations, appelées « casal » complétaient le maillage rural composé de villages autochtones, peuplés de paysans indigènes, de toutes confessions.

Il arrivait parfois que des liens se tissent entre chrétiens latins et orientaux, mais en dehors de ces deux communautés, les populations ne se mélangeaient pas, pour des raisons à la fois culturelles et légales. Les musulmans étaient par exemple considérés comme des serfs, des non-libres, ayant guère plus de droits qu'un meuble.

## Références

Ellenblum Ronnie, *Frankish Rural Settlement in the Latin Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1998.

Pringle Denys, *Fortification and Settlement in Crusader Palestine*, Aldershot : Ashgate Variorum, 2000.

Rey Emmanuel Guillaume, *Les colonies franques de Syrie aux XIIe et XIIIe siècles*, Paris : A. Picard, 1883.
