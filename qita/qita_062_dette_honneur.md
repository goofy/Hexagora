---
date: 2016-12-15
abstract: 1158. Jeune domestique au service de l'archevêque de Tyr, Germain doit faire face à des échéances de prêt qu'il n'arrive plus à honorer. S'enfonçant dans les problèmes, incapable d'obtenir du soutien auprès de sa famille, trouvera-t-il les ressources lui permettant de s'en sortir ?
characters: Herbelot Gonteux, Germain, Pierre de Barcelone, Ya'qoub Gibran, Khalil al Faytrouni, Fromont le Lombart
---

# Dette d'honneur

## Tyr, soirée du vendredi 18 juillet 1158

En cette chaude veillée estivale, la population demeurait dans les rues, bavardant à l'ombre des palmiers dans les cours et les places ombragées, assise sur les pierres gorgées du soleil de la journée. Une légère brise de mer apportait des senteurs iodées et salines, repoussant les miasmes de l'activité humaine vers les terres. Esquivant les badauds flâneurs, un jeune garçon avançait d'un pas rapide, la tête basse et le front soucieux. Tout en marchant, il se répétait à lui-même quelques phrases de réconfort, sans pour autant voir son humeur s'améliorer.

Un peu replet, il suait abondamment du fait de son manque d'exercice. Sa manche était gluante de transpiration à force de la passer sur son visage, mais il continuait de se frotter régulièrement en un réflexe devenu inutile. Lorsqu'il arriva enfin à destination, il s'efforça de remettre un peu d'ordre dans sa tenue. Il tira sur sa cotte, ajusta sa ceinture et glissa ses lourdes boucles noires derrière ses oreilles, bloquant la touffe emmêlée sous le dôme de feutre lui servant de coiffe. Puis il frappa à plusieurs reprises.

Il fut rapidement invité à entrer dans un vestibule où la fraîcheur s'était maintenue au long de la journée. Un long banc courait sous une niche soulignée de mosaïque, abritant plusieurs céramiques au décor bariolé. La domestique l'incita à s'y asseoir le temps pour elle de prévenir le maître de maison. La porte entrouverte au fond du couloir filtrait à peine les bruits d'une fontaine et d'enfants occupés à jouer ainsi que l'agitation normale d'une riche demeure en début de soirée : servants préparant le repas ou l'arrivée de la nuit et voix de convives.

Ya'qoub Gibran poussa l'huis avec vigueur, comme à son habitude. Tête nue, vêtu d'un simple thawb[^thawb] de lin égyptien de qualité, il était visiblement en famille. Néanmoins rien en lui ne trahissait d'agacement ou d'inquiétude. Il rivait ses yeux de charbon sur son hôte, lissant les frisottis de sa barbe soignée. Sa voix de basse résonna dans la pièce tandis que le jeune homme s'inclinait en guise de salut, tordant son chapeau en un geste nerveux.

« À te voir ainsi échevelé, je crains mauvaise nouvelle. Comment est-elle ?

— La toux ne fait que se renforcer et elle n'a rien quasi rien avalé en mon absence.

— Je craignais cela. Il aurait fallu une garde-malade !

— Elle recrache le moindre aliment. Elle souffre bien trop pour manger. »

Le médecin hocha la tête. Il avait craint que le traitement n'arrive que trop tard. La pleurésie était une maladie complexe, difficile à soigner quand elle était bien installée. Surtout dans le corps d'une vieille femme.

« Je ne vois que l'opium qui pourrait la soulager. Ou de la mandragore.

— Si vous me donnez la prescription, j'irai aussitôt m'enquérir de ces drogues. »

Ya'qoub Gibran hocha la tête sans un mot. Il ne doutait pas que ces médicaments ne feraient que soulager la mère du garçon sans la guérir pour autant. Et cela à un coût assez élevé. Sachant la famille nécessiteuse, il avait fait des efforts dans ses honoraires, mais il n'était pas certain que l'apothicaire aurait les mêmes scrupules. Les hommes de commerce obéissaient à d'autres lois que les siennes.

## Tyr, boutique de al Faytrouni, midi du lundi 21 juillet 1158

La lourde chaleur des fours sourdait jusque dans l'échoppe qui ouvrait sur la rue par une large baie. Comme beaucoup de boutiques de verrier, l'atelier de Khalil al Faytrouni s'organisait autour d'une étroite cour ombragée. On y accédait par une venelle serpentant depuis les abords de la forteresse, non loin du quartier vénitien et du port. Les artisans tyriens, réputés par-delà les frontières du royaume, produisaient aussi bien du verre plat pour la construction que des objets de service délicatement ouvragés. Al Faytrouni était spécialisé dans les gobelets et les lampes et s'enorgueillissait de descendre d'un artiste qui avait travaillé pour des califes. Malgré tout, au fil du temps, la renommée de la maison avait baissé et la clientèle était désormais moins prestigieuse.

Assis sur une banquette parmi les pièces colorées et alambiquées, Khalil al Faytrouni versait un peu de leben[^leben] dans deux gobelets aux formes simples qu'il affectionnait tout particulièrement. Il n'était pas mécontent de laisser sa canne de souffleur un petit moment, harassé par la chaleur partout présente. Le corps émacié, il ne portait qu'une chemise de toile fine et un sirwal de coton d'apparence douteuse. Il tendit la boisson au garçon installé sur un pliant face à lui puis avala à lentes gorgées, dégustant la fraîcheur autant que les arômes.

Pendant ce temps, son invité faisait grincer son siège, trépignant de devoir patienter en silence. Peu désireux de tenir sur le gril ce gamin qu'il ne connaissait guère, Khalil décida de mettre un terme à ses angoisses.

« Tu as bien fait de venir me voir. Tout le monde dans la famille ne t'aurait pas ainsi accueilli. »

Il se gratta l'oreille, accorda un maigre sourire dévoilant des dents gâtées.

« Rafqa, ta mère a fait ce qu'elle a fait, ce n'est pas à moi de la punir pour ça. On s'entendait bien, enfants. C'est en mémoire de ça que je te prêterai ce dont tu as besoin. »

Le jeune homme soupira de soulagement en entendant cela. Khalil enchaîna sans lui laisser l'opportunité de prendre la parole.

« Il faut que tu comprennes, Sharbel, que je suis pas riche pour autant et qu'il te faudra me rembourser selon les termes. Ces trois dinars, je vais les prendre dans la caisse qui me sert à acheter des matériaux pour l'atelier. Et tu mes les rendras, au rythme de un besant par semaine, durant sept semaines, après les seize deniers du premier versement. »

En entendant comment on l'appelait, l'adolescent comprit qu'on ne lui faisait pas de cadeau. Il ne s'était jamais senti proche de cette famille dont sa mère s'était coupée en vivant avec un Latin. D'ailleurs ce dernier les avait quittés depuis longtemps et n'avait laissé à son fils qu'un prénom, auquel le jeune s'attachait comme à une relique et qu'il avait fait sien : Germain.

Incapable de trouver de quoi payer les soins par ses propres moyens, il n'avait pas eu d'autre choix que de se tourner vers ces cousins qu'il n'avait croisé que de loin. Khalil était le seul qui ne leur fermait pas sa porte, mais il n'oubliait pas pour autant les égarements passés.

Germain prit un peu de boisson, afin de réfléchir et de se donner une contenance. Il n'avait plus beaucoup d'économies et avait déjà beaucoup dépensé pour les traitements. Il avait peut-être quelques possessions dont il pourrait se défaire auprès du fripier. Mais un besant par semaine, c'était bien plus qu'il ne gagnait. De toute façon, il lui fallait avant tout du temps. Cet argent paierait l'apothicaire qui ne voulait plus lui faire crédit. Il saurait bien trouver un moyen de rembourser.

« Je te mercie, cousin Khalil. C'est très généreux de ta part. Prépare donc le contrat et dis-moi quand repasser le signer »

Khalil acquiesça en vidant son verre. Il se sentait un peu honteux de demander des intérêts à ce gamin esseulé, mais il avait aussi des comptes à rendre. Son épouse était une femme intraitable et sa belle-famille, qui l'avait soutenu pour relever son maigre négoce, lui reprocherait longtemps de venir en aide des proscrits. En faisant payer son service, il pourrait toujours prétendre avoir saisi une opportunité de gagner quelques monnaies. Il regarda un instant ses mains tavelées et crevassées, aux ongles noirs et épais. Il était temps pour lui de retourner aux fours.

## Tyr, atelier de Fromont le braalier, matin du lundi 29 septembre 1158

Brandissant une tablette de cire, Fromont tempêtait, postillonnant tout en agitant les bras comme un dément. Il faisait cela pour la pure forme, incapable qu'il était de mettre la moindre menace à exécution. Il accordait autant de délais de paiement que de prêts et en était réduit à entasser les promesses non tenues dans une cassette qu'il regardait chaque jour avec dépit.

Sa tignasse brune emmêlée voltigeait en unisson avec une barbe où surnageaient quelques reliefs de soupe, donnant un aspect comique au petit bonhomme ventripotent. Malgré cela, Germain envisageait chaque lundi matin avec angoisse, sachant par avance qu'il n'aurait pas de quoi honorer son échéance. Il apportait tout ce qu'il avait, soit dix deniers par semaine, mais cela ne suffisait pas et il accumulait les retards.

L'artisan finit par se calmer et se laissa tomber sur un escabeau devant la table à tréteaux où son ouvrage était disposé. Il tourna la tête de droite et de gauche, comme s'il espérait faire jaillir la somme exigée de quelque recoin obscur de la pièce. Puis il souffla avec emphase.

« Je suis bien désolé pour toi, gamin, mais comprends que je ne peux accepter cela ! Je t'ai donné ma fiance, car je savais ta mère femme de bien. Jamais je n'ai eu la moindre remarque à faire à son ouvrage, toutes ces années. Et je m'entriste de ce qu'elle est passée voilà peu. »

Il marqua une pause, examinant le jeune homme planté devant lui, les yeux baissés, ne sachant que faire de ses mains.

« Tu savais fort bien que tu ne paierais pas quand j'ai accepté de te verser de quoi rembourser ce que tu devais. J'ai l'air de quoi, moi ? Si même les gamins peuvent se jouer de moi, autant bouter le feu à mon échoppe et danser sur les cendres fumantes ! »

Fromont ne savait pas s'il plaignait Germain ou s'il le trouvait insupportable. Il restait chaque fois là, à pleurnicher, l'œil larmoyant. Il disait qu'il ne pouvait payer plus, qu'il déjà avait vendu tout ce qu'il avait et que dix sous représentaient l'intégralité de ses gages au service de l'archevêque de Tyr. C'était cette dernière mention qui agaçait le plus Fromont.

« Et ne va pas croire que tu es à l'abri d'un jugement parce que tu es au service du sire archevêque Pierre. J'en sais assez sur toi pour fort le mécontenter… »

La remarque fit mouche, car Germain releva le menton et s'empourpra immédiatement tandis qu'il répondait d'une voix vive.

« Nul ne peut se plaindre de mon service en l'hostel de l'archevêque…

— Ai-je prétendu le contraire ? Mais sait-il seulement que tu n'es pas plus baptisé que le Soudan d'Égypte[^soudanbabylone] ?

— Comment ça ? bredouilla le jeune, décontenancé.

— Je sais fort bien que nul parrain ne t'a porté sur les fonts pour entrer en l'Église. Peut-être serait-il fort marri de mieux te connaître… Sharbel ! »

Germain encaissa la menace avec effroi. Il se doutait bien que son secret n'en était guère un et que l'histoire de sa mère était connue dans le quartier, mais l'entendre dire ainsi le fit paniquer. Au palais, il était toujours passé pour un Latin, ce qu'il n'avait jamais démenti. Il resta là, tétanisé, le regard dans le vide et les entrailles prêtes à lâcher. Fromont s'en rendit compte et, un peu inquiet, rapprocha la main insensiblement des grands ciseaux posés sur la pile de drap.

« Bon, je ne veux pas non plus te créer d'ennui, gamin. Alors je te le dis tout net : si jamais la semaine prochaine, tu ne m'apportes pas mon dû, je vends les bijoux que j'ai en gages. »

La mise en garde sembla réveiller Germain, qui répliqua d'une voix éraillée par l'émotion.

« C'est tout ce qui me reste de ma mère !

— J'en suis désolé, jeune, mais tu ne me laisses pas le choix. Tu t'en reviens avec le sou hebdomadaire, plus les six deniers de retard, sinon c'en est dit de ces bijoux. »

Germain haussa les épaules. Il était anéanti. Sa mère à peine mise en terre, il n'avait eu de cesse de payer ce qu'il devait à ses créanciers. Il n'avait plus rien en dehors de la tenue qu'il portait. Une fois les bijoux disparus, il demeurerait absolument seul, sans aucun lien avec personne. Il hocha la tête tristement et fit volte-face, retournant dans la rue d'un pas las.

Fromont expira lourdement. Ces séances le fatiguaient de plus en plus avec les années. Il avait pensé que commercer la monnaie serait moins pénible que tirer l'aiguille pour fabriquer des sous-vêtements. Il se souvenait des sommes colossales qu'il croyait pouvoir obtenir de ces opérations. Au final, il découvrait que les pièces partaient plus vite qu'elles ne revenaient et il se laissait trop facilement influencer par les misères invoquées par ses débiteurs. Il fallait que cela change. Que deviendrait sa famille s'il continuait ainsi ?

Tout en réfléchissant, il déplia la toile de coton qu'il allait devoir découper. Au moins les étoffes ne criaient-elles pas pitié chaque fois qu'on s'apprêtait à les tailler.

## Palais de l'archevêque de Tyr, logement d'Herbelot Gonteux, après-midi du mercredi 1er octobre 1158

Dépliant ses habits avec soin, Herbelot Gonteux en vérifiait le bon état avant de les trier en plusieurs tas. La mauvaise saison approchait et il privilégiait alors les étoffes de laine épaisse, plus lourdes, mais aussi bien plus confortables. Il lissa affectueusement sa vieille coule monastique, qu'il n'avait plus guère l'occasion d'enfiler. Bien qu'ayant grandi dans un couvent, il n'avait jamais fait ses vœux et demeurait simple clerc. Il était néanmoins prêtre, promis à un bel avenir, car déjà au service d'un des prélats les plus prestigieux du royaume.

Il n'était pas friand de tenues trop élaborées, portant généralement des cottes sobres de coupe bien que taillés dans des étoffes de qualité. Il préférait voir les tissus les plus riches et les vêtements les plus somptueux aux nécéssités liturgiques, ayant acquis dans le monde clunisien le goût de l'opulence décorative au service de Dieu. Ses inclinaisons personnels étaient plus simples, tant que son besoin de confort était satisfait.

Tandis qu'il se laissait aller à des rêveries, il s'aperçut que son valet, Germain, était en train de ranger les tenues d'hiver au lieu de celle d'été. Il le reprit assez sèchement, habitué qu'il était aux remontrances strictes de l'univers monastique. Voyant que le serviteur ne bronchait pas, le regard triste et désolé, il s'apaisa bien vite.

« Que t'arrive-t-il depuis quelque temps ? Toi qui ne commettais aucun impair, te voilà aussi écervelé que moineau !

— Je m'en excuse, mestre Gonteux. Je suis pareillement distrait en raison de la mort de ma mère.

— Ah, que me voilà contrit. J'avais oublié que tu l'avais portée en terre voilà peu. Me voilà bien, avec mon sermon. Tu en as encore souci ?

— Je n'avais qu'elle, vous savez. Elle me manque cruellement. »

Enfant trouvé confié aux soins des moines, Herbelot fut touché par cet aveu et invita le jeune garçon à s'asseoir auprès de lui.

« Je ne saurais t'apporter parfaits conseils, mais en période de doute, je prie toujours la sainte Vierge mère de Dieu. Elle apporte réconfort aux enfants endeuillés. Tu pourrais même faire dire quelques messes en sa mémoire, cela te mettra baume au cœur, j'en suis acertainé.

— C'est que je n'ai guère de quoi faire aumône.

— N'as-tu pas de quoi offrir un cierge en accompagnement de la prière ? Cela soulage, je t'assure. »

Désormais plus aguerri aux atermoiements de la confession, Herbelot comprit que Germain se refermait et semblait préoccupé par bien plus grave.

« Je te vois bien plus soucieux qu'il ne sied. Si je peux t'aider en quoi que ce soit, n'aie crainte de mon ire. Je n'ai qu'à me louer de toi depuis que tu travailles à mes côtés. Dépose le fardeau qui pèse en ton cœur. »

Germain hésita un petit moment, se frotta le nez qui coulait et avoua dans une voix éteinte.

« C'est que je dois forte somme, mestre. Et ne peux honorer ma dette.

— Que voilà vilaine affaire ! J'espère que ce n'est pas suite à funestes dévoiements.

— Certes pas, mestre, certes pas. C'était pour payer les soins de ma pauvre mère. Le médecin, les drogues d'apothicaire… Toutes mes économies y sont passées et je dois encore une belle somme à celui qui m'a fait l'avance. »

Herbelot fit un rictus. Il avait la plus profonde antipathie pour ceux qui faisaient commerce de l'argent et d'autant plus lorsque c'était aux dépens d'un miséreux.

« Tu n'aurais jamais dû te fier à ces serpents qui vendent le temps de Dieu comme s'il leur appartenait.

— Je ne savais que faire, mestre. Il me fallait trouver de quoi soigner ma pauvre mère.

— Le blâme est surtout sur eux, qui font honteux commerce là où Il a foulé la terre. C'est le genre de bourse qui entraîne de certes une âme aux Enfers. Combien dois-tu ?

— Encore un peu plus d'un besant. Un sou par semaine encore quatre semaines, plus six deniers de retard. »

Herbelot en fut soulagé, la somme n'était pas énorme et il avait largement de quoi la rembourser. Sa prébende était bien supérieure à ses dépenses et il en consacrait une partie à des dons aux nécessiteux. Habitué à se voir doté de tout ce dont il avait besoin par la collectivité, il ne trouvait nul intérêt à amasser les pièces en un trésor futile.

« Avant tout, il te faut te confesser. En recourant à des pratiques condamnables, tu as entaché ton âme immortelle. Voilà bien plus inquiétant méfait, bien que facile à nettoyer par une juste pénitence. Tu vas venir avec moi que je revête l'habit et je t'entendrai, ou tout autre confesseur que tu préféreras. »

Germain sentit une boule se former dans son estomac. Il comprenait qu'il allait franchir un point de non-retour. Jusqu'à présent il s'était contenté de donner le change quand on le croyait Latin. Mais en acceptant de se livrer à ce sacrement, il savait que son crime serait bien plus grave. Il ne s'en vit pas le courage et balbutia quelques mots avant d'arriver à ânonner péniblement son aveu.

« C'est que je ne suis pas latin, mestre Gonteux. Je suis syriaque…

— Que me contes-tu là ? N'y a-t-il pas plus franc que Germain ?

— C'était le nom de mon père, que j'ai repris. Ma mère m'a fait entrer en sa religion sous celui de Sharbel. »

Herbelot secoua la tête de dépit, se grattant la tonsure.

« Voilà donc que tu ajoutes l'imposture à tes méfaits…

— J'ai toujours aimé le nom de mon père, et n'ai jamais prétendu être ce que je n'étais pas !

— Oui da, tu t'es contenté de ne pas démentir ce qu'on croyait de toi. Ton péché n'en est pas moins net, mon garçon ! »

Germain demeura muet sous le regard lourd de reproches d'Herbelot. Pour autant, le clerc n'était pas si fâché que touché par la détresse de son interlocuteur.

« Vu que tu n'es pas à mon service direct, ton sort ne dépend pas de moi. Ce sera au sire archevêque qu'il conviendra de trancher ton sort. D'ici là, tu continueras ton office comme si de rien n'était. »

## Palais de Pierre de Tyr, bureau de l'archevêque, fin de matinée du jeudi 2 octobre 1158

Le modeste bureau était envahi des parfums maritimes, que le vent faisait entrer en rafales par la petite fenêtre. Insensible aux courants d'air qui déplaçaient les documents de la table de travail, le vieux Pierre de Barcelone, archevêque de Tyr portait les yeux au loin par-delà les toits de la ville. Il aurait pu sembler indifférent à ce qui se disait dans la pièce, mais ses familiers savaient qu'il ne faisait jamais preuve d'un tel irrespect envers ses visiteurs. Il réserverait ses absences à ses proches, en faisait un signe d'intimité.

Herbelot avait expliqué rapidement la situation du valet qui le servait au sein du palais et avait plaidé sa cause. C'était selon lui un garçon travailleur et honnête, qui n'avait agi que dans l'insouciance et l'inconséquence de la jeunesse. La remarque avait amusé le vieil ecclésiastique, enclin à prêter la même inexpérience à son secrétaire, malgré toute l'affection qu'il lui vouait.

L'archevêque se tourna vers les deux hommes debout face à lui. Germain lui semblait fort piteux, les mains croisées et le regard rivé au sol. Il lui paraissait bien humble et modeste. Loin de la superbe des barons dont les fautes, pour n'être pas moins nombreuses, étaient souvent beaucoup plus graves. La voix éraillée du prélat emplit la salle sans effort, porteuse d'autorité malgré ses tremblements.

« Entends bien, mon garçon, que tu as lourdement fauté en te jurant à moi sous un faux nom, même si tu l'avais pris en mémoire d'un parent aimé. Outre cela, tu as laissé croire que tu étais fidèle sujet de la sainte Église alors que tu appartenais à une secte orientale. Amie, certes, mais distincte. »

Pierre de Barcelone marqua une pause, plissant ses yeux fatigués pour mieux voir si ces paroles faisaient leur effet sur le jeune homme.

« Dans ton errance, tu as eu l'heur d'encontrer un homme de bien, mon secrétaire, qui parle de toi en termes bien élogieux malgré tes manquements. Je porte cela à ton crédit et suis donc prêt à te tendre la main. Tu devras néanmoins laver ton âme de ces péchés si tu veux demeurer en mon hostel, et donc te confesser et suivre la pénitence qui te sera attribuée. »

Voyant que sa dernière phrase soulevait l'incompréhension de ses deux interlocuteurs, l'archevêque leva une main impérieuse.

« Comme le sacrement de la confession est réservé aux membres de l'Église, il te faudra avant cela rejoindre la communauté des fidèles et te faire baptiser. Sous ce vocable que tu aimes tant, pourquoi pas ? J'encroie que tu sauras trouver bon parrain pour te préparer à cela d'ici la Pentecôte, où cela sera. »

Il allait tendre la main pour donner son anneau à baiser puis se reprit.

« D'ici là, tu rejoindras les pénitents qui jeûnent au pain sec et à l'eau chaque vendredi, pour te faire passer le goût de ces mensonges et préparer ton âme à recevoir l'Esprit saint. »

Après avoir salué comme il seyait, Herbelot et Germain se retirèrent. Ils déambulèrent sans un mot en direction de la grande salle où le repas serait servi. Traversant les jardins, le jeune prêtre arrêta son compagnon d'une main.

« Pourpense bien la chance qui t'est donnée de repartir du bon pied. Je serai heureux d'être ton parrain au jour de ton baptême, qui sera pour toi véritable renaissance, ainsi qu'il sied. »

Puis il sortit d'une de ses manches une bourse un peu dodue qu'il glissa dans la main du valet.

« Outre, tu donneras ça aux sangsues qui te harcèlent. Nul besoin de mettre en péril ton âme plus longtemps. »

Germain en resta bouche bée un instant puis lança un regard affolé vers Herbelot.

« Je ne peux, mestre, vous êtes trop généreux !

— Ne t'enjoie pas trop. Tu devras rembourser cette somme ! Verse un denier par semaine durant une année au moins au chapelain de la Sainte Vierge. Qu'il associe ta mère à ses messes durant ce temps. Nous serons alors quittes. Mais libre à toi de continuer par la suite. »

Puis le petit clerc ventripotent continua sa route sans plus un regard pour son compagnon hébété de tant de sollicitude. Herbelot était encore plus mal à l'aise avec les effusions sentimentales qu'avec les manquements à la Foi. Il se sentait malgré tout incroyablement satisfait de lui. À sa grande honte, tenta-t-il de se corriger. ❧

## Notes

Le prêt à intérêt était généralement interdit par toutes les religions du Moyen-Orient médiéval. Ce qui n'empêchait en rien sa pratique extrêmement banalisée. On lui trouvait de nombreuses excuses, en excluant de cette interdiction les membres d'une autre croyance, en dissimulant les intérêts dans des taux de change ou en établissant des versements différés en nature. Tous les artifices étaient bons et même des ecclésiastiques y recouraient régulièrement, quand ils n'étaient carrément pas usuriers eux-mêmes.

Malgré tout, il arrivait que des crises moralisatrices secouent le monde des affaires et qu'on annule purement et simplement certaines dettes voire qu'on confisque un capital bien mal acquis. Mais c'était malheureusement souvent avec des motifs tout aussi hypocrites que les pratiques décriées.

On trouve mention de différents styles de prêts dans les écrits retrouvés. Un grand nombre concerne des besoins de vie quotidienne et pas du tout des entreprises commerciales. Le prêt contracté par Germain/Sharbel est inspiré d'un courrier dont parle S.D. Goitein, de trois dinars, qui avait servi à rémunérer les soins d'un malade.

## Références

Ashtor Eliyahu, *Histoire des prix et des salaires dans l'orient médiéval*, Paris : École Pratique des Hautes Études, 1969.

Goitein Shlomo Dov, *A Mediterranean Society*, Vol. I à 6, Berkeley et Los Angeles : University of California Press, 1999.

Pringle Denys, *The Churches of the Crusader Kingdom of Jerusalem, Vol. IV. The Cities of Acre and Tyre*, Cambridge : Cambridge University Press, 2009.
