---
date: 2016-11-15
abstract: 1158. Après plusieurs mois d'entraînement, Ernaut est officiellement membre de l'armée. Il participe à sa première confrontation avec les troupes de Nūr ad-Dīn et, surtout, franchit la frontière orientale du royaume, le Jourdain, en direction de l'inconnu.
characters: Ernaut, Gaston, Arnulf
---

# Oultre Jourdain

## Jérusalem, palais royal, après-midi du mercredi 26 février 1158

Le vicomte Arnulf, emmitouflé sous une couverture chamarrée qui traînait jusqu'au sol, dégustait quelques fruits au vin. Il était malade depuis plusieurs jours, victime de fièvres et de toux, mais refusait de garder le lit au grand dam de ses proches. Il était donc installé à sa table de travail, dans une des salles du palais, qu'il affectionnait tout particulièrement en hiver, car elle ouvrait sur une colonnade et des jardins. Plusieurs braseros apportaient suffisamment de chaleur pour qu'une poignée de clercs de l'administration trouvent prétexte à venir là copier, classer ou vérifier des chartes sans avoir à se frotter les doigts.

Sur la pile de documents qu'il venait de compulser trônait une petite missive, hâtivement rédigée d'une main brouillonne, mais portant un sceau qu'il reconnaissait entre tous. Ce n'était pas l'imposante bulle de presque deux pouces de diamètre que chacun attribuait au souverain sans l'ombre d'un doute, mais celle qui accompagnait les messages sans importance, aux simples fins d'identification. Le roi Baudoin indiquait que la campagne dans le Nord s'était fort bien déroulée, qu'il faisait halte à Tripoli quelques jours et qu'il fallait prochainement attendre son retour triomphal, en compagnie de Thierry comte de Flandre.

Arnulf avala encore plusieurs bouchées en silence, tout en étudiant le sergent assis face à lui. Gaston vérifiait un inventaire qu'il venait de réaliser dans plusieurs arsenaux épars dans la cité. Fatigué par les combats, boiteux et parfois revêche, il accusait une vie de violence au service de la couronne. Le vicomte lui faisait confiance pour estimer la valeur des hommes. Capable de constituer une bande compétente pour n'importe quelle tâche en peu de temps, il avait un jugement sûr et se voyait régulièrement en charge de la formation des fantassins. Par égard pour son âge et sa claudication, on lui épargnait les campagnes hivernales. Connaissant la susceptibilité de Gaston, Arnulf lui avait donc demandé cet inventaire, tâche perpétuellement nécessaire, afin de le garder au chaud.

Outre cela, il avait envie de savoir ce que valaient les dernières recrues et si certaines pouvaient servir à autre chose qu'effrayer le larron ou le bourgeois peu désireux de payer la taxe. Il fallait régulièrement renouveler les unités de fantassins et on sélectionnait alors parmi la sergenterie royale ceux qui semblaient les plus aptes. Cela offrait l'avantage de s'appuyer sur des hommes qu'on pouvait former toute l'année et dont on connaissait les forces et les faiblesses.

Un des nouveaux arrivants, qui n'avait pas juré depuis plus d'un an, attirait l'intérêt du vicomte. Ernaut, dont la réputation à la porte de David inquiétait jusqu'aux marchands de Beyrouth, s'était révélé être bien plus qu'un géant au physique de colosse. Restait à savoir si cela lui serait bénéfique pour servir le royaume.

S'essuyant les doigts dans une touaille, Arnulf s'éclaircit la gorge pour interrompre Gaston dans son énumération.

« Laisse donc ces listes pour le moment et conte-moi ce qu'il en est de la formation des piétons…

— Que voulez-vous assavoir, sire vicomte ? Il n'y a rien de particulier à en dire.

— As-tu fini d'apprendre les ordres au dernier contingent ?

— Oui-da. Il ne serait pas inutile de leur faire subir encore assaut par conrois, histoire de les aguerrir. Mais ils savent les ordres, les cris et l'usage des bannières. »

Arnulf réprima un frisson et remonta la couverture sur ses épaules.

« As-tu eu l'heur d'étudier le jeune Goliath, fort récemment ?

— Ernaut ? confirma Gaston dans un sourire. Certes, nous avons compaigné jusqu'à Jaffa peu après la Sainte-Catherine^[Cf. Qit'a « Leviathan »]. Un jeune poulain susceptible de finir beau destrier. Pour peu qu'on lui tienne les rênes courtes.

— A-t-il posé souci ?

— Il s'emporte parfois un peu vite, et n'apprécie rien tant que montrer sa force colossale à quiconque. Lui faire tenir le rang est souventes fois ardu. »

Le vicomte acquiesça, se remémorant qu'il avait pu constater lui-même l'indépendance d'esprit parfois pénible du jeune homme. Ce n'était malgré tout pas pour lui déplaire, tant qu'elle ne s'opposait pas à son autorité personnelle.

« À quel poste l'as-tu formé ?

— Il sert comme pavesier.

— Tu lui attribueras place en senestre de la ligne. Avec semblable donjon en la forteresse, la muraille tiendra bon.

— Il faudra s'assurer que son arrivée en si belle place n'occasionnera pas de heurts. Ce n'est pas l'usage d'avoir telle préséance quand on n'est pas vétéran. »

La remarque de Gaston arracha au vicomte un grommellement sans enthousiasme. Le sergent appréciait ce chevalier, mais il regrettait son désintérêt envers les sentiments des humbles. Pourtant Arnulf passait beaucoup de temps en leur compagnie pour ses différentes tâches et il ne détestait rien tant que l'inefficacité, ayant pu en constater les résultats désastreux à de nombreuses reprises. Las, cela ne l'avait jamais incité à voir parmi ces serviteurs de la couronne plus que des pions qu'il affectait au mieux de ses besoins.

« Tu n'auras qu'à le prendre dans ton conroi[^conroi] et surveiller cela.

— Le roi compte chevaucher sus l'ennemi si tôt rentré ? risqua Gaston.

— Je ne sais. De toute façon, vu que Norredin[^nuraldin] est malade et que son ost a été défait, il y aura sûrement rapide bataille pour profiter de notre avantage. Bonne occasion pour éprouver au feu ces nouvelles lames. »

Gaston hocha la tête en silence. Le jeune roi n'aimait rien tant que chevaucher à la tête de son ost. Il avait déjà à son actif de nombreuses victoires et comptait souvent sur ses succès pour financer les besoins du trésor. L'été précédent, il n'avait échappé que de peu à la capture ou à la mort^[Cf. Qit'a « Chétif destin »], mais cela n'avait en rien tiédi ses ardeurs. Encouragé par son beau-frère Thierry de Flandre, il passait plus de temps en selle la lance à la main que dans son palais.

« Je vais m'occuper d'Ernaut. Il sera fin prêt pour la prochaine campagne, soyez-en assuré. »

Arnulf opina en silence et avala un nouveau fruit, se pourléchant de l'excellente friandise. Il lui faudrait voir comment s'attacher le jeune Ernaut dans sa maisonnée. C'était la valeur de ses hommes qui donnait la puissance à un hôtel. Arnulf espérait bien se servir de cette belle pierre pour y bâtir dessus.

## Terre de Suète, plaine de Buṭayḥa, début d'après-midi du mardi 8 juillet 1158

Appuyée à un palmier, l'unité d'Ernaut avait pris place parmi les broussailles peu denses qui s'étiraient en une haie sauvage jusqu'au milieu de la plaine à leur gauche. Ils s'étaient déployés en fin de matinée depuis l'arrière-garde du corps central d'armée, vers l'aile droite. Derrière eux, une échelle de cavaliers menée par le prince de Galilée attendait à l'abri de la forteresse mouvante des boucliers. Loin en retrait, les mules et animaux de portage étaient égayés en direction du lac de Tibériade, d'où ils puisaient la boisson distribuée par les fourrageurs.

La chaleur était torride et les langues remuaient de la poussière. En dehors des berges, la végétation abondante était brûlée de soleil. Ernaut souleva son casque de fer, qu'il protégeait d'un chapeau de paille, pour y passer un linge déjà détrempé de sueur. Il ne voyait rien s'approcher depuis les lignes ennemies qui semblaient toutes affairées bien plus au nord. Régulièrement des coursiers apportaient des nouvelles aux bannerets et on avançait de quelques pas, parfois d'une portée d'arc. Sans pour autant arriver au contact qu'espérait Ernaut. Il avait hâte de prouver sa valeur, certain de sortir renforcé de la confrontation avec l'adversaire.

Il souffla longuement, plissant les yeux pour tenter de comprendre ce qui se déroulait malgré les ondes de chaleur. Il ne voyait que des formes indistinctes au niveau des combats et ne percevait que de diffuses rumeurs, selon le vent. L'arbalétrier à ses côtés lui tendit l'outre emplie d'eau tiède qu'ils se repassaient depuis le matin.

« On a bonne position. C'est l'ost de Flandre à senestre qui encaisse les traits sarrasinois.

— Ils ne s'approchent donc jamais, frapper de la lance et du glaive ?

— Pas tant qu'ils peuvent… »

Il fut interrompu par une sonnerie de cor. Ils allaient faire mouvement sous peu. Ernaut lança la gourde à un valet qui passait par là et enfila la guiche et les énarmes de son grand bouclier. Enfin, il empoigna l'hast de sa lance, qu'il avait plantée en terre. La tension s'insinua dans les corps, crispant les mâchoires, serrant les poings et fronçant les sourcils. Gaston, à cheval, avait rivé son regard à la bannière de Galilée, prêt à réagir au moindre de ses frémissements. Lorsqu'il la vit s'incliner légèrement à plusieurs reprises, il hurla « Mouvez ! »

En un instant, les pavois se soulevèrent de terre et la ligne ondoya subitement, le temps pour elle de déferler comme une lente vague sur la plaine. Les fûts des lances semblaient une forêt aux terminaisons de métal brillant sous le soleil et, portées au rythme du pas des hommes, ils dansaient sous un vent imaginaire. Ça et là, les étoffes peintes aux couleurs des seigneurs de guerre s'élançaient vers les cieux, marquant l'emplacement des cavaliers prêts à s'avancer vers l'ennemi.

Ils n'avaient pas fait un demi-mille qu'un coursier se présenta au grand galop. Il hurla en passant devant eux « Halte ! Serrez ! » sans même ralentir tandis qu'il s'engouffrait entre deux unités. Il piqua directement vers le prince.

Décontenancé, Ernaut tourna la tête vers Gaston. Il savait que le groupe se fiait à lui pour les mouvements, qu'il était le pilier de référence. Et là il hésitait sur ce qu'il devait faire. Gaston le comprit en un instant et cria « Mouvez ! La bannière est toujours inclinée ! Mouvez ! »
Malgré tout, en disant cela, il fit volter sa monture de façon à s'assurer de réagir rapidement à toute nouvelle indication et fixa la bannière. Peu après l'arrivée du cavalier, elle se redressa ostensiblement et tandis que les olifants propageaient un autre ordre, des voix s'élevèrent d'un peu partout. « Halte ! Serrez ! »

Sans même y réfléchir, Ernaut se rapprocha du groupe à sa gauche jusqu'à le rejoindre. Puis il se figea et planta sa pique en terre tandis que son voisin venait se poster au plus près de lui. La ligne se tassa comme un soufflet que l'on aurait replié, offrant une muraille ininterrompue d'écus hérissés de lances. En retrait, les arbalétriers avaient tendu les arcs, sans pour autant encocher. Ils attendaient.

L'échelle[^echelle] de cavalier semblait tranquille, mais les conrois étaient formés, les heaumes étaient lacés et, autour des montures, les valets et serviteurs s'activaient à équiper les retardataires. Le sol commençait à gronder du piaffement des étalons de guerre impatients de s'élancer, tenus fermes par des poignes d'acier. Ernaut inspira longuement. Il était prêt.

## Terre de Suète, abords du Yarmūk près des caves de Suète, soirée du mercredi 9 juillet 1158

Un campement sommaire s'était établi au débouché de la courte vallée fermée par la forteresse des Caves de Suète. Ernaut avait été ébahi de découvrir que ce n'était pas un édifice dû à l'homme, mais l'aménagement de la falaise terminale. Des grottes étaient agencées avec des passerelles et des encorbellements à flanc de la montagne. De là-haut devait s'offrir une vue imprenable sur les environs, vers le nord par-delà le Yarmūk et les terres contrôlées par Damas.

Le roi avait établi sa tente au plus près de la fortification, avec les principaux nobles et la cavalerie tandis que les fantassins avaient été installés de façon à protéger les abords. Bien qu'ils aient remporté une victoire la veille face à Nūr ad-Dīn, le Turc avait pu battre en retraite et conservait une capacité militaire conséquente. La leçon de l'année précédente avait été entendue par le jeune souverain de Jérusalem et des sentinelles égayées le long des accès garantissaient la sécurité. On disait même que des coursiers allaient faire venir de quoi renforcer la position des Caves de Suète avant que le roi ne reparte.

Le principal affluent du Jourdain en était réduit à serpenter entre les pierres et les racines, lointain souvenir du torrent tumultueux du printemps. L'eau vive était néanmoins fort appréciée des hommes, dont un grand nombre avait pataugé dès que cela leur avait été possible. Ernaut avait été parmi les premiers à s'ébrouer, profitant de l'occasion pour rincer ses vêtements poisseux de sueur. Il s'était après cela installé contre un rocher tiédi par le soleil de la journée, exposant ses linges pour qu'ils sèchent. Portant simplement ses braies, mais avec son imposant coutelas à la ceinture, il était ensuite allé aider à la confection du repas, un gruau hâtivement chauffé et agrémenté de dattes. Il était accompagné d'un vin léger apporté dans de larges outres et qui sentait donc un peu la chèvre.

Regroupés par affinité dans la zone enclose de cordes qui délimitait le camp, les hommes prenaient du bon temps. Quelques chants résonnaient, pas toujours bien élégants, et des lavandières parcouraient les rangs, proposant leurs talents pour s'occuper des corps et des vêtements. Ernaut regardait au loin un spectacle de jongleurs assez vulgaire lorsqu'il entendit l'arrivée d'un cheval. C'était Gaston qui revenait des Caves, où il avait certainement reçu les ordres. Il confia les rênes à un valet et s'approcha du petit groupe où était Ernaut.

« Il me faudra cinq hommes demain à l'aube.

— Quelque bataille en prévision ? s'enthousiasma le jeune homme.

— Certes pas. Le conseil des barons reçoit en ce moment même des coursiers venus de chez Norredin. M'est avis qu'on ne les reverra pas de sitôt. »

Apercevant la mine déconfite du jeune homme, Gaston s'approcha.

« Hé bien quoi, jeune. N'es-tu pas heureux d'avoir survécu à bataille, et sans perdre aucun morceau !

— C'est juste que j'ai trouvé ça… »

Ernaut ne trouvait pas ses mots. Gaston s'accroupit à côté de lui et lui donna une bourrade.

« Aussi long que jour sans pain ? Je m'en doute bien. Mais c'est ça guerroyer, jeune. On attend la journée durant, sous soleil d'enfer. Et d'un coup, ça nous tombe dessus, comme un orage. Le plus souvent, ça se contente de péter au loin, comme le tonnerre.

— Toutes ces journées à porter lance et écu, avancer, reculer… C'est quoi l'intérêt ? On aurait pu aussi bien rouler les dés allongés dans le pré. »

Gaston se gratta l'oreille, comme si ce qu'il entendait irritait sa cervelle.

« Ça, on ne peut jamais l'assavoir avant la fin de la journée. Tu t'ensouviens hier, quand on faisait avance et que le cavalier est venu nous arrêter ?

— Certes oui. J'espérais qu'on irait à curée.

— Ça aurait pu. Le souci, c'est que ces maudits Turcs aiment à feindre la fuite pour nous prendre dans leur nasse. Les barons ont cru à une perfidie de ce type.

— Sauf qu'ils fuyaient vraiment et qu'on aurait pu faire belle moisson. Peut-être même grapper Norredin.

— Et tu te voyais déjà l'assaillir pour le porter toi-même, ficelé comme conil, au sire Baudoin ! » se gaussa le vieil homme.

Il soupira longuement et se laissa tomber au sol. Il chercha de quoi se désaltérer, mais fit une grimace en apercevant l'outre de vin. Il haussa les épaules et sourit à Ernaut.

« Tu as bien œuvré ce jourd'hui, jeune.

— J'ai rien fait, à part porter pavois et haste, avancer, tourner, reculer…

— Toutes choses pour lesquelles on espérait de toi. Apprends donc que tu n'es pas plus Roland que moi. Adoncques, servir à ta place sans gloriole te sera fort plus utile.

— J'espérais certes bien plus de cette levée d'ost. »

Gaston se releva péniblement, époussetant les brindilles de sa cotte.

« T'es pas si vieux. Agis ainsi que tu l'as fait ce jour, et pense à fermer un peu ta grand'goule. T'en seras pas déçu. »

Il assortit sa dernière déclaration d'un clin d'oeil et repartit en boitillant, en quête d'un vin qui siérait à son gosier. Il laissait Ernaut en plein désarroi. Le jeune homme avait été euphorique lorsqu'il avait appris qu'il allait intégrer l'ost royal pour cette campagne. Il l'avait clamé sur tous les toits et annoncé à toutes ses connaissances, même lointaines. Il avait même escompté du butin pour faire quelques emplettes afin d'impressionner Libourc et sa future belle-famille.

Revenir sans haut fait à conter lui broyait le cœur. Il lui fallait trouver un moyen de se débarrasser de ce poids. Comprenant qu'il ne servirait à rien de ruminer seul dans son coin, il se leva soudainement et s'avança parmi ses camarades.

« Quelque courageux pour oser m'affronter à la lutte ? »

En entendant la voix d'Ernaut lancer son défi, Gaston sourit pour lui-même. Le vicomte serait heureux d'apprendre que les obstacles ne minaient en rien l'énergie du jeune homme. ❧

## Notes

L'idée de ce texte a été nourrie par l'envie que j'avais depuis longtemps de présenter le monde militaire médiéval d'un point de vue assez rare, celui de l'infanterie. Loin de n'être que des bagarres sans cohérence, les affrontements demandaient au contraire une solide organisation, car les contingences matérielles et logistiques étaient assez fortes. Les batailles consistaient avant tout en des déploiements et déplacements réciproques de troupes. Le tout avec des moyens de communication assez simples : bannières, instruments de musique, quelques cris et ordres bien précis. Si dans les textes, les combats pouvaient se prolonger des heures, cela désignait la durée totale de présence sur la zone. Les engagements à proprement parler étaient souvent intenses, mais pas forcément longs, et étaient plutôt composés de flux et de reflux épisodiques.

La bataille de Buṭayḥa constitue l'un de ces nombreux affrontements mineurs entre les puissances latines et musulmanes qui n'aboutit à aucun résultat stratégique. Chacun des chroniqueurs loua les succès de son camp tout en minimisant ceux de ses adversaires. Nūr ad-Dīn rentra à Damas et repartit dans le nord où Renaud d'Antioche opérait des razzias. Baudoin put libérer la place forte des Caves de Suète du siège dont elle était l'objet et profita de sa présence pour en améliorer les défenses. Les deux souverains envisagèrent un moment de signer une trêve sans que cela ne débouche sur rien.

Par ailleurs, le titre de cette nouvelle a aussi un sens particulier pour l'auteur, car il s'agit du premier texte publié dès l'origine en licence libre. De la même façon qu'Ernaut, il me semble que c'est là un passage de frontière, afin de fouler des terres inconnues. Dans les deux cas se mêlent l'impatience d'y déployer un savoir-faire et le désir d'y voir fructifier des rêves.

## Références

Audoin Édouard, *Essai sur l'armée royale au temps de Philippe Auguste*, Librairie Anciennes H. Champion, Paris : 1913.

Élisséef Nikita, *Nûr ad-Dîn, Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Institut Français de Damas, Damas : 1967.

Gibb H. A. R., *The Damascus Chronicle of the Crusades. Extracted and translated from the chronicle of Ibn al-Qalanisi*, Londre : Luzac & Co., 1932, réédition : Mineola : Dover Publication, 2002.

Nicolle David, « ̔̔̔‘Ain al-Habis. The Cave de Sueth » dans *Archéologie Médiévale*, XVIII, Rouen : 1988. P;113-140.

Pringle Denys, *Secular Buildings in the Crusader Kingdom of Jerusalem*, Cambridge : Cambridge University Press, 1997.

Verbruggen Jan Frans, *The art of warfare in Western Europe during the Middle Ages, from the eighth century to 1340*, The Boydell Press, Amsterdam, New York, Oxford :1977 (2nde édition).
