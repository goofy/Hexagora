---
date: 2011-11-15
abstract: 1156. Premier pas sur les traces d’Ernaut, tout jeune homme, en partance pour la Terre sainte. À quelques jours de Vézelay, il s’arrête à la Charité-sur-Loire, sur le chemin d’un premier sanctuaire avant de s’embarquer pour l’Outremer.
characters: Ernaut, Lambert, Obert, Anselme, Thibald
---

# Besace et bourdon

## Route du Vif (Châteauneuf-Val-de-Bargis), après-midi du jeudi 28 juin 1156

Intrigué par une odeur suspecte, le renard franchit les broussailles de l'orée du bois, la truffe au ras du sol. Il cligna des yeux, tournant la tête vivement de droite et de gauche, anxieux de se présenter en pleine lumière. Une brise légère berçait les chênes, comme s'ils cherchaient à effacer de leur cime les rares nuages cotonneux maculant le ciel d'un bleu azur.

Le cri d'un geai ne l'inquiéta guère et il avança en foulées souples jusqu'à la large route. Une flaque asséchée semblait être à l'origine de son émoi. Il sortit un bout de langue rosée, mais n'eut guère le temps d'en faire plus. Un tintamarre effrayant s'approchait : des voyageurs chantant des hymnes religieux. Du moins pour l'un d'entre eux. Le second ahanait d'une voix vigoureuse avec un total mépris pour les règles musicales élémentaires, dont la première était de ne pas se fier au volume pour pallier une qualité déficiente.

« *ad Dominum in tribulatione mea clamavi et exaudivit me*[^psaume119]

– Ah doux minou entre hiboux là scions, mais acclame vite et tout ce qu'on dit vite, mais...

– *Domine libera animam meam a labio mendacii a lingua dolosa*

– Deux minets libèrent âne...

– Non, Ernaut ! Non ! Par tous les saints, entends un peu mes paroles. Tu répètes n'importe quoi ! »

Le goupil n'attendit pas de savoir à qui appartenaient ces voix. Avant même que les têtes ne se montrent au-delà de la butte, il avait disparu sans un bruit, frustré de n'avoir pu achever son enquête. En premier apparut un visage à la mâchoire trapue, au nez long et cassé, avec des yeux mobiles surmontés d'une tignasse peu disciplinée couleur des blés, posée telle une écuelle sur le crâne. Le duvet qui ornait ses tempes et son menton indiquait qu'il était encore jeune et pourtant le tout était enchâssé sur un corps qui aurait fait envie à Samson lui-même.

Immense, massif comme un chêne centenaire, il avait un cou de taureau, le torse puissant d'un bûcheron et les bras d'un forgeron. Le plus étonnant était sa taille : il devait rendre facilement une tête à l'homme brun à ses côtés, plus âgé, d'une trentaine d'années, la mine visiblement contrariée. Ils étaient vêtus de toile solide, sans ostentation, mais de qualité, avec des souliers bien semelés. Ils portaient chacun, en plus de sacs variés, une besace et un bourdon qui indiquaient qu'ils étaient pèlerins en marche. La croix à leur épaule faisait savoir qu'ils cheminaient à destination de Jérusalem, le centre du monde. Mais pour l'heure, ils n'étaient qu'à guère plus de trois jours à l'ouest de Vézelay et avaient passé leur temps à se chamailler.

« Tu es censé savoir ce psaume, pourtant !

– Je ne m'y entends guère en latineries.

– Comment s'en étonner ? Il t'aurait fallu écouter le frère pour cela. Tu aurais pu au moins faire effort de retenir les maîtres chants. »

Les bâtons de marche ferrés battaient en cadence, les deux hommes avançaient d'un bon pas, soulevant une fine poussière dans les endroits où le sol n'était que graviers et cailloux.

« Droitement ce que j'ai fait. Seulement je déclame pas le latin comme toi, Lambert, voilà tout. Peut-être est-ce toi qui ne sais pas dire juste... »

Le plus âgé des deux allait répondre, agacé mais il se ravisa et se contenta de soupirer, les yeux levés au Ciel. Il savait que sa patience serait mise à l'épreuve durant leur périple, son frère ne s'avouait jamais vaincu. Lorsqu'il lui avait confié Ernaut, leur père l'avait prévenu. Il estimait trop dangereux de laisser Ernaut se rendre seul dans la Cité sainte. Il lui fallait un gardien, quelqu'un de raisonnable pour veiller sur lui jusqu'à ce qu'il ait accompli son vœu.

Dans un second temps, il était nécessaire qu'un homme d'expérience soit là pour faire les bons choix. Ils comptaient s'installer dans le royaume de Jérusalem, devenir colons comme tant d'autres avant eux. Et si Ernaut ne rechignait que rarement à la tâche, Lambert assistait son père depuis des années dans les affaires et avait bénéficié d'un excellent apprentissage. Il saurait trouver l'endroit idéal, des terres fertiles à travailler.

Ils cheminèrent ainsi pendant quelque temps, accompagnés seulement du bruit de leurs pas, des chants d'oiseaux et de quelques cris de bûcherons dans la forêt. Ils voyaient devant eux depuis un moment une petite carriole tirée par un âne, menée par un jeune homme balançant une badine dans son dos tandis qu'il marchait. Toujours sans rien dire, Ernaut attrapa sa gourde de peau et en avala de grandes lampées avant de la tendre à son frère. Arborant un sourire forcé, ce dernier accepta et but une gorgée.

Ils avaient entretemps rejoint la voiture, arrêtée sur le côté de la voie. Le charretier s'efforçait d'en dégager la roue d'une ornière. Le chargement, quelques paniers du fer extrait des mines environnantes, semblait bien trop lourd pour qu'il puisse espérer s'en sortir seul. Les deux frères stoppèrent et s'approchèrent, affichant un visage amical. L'homme se gratta la tête, arborant quelques dents éparses, surmontées d'un nez camus, en un sourire incertain.

« C'te bestiasse est partie de senestre[^senestre] alors que je veillais pas. Elle a coincé sa rouelle parmi la pierraille et c'est tout bloqué. »

Il se poussa de côté, laissant voir l'ampleur du désastre. Ernaut fit une moue appréciatrice et lui passa son bourdon. Il posa ses sacs au sol et, confiant, il empoigna le plateau, bras tendus et jambes pliées. En un effort, il hissa la roue hors de l'ornière, sous les yeux admiratifs du charretier.

« Par la barbe Dieu, jamais vu si grande force ! Je vous remercie bien, le pérégrin.

– J'ai nom Ernaut et voilà mon frère Lambert. Ce n'était pas grande chose.

– P't être pour vous, mais j'aurais été bien en peine d'hissir pareillement le charroi. »

Tout en parlant, il mena son âne vers le centre de la route, tendant à Ernaut son bâton de marche.

« J'ai nom Obert. Posez donc votre bagage, il pèsera guère et on dit que c'est chrétien de porter aide au pérégrin. J'peux vous mener jusqu'en l'abbaye où la charité des bons frères fera le reste. Vous cheminez vers la Cité, à ce que je vois. »

Lambert hocha le menton tandis qu'il posait son sac. Ils emboitèrent le pas à l'équipage.

« Nous allons passer par Bourges et Limoges pour faire visite à Charroux d'abord. Puis nous prendrons un bateau pour l'outremer ».

Le charretier ne sembla nullement impressionné.

« J'ai usage de voir grand nombre de marcheurs, allant voir la Madeleine[^madeleinevezelay] ou en revenant. Des gens qui vont visiter saint Étienne[^etiennebourges] itou. J'ai même partagé la route avec des Allemands qui se rendaient à Saint-Jacques de Galice[^compostelle]. Ils étaient justement passés par Vézelay, bien sûr. On a beau dire, c'est quand même la seule femme qu'a eu l'oreille du Christ, hein ? C'est quand même pas rien. »

Convaincu de la pertinence de son argument, il l'agrémenta d'un crachat expert qui conclua sa démonstration.

Les trois hommes continuèrent leur chemin en silence, accompagnés cette fois du grincement des roues. Alors qu'ils arrivaient en haut d'une légère pente, Ernaut avisa la longue piste qui partait droit devant eux vers le couchant, parmi les arbres de la forêt. Il hésita un instant avant de poser sa question.

« La voie est-elle donc toujours ainsi bien roide ? On n'en voit plus la fin. »

Odert ricana avant de hocher la tête, amusé. Lambert soupira bruyamment.

« Voilà deux jours de ça, tu te lamentais des sentiers tortueux et ravinés. Maintenant tu désespères de si beau chemin. Vas-tu te décider si tu es fille ou garçonnet ?

– J'en dis que je me verrais plutôt cavalier, cul en selle, monté sur gentil hongre. »

Lambert secoua la tête en dénégation.

« Je te l'ai déjà dit, nous en prendrons à Charroux. D'ici là, nous devons cheminer comme modestes pérégrins, sur nos deux jambes. »

Obert se tourna vers eux.

« L'automne dernier, j'ai eu vu passer un évêque et son train pérégrinant pour Bourges. Ils cheminaient en selle, même le saint homme. »

Le regard noir que lui lança Lambert l'incita à ne pas développer. Le crachat qu'il dirigea sur le sol mit fin à son intervention. Derrière eux, un peu en retrait, Ernaut se mit à fredonner une chanson, un sourire impertinent sur les lèvres.

## Hôpital de la Charité, soirée du jeudi 28 juin 1156

Lorsqu'ils arrivèrent aux abords du bâtiment des moines, Ernaut et Lambert furent accueillis par un homme entre deux âges, à la barbe poivre et sel, à qui il manquait un bras. Il les salua et les invita à s'asseoir sur un des bancs disposés devant un escalier de pierre accédant à l'étage. Deux autres voyageurs étaient installés là, prenant le soleil, après avoir eu leurs pieds lavés par un des servants. Les deux frères prirent place en silence. Ils étaient un peu las, n'ayant pas fait de longs trajets depuis quelques semaines.

Tandis qu'on achevait de les sécher, un convers, à la bure fatiguée, s'approcha d'eux. Il était assez jeune, plutôt laid, et flottait dans son habit comme un crapaud dans un sac. On aurait dit que la verrue qui ornait son nez l'avait incité à loucher. Le sourire déformant ses traits le rendait pourtant inexplicablement sympathique. Son visage rayonna lorsqu'il aperçut les besaces et, surtout, les croix aux épaules.

« La bienvenue en l'hospice des frères de Notre-Dame de la Charité, voyageurs. J'ai nom Thibald. Grande est ma joie à accueillir des marcheurs pour l'outremer. »

Lambert remercia d'un signe de tête, sans interrompre le moine.

« Si vous avez besoin de rapetasser souliers ou vêtements, nécessité d'en changer, nous avons ample fourniment pour vous. Sur votre demande, nous pouvons de même mander le barbier pour tailler barbe et cheveux.

– Grand merci de votre charité, frère, nous ne sommes partis que de peu, et n'avons guère besoin. Un bon repas et un endroit où dormir seront bien assez pour nous. »

Le moine hocha la tête avec énergie.

« Vous avez encore temps de vous rendre en la grande prieurale avant souper. Saint Jovinien nous honore fort souvent de beaux miracles.

– Nous sommes déjà venus prier devant la châsse, nous ne venons que de Vézelay. Je pensais plutôt m'y rendre demain avant le départ.

– Faites selon vos souhaits. Parmi le marché traversé pour venir ici se trouvent des marchands de cierges, si vous souhaitez laisser quelque lumière pour éclairer votre voyage. Je vous laisse, il me faut vaquer à mes occupations. »

Tandis qu'ils se délassaient, Ernaut discuta avec les voyageurs autour d'eux. Deux se rendaient justement à Vézelay, pour demander la guérison de leur sœur, frappée de cécité. Parmi trois autres en chemin pour Fleury s'en trouvait un le visage déformé par une tumeur. Elle lui obstruait pratiquement un œil et pendait jusque sur son épaule, sans qu'il en semble gêné.

Tous étaient impressionnés par le voyage qu'entreprenaient les deux frères. Ils furent peu après rejoints par un vieil homme squelettique, qui n'ouvrit pas la bouche si ce n'était pour tousser et qui suivait leurs discussions en hochant benoîtement du chef. En le voyant, un des servants le houspilla et le fit rentrer, en expliquant qu'il n'avait plus toute sa tête et devait rester au chaud, allongé. Il s'échappait tout le temps et se baladait dans la ville à demi nu. Après cela, le manchot revint chercher Ernaut et Lambert pour les mener à leurs lits, où ils pourraient laisser leurs affaires. Il leur donna des draps propres et des couvertures épaisses, ainsi que deux oreillers chacun. Le tout sentait bon l'herbe fraichement coupée et les fleurs des champs.

Ils étaient installés dans une grande chambre, avec une quinzaine d'autres couches, où n'étaient logés que des hommes. Les moines de l'abbaye trouvaient plus sage de séparer les deux sexes, même s'il se rencontrait parfois des époux parmi les voyageurs. Ils accueillaient sans demander, mais tenaient à diriger selon leurs règles. Au fond de la pièce, un petit oratoire était orné d'un crucifix, chichement éclairé d'une lampe à huile. Assis devant, un jeune garçon priait avec ferveur. Le serviteur leur expliqua à mi-voix, le regard attristé, que c'était le dernier fils d'un charbonnier. Le pauvre avait perdu l'usage de ses jambes suite à un accident dans les bois. Il était là depuis plusieurs semaines.

Lorsqu'ils redescendirent, une bonne odeur de nourriture chaude sortait de la grande salle. Il s'y trouvait une vingtaine de personnes, des pèlerins comme eux, mais aussi quelques malades et de simples voyageurs, trois hommes qui se tenaient à l'écart, vêtus comme des marchands. Des écuelles avaient été distribuées, de larges miches de pain tranchées et des pichets disposés.

Un moine à la tonsure soignée, habillé de laine très sombre d'excellente qualité, surveillait les préparatifs du repas. Son visage fin avait le teint pâle des clercs peu habitués aux grands espaces et ses yeux étaient semblables à deux charbons noirs, profondément enchâssés. D'un geste, il invita les deux frères à s'installer sur un des bancs. Une fois tout le monde assis, il toussa pour s'éclaircir la gorge puis lança d'une voix douce et chantante :

« *Benedic, Domine, nos et haec tua dona quae de tua largitate sumus sumpturi*.[^benedicite] »

Puis il marqua un temps avant de terminer : « *Per Christum Dominum nostrum.* ».

Chacun répondit alors «* Amen* » en se signant. Les serviteurs entrèrent, distribuant un épais potage très parfumé, dans lequel nageaient des morceaux de viande. Ernaut versa à son frère un vin rouge que Lambert apprécia en connaisseur. En quelques instants la rumeur enfla, chacun commentant sa journée, son voyage, ses affaires, avec ses voisins. Passant parmi les tables après avoir rapidement avalé le contenu de son écuelle, le jeune moine vérifiait que chacun avait tout ce qu'il lui fallait. Il ne manqua pas de bénir avec enthousiasme les deux marcheurs à destination de Jérusalem.

## Hôpital de la Charité, nuit du jeudi 28 juin 1156

Réveillé en sursaut par un ronflement plus fort venu de sa droite, Ernaut tournait depuis un moment dans ses draps sans parvenir à replonger dans le sommeil. Lassé de tourner sur une épaule puis l'autre, il se résolut à quitter son lit. Attrapant sa chape, il sortit sans bruit, couvert par le tintamarre des différents grognements, soupirs et gargouillis émis par ses compagnons de chambrée. Il descendit l'escalier extérieur et retrouva la cour, afin de boire quelques gorgées à la fontaine.

Une lueur dans la salle commune du rez-de-chaussée attira son regard. Intrigué, il s'avança et poussa la porte en grand. Thibald, le frère convers qui les avait accueillis était assis à une table, grignotant du fromage. Il avait à côté de lui des tablettes de cire et des feuillets, ainsi qu'une corne d'encre et quelques plumes. Le grincement de l'huis le fit se dresser, les sourcils froncés. Reconnaissant Ernaut et le voyant à peine vêtu, il lui sourit.

« Un peu tôt pour achever sa nuit. Laudes est encore loin... »

Ernaut manifesta son accord par un grognement qu'il s'efforça de rendre poli, ce qui ne rebuta pas le moine qui continua.

« As-tu la gorge sèche ? Je peux te porter quelque boisson.

– Merci non. Je vais juste m'asseoir ici un moment, en espérant que la chambrée sera moins bruyante plus tard. On dirait cantique de gorets. »

La remarque fit s'esclaffer Thibald.

« C'est par malheur bien fréquent. Une fois, la clameur était telle qu'un pèlerin préféra dormir dehors parmi la cour. Mais les choucas s'activèrent si tôt que sa nuit n'en fut guère meilleure. Ses compagnons le gratifièrent du surnom de Choucar depuis lors, m'a-t-on dit. »

Il reprit son sérieux.

« Certains demandent aussi à demeurer en l'église toute la nuitée. Mais les frères ne goûtent point cela. Cela dérange les offices et permet de bien fâcheuses rencontres... »

Il souligna sa dernière remarque d'un regard plein de sous-entendus. Puis se tint coi quelques minutes, fixant son stylet comme s'il le voyait pour la première fois. La flamme de la lampe donnait une couleur satanique à son visage ingrat et le faisait paraître plus vieux qu'il n'était. Lorsqu'il bougea de nouveau, il secoua doucement la tête, puis se tourna vers Ernaut.

« Vous empruntez bien long chemin, je vous envie.

– Nous allons prendre nef à Gênes, mais devons passer par Charroux auparavant.

– Excellente idée ! J'ai vu moult pérégrins faire semblable choix. »

Il marqua un temps avant de poursuivre, la mine désolée.

« J'aurais pris grande joie à faire semblable voyage, mais j'ai trop de choses à faire ici...

– Vous ne pouvez demander congé à l'évêque ?

– Pour moi, ce serait auprès du prieur et de l'abbé Pierre[^pierrevenerable], à Cluny. Mais ce serait bien lourd tracas pour tous de me remplacer, il me faut demeurer à ma place. Parlons de toi plutôt. Tu es bien jeune pour pareil vœu, le fais-tu pour un parent, un bienfaiteur ? »

Ernaut réfléchit quelques instants, un peu embarassé. Puis répondit :

« Non pas. Nous allons faire nos dévotions au Sépulcre, mais comptons nous établir là-bas. On nous a dit qu'ils espèrent la venue de nombreux colons. »

Thibald ouvrit de grands yeux, visiblement impressionné.

« C'est merveilleux d'avoir ainsi entendu l'appel ! Tes parents doivent être bien fiers, garçon. »

Peu désireux de poursuivre cette conversation, Ernaut se leva, arborant l'air le plus fatigué qu'il pouvait.

« Certes. Père a tout fait pour que notre installation là-bas se passe au mieux.

– Mes pensées vous accompagneront, ton frère et toi, en tout cas. À Dieu te mande[^dieumande].

– Grand merci, frère. Je vais d'ailleurs de ce pas me reposer en la couche que vous nous avez préparée. »

Après un rapide salut, Ernaut sortit de la salle, sous le regard attendri du moine. Thibald trouvait néanmoins étrange ce manque d'enthousiasme. Quelques années plus tôt, un tel voyage et la perspective de s'installer en Terre sainte auraient constitué pour lui un rêve fabuleux. Pourtant Ernaut semblait le vivre péniblement. Le convers se demanda un instant si le pèlerinage était bien une aspiration du jeune homme. Puis, balayant ces idées de sa tête, il se replongea dans l'inventaire de ses besoins pour le mois à venir.

Dehors Ernaut déambula un peu dans la cour, se rendit jusque sur les bords de la Loire. La lune gibbeuse, habillée de lambeaux filandreux, éclairait le pont de bois qui filait vers l'ouest. Le vacarme des flots était accompagné du grincement des moulins attachés aux piles. Deux silhouettes flânaient, une lanterne à la main, au débouché sur la rive. Quelques masures sur l'île en face avaient leurs cheminées encore fumantes et, parfois, un rai de lumière éclaboussait de jaune les sombres façades. Le jeune homme demeura un long moment le regard dans le vide, humant à pleins poumons l'air frais. La nuit débutait à peine, il avait le temps. Il se frotta le crâne de la paume, nerveux. Une angoisse commençait à lui peser sur la poitrine : il se rendait compte qu'il ne reverrait jamais plus ces endroits. ❧

## Notes

Les pèlerins médiévaux suivaient les routes des autres voyageurs et, même si certaines voies étaient plus fréquentées que d'autres, beaucoup choisissaient leur parcours en fonction d'impératifs personnels. La plupart du temps, ils allaient à un sanctuaire régional,
voire local, ou cheminaient de l'un à l'autre. Il est difficile d'indiquer un tracé idéal.

Sur le trajet , l'hébergement des plus humbles était aléatoire, et bien souvent, c'était une institution monastique charitable qui s'occupait de nourrir et d'accueillir tous ceux qui se présentaient ainsi que les malades. Des abus existèrent, mais cela ne remit pas en cause la vocation hospitalière de nombreux établissements, du moins pas au XIIe siècle.

Le texte exact du cantique, le psaume 119 (120), massacré par Ernaut est le suivant :

>1\. Dans ma détresse, c'est à l'Éternel Que je crie, et il m'exauce  
>1\. *Ad Dominum in tribulatione mea clamavi et exaudivit me*

>2\. Éternel, délivre mon âme de la lèvre mensongère, De la langue trompeuse !  
>2\. *Domine libera animam meam a labio mendacii a lingua dolosa*

>3\. Que te donne, que te rapporte Une langue trompeuse ?  
>3\. *quid detur tibi aut quid adponatur tibi ad linguam dolosam*

>4\. Les traits aigus du guerrier, Avec les charbons ardents du genêt.  
>4\. *sagittae potentis acutae cum carbonibus iuniperorum*

>5\. Malheureux que je suis de vivre en exil, d'habiter parmi les tentes de Kédar !  
>5\. *heu mihi quia peregrinatio mea prolongata est habitavi cum tabernaculis Cedar*

>6\. Assez longtemps mon âme a demeuré Auprès de ceux qui haïssent la paix.  
>6\. *multum peregrinata est anima mea cum odientibus pacem*

>7\. Je suis pour la paix ; mais dès que je parle, Ils sont pour la guerre.  
>7\. *ego pacifica loquebar et illi bellantia*

## Références

Collectif, *Voyages et voyageurs au Moyen Âge*, Paris : Publications de la Sorbonne, 1996.[En ligne], consulté le 23
septembre 2011, Adresse URL: <http://www.persee.fr/issue/shmes_1261-9078_1996_act_26_1>

Heath Sydney, *Pilgrim Life in the Middle Ages,* London, Leipsic: T. Fisher Unwin, 1911.

Verdon Jean, *Voyager au Moyen Âge*, Paris : Perrin, 2003.

Webb Diana, *Pilgrims and Pilgrimage in the Medieval West*, London - New York: I.B. Tauris, 1999.
