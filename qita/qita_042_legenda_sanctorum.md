---
date: 2015-05-15
abstract: 1157-1158. Désormais au service d’un puissant prélat de Terre Sainte, Herbelot se rend compte de la difficulté à concilier ses aspirations spirituelles et la réalité du pouvoir qui s’exerce en ces lieux. Son naturel spirituel et son goût pour la littérature vont l’amener à chercher une solution originale à cette situation.
characters: Herbelot Gonteux, Gilbert
---

# Legenda sanctorum

## Jérusalem, veillée du vendredi 27 décembre 1157

Malgré les épaisses chausses de laine, Herbelot sentait le froid lui étreindre les pieds. Il tentait par moment de remuer les orteils de façon à faire circuler le sang, mais rien ne semblait réchauffer ses extrémités. Ses mains, enfouies dans les manches, avaient trouvé refuge sous ses aisselles et il n'émergeait de sa capuche que le bout de son nez pointu, aussi rouge que celui des clercs qui l'environnaient. La ville sainte était toujours plus froide que la côte, et il en faisait la triste expérience.

Depuis qu'ils étaient arrivés, quelques jours plus tôt, pour les célébrations de Noël, il ne faisait que grelotter et renifler. La vaste rotonde du Saint-Sépulcre lui semblait une vraie glacière, et la foule pressée dans ses murs n'avait pas suffi à en réchauffer les pierres. De plus, il n'y avait guère d'entrain dans les cérémonies, qui voyaient pour la première fois l'absence définitive du maître des lieux, le patriarche Foucher, dont la voix tonnait si vigoureusement contre les ennemis de l'Église.

Remplaçant ses sermons adroitement menés, ce n'étaient que chuchotis et murmures feutrées, alors que l'élection de son successeur occupait de nombreux esprits. Les frères de l'Hôpital jubilaient, sans trop le montrer, débarrassés de leur plus farouche adversaire et les clercs ambitieux en appelaient à l'arrière-ban de leurs soutiens pour s'installer dans la cathèdre. Le cloître des chanoines, que rejoignit Herbelot après vêpres, résonnait des confidences échangées et des sourires de connivence.

La corne de la lune accrochait des dentelles de brume dans un ciel étoilé. Encore un signe de froid, se lamenta en silence Herbelot. Il tapa des pieds et toussa, profitant de la liberté accordée au-dehors. Il avait grandi dans un monastère aux habitudes strictes, et se conformait sans même y penser à la discipline la plus sévère. Pour lui, la règle de vie des chanoines du Saint-Sépulcre était bien souple et permissive, et lui semblait comme des vacances.

Il avait accompagné l'archevêque Pierre de Tyr, et en tant que fonctionnaire du prestigieux prélat, était hébergé dans le palais du patriarche. Mais il aimait assister aux offices des heures, qui avaient si longtemps rythmé son existence, enfant. Cela lui apportait une régularité qu'il appréciait fort pour le calme que cela insufflait dans son esprit.

Il quitta les frères dans le cloître pour rejoindre sa chambre par le dédale de couloirs et de courettes qui s'emboîtaient dans le labyrinthe des bâtiments canoniaux. Il partageait là une petite pièce à deux alcôves, avec un des suivants de l'évêque de Lydda, Gilbert. Joufflu et ventru comme seuls savent l'être ceux qui passent plus de temps à bâfrer qu'à s'activer, il était malgré tout un agréable compagnon, à la voix douce, au timbre juste, faite pour les hymnes les plus difficiles. Il était animé d'une passion sans borne pour tout ce qui avait trait à la musique, et espérait bien un jour devenir chantre de sa congrégation.

En attendant, il se contentait d'aider comme clerc de second rang, prenant des notes, classant les documents, et portant les messages. Il ne s'en offusquait pas et fredonnait gaiement sans cesse du soir au matin, ponctuant la moindre tâche de strophes légères. Chaque soir, Herbelot profitait de quelques interprétations, accompagnées du psaltérion ou de clochettes, qui lui ravissaient le cœur. Il avait sympathisé avec le jeune homme, heureux de trouver une oreille compatissante à son sort.

Lui aussi était un oblat, originaire d'Allemagne. Son vrai nom était Geizbart, et il était orphelin, recueilli par une généreuse patronnesse qui l'avait doté pour le monastère. Il parlait régulièrement d'elle, dame Ottilie, comme d'une sainte. Qu'il n'avait jamais eu l'occasion de rencontrer, mais pour qui il priait souvent, comme nombre d'enfants qu'elle avait ainsi sauvé. Il espérait aider au salut de son âme, convaincu que les dévotions à son intention porteraient leurs fruits.

Herbelot trouva le jeune clerc assis, occupé à accorder les cordes de son psaltérion à la lueur des lampes.

« La bonne nuit, frère, lui lança-t-il en entrant, tout sourire.

– La paix de Dieu, mon frère, répondit Gilbert, sans quitter son ouvrage des yeux.

– Vous avez souci de votre instrument ?

– Non pas. C'est juste l'humidité et le froid qui le font sonner de bien male façon. J'ai grande hâte de retrouver mon douillet logis à Lydda.

– Je n'aurais pas cru dire un jour regretter de célébrer la naissance du Christ notre Sauveur auprès de son tombel. Mais voilà bien tristes cérémonies. »

Gilbert opina sans un mot, tout à son ouvrage, laissant le silence s'installer un moment avant de reprendre.

« J'ai ouï dire que de nouveaux courriers vont être faits, pour inciter barons et souverains à venir sous le signe de la Croix.

– Sans le père Bernard pour les sermonner, je ne sais si beaucoup auront le courage de venir.

– Gardez espoir, mon frère. Le puissant comte Thierry ne bataille-t-il pas en ce moment même dans les fiefs de Norredin ? Il doit bien y en avoir d'autres comme lui. Il faut seulement les rappeler à leurs devoirs. »

Herbelot soupira, moins enthousiaste que son compagnon. Le comte de Flandre était le frère par alliance du roi Baudoin, et un croisé convaincu, un homme de foi autant que de guerre. Les dirigeants d'Europe ne se souciaient plus guère des territoires latins de Terre sainte. Il lui semblait que la flamme qui avait animé leurs prédécesseurs avait fini par s'éteindre après avoir longtemps vacillé, inlassablement soutenue par le père abbé de Clairvaux, Bernard.

Il s'installa sous les couvertures et souhaita une bonne nuit à son compagnon de chambrée. Avant de s'allonger, il récita plusieurs *Ave Maria*, persuadé que la mère de Dieu était la plus à même de venir en aide à ceux dans le besoin.

## Jérusalem, après-midi du samedi 28 décembre 1157

« *Fuit homo missus a Deo, cui nomen erat Joannes.* »

Herbelot avalait sans trop y penser son repas, en silence, comme de coutume. On n'entendait que des raclements de gorge discrets, le frottement des pas des valets qui servaient les plats, les reniflements des malades. Malgré la relative souplesse des règles, il appréciait qu'ici aussi on mange en commun, sans parler, tandis qu'un frère lisait un texte sacré, ou règlementaire. Tout ce qui était propice à l'introspection et l'étude plaisait à son esprit méthodique et routinier.

« *Erat lux vera, quæ illuminat omnem hominem venientem in hunc mundum.* »

Les lentilles étaient agrémentées d'épices au goût douceâtre, et leur couleur dénonçait l'ajout de safran, mais au moins n'y avait-il pas de viande ou de garniture trop riche. Le vin était également de qualité supérieure, et la croûte du pain frais craquait sous la dent. Malgré son éducation et toutes ses préventions, le jeune clerc appréciait fort la bonne chère, dernier plaisir auquel les moines s'adonnaient sans trop de retenue. Leurs vastes domaines et leur richesse permettaient de doter grassement les celliers, et la dérive était par trop fréquente. Un peu de tempérance n'était néanmoins pas malvenu selon Herbelot, pour qui les joies de la table constituaient un péché coupable qu'il était bien souvent obligé d'avouer à confesse. Ce n'était certes pas son seul défaut, mais celui auquel il succombait le plus aisément. En dehors peut-être de celui de la vanité, qu'il reconnaissait bien moins volontiers.

« *Hic erat quem dixi : Qui post me venturus est, ante me factus est : quia prior me erat.* »

Un plat de darnes de poisson, à l'ail, au persil et au citron fut déposé à son intention devant lui. Le fumet brulant lui réchauffait le visage autant qu'il satisfaisait ses narines. Il avala le contenu de l'écuelle avec un grand plaisir, autant pour le goût que pour la bienfaisante sensation de chaleur qui glissait en lui.

« *Et ego vidi : et testimonium perhibui quia hic est Filius Dei.* »

Une fois la compotée de fruits avalée, tandis que les valets débarrassaient le couvert, la file des chanoines se mit en marche vers le cloître. Le lecteur avait achevé le premier chapitre de l'Évangile selon saint Jean et s'employait à ranger l'imposant grimoire dans un placard d'où il ne ressortirait qu'au prochain repas. L'église était assez riche pour avoir plusieurs copies de tels précieux livres, sans devoir transporter les ouvrages d'un lieu à l'autre au gré des besoins.

Après manger, l'ambiance était en général assez détendue, et de nombreux clercs profitaient de ce moment de récréation pour discuter dans le cloître. Quand Herbelot arriva dans l'aile est, il entendit justement un petit groupe qui palabrait avec animation.

« Je ne crois pas qu'il puisse y avoir meilleur salut de l'âme que celui de prononcer ses vœux, mon frère, assénait un grand moine au nez tordu.

– Mais tout le monde ne peut se tonsurer pour autant…

– D'autant qu'il faut bien combattre les infidèles, ajouta un troisième.

– Ils n'ont qu'à se faire clercs au soir de leur vie, nul besoin de se faire oblat.

– Dieu approuve les combats contre les ennemis de sa Foi. Les saints ont permis la prise des cités où nous sommes ce jourd'hui.

– C'est le relâchement des chrétiens désormais qui est puni par Le Très Haut. »

Aucun des chanoines présents ne remit en cause cette dernière assertion. Le clerc poussa son avantage.

« Nous voyons arriver certains saints inconnus de nous jusqu'alors mais leurs prouesses ravissent le cœur du juste. Ils sont comme les Macchabées les guerriers saints du Seigneur, mais affirment haut et clair que de la prière viendra le Salut, pas du combat l'épée en main.

– Il faudrait un tel homme à la tête des royaumes, un moine qui saurait faire juste usage de la force. Seul l'oint du Seigneur sait tenir son cœur pur des appâts du siècle.

– Le roi Godefroy l'avait bien compris, et tenait patriarche pour son sire.

– Puisse Dieu nous envoyer de nouvel un tel roi ! », confirma le moine.

Bien que récemment arrivé en Terre sainte, Herbelot partageait l'analyse de ses compagnons tonsurés. Depuis la perte du comté d'Édesse, le royaume latin semblait malmené, en passe de disparaître régulièrement sous les assauts des infidèles. Il en rejetait la faute sur les dirigeants, plus prompts à se disputer le moindre butin, le plus petit casal, que de porter la parole du Seigneur comme l'avaient fait leurs illustres prédécesseurs quelque cinquante ans plus tôt.

La puissance militaire ne suffirait pas si la Foi ne guidait pas leurs pas, la grande expédition de 1148 l'avait amplement démontré. Le roi Baudoin était encore jeune, et il y avait espoir qu'il se montre à la hauteur des attentes. Il était né ici, en Terre Sainte, et serait peut-être le souverain qui saurait rallumer la flamme qui éclairerait leur avenir. Herbelot pria pour que ses précepteurs lui aient instillé l'amour de Dieu plus que des choses de ce monde

## Tyr, après-midi du jeudi 9 janvier 1158

La pluie sonnait aux carreaux de verre du petit cabinet où l'archevêque aimait à se réfugier pour traiter les affaires administratives. Une épaisse couverture de laine anglaise sur les genoux, un brasero à ses côtés, il répondait à son courrier, approuvait des textes et chartes d'un signe de tête entre deux siestes. Quand le temps le permettait, il ouvrait les châssis et admirait l'horizon maritime, les vagues et les voiles y dansant. Il ne s'émouvait guère des cris et des bruits de la ville qui parvenaient ainsi jusqu'à sa demeure et s'amusait parfois des appels lancés dans les rues.

Aujourd'hui, il se contentait de caresser la fourrure épaisse du petit chien au poil gris qui lui chauffait les genoux. Le roquet, à l'ambition de molosse quand il était jeune, était au fil du temps devenu aveugle et avec sa vue sa méchanceté était partie. Il faisait désormais fête à qui le flattait et dormait partout où ses pas le conduisaient, toujours dans l'orbite du prélat. Herbelot, qui ne l'avait pas connu dans ses vertes années féroces, appréciait fort l'animal, et se baladait parfois avec Hieronimus dans les bras lorsqu'il allait déambuler dans les jardins.

« Je me demande si je ne vais pas préparer un sermon spécial pour le dimanche de la Purification de la Vierge », déclara soudain le vieil archevêque.

Délaissant les documents qu'il relisait, Herbelot attrapa une tablette vierge et un stylet et s'apprêta à noter. Mais rien ne vint durant un long moment, jusqu'à ce que l'archevêque Pierre se tourna vers son clerc, souriant, et lui demanda :

« Le Sire Christ lui-même a été présenté au Temple, n'est-ce pas là signe que toute puissance ne vient que de l'Église ? Qu'en dis-tu ?

– Je voyais le moment plus propice à célébrer la mère de Dieu, comme chacun. Mais je n'ai pas votre sapience, père »

L'archevêque s'amusa de la répartie, toute de docilité de son subordonné. Il aimait bien Herbelot, dont le penchant pour la vie monastique lui agréait fort, bien qu'il le trouva parfois trop timoré et bien trop obséquieux. Son enfance chez les bénédictins en avait fait un mouton, enclin à ne jamais chercher à lever la tête du troupeau, et peu désireux de s'attirer l'ire du pasteur.

« Avec l'élection du nouveau patriarche, il me revient en tête toutes les bassesses que les hommes acceptent pour cueillir les fruits de leur ambition. Je crois qu'il est temps de rappeler que le premier devoir, surtout des puissants, est d'honorer Dieu en toute circonstance. Qu'il n'est nul pouvoir qui puisse refléter, ne serait-ce qu'imparfaitement, Sa Majesté. »

Tout en hochant la tête, Herbelot notait scrupuleusement, sachant que ces quelques remarques jetées apparemment sans ordre étaient souvent le fruit d'une longue réflexion et fourniraient les bases d'un texte plus solide.

« Je vois chaque jour des barons au cœur avide des attraits de la poussière du siècle, et sans un saint homme à leur côté, ils se perdent. Regarde le roi Louis de France, sans Suger pour lui insuffler sagesse et tempérance, il gaspille pire que le fils prodigue. »

Il se passa une main chenue sur son visage osseux, essayant d'en faire glisser la fatigue qui s'y lisait dans les plis crevassés.

« Des gens de cœur et de sapience, comme Frédéric[^fredericroche], pour lequel j'ai grande estime, se font engluer par la tourbe du siècle. Il faut certes avoir cœur de saint pour espérer régner ici-bas sans se voir souillé par ses propres désirs. »

Il marqua un temps, ajoutant à mi-voix, pour lui-même, ou pour son confident, qui comprit qu'il ne s'agissait pas d'une remarque à noter :

« C'est pour cela que j'ai toujours fui les honneurs, je sais ma chair trop faible. Puisse ceux qui les empoignent faits d'un or plus pur ! »

Il sortit un carré d'étoffe de sa manche et se moucha longuement, bruyamment, avant de reprendre son monologue.

« Nous n'appartenons à nulle terre, pas plus qu'aucune province n'est nôtre. Il nous faut apprendre à servir avant tout, car nous sommes tous Ses sujets. »

Il se tourna une fois de plus vers son clerc, un demi-sourire sur son visage décharné, une lueur amusée dans son regard brouillé et larmoyant. Herbelot se chercha une contenance dans les notes qu'il suivait des yeux sur la cire, puis échappa doucement :

« Les clercs, les prêtres apprennent à servir, ainsi que je l'ai fait en moustier, à la Charité des saints Pères.

– Tu parles de juste, comme souventes fois, mon jeune ami. Un souverain devrait avant tout être le prêtre de son royaume. »

## Tyr, veillée du mardi 25 février 1158

Herbelot parlait d'une voix douce tandis qu'il lisait, le doigt sous la ligne, le texte du manuscrit. Un de ses privilèges, en tant que clerc de l'archevêque de Tyr, était de pouvoir accéder à tout moment à la bibliothèque du prélat, dont il avait les clefs. Il pouvait détacher des meubles les recueils de prix, et ouvrir les armoires où étaient resserrés les plus précieux. Il ne s'y trouvait rien de licencieux, mais pas mal d'ouvrages décorés finement, importés à grands frais des scriptoriums les plus prestigieux. Le volume que parcourait Herbelot était d'ailleurs orné de magnifiques lettrines de couleur, ornées de feuillages aux complexes volutes, couchées sur un vélin doux au toucher.

Il acheva sa lecture, empreint de sérieux.

« *Gratia Domini nostri Jesu Christi, et caritas Dei, et communicatio Sancti Spiritus sit cum omnibus vobis. Amen*[^citevangile4].»

Il releva la tête, comme remâchant la leçon apprise de ce texte. Il aimait beaucoup saint Paul. Pas tant pour le contenu que par la richesse de ses épîtres. Comme l'abbé de Clairvaux, il avait entretenu une correspondance nombreuse et variée, désireux de répandre la profondeur de ses connaissances des mystères divins à tous les fidèles en détresse. Il apportait la lumière à ceux qui erraient dans les ténèbres.

Il avala un peu de vin chaud qu'il s'était fait porter et s'assit confortablement sur la cathèdre emplie de coussin que l'archevêque affectionnait les rares fois où il venait. La faible lueur des deux lampes à huile tapissait de carmin et de safran les profils des ouvrages soigneusement empilés sur les étagères. La fenêtre, barrée d'un épais volet de bois, ne laissait pas deviner la noirceur de la nuit.

Herbelot se sentait bien, ici. Il aimait se recueillir sur les textes sacrés et philosophiques, ou feuilletait de temps à autre quelques volumes profanes, comme les extraits de la Chanson d'Alexandre qu'il appréciait fort. Il s'y narrait bien évidemment les parties qui racontaient la jeunesse du grand conquérant, et le siège de Tyr, la ville même où se trouvait Herbelot. Plus il y repensait, plus il était persuadé qu'il était nécessaire de faire comme le grand saint Paul, qu'il fallait écrire des missives. Non pas pour quémander comme le faisaient la plupart des gens ici, mais pour conseiller, infléchir, orienter. Les rois, le basileus, avançaient tels des enfants dans les ténèbres, sans guide éveillé pour les mener, sans modèle à imiter. Ils suivaient leurs instincts, forcément vils et bas, mais ne savaient pas comment faire appel à toutes leurs ressources pour devenir des monarques défenseurs de la vérité divine. Il leur fallait quelqu'un qui leur écrive, qui leur indique comment faire, qui les inspire et les galvanise. Un grand souverain, qui serait à la fois un sage, un prêtre et un homme de foi. Il repensa à saint Jean. ❧

## Notes

Les citations en italique dans la seconde partie du texte sont extraites du premier chapitre de l'Évangile selon saint Jean.

>« *Fuit homo* *missus a Deo,* *cui nomen erat Joannes.* », verset 6  
Il y eut un homme envoyé de Dieu, son nom était Jean.

>« *Erat lux vera, quæ illuminat omnem hominem venientem in hunc mundum.*», verset 9  
Cette lumière était la véritable lumière, qui, en venant dans le monde, éclaire tout homme.

>« *Hic erat quem dixi : Qui post me venturus est, ante me factus est : quia prior me erat.* », fin du verset 15  
C'est celui dont j'ai dit: Celui qui vient après moi m'a précédé, car il était avant moi.

>« *Et ego vidi : et testimonium perhibui quia hic est Filius Dei.* », verset 34  
Et j'ai vu, et j'ai rendu témoignage qu'il est le Fils de Dieu.

Il est difficile de se représenter l'univers mental des oblats qui, comme Herbelot, scrutaient le monde à travers le prisme unique de leur éducation religieuse. Évoluant dans un contexte social relativement préservé, où ils ne connaissaient que peu les aléas de la vie du commun, il leur semblait naturel de plaquer sur le moindre fait la grille de lecture catholique et savante dont ils étaient imprégnés.

Cela ne signifiait pas pour autant qu'ils n'étaient pas capables de penser par eux-mêmes, la richesse intellectuelle du XIIe siècle est là pour le prouver . Par ailleurs, cela créait une universalité culturelle qui transcendait les origines géographiques, permettant de se sentir comme chez soi dans la plus petite communauté religieuse, pourvu qu'elle respectât au minimum les règles générales.

Le titre de ce texte vient du début du titre originel de la *Légende dorée*, ouvrage du XIIIe siècle rédigé par Jacques de Voragine, qui présente la vie de nombreux saints : « Ce qui doit être lu des saints… »

## Références

Voragine, Jacques de, Roze Abbé J.-B. M., *La légende dorée*, Paris : Édouard Rouveyre éditeur, 1902. Une version en ligne existe : <http://www.abbaye-saint-benoit.ch/voragine/index.htm> \[consulté le 9 avril 2015\].

Le Goff Jacques, Rémond René, *Histoire de la France religieuse. Tome 1. Des dieux de la Gaule à la Papauté d'Avignon (des origines au XIVe siècle)*, Paris, éditions du Seuil, 1988.

[^citevangile4]: Dernier verset du chap. 13 de la Seconde épitre de saint Paul aux Corinthiens : « Que la grâce de notre Seigneur Jésus-Christ, l'amour de Dieu et la communication du Saint-Esprit soient avec vous tous ! »
