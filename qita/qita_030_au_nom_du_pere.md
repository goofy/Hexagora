---
date: 2014-05-15
abstract: 1157-1158. Le pouvoir dans le royaume de Jérusalem n’est certainement pas qu’entre les mains des barons. Lorsque le Patriarche Foucher d’Angoulême disparaît, les appétits s’aiguisent, les ambitions se dévoilent. Assistant à cela de loin, Herbelot découvre le monde intrigant des écclésiastiques de haut rang, bien loin de son monastère natal ou de l’école diocésaine. Y sera-t-il vraiment à sa place ?
characters: Daimbert, Herbelot Gonteux, Frédéric de la Roche, Frère Matthieu, Frère Stasino, Pierre de Barcelone, Raoul de Bethléem
---

# Au nom du père

## Jérusalem, soir du jeudi 21 novembre 1157

Le glissement des souliers n'était pas le seul bruit qu'on entendait à la sortie du chapitre de Saint-Jean de l'Hôpital. Le cloître bruissait des voix feutrées, des commentaires susurés à son voisin, des questions lancées à ses camarades. Les lampes traçaient des ombres aiguës parmi les feuillages des chapiteaux, habillaient d'or rouge les plantes endormies pour l'hiver, au centre de la cour. La longue silhouette de frère Matthieu était entourée d'un cercle de curieux, impatients d'en apprendre plus. Il était de ceux qui fréquentaient souvent le palais royal, ou voyageaient pour les besoins de l'ordre. Quoique peu porté aux commérages, il aimait à montrer son importance par le savoir qu'il avait du monde par-delà la clôture de leur couvent.

« Il est bien trop tôt pour savoir qui sera le prochain sire patriarche, mes frères.

– N'y-a-t-il aucun favori ? Le roi a sûrement quelque protégé à mettre en avant…

– Le sire Baudoin guerroie au nord ces temps-ci. Peut-être ne sait-il pas encore que le vieux Foucher est passé. Je serai bien étonné que la situation soit tôt réglée.

– Alors, prions pour que le Seigneur nous envoie un prélat plus compréhensif ! »

La dernière remarque avait été proférée avec une colère contenue qui contrastait avec la clémence du propos. Celui qui l'avait lancée était un moine respecté de la communauté, frère Garin, un des responsables des malades en charge d'une des *rues*, une des allées du grand Hôpital. Il avait connu tous les affrontements avec le vieux patriarche et le chapitre du Saint-Sépulcre. Quelques visages hochèrent en assentiment, l'invitant à poursuivre.

« Maintenant qu'il est mort, nul besoin de s'acharner sur sa mémoire et en bons chrétiens, nous le mettrons dans nos prières, comme savent le faire les âmes charitables. Mais j'espère que nous aurons moins d'ennuis avec le prochain.

– Il avait reçu le siège de Tyr des mains du vieux Guillaume[^guillaumemalines], non ?

– Si fait, et devenu comme lui patriarche après ça… »

Les visages se fermèrent, le temps qu'ils envisagent la suite logique de la proposition. Une voix timide brisa finalement le silence.

« Le vieux Pierre de Barcelone ?

– Il est bien trop âgé pour devenir patriarche.

– Les ans n'ont rien à voir là-dedans. Nous avons bonne entente avec lui pour l'heure. C'est homme de raison, dévot et craintif de Dieu. »

Sur cette dernière remarque, le groupe se défit peu à peu, les clercs profitant du répit avant l'office des vigiles pour vaquer à leurs occupations personnelles. Frère Matthieu s'avançait vers le dortoir lorsqu'une main le retint. Stasino, un autre des responsables d'une des rues semblait ennuyé, les plis de graisse de son visage poupin noyant son regard d'habitude acéré dans une mer d'ombre. Matthieu n'aimait guère les façons de faire de cet Italien, qu'il estimait retors, ou du moins trop emphatique dans ses démonstrations de piété comme dans ses colères. Lui avait appris à demeurer en retrait, calme, ainsi qu'il seyait à un clerc. Un esprit et un corps agités ne pouvaient contenir qu'une âme tourmentée selon lui.

Il fit néanmoins bonne figure et, d'un sourire, invita son compagnon à parler. Ils s'installèrent dans un recoin du cloître, entre les colonnes et les chapiteaux sculptés. Le chant nocturne d'un pipit tardif résonnait parmi les buissons et les arbres soigneusement taillés de la cour.

« Un souci, frère Stasino ? »

Le petit homme semblait osciller d'un pied sur l'autre, les plis de son ample froc jouant dans la chiche lumière des lampes et de la lune. Il passa finalement une main sur son visage replet et prit la parole, visiblement à contrecœur.

« C'est à propos de mon client, celui qui n'a pas pu avoir chrétienne sépulture.

– Vous parlez de celui qui troubla les Pâques ?

– Celui-là même, si fait. La désignation du nouveau sire patriarche donnera lieu à moult réjouissances, et pardon sera accordé à de nombreux pécheurs. »

La sincérité et la charité de la demande adoucirent les traits habituellement fermés du grand clerc.

« Il a commis fort méchants péchés, et ne saurait être aisément accueilli dans la communauté.

– J'en ai bien conscience, frère, et j'avoue que je l'associe malgré tout à mes prières, espérant qu'il trouve par-delà la mort la paix qu'il n'a su encontrer ici-bas. »

Matthieu s'assit sur un des bandeaux sculptés et réfléchit un petit moment avant de reprendre.

« Le sire patriarche Foucher était homme intraitable, accroché au plus modeste de ses privilèges et détestait la moindre offense. Il y a espoir que le nouveau sera plus enclin au pardon, cela ne serait que facile. D'autant qu'il n'aura peut-être pas connu les blasphèmes directement.

– Vous pensez au sire archevêque Pierre de Tyr alors ? »

Le grand moine haussa les épaules.

« Je l'ai dit, je n'en sais rien encore, il est trop tôt. C'est là usage fréquent, et tradition fort ancienne. »

Stasino acquiesça, rasséréné à l'idée que le débonnaire Pierre de Tyr remplace le prélat autoritaire qu'était le vieux Foucher. Il fut coupé dans ses pensées par Matthieu.

« N'oubliez pas que le roi aura son mot à dire. Le sire évêque Raoul, son chancelier, aura peut-être désir de prendre revanche sur Pierre. Il n'a pu garder le siège de Tyr, il apprécierait de certes être celui qui couronne les rois.

– Est-il homme de pardon ?

– Comme chacun de nous, frère Stasino. Plus enclin à pardonner les offenses faites à d'autres qu'à lui-même… »

## Tyr, midi du mardi 7 janvier 1158

Accueillant deux braseros, la petite pièce était étouffante de chaleur. Pourtant, le vieux Pierre de Barcelone, archevêque de Tyr était assis sous une couverture fourrée, et un bonnet de feutre écarlate r tombait sur son front parcheminé. Le visage las, creusé de rides, transpirait la fatigue, sinon la tristesse. Une main hésitante, aux ongles soignés, aux veines saillantes, émergeait de l'amas de vêtements brodés et passementés, serrant un verre vide rehaussé de motifs en relief.

À ses côtés, installé lui aussi sur une chaise curule mais d'une façon beaucoup plus maitrisée et stricte, un clerc au nez pointu, au regard vif, attendait, prêt à noter sur une tablette de cire ce que l'archevêque lui dicterait. Il sirotait tranquillement son vin, cherchant à se faire oublier dans la discussion, comme le moine discret qu'il aimait être.

Assis face à eux, un autre prélat se tenait, de grande taille, plus jeune, et surtout plein d'énergie. Il était vêtu d'une tenue de prix, d'un bliaud qui n'aurait pas dépareillé à la Haute Cour. Néanmoins, sa tonsure soignée ne laissait pas le moindre doute sur son statut, pas plus que la lourde croix émaillée qu'il arborait sur la poitrine, ainsi que son anneau. Frédéric de la Roche, évêque d'Acre était un homme à l'activité débordante, qui accompagnait volontiers le roi Baudoin lors de ses expéditions guerrières.  On le disait aussi habile à parler de Végèce[^vegece] qu'à citer les Pères de l'Église. Il digérait les paroles du vieil archevêque tout en faisant glouglouter le vin entre ses joues, l'air pensif. Il semblait percevoir la ville et, au-delà, la mer par les deux petites fenêtres donnant au nord. Puis son regard revint sur Pierre, sur le clerc à ses côtés, sur les documents qui recouvraient la table sombre, sur les plumes et les encriers précieux, pour mourir sur les motifs arabesques des fresques murales. Ce n'est qu'en arrivant sur l'imposant Christ de bois peint qui paraissait les dévisager tous qu'il se décida à ouvrir de nouveau la bouche.

« Mon sire, si vous ne souhaitez pas devenir patriarche, cela va sans nul doute inciter à plus de candidatures. Le royaume se remet à peine des troubles de la succession.

– Je sais bien que d'aucuns estiment qu'il est d'usage pour l'archevêque de Tyr de finir patriarche, mais Dieu m'est témoin que je suis bien fatigué assez pour m'en garder. Nul doute que le roi saura choisir celui qui convient le mieux. »

Le vieil homme sourit, sans joie, et planta son regard dans celui de son suffragant.

« Ne soyez pas inquiet, je n'ai guère d'appétit pour un autre siège, et je n'en ai guère plus pour la chère. Mon siège sera vacant assez tôt pour que vous puissiez y prétendre.

– Père, mes prières ne demandent qu'à vous soutenir, loin de moi l'idée…

– Ne vous en défendez pas, mon fils, votre espoir est bien légitime. Vous êtes homme de sapience et au fait des affaires du siècle. Dieu ne veut que mettre ainsi votre patience à l'épreuve, car c'est là qualité essentielle pour le berger. Je vous porte en assez bonne estime pour assavoir que vous préféreriez me succéder en me sachant à Jérusalem qu'allongé en mon tombeau. »

L'évêque sourit de mauvaise grâce à cette assertion perfide et reposa son verre, se levant dans le même mouvement.

« En ce cas, l'élection sera encore décidée par les gens de la Haute Cour.

– La reine conserve forte influence sur ces choses. Sa piété et ses largesses donnent beaucoup de poids à ses recommandations, sans parler même de ses sœurs, la mère Yvetta ou la comtesse de Flandre.

– Avez-vous idée de leur favori ?

– Vous êtes plus au fait des choses du siècle que moi, mon ami. »

L'évêque Frédéric opina sans un mot puis s'avança et s'inclina pour baiser la bague de son supérieur. L'archevêque Pierre n'était pas un homme intéressé par la politique, du moins pas par celle qui dépassait le territoire dont il avait la charge.

Le calme demeura un long moment après le départ de l'ecclésiastique. Herbelot, le clerc, faisait glisser parfois des documents les uns sur les autres, dans l'espoir que cela attirerait l'attention du prélat. Celui-ci s'obstinait à fixer sa main, qui avait repris le verre vide. La voix fatiguée, éraillée lorsqu'elle n'était pas employée à un sermon dans la basilique, brisa le silence feutré.

« Dis-moi, Herbelot, que penses-tu de notre évêque ? »

Le jeune clerc se tortilla sur son siège, mal à l'aise à l'idée de se montrer irrespectueux. On lui avait inculqué l'importance de l'obéissance pendant toute son enfance chez les moines, et il ne savait pas comment se comporter face aux vieux renards de la politique. En outre, il était persuadé également que, bien souvent, les questions qu'on posait aux subalternes comme lui n'étaient que rhétoriques et qu'il suffisait d'attendre. Son intuition et sa prudence furent une nouvelle fois payantes.

« Tu n'en dis rien ? Voilà bien sage opinion. J'ai forts regrets de voir un clerc si empli de science se consacrer au siècle avant tout. Que nous importe de savoir qui sera le patriarche ? Nous le saurons bien assez tôt. Je me demande d'ailleurs s'il n'avait espoir de m'inciter à avouer vers qui mon cœur penche. »

La dernière phrase s'acheva dans un souffle et le silence revint. Herbelot en profita pour tenter de glisser quelques documents, chartes et diplômes que l'archevêque devait viser avant leur mise au propre. Mais Pierre de Tyr n'avait pas envie de s'adonner à des tâches administratives et survola les feuillets d'un œil indifférent avant de se tourner vers son clerc, de nouveau.

« Tu es comme moi, Herbelot, un homme de cloître, de silence et de prière, n'est-ce pas ?

– Je le confesse bien volontiers, père.

– En fait j'ai espoir que le prochain patriarche sera ainsi. Notre bienaimé père Foucher, que Dieu l'ait en sa sainte garde, était un grand homme, apte à faire de belles et louables choses. Mais j'ai parfois regretté qu'il ait dû se plonger si fort avant en le siècle. Frédéric est ainsi, comme l'était aussi Raoul de Bethléem, qui tenta de me ravir ce siège à mes débuts.

– *Maria optimam partem elegit quae non auferetur ab ea*[^citevangile1] »

La citation fit naître un sourire enjoué, presque infantile, au sein des traits affaissés du vieil homme qui reposa enfin son verre. Puis il vint tracer d'un doigt léger un signe de croix sur le jeune clerc, le regard empli de bienveillance.

« Amen »

## Bethléem, matin du mardi 18 février 1158

« Le sire roi ne fera donc rien contre cela ? »

L'évêque Raoul de Bethléem était furieux, il secouait les manches tout en hurlant, allant et venant dans la petite pièce qui lui servait de cabinet de travail.

Face à lui, le sergent Baset, de l'administration royale, n'en menait pas large. Il avait été envoyé pour informer le chancelier de Baudoin que ce serait finalement le prieur du Saint-Sépulcre, Amaury de Nesle, qui avait la préférence pour l'élection au patriarcat de Jérusalem. Par la même occasion, il avait escorté Daimbert, le clerc principal qui supervisait toute la correspondance officielle du prélat et du royaume. Lorsqu'on le congédia d'une main agacée, il détala comme un lapin dans l'escalier de la tour, laissant retomber la lourde portière de tissu derrière lui. Il manqua de peu de glisser en dévalant les marches, soucieux de mettre le plus de distance entre lui et la méchante humeur de Raoul. Il pouvait encore entendre vociférer l'intraitable ecclésiastique depuis l'étage inférieur, dans un grondement en partie étouffé.

« Je pensais que le roi me soutiendrait, moi qui le sers fidèlement depuis des années !

– Je crains, mon sire, que le roi n'ait justement quelque crainte de vous perdre comme chancelier si vous deveniez patriarche. Nous avons déjà vécu cela par le passé. »

Raoul fit une grimace d'agacement et fronça les sourcils en direction de son clerc.

« Je sais bien, mais la situation n'est pas la même ! Le pouvoir du roi n'est plus si fragile. »

Il rumina un instant et s'abattit sur son siège, affalé comme un ivrogne. Dépité, il se passa une main sur le front.

« Ce ne peut être que Mélisende, cette vieille truie ! »

Le clerc sursauta à l'insulte, choqué et écarquilla les yeux. Raoul s'en aperçut et concéda un rictus d'excuse.

« Je ne sais ce qu'elle a à me reprocher ! Je l'ai fidèlement servie, et elle m'a déjà trahi par le passé. Quels gages lui faut-il donc ?

– On dit que le prieur Amaury avait le soutien de mère Yvetta surtout, et celui de la comtesse de Flandre.

– Elles ne sont rien…

– Peut-être pas, mais on les voit souvent à la Cour, et nul ne peut nier leur engagement auprès de l'Église.

– Le jeune Amaury de Nesle… »

L'évêque semblait abattu, comme si le monde venait de s'écrouler. Il chercha un gobelet de verre peint et le tendit à Daimbert pour qu'il le lui remplisse. Puis il avala en silence plusieurs gorgées, analysant la situation. On pouvait presque entendre les engrenages s'entrechoquer dans sa cervelle affairée, calculatrice, méthodique.

« On le dit fort peu au fait du siècle, homme de cloître et d'autel… N'y aurait-il pas là quelque manigance de l'évêque d'Acre ?

– Il aurait été plus adroit pour lui de pousser son archevêque, de façon à pouvoir prendre sa place.

– Pierre de Barcelone n'a nulle ambition, il est là justement pour cette raison, j'ai payé bien assez pour le comprendre. Par contre, le voilà fort âgé, et Frédéric d'Acre espère peut-être le voir gésir en tombel d'ici peu. »

Il avala d'une longue lampée bruyante le fond du verre, le reposa avec douceur sur un coffre marqueté puis se leva, avant d'aller à la fenêtre. En ces mois de frimas, le châssis était fermé, mais il le débloqua et huma l'air froid qui s'engouffrait dans la pièce, en provenance des monts de Judée qui se déployaient devant lui.

« Les puissants de ce monde sont bien ingrats, Daimbert. La reine a payé ma fidélité de son désaveu et le roi récompense mes talents par l'égoïsme. *Quæritis me non quia vidistis signa, sed quia manducastis ex panibus et saturati estis*[^citevangile2]. Le seul qui vaille la peine dans cette famille, c'est le jeune Amaury.

– D'ailleurs, avec la mort du vieux Foucher disparait le principal opposant à son mariage avec la jeune Courtenay. »

Raoul tourna la tête vers son clerc et lui accorda un sourire rusé.

« Ah, Daimbert, Daimbert, que deviendrais-je sans toi ? »

Il vint poser une main amicale sur l'épaule du petit homme puis reprit place dans son siège.

« Donne des ordres, je repars pour la Cité. Il me faut entretenir le comte de Jaffa de mon soutien en cette histoire de consanguinité. » ❧

## Notes

Le royaume de Jérusalem n'était pas un territoire comme les autres et les affaires ecclésiastiques étaient intimement liées à la politique séculière. Tous les postes des grands prélats avaient un pouvoir réel au point de vue administratif et militaire. Le roi de Jérusalem et la noblesse en général s'employaient donc à influencer toutes les élections, sans que cela n'engendre pour autant d'opposition de principe comme la querelle des Investitures dans les pays européens.

L'équilibre était maintenu par un savant jeu diplomatique, de manipulations de la part des ordres religieux, des clercs et des nobles.

On s'associait par communauté de point de vue, parfois de façon temporaire, sans vraiment tenir compte de son appartenance à un milieu ou l'autre. Chacun avait à cœur de ménager ses intérêts propres, ceux de son groupe d'origine, tout en pesant au mieux sur la politique du royaume (et des états latins d'Orient en général). Cela ne signifiait pas pour autant que les ecclésiastiques étaient des cyniques sans états d'âme, le comportement d'un certain nombre d'entre eux démontra au contraire qu'ils étaient au final des serviteurs assez scrupuleux de l'Église.

## Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972, p.93-182.

Prawer Joshua, *Histoire du royaume latin de Jérusalem*, Paris : CNRS éditions, 2001.
