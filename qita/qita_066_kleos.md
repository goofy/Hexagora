---
date : 2017-04-15
abstract : 1154 - 1157. Bien que ne dirigeant plus officiellement le royaume, Mélisende continue d’œuvrer pour l’avenir, désireuse de prodiguer ses conseils à son entourage. Elle tente de laisser son empreinte de façon durable et d’influencer la politique extérieure pour de nombreuses années.
characters : Mélisende de Jérusalem, Bernard Vacher, Amaury de Jérusalem, Henri le Buffle, Geoffroy Foucher, Philippe de Milly
---

# Kleos

## Naplouse, demeure de la reine mère, soirée du lundi 19 avril 1154

La porte de la chapelle grinça dans le silence qui avait suivi le *Ite missa est* final. En même temps que la lumière entra une haute silhouette puis résonnèrent les éperons choquant les dalles de pierre au rythme d’un pas décidé. Au sein de la petite assemblée, toutes les têtes se tournèrent vers la femme frêle au plus près de l’autel, dans l’attente de sa réaction. Avec une grâce maîtrisée, elle fit voleter son riche bliaud, réajusta son voile et, levant le menton, dévisagea le nouvel arrivant avec autorité. Elle arbora son sourire le plus large en découvrant le visage familier de Bernard Vacher, un des chevaliers de l’hôtel du roi.

« Que voilà belle surprise, votre visite en mon exil ! »

Avant de répondre, l’imposant chevalier s’agenouilla en un vacarme de métal.

« Votre Majesté, je vous apporte le salut de votre fils notre roi Baudoin. Je suis en route pour le nord. »

Mélisende leva une main diaphane et invita le vieux serviteur de la couronne à se relever et à l’accompagner tandis qu’elle se dirigeait vers la sortie.

« Aurez-vous temps de demeurer avec moi pour le souper ? Si ce n’est pour la veillée ?

— J’en aurais goûté l’honneur, mais il me faut joindre Sidon au plus vite, presser le rappel auprès du roi.

— Le sire Géraud n’est pourtant généralement pas le dernier pour porter le fer à nos ennemis.

— Certes pas. Seulement, devoir fuir avec les galées à Ascalon l'a fort marri. Lors de la semonce, il était occupé en ses affaires nautiques, loin de penser à lever son ban pour joindre l’ost royal en si grande urgence. »

Mélisende hocha la tête lentement en silence et continua d’avancer, réfugiée dans ses pensées. Elle plissa les yeux lorsqu’elle se retrouva dans le jardin, mais ne ralentit pas son allure. Indifférente à la troupe qu’elle entraînait dans son sillage, elle se dirigea d’autorité vers sa chambre, gravissant un petit escalier sans plus adresser la parole à Bernard Vacher dont le pas pesant l’accompagnait.

Arrivée dans ses appartements, elle montra d’un doigt autoritaire un escabeau à son invité puis s’assit sur un coffre au pied de son vaste lit. Entre-temps, sa suite s’était évaporée dans les différents espaces qu’ils avaient traversés. Comprenant que le soldat venait à peine de quitter la selle, la reine fit mander une aiguière et un bassin, qu’il puisse se rafraîchir, ainsi que des boissons et une collation.

Tandis qu’il se frottait les mains et se débarbouillait le visage, elle s’entretint quelques instants avec un de ses clercs. Elle donna également des instructions pour qu’on s’occupe au mieux des suivants du messager. Lorsqu’elle en eut fini, Bernard Vacher attendait tranquillement, sa silhouette avachie sur le petit siège, mais le regard vif à l’affût derrière ses paupières lourdes.

« Portez-vous missive à mon intention ?

— Aucunement. Le roi et le sire connétable assemblent les troupes pour assaillir Nūr ad-Dīn[^nuraldin] et libérer Damas.

— Croyez-vous qu’il y ait vraiment un risque de voir le Turc prendre la cité ?

— Plus que jamais ! Ses émirs sont là depuis le début du mois et il a de nombreux alliés dans la place. De toute façon, il a éliminé ceux qui pouvaient s’opposer à lui.

— ô combien ! Muğīr n’est que l’ombre de Mu‘īn ad-dīn Anur. Il n’est pas de taille. »

Bernard Vacher acquiesça gravement, avalant un peu de vin.

« Savez-vous quand les nôtres arriveront là-bas ?

— Le sire connétable misait sur la saint Georges^[23 avril.]. Cependant nul n’y croit vraiment. Moult éperonnent en tout sens pour clamer l’urgence, mais cela demeure trop court. Outre, trop de mauvais souvenirs demeurent des dernières campagnes en ces lieux.

— Vous savez, Bernard, que j’ai parlé contre l’attaque faite à Damas lors de la venue des rois Louis et Conrad^[Allusion à la Seconde croisade de 1148.]. Mais la situation était différente, alors. Nous avons pris Ascalon l’été dernier, Damas est notre prochaine priorité.

— Notre sire Baudoin a bien conscience de cela. Seulement, la cause est difficile. Les caisses sont vides et nul puissant baron croisé pour nous appuyer. »

La reine mère se raidit un peu, accentuant son allure empesée et hiératique. Elle avait toujours eu du respect pour le porte-bannière royal. Il était un fidèle serviteur de la couronne, peu enclin à pousser son avantage. En outre, il avait une vision assez éclairée des différentes forces en présence. Enfin, il savait rester à sa place. Elle qui avait été éduquée pour régner par un père exigeant appréciait ce genre de subordonnés. Il fallait juste de temps à autre leur rappeler qu’ils n’avaient pas toutes les clefs et les capacités pour diriger.

« J’encrois plutôt qu’il est bon que nul puissant de France, de Flandre ou de Germanie ne soit parmi nous pour en demander l’octroi. Le comte Thierry[^thierryalsace], mon cousin, est féroce batailleur, mais aussi fort gourmand de terres. Je préfère que le royaume crée quelque nouvelle baronnie féale. Nul besoin de principauté avide d’indépendance. »

Le chevalier concéda un sourire. Il comprenait le point de vue de Mélisende, mais il doutait de la force royale en ce moment. La prise de Damas ne serait pas aisée, quand bien même le seigneur d’Alep en serait repoussé sans trop de dommages.

## Naplouse, jardins de la demeure de la reine, après-midi du lundi 26 avril 1154

Les voix angéliques des ecclésiastiques s’élevaient vers le ciel sans rencontrer aucun autre obstacle que de maigres nuages cotonneux. Installée sous une pergola verdoyante autour de la fine silhouette de Mélisende, une assemblée de femmes écoutait avec attention. Peu d’entre elles comprenaient les paroles arméniennes des chants, mais la douleur qu’ils exprimaient était palpable. L’évêque Nersès[^nerses4] était un des nombreux correspondants de la reine et il avait fait porter sa dernière missive par de jeunes clercs instruits de sa création *Lamentation sur Édesse*.

Aux alentours, des jardiniers s’activaient à biner les carrés de fleurs et d’aromatiques. Dans la cour prolongeant les espaces verts, un petit groupe de valets et de lavandières battait avec vigueur des couvertures et des oreillers. Le printemps offrait les plus belles journées, avant les chaleurs sèches de l’été et après les rigueurs de l’hiver. La plupart des serviteurs appréciaient donc de se voir attribuer des tâches à l’extérieur.

Parmi la domesticité de la reine, Gui n’était pas de ceux-là. Le secrétaire n’aimait rien tant que le confort des bâtiments, peu enclin à s’exposer aux éléments, quels qu’ils soient. Son teint blafard attestait d’ailleurs bien de cette affinité. Il avait malgré tout pris sur lui de sortir, car il avait une nouvelle urgente à annoncer. Urgente et mauvaise, de celles qu’il ne prenait jamais plaisir à délivrer. Il savait suffisamment bien ses classiques pour redouter l’accueil qui lui serait fait.

Le sourire qu’il arborait donc en s’inclinant devant la reine n’était que de circonstance et suffisamment emprunté pour que Mélisende en fronçât immédiatement ses fins sourcils. Elle lui fit signe de s’approcher pour qu’il lui parle à l’oreille.

« La cité de Damas a été forcée par Norreddin, majesté. Un espie en route pour Jérusalem vient de faire halte pour changer de monture en nos écuries. »

Écarquillant les yeux de mécontentement, Mélisende fit faire silence d’une voix autoritaire. Puis elle darda son regard impérieux sur le vieil homme à la posture contrariée face à elle.

« Que me dis-tu là ? L’ost royal n’a-t-il pas mis bon ordre à cela ?

— Il faut croire qu’il n’en a pas eu l’heurt. C’est un sergent le roi qui portait la nouvelle. »

Les yeux de la reine lançaient des éclairs et, rapidement, les rangs s’éclaircirent autour d’elle. Personne n’avait le désir de rester dans les parages quand elle voyait ses desseins contrariés. Hésitant à se dérober, Gui dansait d’un pied sur l’autre, tordant ses mains fines devant sa panse rebondie.

« Baudoin est-il de retour ? Où se trouve-t-il en ce moment ?

— Il est demeuré avec l’ost, mais j’encrois qu’il ne demeurera pas dans le nord. Le Turc va de certes passer un moment à fortifier sa prise.

— A-t-il porté grand mal à l’enceinte ? Aux troupes ? Baudoin n’a pas désir d’assaillir à son tour ? Fais donc venir ce messager que je l’interroge.

— Il a juste changé de monture et s’en est allé. »

La reine serra les poings de se voir ainsi mise à l’écart. Il était un temps, encore pas si lointain, où les coursiers lui faisaient rapport, où les informations convergeaient vers elle. Elle se contint et respira lentement, attrapant un second voile pour s’en couvrir afin de déambuler jusqu’à la galerie. D’un regard elle fit comprendre à son secrétaire de l’accompagner.

« Nous partons. Je veux accueillir mon fils, le roi, quand il arrivera.

— Croyez-vous qu'il fera halte en nos murs ?

— J'en doute fort, c'est à nous d'aller l'encontrer.

— Nous n’avons guère d’hommes, ma reine, ils sont tous avec l’ost royal.

— Nous voyagerons légers. Mande le minimum de mes gens et peu d’affaires, je ne pense pas demeurer long temps à Jérusalem. »

Voyant la surprise sur le visage de son fidèle serviteur, Mélisende esquissa un sourire sans joie.

« Je n’ai plus loisir de diriger le royaume comme reine. Mais Baudoin demeure mon fils et il aura besoin des conseils de sa mère. Hâte-toi donc ! »

Abandonnant son secrétaire à l’entrée de ses appartements, la reine se dirigea vers la fenêtre à claustra qu’elle appréciait tant, ouvrant vers la cité de Naplouse et la vallée occidentale. Sans la voir, elle savait la mer proche. Si Damas était désormais aux mains d’un pouvoir fort, il fallait oublier l’espoir de s’en emparer. Elle repensa à la grande expédition lancée quelques années plus tôt et eut quelques regrets de s’y être opposée, inquiète qu’elle était du destin du royaume à l’issue d’une telle prise en présence de nombreux croisés. Mais elle ne s’attarda pas à ces mornes réflexions. Elle n’était pas du genre à se lamenter, comme Nersès, pas même de la perte de son comté natal. Femme d’action, elle échafaudait déjà de nouveaux plans de conquête.

## Naplouse, demeure de la reine, soirée du jeudi 17 juin 1154

La haute table n’accueillait que deux convives en dehors de la reine : Amaury, le comte de Jaffa, fils cadet de Mélisende et Geoffroy Foucher, un Templier à la barbe rase. Son visage anguleux et quasiment décharné contrastait avec les formes plus rebondies du jeune prince. Ils s’entendaient néanmoins très bien, amis qu’ils étaient depuis de nombreuses années. Geoffroy était un familier de Mélisende depuis son arrivée en Terre sainte, recommandé par son lointain cousin Bernard, abbé de Clairvaux[^saintbernard].

Amaury se sentait d’une humeur badine et il n’avait cessé d’argumenter auprès de sa mère pour qu’elle se décide à confier la copie de volumes techniques à son scriptorium. Elle avait en effet attiré à elle de nombreux calligraphes et miniaturistes de talent, sans compter les relieurs et graveurs, qui produisaient des manuscrits de qualité. Amaury appréciait également les livres, plus pour leur contenu que pour leur aspect, et trouvait dommage de se contenter de textes sacrés alors qu’il existait si peu de d’exemplaires d’ouvrages essentiels selon lui. Mélisende supportait avec affabilité ses assauts, sans jamais manifester le moindre agacement, bien qu’il se montrât bien plus insistant qu’elle ne l’aurait accepté de quiconque. Venant de ce fils, rien ne semblait jamais la contrarier. Elle finit malgré tout par lui répondre de façon sarcastique.

« Hé bien, que ne lances-tu pas un assaut depuis Ascalon vers Alexandrie ? Il s’y trouve peut-être quelques beaux vestiges de la bibliothèque des Romains.

— Si fait, mère. J’en ai l’idée, un jour. Toutefois, le désert fait robuste muraille, je finirai par m’échouer dans les sables.

— Alors demande à nos bien-aimés frères de la milice. Ils savent vaincre au parmi des dunes, n’est-ce pas, Geoffroy ?

— Majesté,vous montrez trop de mansuétude. Ce n’était qu’escarmouche contre des fugitifs. Elle aura belle répercussion, mais c’était loin d’être l’armée califale^[Voir *Tortueuse âme*, seconde partie].

— Gageons que votre cousin André saura faire fructifier tout cela. Il ne faudrait pas faire deux fois la même erreur. »

Geoffroy reposa sa cuiller et prit le temps de réfléchir à la dernière remarque de la reine. Elle parlait souvent ainsi, par allusion, sans jamais préciser le fond de sa pensée. Il avait pour elle beaucoup d’amitié, mais ne se sentait jamais totalement à l’aise devant cette femme de pouvoir. Il préférait l’environnement rude des guerriers du Temple auquel il était habitué. Il savait qu’on reprochait grandement à son ordre la façon dont l’assaut sur Ascalon s’était déroulé. Il se murmurait même qu’ils avaient tenté d’en accaparer la prise, ce qui avait mené à la capture et l’exécution de plusieurs dizaines d’entre eux par les forces musulmanes, dont le grand maître lui-même, Bernard de Tramelay. Heureusement, André de Montbard, un des fondateurs, était pressenti pour lui succéder et nul ne doutait de sa droiture et de sa vaillance. Jamais la reine n’aurait commis l’affront d’évoquer un tel souvenir devant un homme familier d’André, qu’elle appréciait entre tous. Elle avait donc une autre intention en disant cela.

Pendant ce temps, Amaury avait déjà enchaîné et parlait des richesses documentaires qu’il espérait voir advenir d’une conquête de l’Égypte. En tant que seigneur de Jaffa et d’Ascalon, il était le premier baron concerné. Il envisageait de s’appuyer sur les Bédouins de ses régions pour faciliter le passage vers le delta du Nil.

« Il sera bien utile de ne point se les aliéner, sire comte, j’en conviens, mais pour moissonner le blé d’Égypte, cela ne saurait suffire.

— Les miliciens du Temple ont-ils quelque secret stratagème pour conquérir l’endroit ? se moqua amicalement Amaury.

— Pas à ma connaissance. Je parlais de mon propre chef. Si je devais me lancer à l’assaut de ces terres, je me garantirais le contrôle des mers avant tout. Géraud de Sidon, malgré sa vaillance, n’a pas eu le front de s’opposer aux galées du calife. Elles vont et viennent comme des frelons sur nos côtes et nous n’avons que nos murailles et nos chaînes à leur opposer. »

Mélisende sourit au bon sens de la remarque. Elle se tourna vers son invité, intercepta son regard.

« Nous manquons de bois pour avoir belle flotte, c’en est misère. Comment pourrions-nous avoir suffisamment de naves pour les vaincre en mer ?

— Je ne sais, à dire le vrai. Mais à Ascalon, nous avons failli être défaits par leur forçage du blocus. Imaginez ce que ce serait si nous étions de l’autre côté du désert, en leurs provinces, avec nos maigres vaisseaux à leur merci. »

La reine dévisagea alors son jeune fils, satisfaite de voir qu’il écoutait avec attention et semblait réfléchir à cela. L’Égypte était en pleine déliquescence et risquait de sombrer à son tour dans le giron d’un pouvoir prêt à s’en emparer. Cette fois, il s’agissait d’une région encore plus cruciale que Damas. Ce n’était pas juste quelques plaines maraîchères et céréalières qui étaient en jeu, ni même la frontière naturelle du désert, mais une zone extrêmement riche, nœud commercial et porte entre la Méditerranée et les mers d’Orient. Et, surtout, bien qu’elle soit gouvernée par des musulmans, une large frange de sa population était chrétienne.

Geoffroy avait indiqué que lors de l’affrontement aux abords de l’Égypte, Usamah ibn Munqidh avait réussi à passer à travers les mailles du filet. Connaissant le vieux renard, il avait sûrement trouvé refuge chez ses anciens amis à Damas. Soucieux de toujours flatter les puissants, il saurait vanter au nouveau maître de la ville la richesse des territoires à conquérir à l’Ouest.

## Naplouse, demeure de la reine, veillée du lundi 16 avril 1156

Philippe de Naplouse étira sa longue carcasse tout en bâillant. Il était fatigué des derniers jours passés en selle et, si ce n’était pour égayer un peu la soirée de la reine, il aurait préféré aller se coucher au plus vite. Il avait néanmoins renoncé à cette idée en venant saluer celle qu’il servait depuis tant d’années. Il avait recruté des montreurs de panthères sur Saint-Jean d’Acre et voulait lui en faire la surprise. Il pensait que c’était là belle attraction pour fêter la fin des Pâques.

Il avait été désarçonné par l’aspect chétif de la femme qui l’avait reçu. Elle avait toujours été mince, de petite taille, mais ces derniers mois elle semblait s’être étiolé brutalement, comme si toute la fougue, l’énergie qui l’animait depuis si longtemps s’était tarie. Philippe et son frère Henri n’avaient pas eu le cœur de refuser l’invitation au banquet du soir et à la veillée, dont les montreurs d’animaux constituaient le point d’orgue.

Philippe savait que Mélisende n’avait renoncé que de très mauvaise grâce au pouvoir, pourtant elle respectait scrupuleusement ses engagements. Il constatait avec dépit que sa vie était intimement liée au royaume et elle ne s’animait vraiment que lorsqu’on abordait un sujet politique. Même les arts ou la religion n’évoquaient plus autant de joie en elle. Ses joues se creusaient avec les années et ses yeux semblaient chaque mois plus profondément enchâssés.

Après un excellent repas, les quelques participants s’étaient installés dans le jardin, sous les palmiers et parmi les fortes senteurs des renoncules multicolores. Tandis que les panthères obéissaient en feulant à leur dresseur, des fruits secs et un fromage frais avaient été servis. Mélisende avait invité Philippe à côté d’elle, à la place d’honneur, et Henri se trouvait sur un escabeau un peu devant eux, en retrait.

« Cher Philippe, vous avez toujours su apporter de la gaieté en mon hostel. Je n’aurais pas cru finir les Pâques avec si charmant spectacle. »

Elle se tourna vers lui, posant sa main en un geste délicat, frêle oiseau, sur l’avant-bras noueux du guerrier.

« Votre absence m’a été cruelle, ces derniers temps. J’ai si peu d’amis…

— Tous les preux de ce royaume donneraient leur vie sur un simple mot de vous, ma reine.

— Vous ne m’aurez pas avec nos paroles flatteuses, mon ami, rit-elle. »

Elle baissa la voix et inclina la tête vers lui, en un geste empli de grâce.

« Autant que de votre présence, ce sont de vos conseils que j’ai besoin. N’avez-vous pas quelques liens avec les Pisans ? »

Philippe de Naplouse retint à peine une exclamation de surprise. Si elle lui était apparue fatiguée, son esprit demeurait alerte. Il faudrait qu’elle soit allongée en son sépulcre pour qu’elle cesse définitivement de se mêler de gouvernement. Philippe de Naplouse indiqua Henri du menton.

« C’est surtout le Buffle qui les connaît. Il garde à l’œil toutes les cités commerçantes qui ont quartier en nos villes. Auriez-vous quelque inquiétude à leur propos ? »

Il tapa sur l’épaule de son frère afin de l’inciter à rejoindre la conversation, lui demandant s’il pouvait répondre aux interrogations de la reine sur les Pisans.

« Ils sont plus actifs dans la principauté d’Antioche et chez les Griffons. Mais, surtout, ils viennent d’obtenir du vizir fātimide la possibilité d’ouvrir leur propre fondaco à Alexandrie. Ils sont depuis toujours moins proches du royaume que les Génois.

— Je trouve justement que ces derniers deviennent trop gourmands. Ils oublient qui est le maître ici. J’ai tendance à croire que les hommes de Pise se montreraient plus raisonnables.

— Le souci, ma reine, est qu’ils font négoce avec l’ennemi. Récemment, plusieurs d’entre eux ont vu leur cargaison saisie et leurs commissionnaires saisis par les hommes du roi.

— Voilà bonne rançon à leur proposer justement.

— En échange de quoi, ma reine ? » s’inquiéta Philippe.

Mélisende n’aimait guère dévoiler ses projets avant de les mettre à exécution, mais désormais à l’écart du pouvoir, elle ne pouvait exercer sa volonté que par les influences qu’elle tissait. Elle consentit donc de bonne grâce à éclairer ses interlocuteurs sur son idée.

« Il va nous falloir des vaisseaux pour assaillir l’Égypte, avant que Nūr ad-Dīn n’y étende son influence. Les cités marchandes en ont de pleins ports. Pas plus que nous les Pisans n’apprécient les Normands établis à Palerme, voilà un terrain à ensemencer pour l’avenir. Mais s’ils font leurs affaires avec le calife, jamais ils ne voudront perdre un avantage commercial.

— Au contraire, ma reine, objecta Henri. Le fait qu’ils soient déjà sur place nous offre la meilleure des opportunités. Qui peut résister au fruit complet lorsqu’il en a goûté le suc ? »

## Jérusalem, palais royal, chambre d’Amaury, soirée du lundi 11 février 1157

Quelques braseros apportaient un peu de chaleur dans la vaste pièce largement éclairée de multiples lampes disséminées sur tous les meubles. Installés sur des sièges bas couverts de coussins, le jeune comte de Jaffa et sa mère étaient emmitouflés chacun sous une chaude courtepointe. Un clerc leur faisait la lecture, détaillant les aventures d’un certain Digénis Akritas, accompagné d'un musicien. Après en avoir acheté une copie pour en faire une traduction, Amaury avait été déçu d’apprendre qu’il s’agissait d’un récit littéraire. Il s’était donc résolu à en faire une attraction de veillée.

Le lendemain, mercredi des Cendres, commencerait la longue période de recueillement du carême et, avec une météo plutôt grisâtre, il était bon d’égayer les soirées par des prouesses héroïques. Tout à sa joie de recevoir sa mère, venue assister aux cérémonies données par le vieux Foucher d’Angoulême, avec qui elle était très liée, il sentait bien qu’elle n’avait guère la tête aux réjouissances. Lorsque le conteur marqua une pause, profitant de ce que son accompagnateur retendait les cordes de son psaltérion pour prendre quelques gorgées de vin, Amaury se pencha vers Mélisende.

« Je vous encontre fort soucieuse, mère. Y a-t-il quelque fardeau dont je pourrais alléger vos épaules ?

— Nul poids en ce qui me concerne, mon garçon. J’ai juste longuement réfléchi à propos de la charte que ton frère a concédée récemment aux Pisans.

— Sur vos conseils, j’ai cru comprendre. Ces négociants ont-ils valeur cellée à mes yeux que vous vous fassiez ainsi leur avantparlier ?

— Amaury, mon garçon, Damas nous a échappé pour de nombreuses années. Si nous ne voulons pas que les Turcs, ou pire, les Siciliens, soient les maîtres au Caire, il nous faut des navires pour y soutenir notre campagne.

— Les Génois sont traditionnels alliés du royaume. Ils nous ont aidés fidèlement à prendre les ports…

— Ils se montrent aussi fort rétifs à nos réquisitions. Ils doivent comprendre que nous pouvons nouer belle entente avec d’autres qu’eux pour la maîtrise des mers.

— Qu’avez-vous donc en tête ? Leur promettre une partie de nos conquêtes ?

— Non Amaury, je veux que tu leur cèdes une partie de tes terres, histoire d’en faire tes liges lorsque sera venu le temps de s’emparer de l’Égypte. Il en est ainsi comme des oliviers, qui ne portent fruit qu’après plusieurs années. » ❧

## Notes

Après s’être opposée de façon très violente à son fils Baudoin, Mélisende s’est assez brutalement retirée de la vie publique, ayant perdu le soutien de la majorité de la Haute Cour. Peu de ses proches ont trouvé grâce auprès du nouveau maître de Jérusalem et on n’arrive plus guère à suivre les activités politiques de la reine mère jusqu’à sa mort. Une des rares mentions de son influence se trouve dans une charte signée par Baudoin III en faveur des Pisans (Regesta Regni Hierosolymitani no 322). Le rôle de Mélisende y est signalé explicitement. Comme Amaury concède à son tour pas mal d’avantages à la cité italienne peu de mois après cela, je me suis plu à imaginer que Mélisende aidait à préparer l’avenir du royaume.

Native de Terre sainte et éduquée par Baudoin II du Bourg comme son héritière, elle a toujours fait preuve d’une audace féroce et d’un sens tactique consommé dans les jeux de pouvoir. Je lui prête donc une vision stratégique assez ample, suffisamment éclairée pour influencer durablement les orientations décidées par Amaury lorsqu’il sera roi.

En effet, au cours de la décennie suivante, l’Égypte fut au cœur des affrontements. Pas seulement entre les forces de Nūr ad-Dīn et les troupes latines, mais aussi au sein même des puissances et entre alliés. Par exemple, les ordres religieux militaires y frôlèrent la banqueroute et leur participation fut l’objet d’âpres négociations. Les opérations armées se doublaient de complexes jeux diplomatiques, d’influences interne et externe au califat décadent. Au final, Saladin y prit le pouvoir et y appuya sa conquête du territoire syrien, récupérant à son compte l’héritage de Nūr ad-Dīn. Une fois cela fait, il ne lui fallut guère de temps pour balayer le pouvoir latin.

PS: Le titre ce Qit'a est issu de mes lectures de Jean-Pierre Vernant sur la Grèce antique. Le terme désigne la gloire, la renommée des héros, célébrée par les poètes.

## Références

Mayer Hans Eberhard, « Studies in the History of Queen Melisende of Jerusalem », dans *Dumbarton Oaks Papers*, vol. 26, 1972.

Heywood William, *A History of Pisa. Eleventh and Twelfth Centuries*, Cambridge University Press, Cambridge : 1921.

Reinhold Röhricht, *Regesta Regni Hierosolymitani* (MXCVII-MCCXCI), Oeniponti : Libraria Academica Wagneriana, 1893.
