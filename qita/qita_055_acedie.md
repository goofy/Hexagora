---
date : 2016-05-15
abstract : 1140-1157. Au fil des années, les projets du chanoine Roger se réduisent comme peau de chagrin. En même temps que les territoires latins se voient stoppés dans leur progression, voire régressent, il sent que la glorieuse destinée à laquelle il aspirait lui échappe peu à peu.
characters: Frère Roger, Frère Paul, Bernard Vacher, Yalim al-Buzzjani, Sawwar
---

# Acédie

## Baniyas, après-midi du mercredi 6 novembre 1140

Un vent léger caressait les contreforts du Golan, faisant frissonner les pins et les oliviers accrochés à la pente. Hauts dans le ciel, des nuages lancés depuis les côtes lointaines couraient dans une mer d'azur. Sur les restanques environnant la cité, citronniers et orangers piquetaient de points colorés la verdure envahissante. Le chanoine avait d'ailleurs noté qu'à plusieurs endroits, on récoltait les fruits, mûris à point. Privilège du lieu, il pourrait savourer des jus pressés dont il était si friand.

Profitant de l'arrêt de la caravane à l'entrée des murailles, il descendit de sa mule et fit craquer son dos. Il n'avait pas la trentaine, mais un léger embonpoint et des années d'inactivité physique l'avaient rendu fragile. Il espérait que le confort des installations serait à la hauteur, ne doutant pas de la présence de bains, étant donné la richesse en eau de l'endroit. On y cultivait d'ailleurs aussi un riz réputé. Sur le vieux rempart, il aperçut des silhouettes déambulant, l'air nonchalant. La bannière de Rénier Brus flottait désormais sur les tours de l'enceinte, ainsi que celle du roi Foulques de Jérusalem sur le donjon. Nul clocher ne s'élançait vers les cieux, seulement un minaret plus élevé que les autres, au sommet duquel on avait hâtivement dressé une croix.

Roger sourit pour lui-même. Il était de ceux qui allaient rendre la région à la vraie Foi. Ce n'était pour l'heure qu'un avant-poste sur la route de Damas, modeste, mais avoir été érigé en évêché indiquait bien le désir d'en étendre le territoire. Et vers où si ce n'était vers les cités infidèles, au Levant ? Arrivé comme novice d'un chapelain accompagnant un riche croisé, Roger était demeuré au Saint-Sépulcre et y avait prononcé ses voeux. Il avait depuis vu le royaume se renforcer et se développer au fil des ans, malgré les revers et les difficultés.

Ayant choisi de marcher à côté de la file des animaux de bât, pour soulager son postérieur et son dos, il déambulait un sourire bonhomme sur le visage. La plupart des habitants qu'il rencontrait étaient des indigènes, qui avançaient le regard bas, la mine triste. De rares Latins se croisaient, généralement en armes ou occupés à emménager dans les meilleurs bâtiments. Quelques-uns le saluèrent de loin, visiblement satisfaits de voir s'installer des ecclésiastiques.

L'évêque Adam, ancien archidiacre d'Acre n'arriverait pas avant plusieurs jours. La demi-douzaine de chanoines nommés pour l'assister allait s'activer à en préparer la venue. Tout était à faire, ce qui réjouissait la face rubiconde de Roger. Il servirait comme trésorier, portant dans ses sacs et bagages les instruments indispensables aux célébrations selon la liturgie romaine. Il n'avait pris que l'essentiel, fruit des dons d'autres établissements, agrémentés des offrandes de fidèles argentés. Il comptait dès à présent démanteler le lieu de culte mahométan pour en refondre la majeure partie des décors. En outre, il avait appris que Rénier avait interdit qu'on en pille les riches tapisseries, et n'en avait distrait que de rares exemplaires pour ses besoins propres. Il y avait donc un beau butin qu'il pourrait revendre, n'ayant que peu usage de tels artefacts pour l'ornementation du sanctuaire.

Les rues qu'il parcourait lui semblaient en assez bon état, malgré quelques déprédations récentes. De nombreuses boutiques et ateliers s'entrevoyaient dans des cours plus ou moins closes, à la mode des autochtones. La file d'animaux s'arrêta finalement dans l'une d'entre elles, adjacente à l'ancienne mosquée. Les huisseries en partie forcées et l'abandon du lieu le frappèrent. Malgré l'animation des bêtes, un vent de désolation soufflait dans l'endroit. À l'étage, une galerie couverte permettait d'en faire le tour à l'abri et il y aperçut un autre clerc, le visage fermé, assistant à leur arrivée. Il disparut pour surgir au rez-de-chaussée, silhouette mince dans un habit de laine fine d'excellente qualité. Sa barbe taillée et sa chevelure strictement tonsurée encadraient une face longue, au nez pointu. Un sourire chevalin illumina soudain l'austère apparition. Il donna une accolade empressée à Roger avant de l'accueillir plus solennellement d'un baiser de paix.

« Grande joie de vous accueillir en notre clôture, frère. J'ai nom Paul d'Hébron et serait céans le cellérier.

– Je suis Roger de Jérusalem, trésorier. J'apporte d'ailleurs de nombreux bagages à mettre au secret au plus vite.

– Quelle joie de vous recevoir ! Nous pourrons enfin célébrer les heures dignement… »

Il donna quelques ordres d'une voix autoritaire de basse, forgée par des heures de chants, avant d'inviter d'un geste Roger à le suivre.

« Nous ne sommes encore que trois arrivés, frère Jorge s'emploie à faire nettoyer ce qui sera le choeur. Nous avons aussi une petite salle fort correcte donnant sur une autre cour, avec une fontaine, qui fera une honnête salle du chapitre.

– Avez-vous des nouvelles de notre sire Adam ?

– Il ne saurait tarder, nous avons reçu parties de son bagage dès avant la Toussaint. Le tout l'attend désormais en son palais. Valets en suffisance le serviront et font de leur mieux pour rendre l'endroit acceptable. »

Tout en devisant, ils traversaient des salles et des couloirs, parfois en piteux état, hébergeant souvent les restes d'anciens commerces chassés lors de la prise de la ville. Finalement, Paul s'arrêta devant une échoppe contenant plusieurs recoins, où du mobilier avait été récemment installé. Une porte neuve s'agrémentait d'une impressionnante serrure de métal.

« Dans l'attente, je me suis dit qu'il serait plus judicieux de vous installer ici, vous aurez de place assez pour y resserrer les objets précieux sous votre garde. Nous aviserons plus tard avec le chapitre et notre sire Adam quant à l'avenir.

– Quand nous aurons été dûment reçus en notre chapitre nouvellement créé, et qu'un doyen nous guidera. » approuva Roger, tout en posant sa besace près de l'entrée.

Il devait s'agir d'une ancienne boutique, dont l'arrière-salle aveugle, voûtée d'arêtes sentait la poussière récemment balayée. Un lit de bonne taille flanqué d'une table basse, un coffre massif aux pentures ouvragées, deux escabeaux et un crucifix au mur meublaient la première pièce.

« Il y a pleintée de lampes en la salle commune où nous prenons nos repas. Je vous y conduirai une fois que vous aurez posé vos paquets et pris un peu de repos. Nous n'avons pas encore de cloche, un valet vous préviendra des offices et repas. Il faudra voir lesquels dépendront de vous, d'ailleurs. »

Souriant une dernière fois à son commensal, frère Paul disparut dans un froufroutement d'étoffe. Roger soupira longuement : une chambre particulière, fermée à clef, des valets en nombre. Son ascension tant espérée commençait-elle enfin ?

## Baniyas, soir du vendredi 28 décembre 1151

Protégé d'une épaisse couverture aux motifs bariolés, Frère Roger frottait ses mains devant le brasero dans la salle du chapitre. Du moins ce qui en tenait lieu depuis des années. Comme ils n'étaient qu'une demi-douzaine à peine, ils s'en servaient aussi comme chauffoir, voire office de travail quand les frimas étaient trop intenses. Les années avaient tavelé le visage du chanoine, la bonne chère et les abus de boisson lui avaient offert une panse honnête et un teint rougeaud que le froid avivait. Les fils blancs gagnaient sa tonsure, et ses dents, fort mauvaises, n'étaient plus en si grand nombre que par le passé. Sa diction en était devenue mouillée, quoique sa voix résonna aussi fort que lorsqu'il était novice.

Avec le temps, il accordait plus de soin à sa parure, la coule originelle de toile simple était plus souvent que nécessaire remplacée par des tenues aux couleurs chamarrées, de bonne coupe. Il arborait également des bagues de qualité et un épais crucifix ciselé suspendu à une lourde chaîne ornait son cou. Malgré cette apparence dispendieuse, il conservait une bonhomie naturelle, entachée de mélancolie. Il était au final aussi peu exigeant avec les autres qu'il l'était envers lui-même, ce qui en rendait le commerce aisé. Un peu de retard sur le versement de la dîme ou du cens n'était jamais dramatique selon lui et toute facilité de paiement pouvait être obtenue, pourvu qu'on en fasse la demande avec respect et humilité.

Emmitouflé comme un nourrisson, il attendait l'arrivée de leurs visiteurs. Bernard Vacher, un des barons du roi, accompagnait des émissaires de l'atabeg de Damas. Ces derniers avaient intercepté un fort parti de pilleurs turkmènes qui avaient dévasté voilà peu les territoires latins voisins. Scrupuleuses de la trêve signée, les autorités de Damas retournaient le fruit de cette razzia au souverain de Jérusalem, en signe de bonne volonté. Malgré sa proximité avec les principautés musulmanes, Roger ne s'intéressait guère à la politique extérieure et se contentait de regretter poussivement que l'expansion du royaume se soit arrêtée.

Il connaissait bien Bernard Vacher, qu'il avait pu rencontrer à de nombreuses reprises. C'était un grand hâbleur, mais il savait faire montre de dévotion, et marquait toujours une déférence polie envers les serviteurs de Dieu. On le disait fort ami des mahométans, et d'aucuns le prétendaient même corrompu. Roger se contentait d'apprécier l'homme et lui faisait bon accueil  quand il le croisait. Cette fois-ci était particulière : il y avait quelques objets de culte dans le butin repris et le chevalier avait donc proposé que ce soit le chapitre de Baniyas qui en dispose, à charge pour lui d'en assurer le retour en leur emplacement originel.

En l'absence du seigneur de la cité, Onfroy de Toron, Bernard Vacher avait souhaité s'entretenir avec l'évêque, qui lui avait offert de l'héberger pour la nuit. Bien que son palais n'était que de bric et de broc, assemblage disparate de bâtiments anciens, il était bien plus confortable que l'austère citadelle. La table y était bien plus attrayante que la ration des soldats, même améliorée pour un chevalier de la maison du roi.

La porte s'ouvrit dans un tourbillon de gouttes, entraînant avec elle la fraîcheur nocturne. Trois ombres pénétrèrent, tapant des pieds et ronflant après le froid. Tout en se levant pour les accueillir, Roger reconnut la lourde silhouette de Bernard, mais s'arrêta en apercevant les tenues turques chez les deux autres hommes. Le chevalier perçut la gêne et s'avança, un large sourire forçant les traits frigorifiés de son visage.

« Mon père, je suis aise de vous revoir. Je fais escorte à Yalim al-Buzzjani, émissaire de l'atabeg de Damas Mujīr ad-Dīn ʿAbd al-Dawla auprès de notre souverain Baudoin. Il voyage avec un de ses ghilmen, Sawwar. »

Les deux musulmans saluèrent poliment sans ostentation, visiblement mal à l'aise de devoir fréquenter autant de représentants de la religion ennemie. Ils arboraient le sharbush[^sharbush] et la veste croisée des professionnels de la guerre. Sans façon, Bernard les invita à s'asseoir près de lui sur le banc face à Roger.

« Nous avons là une liste des objets de culte, ou possiblement, rapportés par nos amis. Ils sont scellés dans les trois coffres dont je vous ai remis les clefs. »

Bernard tendit deux tablettes de cire au chanoine.

« Nous ferons enquête pour les restituer, soyez-en assurés. »

Il se tourna vers les deux musulmans puis vers Bernard.

« Pourriez-vous les remercier de ce retour d'objets sacrés à nos yeux ?

– Faîtes-le vous-même, s'en amusa le chevalier tandis que Yalim déclarait, avec une intonation rauque :

– J'ai usage de votre langue, je suis souvent en palabres avec les vôtres.

– Ah ? Bien, bien, bien. C'est heureux ! En ce cas, recevez mes remerciements les plus sincères pour votre maître. »

Le turc inclina la tête poliment tandis que Roger dévisageait, un peu angoissé, Bernard. Jusqu'à ce que ce dernier écarquilla les yeux, en invite à exprimer sa pensée.

« C'est à dire que je voulais vous proposer quelque boisson, mais je n'ai nulle habitude de faire héberge à... enfin, à d'autres que nos coreligionnaires. On dit qu'ils ont le vin en horreur et j'en avais chauffé avec des épices, efficace remède contre les frimas. »

Le Turc haussa les épaules.

« Nulle gêne pour nous, nous ne serions pas contre tout ce qui peut réchauffer, après cette journée à se glacer en selle. »

Le chanoine se leva donc pour aller quérir un valet. Lorsqu'il revint, les trois hommes discutaient bruyamment en arabe, sans qu'il ne lui soit possible de comprendre. D'autant que les voix s'éteignirent quand il souleva la portière.

Ils ne mirent pas longtemps à faire le point sur les objets récupérés, Roger ayant établi sa propre liste descriptive. Cela faisait au final un bel ensemble et le geste du seigneur de Damas lui était complètement indéchiffrable. Néanmoins, il avait appris à ne pas trop s'interroger, incapable qu'il était de suivre les méandres subtils des relations entre les différentes principautés de la région. Ne disait-on pas le royaume au bord de l'explosion, la reine mère et son fils s'arrachant des lambeaux de pouvoir l'un à l'autre tandis que les ennemis de la Foi reprenaient l'initiative ?

Al-Buzzjani répondait poliment aux rares questions qui lui étaient posées, mais sans chercher à entretenir la conversation. Malgré tout, il se dérida un peu au fil du repas, fort goûteux au demeurant. Roger se félicita in petto que ce soit un jour de poisson, certain de ne pas offenser ses invités. Il fut malgré tout surpris de voir qu'ils ne regimbaient pas devant le vin, tout en faisant excellent accueil à tous les plats. Le ghūlam, impassible, avalait chaque met avec une régularité de métronome, recueillant la moindre miette déposée dans son assiette comme s'il n'était pas assuré de manger à sa faim à l'avenir.

Gourmand en diable, Roger savait se montrer disert quand on abordait la question de la nourriture et il détailla par le menu chacune des recettes. Satisfait de voir la discussion s'établir sur d'inoffensifs sujets, Bernard Vacher se contentait de hocher la tête avec enthousiasme. On fit même venir le chef de cuisine, un syrien nommé Basil qui remercia avec gêne, un peu décontenancé devant l'assemblée disparate qui louait ses talents. Il ne put s'éclipser sans dévoiler le secret de sa façon pour l'excellent poisson servi.

« C'est recette que je tiens de ma mère. Elle l'appelait le samak summâqiyya. On le prépare avec des baies de sumac, séchées et pilées. On fait tout d'abord frire les poissons, grossièrement taillés en dés dans de l'huile bien chaude, ornée de feuilles de coriandre moulues, à son goût. Puis on met de côté les pièces au chaud, allongeant l'huile pour y jeter pêle-mêle baies de coriandre et de sumac broyées, ail pilé, poivre noir fraîchement moulu. Ils accompagnent de l'oignon à poignée qui doit dorer doucement, tandis qu'on y ajoute de généreuses portions de tahîna[^tahina], et quelques giclées de citron pour délayer et rehausser par l'acide ces saveurs. Pour finir, on dispose le tout dans un plat, arrosant de noisettes grossièrement broyées et saisies à feu vif. »

Il quitta les convives sous les félicitations enthousiastes et des remerciements chaleureux, y compris d'al-Buzzjani.

Le voyage pour Jérusalem étant prévu le lendemain, les deux émissaires préférèrent se retirer assez tôt, menés par un valet jusqu'à leurs quartiers. Roger se retrouva autour de quelques oublies avec son invité, poussant les pions d'un jeu de jacquet. Alors qu'ils préparaient le plateau pour une nouvelle partie, Bernard sentit que le chanoine mourait de curiosité. Il avala un peu de vin lourdement épicé, rehaussé de zestes, avant de s'enquérir de la question qui taraudait le prélat.

« Je ne suis comme vous versé en toutes ces matières diplomatiques, mais ne convenez-vous pas que nous marchons sur la tête ? J'en viens parfois à me demander si les trompettes de saint Jean ne sont pas pour bientôt !

– Seriez-vous chagriné de ce retour ?

– Certes pas ! Mais lorsque le lion se fait agneau, je ne sais que penser. Voilà quelques mois, l'ost assaillait Damas, et celui-ci nous fait désormais assaut à son tour, mais d'amabilités.

– Nous avons conclu un traité, fort avantageux pour nous, et l'atabeg veut marquer sa bonne volonté.

– Ne cherche-t-il pas à nous endormir ? Nous soudoyer ? Nos devanciers prenaient les cités par force et rien ne résistait à leur Foi. Je me demande si nous avons hérité leur vaillance, à commercer ainsi avec ceux que nous devrions soumettre…

– Je n'ai guère de réponse à vous apporter mon père. Mais je sais avec certeté qu'il est de plus dangereux adversaires au royaume, en leur cité du Nord. Et que tous ceux qui s'opposeront à eux peuvent être utiles à nos desseins. »

Le chevalier déchira un morceau d'oublie et le grignota lentement, le regard dans le vague, avant d'ajouter, tandis qu'il prenait le gobelet pour y regrouper les dés :

« Outre cela… »

Il se mordit la lèvre, sa voix mourut, puis il sourit et tendit le godet au clerc.

« À vous d'engager, mon ami »

## Baniyas, cloître canonial, midi du vendredi 13 janvier 1156

Les cuillers d'argent heurtaient les assiettes en céramique au rythme des bouchées des chanoines, résonnant dans la haute salle quasi vide. Emmitouflés dans d'épais vêtements, ils ressemblaient à des ours, lippant leur soupe dans de grands bruits de succion. Ils n'étaient que quatre, le doyen ayant depuis plusieurs mois déserté leur table, réfugié dans un chauffoir afin de préserver ses vieux os. Cela faisait des années qu'ils n'écoutaient plus de sainte lecture, le transport des précieux livres étant toujours problématique. Sans compter qu'ils n'avaient plus de novice pour cet office et qu'aucun d'eux n'avait envie de se priver d'un repas sur quatre pour faire entendre aux autres les textes sacrés.

De plus, au fil du temps, le silence réglementaire avait laissé la place à de timides échanges autour des affaires courantes qui n'avaient pas à voir avec le chapitre. La fréquente absence de l'évêque les incitait à s'organiser au mieux en fonction de leurs besoins et impératifs sans trop se restreindre en raison de règles souvent inutiles et contraignantes, du moins selon eux.

Ce jour-là, l'ambiance était morne. Roger avait appris une mauvaise nouvelle, sa candidature n'était pas retenue pour le poste prestigieux de secrétaire de l'archevêque de Tyr. Ses collègues, Paul en tête, tentaient de le réconforter, sans grand succès jusque là.

« Pierre de Barcelone est fort avancé en âge, son successeur ne gardera peut-être pas un jeune clunisien peu au fait des choses d'ici.

– Crois-tu qu'un inconnu me préférera ? Au Saint-Sépulcre, Pierre a été mon prieur, et sait combien j'aurais pu lui être utile. Il a juste estimé que je n'étais pas assez doué à la tâche.

– M'est avis qu'il y a là collusion pour mettre en avant jeune poulain. Neveu de quelque prélat bien en cour.

– Tu es homme de savoir et de sapience, ton mérite saura être reconnu.

– On m'a coincé ici, comme vous, sans espoir d'en sortir. Notre sire évêque lui-même est plus souvent au-dehors qu'au-dedans. N'est-ce pas là signe de notre oubli ? »

Paul fit la moue. Il appréciait bien la place, à l'écart des principaux centres. Il y avait fait sa pelote, possédait plusieurs maisons en propre, et s'autorisait quelques libertés avec la règle de célibat, malgré des remontrances répétées. Aussi fréquentes que ses rechutes, à dire le vrai. On murmurait d'ailleurs qu'il entretenait plusieurs enfants, ne s'interdisant pas de retomber dans l'erreur avec certaines de ses anciennes amies à l'occasion d'une visite paternelle. Ses collègues ne le condamnaient que du bout des lèvres, étant en général dans une position au moins aussi délicate. Il n'était que Roger, malgré toutes ses complaintes, qui ne semblait guère profiter du relâchement des moeurs. Il aimait geindre à tout propos, dépité du moindre avancement de sa carrière. Il ne manifestait néanmoins aucune animosité envers ses confrères indisciplinés, se contentant désormais de baigner dans un permanent abattement.

« Entre un trône vacillant et une Église plus prompte à promouvoir les fils à barons que ses fidèles serviteurs, je n'augure rien de bon pour les années qui viennent, mes frères. »

Il reposa violemment sa cuiller sur l'épais plateau de bois, faisant sursauter les présents. Surpris de sa propre virulence, il leur adressa un regard désolé, mais enchaîna, la bouche amère.

« Nous n'avons même pas de véritable basilique pour y célébrer l'office divin, depuis plus de dix ans que nous espérons. Verrons-nous la moindre pierre posée en ce sens ?

– Le sire connétable Onfroy a bien indiqué qu'il parlerait en ce sens à la Cour le roi pour Noël.

– Il n'a même pas de quoi augmenter ses murailles et solder ses troupes. Tellement aux abois qu'il compte mi-partir la cité avec les frères de Saint-Jean !

– N'est-ce pas là bonne nouvelle ? Ils ont besants bien assez pour construire beaux édifices.

– Parles-en au saint père Foucher, ils ont bien failli l'embrochier de leurs traits. »

Tout le monde savait qu'il n'en était rien, et que si l'opposition entre le patriarche et les frères hospitaliers de Saint-Jean avait dégénéré en conflit, jamais la personne sacrée n'avait été en danger. Mais aucun n'ignorait qu'il était inutile de chercher à pointer les énormités proférées par Roger lorsqu'il avait ses crises mélancoliques. Ils se contentèrent de plonger la tête dans leur assiette, se réfugiant dans leurs pensées. Après tout, ils ne le fréquentaient que peu. Chacun mangeait au chaud dans sa cellule soir et matin, parfois même dans une maison au dehors. Et le chapitre était chaque veillée rondement mené, le froid et l'humidité fouettant leurs ardeurs administratives.

## Baniyas, souk des cordonniers, soir du lundi 20 mai 1157

Éperdu, Roger ne savait que faire. Au matin, il était dans sa cellule à se ronger les sangs lorsqu'il avait entendu la rumeur : les musulmans venaient de percer leurs défenses et se déversaient dans la ville comme  autant de criquets et de serpents. Conscient que l'édifice cathédral serait un des premiers visés, il l'avait abandonné et s'était dépouillé de ses attributs les plus visibles. Il avait dissimulé quelques pièces d'orfèvrerie dans une besace à son flanc et y avait ajouté une miche de pain. Il n'avait pu obtenir aucune nouvelle des autres chanoines, certainement calfeutrés de leur mieux.

La cité était déserte, tous les habitants s'étaient enfermés, et il n'avait pu pénétrer que dans un marché dont il avait hâtivement tenté de barrer le portail. Puis il s'était terré dans une alcôve abandonnée, parmi les déchets et les décombres. De temps à autre, un bruit de course, de cavalcade, des cris résonnaient dans les alentours, ajoutant l'angoisse à ses craintes.

Un moment, on avait éprouvé la solidité de la grande porte, à plusieurs reprises, puis le calme était revenu. Il n'avait vu personne se risquer hors des maisons de la zone, et même à l'approche de la nuit, aucune lueur n'osait s'annoncer aux fenêtres.

Il commençait à s'assoupir lorsque le grincement des gonds le fit sursauter. Des hommes porteurs de torches venaient de pénétrer dans la cour. Des Turcs, lourdement équipés, qui s'avançaient l'arme à la main. Il se recula dans les ténèbres, espérant passer inaperçu. En pure perte.

Des voix impérieuses hurlèrent devant sa cachette, qu'il fit mine de ne pas entendre, puis des cris agacés, rapidement augmentés d'un remue-ménage dans les décombres à l'entrée. Il tremblait de tous ses membres, incapable de faire le moindre geste. Il sentait que ses intestins allaient le lâcher. Puis une main accrocha son épaule et il fut extrait sans ménagement de son refuge.

Les coups s'abattirent tandis qu'on le traînait sauvagement au sol. Les bras devant le visage, il demeurait recroquevillé, hébété, implorant pitié. On l'obligea finalement à se mettre debout, lui arrachant une partie de ses vêtements. Puis une frappe dans le dos l'incita à avancer. La lueur ambrée des torches donnait des relents d'Enfer aux lieux qu'il traversait. Très vite, il rejoignit une petite place ornée d'une fontaine décorée de mosaïques. Mais il n'entendait plus son chant si rafraîchissant en été, pas plus que les ébats joyeux des gamins auxquels il lançait quelques billons.

Des chevaux, énervés par l'ambiance, hennissaient, s'agitaient dans une des rues adjacentes. Partout, des voix fortes rugissaient des ordres, et des suppliques leur répondaient. Des hurlements, de frayeur ou de douleur s'échappaient parfois des alentours. Plusieurs groupes de prisonniers étaient assemblés. Roger eut des hauts le coeur à la vision de plusieurs têtes coupées enfichées sur des bâtons. Ses jambes défaillirent lorsqu'il aperçut les corps décapités, à demi nus, souillés, gisant dans le plus grand désordre, là où on le menait.

Tout s'enchaîna très vite. On le fit tomber sur ses  genoux avec violence dans une file. Il entendait les lamentations, les suppliques, les cris de défi. Et les coups, répétés, distribués en ahanant par des hommes peinant comme bûcherons au labeur. Toujours plus près…

On le tira brutalement par les cheveux, lui tordant le cou en avant, et il s'abandonna alors, incapable d'autre chose que de murmurer le Credo, reniflant, pleurant, balbutiant. Une voix ferme résonna, des paroles furent échangées autour de lui tandis qu'on le lâchait. Une main puissante lui leva la tête. Il n'osa pas regarder.

« Avez-vous désir de vivre encore un peu ? » lui demanda-t-on dans sa langue.

Il entrouvrit une paupière inquiète, frotta son nez dont la morve coulait sur ses lèvres, hésitant à parler. Devant lui, un soldat magnifiquement équipé, vrillait son regard d'aigle sur lui. Son armure de lamelles laquées était recouverte d'un qaba[^qaba] aux revers brodés, de somptueux tiraz[^tiraz] ornant les bras. À ses hanches pendaient sabre, carquois, arc courbe, et sur sa tête protégée d'un casque peint, surmonté d'un plumet, une cagoule de maille ne dévoilait que les yeux.

« Je vous connais, vous êtes un de leurs prêtres. »

L'homme, visiblement officier, lança quelques ordres et on lui amena peu après à la pointe de l'épée quelques autochtones. Bien qu'ils ne semblaient pas prisonniers, ils étaient effrayés et répondaient avec appréhension à ses questions, les mains jointes en une attitude soumise et craintive. Puis il les chassa d'un geste et revint vers Roger, qui attendait, immobile, prostré.

Le soldat ôta son casque, sa coiffe, dévoilant un visage connu. Le regard s'était durci et des rides autoritaires encadraient désormais la bouche.

« Les gens d'ici vous appellent *al-tayyib*[^tayyib], vous saviez ?

– …

– Vous parlez notre langue ? »

Il arbora un sourire froid, croisa les mains dans le dos, soupira.

« Notre sultan, qu'Allah lui prête longue vie, souhaite nettoyer cette enclave de tous les impurs. Rembourser le sang versé par les vôtres… »

Roger passa une langue de carton sur ses lèvres râpeuses, incapable d'avaler sa salive tant sa gorge le nouait. Le Turc se pencha vers lui, jusqu'à lui souffler sur le visage.

« D'un autre côté, votre chef s'est réfugié en la citadelle. Lui envoyer émissaire de confiance pourrait l'inciter à ne pas résister stupidement. Voudriez-vous être celui-là ? Le convaincre de l'inutilité d'un combat perdu d'avance ? »

Roger hésita un moment, incapable de comprendre. Puis il hocha la tête, lentement, comme dans un rêve, tel un animal fasciné par son prédateur.

Al-Buzzjani inspira profondément tandis qu'on éloignait le prisonnier de lui. Il ordonna qu'on le lave avant de le présenter à l'émir : il s'était souillé et empestait à plusieurs mètres à la ronde. Il fronça les sourcils, s'efforçant de ne pas penser au bon accueil qu'il avait reçu lorsqu'il avait rencontré le chanoine, des années auparavant. Au moins lui avait-il évité la décapitation immédiate. Il haussa les épaules, tentant de se libérer du poids qu'il sentait s'accumuler au fil des ans. Il était né au monde en tant que soldat, et s'accomplissait dans le service. Obéissant à son protecteur sans discuter, il tenait son rang avec une loyauté indéfectible. C'était un Ghūlam[^ghulam]. ❧

## Notes

La ville de Baniyas fut au centre des affrontements entre le royaume de Jérusalem et les princes musulmans tout au long de la décennie 1150. La cité était idéalement positionnée entre les Latins et Damas et tirait parti des péages vers et depuis la côte. Celui qui la détenait pouvait aisément y amasser des unités en vue d'une attaque contre son adversaire. Nûr al Dîn ne s'y était pas trompé et saisit la moindre occasion de s'en emparer. Lorsque le sultan cherchait à conquérir Damas, l'atabeg Anur s'était arrangé pour en laisser le contrôle aux Francs, espérant ainsi une aide rapide en cas d'appel sa part.

Une fois les territoires de Syrie unifiés, le sultan s'employa à garantir ses positions sur de tels accès et mit plusieurs fois le siège devant la cité, la rasant de son mieux quand il ne put la garder. Plusieurs décennies après, l'édification de la forteresse de Vadum Jacob par Baudoin répondait aux mêmes impératifs : avoir une base puissante aux marches du territoire, à la fois pour se protéger des chevauchées, mais également pour facilement s'avancer en terre adverse. Saladin comprit aussi bien que Nûr al Dîn le danger et mit toute sa force en oeuvre pour empêcher que cela ne se fasse.

Le passage décrivant le plat est une modeste allusion à la pratique gourmande de Jean-François Parrot dans *Les enquêtes de Nicolas le Floch*. Il y narre une autre époque (le XVIIIe siècle) avec un talent scripturaire et une attention aux détails historiques (et culinaires) fort inspirants.

## Références

Ellenblum Ronnie, « Who Built Qalʿat al-Ṣubayba? », dans *Dumbarton Oaks Papers*, vol. 43 (1989), p. 103-112.

Élisséef Nikita, *Nur Ad-Dîn. Un grand prince musulman de Syrie au temps des Croisades (511-569 H./1118-1174)*, Tome II, Damas : Institut Français de Damas, 1967.

Graboïs Aryeh, « La cité de Baniyas et le château de Subeibeh pendant les croisades », dans *Cahiers de civilisation médiévale*, 13e année, n°49, Janvier-Mars 1970, p.43-62.

Waines David, Sabard Marie-Hélène (trad.), *La cuisine des califes*, Coll. l'Orient Gourmand, Sindbad, Arles : Actes Sud, 1998.
