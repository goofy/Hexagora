 Source : https://commons.wikimedia.org/wiki/File:BLYatesThompson26LifeCuthbertFol26rCuthSails.jpg

Title 	Folio 26 recto from a 12th century manuscript of Bede's Life of St. Cuthbert, Cuthbert Sailing.
Current location 	
British Library Link back to Institution infobox template wikidata:Q23308
Accession number 	Yates Thompson 26

Public domain 	

This work is in the public domain in its country of origin and other countries and areas where the copyright term is the author's life plus 70 years or less.

Dialog-warning.svg You must also include a United States public domain tag to indicate why this work is in the public domain in the United States. Note that a few countries have copyright terms longer than 70 years: Mexico has 100 years, Jamaica has 95 years, Colombia has 80 years, and Guatemala and Samoa have 75 years. This image may not be in the public domain in these countries, which moreover do not implement the rule of the shorter term. Côte d'Ivoire has a general copyright term of 99 years and Honduras has 75 years, but they do implement the rule of the shorter term. Copyright may extend on works created by French who died for France in World War II (more information), Russians who served in the Eastern Front of World War II (known as the Great Patriotic War in Russia) and posthumously rehabilitated victims of Soviet repressions (more information).
