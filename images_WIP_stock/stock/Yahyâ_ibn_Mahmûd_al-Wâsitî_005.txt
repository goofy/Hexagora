 Récupéré sur https://commons.wikimedia.org/wiki/File:Yahy%C3%A2_ibn_Mahm%C3%BBd_al-W%C3%A2sit%C3%AE_005.jpg

**********************IMAGE INVERSEE POUR LA PAO********************

Artist 	
Yahyâ ibn Mahmûd al-Wâsitî Link back to Creator infobox template wikidata:Q472751
Title 	
Deutsch: Pilgerkarawane in Ramleh (31. Maqâmât)
English: Caravan of pilgrims in Ramleh (31st Maqamat)
Français : Caravane de pèlerins à Ramleh(31e Maqamat)
Description 	
Deutsch: Dieses Malerei stammt von einen Munuskript von Maqâmât (Versammlungen) des al-Harîrî
English: This painting is coming from a manuscript of Maqâmât of al-Harîrî
Français : Cette peinture provient d'un manuscrit des Maqâmât (Séances) de al-Harîrî
Date 	634 H. / 1236-1237 AD.
Medium 	painting and gold on paper
Dimensions 	25.3 × 26.7 cm (10 × 10.5 in) (image)
Current location 	
Bibliothèque nationale de France Link back to Institution infobox template wikidata:Q193563
Département des manuscrits orientaux
Accession number 	Arabe 5847, fol. 94v.
Object history 	place of creation: Baghdad ; collection of Schefer
Credit line 	1899: purchased
Notes 	
Deutsch: Buchmalerei
References 	[1] ; Richard Ettinghausen, La peinture arabe, Genève : Skira, 1977, p. 119.
Source/Photographer 	The Yorck Project: 10.000 Meisterwerke der Malerei. DVD-ROM, 2002. ISBN 3936122202. Distributed by DIRECTMEDIA Publishing GmbH.
Permission
(Reusing this file) 	[2]

The work of art depicted in this image and the reproduction thereof are in the public domain worldwide. The reproduction is part of a collection of reproductions compiled by The Yorck Project. The compilation copyright is held by Zenodot Verlagsgesellschaft mbH and licensed under the GNU Free Documentation License.
