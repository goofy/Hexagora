 https://commons.wikimedia.org/wiki/File:Codex_aureus_Epternacensis_folio_20_recto.jpg

Artist 	Meister des Codex Aureus Epternacensis
Title 	
Deutsch: Codex Aureus Epternacensis (Goldenes Evangeliar), Prunkhandschrift
Date 	
Deutsch: um 1035-1040
English: c. 1035-1040
Medium 	
Deutsch: Pergament
Dimensions 	
Deutsch: 44,5 × 31 cm
Current location 	
Deutsch: Germanisches Nationalmuseum
Deutsch: Nürnberg
Notes 	
Deutsch: Buchmalerei, Schule von Reichenau unter Abt Humbert von Echternach (1028-1051)
Source/Photographer 	scanned from book
Permission
(Reusing this file) 	own scan

This work is in the public domain in its country of origin and other countries and areas where the copyright term is the author's life plus 100 years or less.

Dialog-warning.svg You must also include a United States public domain tag to indicate why this work is in the public domain in the United States.
